;; publish.el --- Publish org-mode project on Gitlab Pages
;; Initially taken from: Rasmus

;; Author: Gilles Marait

;; This publish script tangles all org files in ./include
;; into the source code of the library.

;; For developers, it is recommended to modify org code
;; and use this publish script to obtain up-to-date source code

;; From a terminal, one can invoke the publication using:
;;emacs --batch --load publish.el --eval '(org-publish "generate-source-code")'

;; To generate HTML pages from the .org, one can use:
;;(cd ./doc/html/ && ./make_doc.sh)
;;emacs --batch --no-init-file --load publish.el --funcall org-publish-all

(require 'package)
(package-initialize)

(require 'org)
(require 'ox-publish)

(setq org-html-htmlize-output-type 'css)

;; Use timestamp to avoid publishing
(setq org-publish-use-timestamps-flag nil)

;; This fix is necessary to obtain relative path when tangling with comments: link
;; It has been fix for org version 9.3.7 (but it's too)
(if (string< (org-version) "9.3.7")
    (load-file "./.ci-extra/ob-tangle-fix.el") ())

(defvar site-attachments
  (regexp-opt '("m" "py" "ipynb" "scm"
                "jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

(setq org-publish-project-alist
      (list
       (list "tutorial"
             :base-directory "./scripts"
             :base-extension "org"
             :recursive nil
             :publishing-function '(org-babel-tangle-publish)
             :publishing-directory "./public"
             :auto-sitemap nil)
       (list "generate-source-code"
             :base-directory "."
             :base-extension "org"
             :recursive nil
             :publishing-function '(org-babel-tangle-publish)
             :publishing-directory "."
             :auto-sitemap nil)
       (list "site-org"
             :base-directory "."
             :base-extension "org"
             :recursive nil
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>"
             :auto-sitemap nil)
       (list "site-static"
             :base-directory "./img"
             :base-extension "png\\|csv\\|txt"
             :recursive nil
             :publishing-directory "./public"
             :publishing-function '(org-publish-attachment)
             :recursive nil)
       (list "site" :components '("site-org" "site-static"))))

(provide 'publish)
;;; publish.el ends here
