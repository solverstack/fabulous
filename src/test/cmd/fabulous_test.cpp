#include  <unistd.h>

#include "../../common/Test.hpp"
#include "../../common/UserInputMatrix.hpp"
#include "../../common/CSCMatrix.hpp"
#include "../../common/COOMatrix.hpp"
#include "../../common/BadMatrix.hpp"

#include "../../common/MatrixMarketLoader.hpp"
#include "../../common/RandomMatrixLoader.hpp"
#include "../../common/MatrixIWLoader.hpp"
#include "../../common/BadLoader.hpp"

#include "cmdline.h"

/**
 * \file testMatrixMarketChoice.cpp
 * \brief all parameter can be passed on command line
 *
 *  this file basically generate all possible classes
 *  maybe we can use this for some sort of testing (compilation integration)
 */

using namespace fabulous;

OrthoScheme get_ortho_scheme(const struct gengetopt_args_info &info)
{
    switch (info.scheme_arg) {
    case scheme_arg_CGS: return OrthoScheme::CGS;
    case scheme_arg_ICGS:return OrthoScheme::ICGS;
    case scheme_arg_MGS: return OrthoScheme::MGS;
    case scheme_arg_IMGS:return OrthoScheme::IMGS;
    default: FABULOUS_FATAL_ERROR("invalid ortho");
    }
    return OrthoScheme::MGS;
}

OrthoType get_ortho_type(const struct gengetopt_args_info &info)
{
    switch (info.type_arg) {
    case type_arg_RUHE: return OrthoType::RUHE;
    case type_arg_BLOCK:return OrthoType::BLOCK;
    default: FABULOUS_FATAL_ERROR("invalid ortho");
    }
    return OrthoType::RUHE;
}

template<class S, class Algo, class Matrix, class Precond, class Restart, class P>
void run_test_filelog(const std::string &output,
                      const Matrix &A, const Precond &M,
                      Block<S> &RHS, Block<S> XExact,
                      const std::vector<P> &epsilon,
                      Algo algo, Parameters param,
                      OrthoParam &ortho, Restart &restart, bool suffix)
{
    run_test_BGMRES_filelog(
        output, A, M, RHS, XExact, epsilon,
        algo, param,
        ortho, restart, suffix
    );
}

template<class S, class Matrix, class Precond, class Restart, class P>
void run_test_filelog(const std::string &output,
                      const Matrix &A, const Precond &M,
                      Block<S> &RHS, Block<S> XExact,
                      const std::vector<P> &epsilon,
                      decltype(bgcro::ibdr()) algo, Parameters param,
                      OrthoParam &ortho, Restart &restart, bool suffix)
{
    run_test_BGCRO_filelog(
        output, A, M, RHS, XExact, epsilon,
        algo, param,
        ortho, restart, suffix
    );
}

template<class S, class Matrix, class Precond, class Restart, class P>
void run_test_filelog(const std::string &output,
                      const Matrix &A, const Precond &M,
                      Block<S> &RHS, Block<S> XExact,
                      const std::vector<P> &epsilon,
                      decltype(bgcr::std()) algo, Parameters param,
                      OrthoParam &ortho, Restart &restart, bool suffix)
{
    (void) restart;

    run_test_BGCR_filelog(
        output,
        A, M, RHS, XExact, epsilon,
        algo, param,
        ortho, suffix
    );
}

template<class S, class Matrix, class Precond, class Restart, class P>
void run_test_filelog(const std::string &output,
                      const Matrix &A, const Precond &M,
                      Block<S> &RHS, Block<S> XExact,
                      const std::vector<P> &epsilon,
                      decltype(bcg::std()) algo, Parameters param,
                      OrthoParam &ortho, Restart &restart, bool suffix)
{
    (void) restart;
    (void) ortho;

    run_test_BCG_filelog(
        output,
        A, M, RHS, XExact, epsilon,
        algo, param, suffix
    );
}

template<class S, class Algo, class Restart, class Loader, class Matrix>
void run_test_storage(const struct gengetopt_args_info &info,
                      Algo algo, Restart restart, Loader matrix_loader, Type<Matrix>)
{
    using P = typename Arithmetik<S>::primary_type;
    using Block   = Block<S>;
    int iter = info.iteration_arg;
    if (iter < 2) {
        FABULOUS_FATAL_ERROR("orthogonalization iteration parameter must be >= 2");
    }

    int matrix_size = 0;
    std::string filename{info.file_arg};
    if (  info.loader_arg == loader_arg_RD
          || info.storage_arg == storage_arg_BAD) {
        matrix_size = info.matrix_size_arg;
        filename = (info.loader_arg == loader_arg_RD
                    ? "random_matrix"
                    : "bad_matrix" );
    }
    Matrix A{matrix_size};
    BasicDiagDominantPrecond<Matrix> M{A, info.precond_flag != 0};

    // std::cout<<"Filename ::\t"<<filename<<"\n";
    matrix_loader.LoadMatrix(A, filename);

    const int dim = A.size();
    const int nbRHS = info.nrhs_arg;

    Block XExact{dim, nbRHS};
    Block RHS{dim, nbRHS};
    std::vector<P> epsilon{static_cast<P>(info.epsilon_arg)};

    RandomMatrixLoader rnld;
    rnld.LoadMatrix(XExact, "XExact");
    // for (int i = 0; i < nbRHS; ++i)
    //     XExact.at(i, i) = S{1.0/*,1.0*/};
    A(XExact, RHS);

    auto ortho = orthogonalization(iter)
        + get_ortho_type(info) + get_ortho_scheme(info);

    std::string output;
    if (info.output_given)
        output = std::string{info.output_arg};
    else
        output = std::string{fabulous::basename(filename.c_str())};

    int max_mvp = info.max_mvp_arg;
    int max_space = info.max_space_arg;
    if (max_space == -1)
        max_space = dim+1;

    auto param = parameters(max_mvp, max_space);
    param.set_quiet(info.quiet_flag != 0);
    param.set_max_kept_direction(info.max_kept_direction_arg);

    run_test_filelog(
        output,
        A, M, RHS, XExact, epsilon,
        algo, param,
        ortho, restart, bool(info.suffix_flag != 0)
    );
}

template<class S, class Algo, class Restart, class Loader>
void run_test_loader(const struct gengetopt_args_info &info, Algo algo, Restart restart, Loader loader)
{
    switch (info.storage_arg) {
    case storage_arg_DENSE: run_test_storage<S>(info, algo, restart, loader, Type<BasicDenseMatrix<S>>{}); break;
    case storage_arg_CSC: run_test_storage<S>(info, algo, restart, loader, Type<CSCMatrix<S>>{}); break;
    case storage_arg_COO: run_test_storage<S>(info, algo, restart, loader, Type<COOMatrix<S>>{}); break;
    case storage_arg_BAD: run_test_storage<S>(info, algo, restart, loader, Type<BadMatrix<S>>{}); break;
    default: FABULOUS_FATAL_ERROR("invalid storage type"); break;
    }
}

template<class S, class Algo, class Restart>
void run_test_algo(const struct gengetopt_args_info &info, Algo algo, Restart restart)
{
    bool spd = info.algo_arg == algo_arg_BCG;

    switch (info.loader_arg) {
    case loader_arg_MM: run_test_loader<S>(info, algo, restart, MatrixMarketLoader{}); break;
    case loader_arg_IW: run_test_loader<S>(info, algo, restart, MatrixIWLoader{}); break;
    case loader_arg_RD: run_test_loader<S>(info, algo, restart, RandomMatrixLoader{spd}); break;
    case loader_arg_BAD: run_test_loader<S>(info, algo, restart, bad_loader<S>()); break;
    default: FABULOUS_FATAL_ERROR("invalid parser type"); break; break;
    }
}

template<class S, class Restart>
void run_test_restart(const struct gengetopt_args_info &info, Restart restart)
{
    switch (info.algo_arg) {
    case algo_arg_GCRO:   run_test_algo<S>(info, bgcro::ibdr(), restart); break;
    case algo_arg_STDDR:  run_test_algo<S>(info, bgmres::std(), restart); break;
    case algo_arg_QRDR:   run_test_algo<S>(info, bgmres::qrdr(), restart); break;
    case algo_arg_QR:     run_test_algo<S>(info, bgmres::qr(), restart); break;
    case algo_arg_IB:     run_test_algo<S>(info, bgmres::ib(), restart); break;
    case algo_arg_IBDR:   run_test_algo<S>(info, bgmres::ibdr(), restart); break;
    case algo_arg_QRIBDR: run_test_algo<S>(info, bgmres::qribdr(), restart); break;
        #if !defined(FABULOUS_USE_CHAMELEON)
    case algo_arg_CHAMDR:  /* fall-thru */
    case algo_arg_CHAMQR:  /* fall-thru */
    case algo_arg_CHAMIB:
        FABULOUS_FATAL_ERROR("chameleon is not enabled in this executable"); break;
        #else // !defined(FABULOUS_USE_CHAMELEON)
    case algo_arg_CHAMDR: run_test_algo<S>(info, bgmres::cham_tl(), restart); break;
    case algo_arg_CHAMQR: run_test_algo<S>(info, bgmres::cham_qr(), restart); break;
    case algo_arg_CHAMIB: run_test_algo<S>(info, bgmres::cham_ib(), restart); break;
        #endif // !defined(FABULOUS_USE_CHAMELEON)
    case algo_arg_GCR:    run_test_algo<S>(info, bgcr::std(), restart); break;
    case algo_arg_BCG:    run_test_algo<S>(info, bcg::std(), restart); break;
    default: FABULOUS_FATAL_ERROR("Invalid algo");
    }
}

template<class S>
void run_test_restart(const struct gengetopt_args_info &info,
                      DeflatedRestart<S> restart)
{
    switch(info.algo_arg) {
    case algo_arg_GCRO: run_test_algo<S>(info, bgcro::ibdr(), restart); break;
    case algo_arg_STDDR: run_test_algo<S>(info, bgmres::std(), restart); break;
    case algo_arg_QRDR:  run_test_algo<S>(info, bgmres::qrdr(), restart); break;
    case algo_arg_IBDR:  run_test_algo<S>(info, bgmres::ibdr(), restart); break;
    case algo_arg_QRIBDR:  run_test_algo<S>(info, bgmres::qribdr(), restart); break;
        #if !defined(FABULOUS_USE_CHAMELEON)
    case algo_arg_CHAMQR: /* fall-thru */
    case algo_arg_CHAMIB:
        warning("This version does not support deflated restarting");
        /* fall-thru */
    case algo_arg_CHAMDR:
        FABULOUS_FATAL_ERROR("chameleon is not enabled in this executable"); break;
        #else // !defined(FABULOUS_USE_CHAMELEON)
    case algo_arg_CHAMDR: run_test_algo<S>(info, bgmres::cham_tl(), restart); break;
    case algo_arg_CHAMQR: /* fall-thru */
    case algo_arg_CHAMIB: /* fall-thru */
        #endif // !defined(FABULOUS_USE_CHAMELEON)
    case algo_arg_QR: /* fall-thru */
    case algo_arg_IB: /* fall-thru */
    case algo_arg_GCR: /* fall-thru */
    case algo_arg_BCG:
        FABULOUS_FATAL_ERROR("The chosen algorithm does not support deflated restarting.");
        break;
    default: FABULOUS_FATAL_ERROR("Invalid algo.");
    }
}

template<class S>
void run_test(const struct gengetopt_args_info &info)
{
    int k = info.nb_eigen_pair_arg;

    switch (info.restart_arg) {
    case restart_arg_CLASSIC:
        run_test_restart<S>(info, classic_restart()); break;
    case restart_arg_DEFLATED:
        run_test_restart<S>(info, deflated_restart(k, S{0.0})); break;
    default: FABULOUS_FATAL_ERROR("invalid restarting type");
    }
}

int main(int argc, char *argv[])
{
    gengetopt_args_info info;
    cmdline_parser(argc, argv, &info); // <- code generated with gengetopt

    #if defined(FABULOUS_USE_CHAMELEON)
    int NCPU = info.nb_starpu_worker_arg;
    if (NCPU == -1) {
        NCPU = sysconf(_SC_NPROCESSORS_ONLN);
    }
    CHAMELEON_Init(NCPU, 0);
    CHAMELEON_Enable(CHAMELEON_WARNINGS);
    CHAMELEON_Disable(CHAMELEON_AUTOTUNING);
    int TAG_SEP = info.chameleon_tag_sep_arg;
    printf("TAG_SEP=%d\n", TAG_SEP);
    CHAMELEON_user_tag_size(31, TAG_SEP);
    #endif

    switch (info.arithmetic_arg) {
    case arithmetic_arg_REAL_FLOAT:     run_test<float>(info); break;
    case arithmetic_arg_REAL_DOUBLE:    run_test<double>(info); break;
    case arithmetic_arg_COMPLEX_FLOAT:  run_test<std::complex<float>>(info); break;
    case arithmetic_arg_COMPLEX_DOUBLE: run_test<std::complex<double>>(info); break;
    default: FABULOUS_FATAL_ERROR("Invalid arithmetic"); break;
    }

    #if defined(FABULOUS_USE_CHAMELEON)
    CHAMELEON_Finalize();
    #endif
    return 0;
}
