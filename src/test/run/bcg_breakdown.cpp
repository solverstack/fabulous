#include "../../common/Test.hpp"
#include "../../common/UserInputMatrix.hpp"
#include "../../common/MatrixMarketLoader.hpp"
#include "../../common/RandomMatrixLoader.hpp"

using namespace fabulous;

int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};

    using P = double;
    using S = double;
    using Matrix = BasicDenseMatrix<S>;

    std::string filename{"../data/bcsstk14.mtx"};
    std::cout << "File ::\t" << filename << "\n";

    // fabulous::set_output_colored(false);

    Matrix mat;
    MatrixMarketLoader mml;
    mml.LoadMatrix(mat, filename);

    int dim = mat.size();
    int nbRHS = 100;
    int max_mvp = 10000;

    if (args.size() > 1)
        nbRHS = std::stoi(args[1]);

    Block<S> XExact{dim, nbRHS};
    Block<S> RHS{dim, nbRHS};
    std::vector<P> epsilon{1e-4};
    FABULOUS_ASSERT( nbRHS % 2 == 0 );

    // for (int i = 0; i < nbRHS; ++i)
    //     XExact.at(i, i) = S{1.0/*,1.0*/};
    Block<S> left = XExact.sub_block(0, 0, dim, nbRHS/2);
    Block<S> right = XExact.sub_block(0, nbRHS/2, dim, nbRHS/2);

    RandomMatrixLoader rnld;
    rnld.LoadMatrix(left);
    right.copy(left);

    mat(XExact, RHS);

    try {
        auto params = parameters(max_mvp);
        noop_placeholder _;
        run_test_BCG_filelog(
            "bcsstk_bcg_breakdown",
            mat, _, RHS, XExact, epsilon,
            bcg::v2(), params
        );
    } catch (::fabulous::Error &err) {
        std::cerr<<err.what()<<"\n";
        exit(EXIT_FAILURE);
    }
    return 0;
}
