#include "../../common/Test.hpp"
#include "../../common/UserInputMatrix.hpp"
#include "../../common/RandomMatrixLoader.hpp"
#include "../../common/MatrixMarketLoader.hpp"

#include <chameleon.h>

using namespace fabulous;

int main(int argc, char *argv[])
{
    int NCPU = 2;
    CHAMELEON_Init(NCPU, 0);
    CHAMELEON_Enable(CHAMELEON_WARNINGS);
    CHAMELEON_Disable(CHAMELEON_AUTOTUNING);
    CHAMELEON_user_tag_size(31, 18);

    std::vector<std::string> args{argv, argv+argc};

    using P = double;
    using S = std::complex<P>;
    using Matrix = BasicDenseMatrix<S>;

    Matrix A;
    MatrixMarketLoader mml;
    mml.LoadMatrix(A, "../data/young1c.mtx");

    int nbRHS = 10;
    int dim = A.size();
    int max_space = dim+1;
    int max_mvp = 30000;

    if (args.size()>1)
        nbRHS = std::stoi(args[1]);
    if (args.size()>3)
        max_space = std::stoi(args[3]);

    std::vector<P> epsilon = {1e-4};
    Block<S> RHS{dim, nbRHS};
    Block<S> X_Exact{dim, nbRHS};

    RandomMatrixLoader rndld;
    rndld.LoadMatrix(X_Exact);
    A(X_Exact, RHS); // Compute RHS as Mat*X_Exact

    auto params = parameters(max_mvp, max_space);
    noop_placeholder _;
    run_test_BGMRES_filelog(
        "desc2",
        A, _, RHS, X_Exact, epsilon,
        bgmres::cham_qr(), params,
        orthogonalization(OrthoScheme::ICGS, OrthoType::BLOCK),
        classic_restart()
    );

    CHAMELEON_Finalize();
    return 0;
}
