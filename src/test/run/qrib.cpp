#include <iostream>

#include "../../common/Test.hpp"
#include "../../common/BadMatrix.hpp"
#include "../../common/BadLoader.hpp"
#include "../../common/RandomMatrixLoader.hpp"

using namespace fabulous;

int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};
    for (auto arg : args) {
        if (arg == "-h" || arg == "--help") {
            std::cerr<<"Usage: "<<args[0]
                     <<" [ Max_Krylov_Space_Size(=600) [ Nb_Eigen_Pair(=5) ] ]\n";
            exit(EXIT_SUCCESS);
        }
    }

    using P = double;
    using S = std::complex<P>;

    const int M = 20000;
    const int N = 100;
    const int max_mvp = 5000;
    // const S target = S{0.0};
    const std::vector<P> epsilon{1e-4};

    // int nb_eigen_pair = 5;
    int max_space = 5000;
    if (args.size() > 1)
        max_space = std::stoi(args[1]);
    // if (args.size() > 2)
    //     nb_eigen_pair = std::stoi(args[2]);

    BadMatrix<S> mat{M};
    bad_loader<S>().LoadMatrix(mat, "");

    Block<S> XExact{M, N};
    Block<S> RHS{M, N};
    RandomMatrixLoader rld;
    rld.LoadMatrix(XExact);
    Block<S> right = XExact.sub_block(0, N/2, M, N/2);
    Block<S> left  = XExact.sub_block(0,   0, M, N/2);
    left.copy(right);

    mat(XExact, RHS); // RHS <- mat * XExact;

    auto params = parameters(max_mvp, max_space);
    noop_placeholder _;
    auto r = run_test_BGMRES_filelog(
        "test",
        mat, _, RHS, XExact, epsilon,
        bgmres::qrib(), params,
        OrthoScheme::MGS + OrthoType::BLOCK,
        classic_restart()
        //deflated_restart(5, S{0.0})
    );
    print_test_ret(r, "IB");
    return 0;
}
