#include <iostream>

#include "../../common/Test.hpp"
#include "../../common/UserInputMatrix.hpp"
#include "../../common/MatrixIWLoader.hpp"

using namespace fabulous;

int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};
    for (auto arg : args) {
        if (arg == "-h" || arg == "--help") {
            std::cerr<<"Usage: "<<args[0]
                     <<" [ Max_Krylov_Space_Size(=600) [ Nb_Eigen_Pair(=5) ] ]\n";
            exit(EXIT_SUCCESS);
        }
    }

    using P = double;
    using S = std::complex<P>;
    using Matrix = BasicDenseMatrix<S>;

    MatrixIWLoader miwl;
    Matrix mat{};
    Block<S> RHS{};//{};

    miwl.LoadMatrix(mat, "../data/matrices_BEM/MatconeSpherePC_MAIN_MAIN_0");
    miwl.LoadMatrix(RHS, "../data/matrices_BEM/coneSpherePC_RHS_MAIN.0.res", false);

    int dim   = RHS.get_nb_row();
    int nbRHS = RHS.get_nb_col();
    Block<S> XExact{dim, nbRHS};
    int nb_eigen_pair = 5;
    S target = S{0.0};
    int max_mvp = 10000;
    int max_space = 600;

    if (args.size() > 1)
        max_space = std::stoi(args[1]);
    if (args.size() > 2)
        nb_eigen_pair = std::stoi(args[2]);

    std::vector<P> epsilon{1e-4};

    auto params = parameters(max_mvp, max_space);
    noop_placeholder _;
    auto r = run_test_BGMRES_filelog(
        "matcone",
        mat, _, RHS, XExact, epsilon,
        bgmres::std(), params,
        orthogonalization() + OrthoScheme::MGS + OrthoType::RUHE,
        deflated_restart(nb_eigen_pair, target)
    );
    print_test_ret(r, "Standard+DR");
    return 0;
}
