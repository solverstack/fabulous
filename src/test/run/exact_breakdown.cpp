#define FABULOUS_IB_EXACT_BREAKDOWN_CONTINUE

#include "../../common/Test.hpp"
#include "../../common/MatrixMarketLoader.hpp"
#include "../../common/RandomMatrixLoader.hpp"
#include "../../common/UserInputMatrix.hpp"

using namespace fabulous;

int main(int argc, char *argv[])
{
    std::vector<std::string > args{argv, argv+argc};

    using P      = double;
    using S      = double;
    using Matrix = BasicDenseMatrix<S>;

    Matrix A;
    MatrixMarketLoader mml;
    mml.LoadMatrix(A, "../data/bcsstk14.mtx");

    int dim = A.size();
    P normA = A.get_inf_norm();
    std::cerr<<"Matrix Norm inf ::\t"<<normA<<"\n";

    int nbRHS = 6;
    if (argc > 1)
        nbRHS = std::stoi(args[1]);

    Block<S> XExact{dim, nbRHS};
    Block<S> RHS{dim, nbRHS};
    std::vector<P> epsilon{1e-4};

    for (int i = 0; i < nbRHS; ++i)
        XExact.at(i, i) = S{1.0};
    // RandomMatrixLoader random_ld;
    // random_ld.LoadMatrix(XExact);

    A(XExact,RHS);
    int max_mvp = 3000, max_space = 300;
    if (args.size() > 2)
        max_space = std::stoi(args[2]);

    auto params = parameters(max_mvp, max_space);
    noop_placeholder _;
    auto r = run_test_BGMRES_filelog(
        "bcsstk14_exact_breakdown",
        A, _, RHS, XExact, epsilon,
        bgmres::ibdr(), params,
        orthogonalization() + OrthoScheme::CGS + OrthoType::RUHE,
        deflated_restart(10, S{0.0})
    );
    print_test_ret(r, "bcctsk14_exact_breakdown");
    return 0;
}
