#include <iostream>
#include <sstream>

#include "../../common/Test.hpp"
#include "../../common/UserInputMatrix.hpp"
#include "../../common/MatrixIWLoader.hpp"

using namespace fabulous;

int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};
    for (auto arg : args) {
        if (arg == "-h" || arg == "--help") {
            std::cerr<<"Usage: "<<args[0]
                     <<" [ Max_Krylov_Space_Size(=600) [ nbRHS(=10) ] ]\n";
            exit(EXIT_SUCCESS);
        }
    }

    using P = double;
    using S = std::complex<P>;
    using Matrix  = BasicDenseMatrix<S>;

    Matrix mat{};
    MatrixIWLoader miwl;
    miwl.LoadMatrix(mat, "../data/matrices_BEM/MatconeSpherePC_MAIN_MAIN_0");

    int dim = mat.size();
    int nbRHS = 10;
    int max_mvp = 10000;
    int max_space = 600;

    if (args.size() > 1)
        max_space = std::stoi(args[1]);
    if (args.size() > 3)
        nbRHS = std::stoi(args[3]);

    Block<S> RHS{dim, nbRHS};
    Block<S> XExact{dim, nbRHS};
    std::vector<P> epsilon{1e-6};

    for (int i=0; i < nbRHS; ++i)
        XExact.at(i, i) = S{1.0};
    mat(XExact, RHS);

    auto params = parameters(max_mvp, max_space);
    noop_placeholder _;
    auto r = run_test_BGMRES_filelog(
        "matcone_norhs",
        mat, _, RHS, XExact, epsilon,
        bgmres::ib(), params,
        orthogonalization(OrthoScheme::ICGS, OrthoType::BLOCK),
        classic_restart()
    );
    print_test_ret(r, "Inexact Breakdown");
    return 0;
}
