#include "../../common/Test.hpp"
#include "../../common/UserInputMatrix.hpp"
#include "../../common/MatrixMarketLoader.hpp"
#include "../../common/RandomMatrixLoader.hpp"

using namespace fabulous;

int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};
    for (auto arg : args) {
        if (arg == "-h" || arg == "--help") {
            std::cerr<<"Usage: "<<args[0]
                     <<" [ nbRHS(=100) [ Max_Krylov_Space_Size(=Dim(A)+1) ] ]\n";
            exit(EXIT_SUCCESS);
        }
    }

    using P = double;
    using S = std::complex<P>;
    using Matrix  = BasicDenseMatrix<S>;

    std::string filename{"../data/young1c.mtx"};
    std::cout<<"File ::\t"<<filename<<"\n";

    Matrix mat;
    MatrixMarketLoader mml;
    mml.LoadMatrix(mat, filename);

    int dim = mat.size();
    int nbRHS = 100;
    int max_space = 0;
    int max_mvp = 10000;

    if (args.size() > 1)
        nbRHS = std::stoi(args[1]);
    if (args.size() > 2)
        max_space = std::stoi(args[2]);
    else
        max_space = dim+1;

    Block<S> XExact{dim, nbRHS};
    Block<S> RHS{dim, nbRHS};
    std::vector<P> epsilon{1e-4};
    FABULOUS_ASSERT( nbRHS % 2 == 0 );

    // for (int i = 0; i < nbRHS; ++i)
    //     XExact.at(i, i) = S{1.0/*,1.0*/};
    Block<S> left = XExact.sub_block(0, 0, dim, nbRHS/2);
    Block<S> right = XExact.sub_block(0, nbRHS/2, dim, nbRHS/2);

    RandomMatrixLoader rndld;
    rndld.LoadMatrix(left, "XExact.left_part");
    right.copy(left);

    mat(XExact, RHS);

    noop_placeholder _;
    // run_test_BGMRES_filelog(
    //     "young1c_IB",
    //     mat, _, RHS, XExact, epsilon,
    //     bgmres::ib(), max_mvp, max_space,
    //     orthogonalization(OrthoScheme::IMGS, OrthoType::BLOCK),
    //     classic_restart()
    // );

    auto params = parameters(max_mvp, max_space);
    run_test_BGCR_filelog(
        "young1c_gcr_breakdown",
        mat, _, RHS, XExact, epsilon,
        bgcr::std(), params,
        orthogonalization() + OrthoScheme::IMGS + OrthoType::BLOCK
    );
    return 0;
}
