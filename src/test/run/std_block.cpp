#include "../../common/Test.hpp"
#include "../../common/UserInputMatrix.hpp"
#include "../../common/RandomMatrixLoader.hpp"

using namespace fabulous;


int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};

    int nbRHS=3, nbBlock=10, max_space=15;

    if (args.size()>1)
        nbRHS = std::stoi(args[1]);
    if (args.size()>2)
        nbBlock = std::stoi(args[2]);

    int dim = nbRHS*nbBlock;
    max_space = dim+nbRHS;

    if (args.size()>3)
        max_space = std::stoi(args[3]);

    using P = double;
    using S = std::complex<P>;
    using Matrix  = BasicDenseMatrix<S>;

    Matrix A{dim};
    RandomMatrixLoader rndld;
    std::vector<P> epsilon = {1e-6};
    Block<S> RHS{dim, nbRHS};
    Block<S> X_Exact{dim, nbRHS};

    rndld.LoadMatrix(A);
    A.display();

    // X_Exact:
    // rn_ld.LoadMatrix(X_Exact); /* Fill the (SizeBLock-1) first vectors with random */
    for (int i = 0; i < nbRHS; ++i) { // diagonal of 'one':
        X_Exact.at(i,i) = S{1.0};
    }
    for (int i = 0; i < dim; ++i) { // Fill the last col with the sum of the others
        for (int j = 0; j < nbRHS-1; ++j) {
            X_Exact.at(i, nbRHS-1) += X_Exact.at(i, j);
        }
    }

    A(X_Exact, RHS);     // Compute RHS as Mat*X_Exact
    X_Exact.display("X_Exact");
    RHS.display("RHS");

    auto params = parameters(dim+nbRHS, max_space);
    noop_placeholder _;
    run_test_BGMRES_filelog(
        "random+identity",
        A, _, RHS, X_Exact, epsilon,
        bgmres::std(), params,
        orthogonalization(OrthoScheme::ICGS, OrthoType::BLOCK),
        classic_restart()
    );
    return 0;
}
