#include <iostream>

#include "../../common/Test.hpp"
#include "../../common/UserInputMatrix.hpp"
#include "../../common/MatrixIWLoader.hpp"

using namespace fabulous;

int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};
    for (auto arg : args)
        std::cout << arg  << "\n";

    using P= double;
    using S = std::complex<double>;
    using Matrix = BasicDenseMatrix<S>;
    using Block = Block<S>;

    MatrixIWLoader miwl;
    Matrix mat{};
    Block RHS{};//{};

    miwl.LoadMatrix(mat, "../data/matrices_BEM/MatconeSpherePC_MAIN_MAIN_0");
    miwl.LoadMatrix(RHS, "../data/matrices_BEM/coneSpherePC_RHS_MAIN.0.res", false);

    int dim   = RHS.get_nb_row();
    int nbRHS = RHS.get_nb_col();
    Block XExact{dim, nbRHS};

    int max_mvp = 10000;
    int max_space = 600;
    std::vector<P> epsilon{1e-4};

    auto params = parameters(max_mvp, max_space);
    noop_placeholder _;
    auto r = run_test_BGMRES_filelog(
        "matcone",
        mat, _, RHS, XExact, epsilon,
        bgmres::std(), params,
        orthogonalization() + OrthoScheme::MGS + OrthoType::RUHE,
        classic_restart()
    );
    print_test_ret(r, "Standard Version");
    return 0;
}
