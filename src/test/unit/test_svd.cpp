#include <vector>
#include <string>

#include "fabulous/data/Block.hpp"
#include "../../common/RandomMatrixLoader.hpp"

using namespace fabulous;

int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};
    for (auto arg : args)
        std::cout << arg  << "\n";

    using P = double;
    using S = std::complex<P>;

    int dim = 25;
    int nbRHS = 5;

    Block<S> B{dim, nbRHS};
    RandomMatrixLoader rnld;
    rnld.LoadMatrix(B);

    P epsilon = 2.0;
    Block<S> U;

    decomposition_svd(B, U, epsilon);
    U.display("Left singular vectors : U");

    std::cout<<"Number of singular values kept over max number : "
             <<U.get_nb_col()<<"/"<<nbRHS<<std::endl;

    return 0;
}
