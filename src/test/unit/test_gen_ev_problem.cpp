#include <random>
#include <cassert>

#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/ggev.hpp"
#include "fabulous/data/Block.hpp"
#include "../../common/RandomMatrixLoader.hpp"

using namespace fabulous;

/**
 * Generalized nonsymetric Eigenvalue Problems
 *
 * Refs : https://software.intel.com/en-us/node/521094
 *        https://software.intel.com/en-us/node/521179
 */
int main(int argc, char *argv[])
{
    std::vector<std::string> args(argv, argv+argc);
    for (auto arg : args)
        std::cout << arg  << "\n";

    using P = double;
    using U = std::complex<P>;

    int dim = 20;

    Block<U> A{dim, dim};
    Block<U> B{dim, dim};

    RandomMatrixLoader rnld;
    rnld.LoadMatrix(A);
    rnld.LoadMatrix(B);

    //Save the matrices for later (in case they are modified, which
    //can occure)
    Block<U> A_save{dim, dim};
    Block<U> B_save{dim, dim};
    A_save.copy(A);
    B_save.copy(B);

    Block<U> alpha{dim,1};
    Block<U> beta{dim,1};
    Block<U> vl{};
    Block<U> vr{dim,dim};

    int error = lapacke::ggev(dim,
                              A.get_ptr(), A.get_leading_dim(),
                              B.get_ptr(), B.get_leading_dim(),
                              alpha.get_ptr(), beta.get_ptr(),
                              vl.get_ptr(),vl.get_leading_dim(),
                              vr.get_ptr(),vr.get_leading_dim());
    FABULOUS_ASSERT( error == 0 );

    //Verification :
    //For i : 0 -> dim,
    //    Do || A*vr[i] - (alpha[i]/beta[i]) * B * vr[i] || =? 0
    Block<U> res{dim,dim};
    for (int i=0; i< dim; i++) {
        U theta_i = alpha.get_ptr()[i]/beta.get_ptr()[i];
        //res_i = A*vr_i
        lapacke::gemm(dim,1,dim,
                      A_save.get_ptr(), A_save.get_leading_dim(),
                      vr.get_vect(i), vr.get_leading_dim(),
                      res.get_vect(i), res.get_leading_dim(),
                      U(1), U(0));
        //res_i = (-1)res_i + (alpha[i]/beta[i])B * vr_i
        lapacke::gemm(dim,1,dim,
                      B_save.get_ptr(), B_save.get_leading_dim(),
                      vr.get_vect(i), vr.get_leading_dim(),
                      res.get_vect(i), res.get_leading_dim(),
                      theta_i, U(-1));
    }
    auto minmax = min_max_norm(res);
    std::cout<<"Min max norm of res :\tMIN\t"<<minmax.first
             <<"\tMAX\t"<<minmax.second<<"\n";

    return 0;
}
