#include <random>
#include <cassert>

#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/ggev.hpp"
#include "fabulous/data/Block.hpp"
#include "../../common/RandomMatrixLoader.hpp"

using namespace fabulous;

template<class Complex, class P>
void RealMatxComplVector(Block<P>& A,
                         Block<Complex>& inVect,
                         Block<Complex>& outVect)
{
    if (A.get_nb_col() == inVect.get_nb_row()
        && A.get_nb_col() == A.get_nb_row())
    {
        int dim = A.get_nb_col();
        for (int i=0; i<dim; ++i)
            for (int k=0; k<dim; ++k)
                outVect.at(i,0) += A.at(i,k) * inVect.at(k, 0);
    }
}

/**
 * Generalized nonsymetric Eigenvalue Problems
 *
 * Refs : https://software.intel.com/en-us/node/521094
 *        https://software.intel.com/en-us/node/521179
 */
int main(int argc, char *argv[])
{
    std::vector<std::string> args{argv, argv+argc};
    for (auto arg : args)
        std::cout << arg  << "\n";

    using P=double;
    using S=double;
    int dim = 20;

    if (argc > 1)
        dim = std::atoi(argv[1]);

    Block<S> A{dim,dim};
    Block<S> B{dim,dim};

    RandomMatrixLoader rnld;
    rnld.LoadMatrix(A);
    rnld.LoadMatrix(B);

    //Save the matrices for later (in case they are modified, which
    //can occure)
    Block<S> A_save{dim,dim};
    Block<S> B_save{dim,dim};
    A_save.copy(A);
    B_save.copy(B);
    //Display parameters
    if (dim < 10) {
        A_save.display("A :");
        B_save.display("B :");
    }

    //Prototype:
    /*
     lapack_int LAPACKE_sggev( int matrix_layout, //Standard
     char jobvl, char jobvr,                      //Standard aussi
     int n, float* a, int lda, float* b, int ldb, //Matrices, ordre, etc
     float* alphar, float* alphai,                //Real and Im part of g.e.values
     float* beta,                                 //g.ev to be norma by
     float* vl, lapack_int ldl,                   //Left eigen vectors
     float* vr, lapack_int ldvr );                //Right eigen vectors
     */


    Block<std::complex<P>> alpha{dim,1};
    Block<S> beta{dim,1};
    Block<S> vl{};
    Block<S> vr{dim,dim};

    int error = lapacke::ggev(dim,A.get_ptr(), A.get_leading_dim(),
                              B.get_ptr(), B.get_leading_dim(),
                              alpha.get_ptr(), beta.get_ptr(),
                              vl.get_ptr(), vl.get_leading_dim(),
                              vr.get_ptr(), vr.get_leading_dim());
    FABULOUS_ASSERT( error == 0 );

    //display eigen values
    std::cout<<"Eigen values are \t ( alpha[i] ) / beta[i]\n";
    for (int i=0; i<dim; ++i)
        std::cout<<"\t("<<alpha.at(i,0)<<") / "<<beta.at(i,0)<<"\n";

    std::cout<<"Eigen values are \t ( alphar[i] + i * alphai[i] )\n";
    for (int i=0; i<dim; ++i)
        std::cout<<"\t"<<alpha.at(i,0)/beta.at(i,0)<<"\n";

    using Complex=std::complex<P>;

    int idx = 0;
    //Verification :
    //For i : 0 -> dim,
    //    Do || A*vr[i] - (alpha[i]/beta[i]) * B * vr[i] || =? 0

    std::vector<P> diff;
    //Litteral ...
    std::complex<double> J{0,1};

    while (idx<dim) {
        if (alpha.get_ptr()[idx].imag() != 0) { //we got a complex eigen value here
            std::cout<<"Complex eigen value detected\n";
            //Form the complx eigen vector
            Block<Complex> Vri{1,dim};
            for (int i=0; i<dim; ++i) {
                Vri.get_ptr()[i] = vr.at(i,idx) + J*vr.at(i,idx+1);
            }

            //Right part : alpha[i]/beta[i]) * B * Vri
            Block<Complex> BxVri{dim,1};
            RealMatxComplVector<>(B_save, Vri, BxVri);
            for (int i=0; i<dim; ++i)
                BxVri.at(i,0) *= (alpha.at(idx,0) / beta.at(idx,0));
            //Left part : A*Vri
            Block<Complex> AxVri{dim,1};
            RealMatxComplVector<>(A_save,Vri,AxVri);

            //Substraction :
            for (int i=0; i<dim; ++i)
                AxVri.at(i,0) -= BxVri.at(i,0);
            diff.push_back(AxVri.get_norm(0));

            idx+=2;
        } else { //we got a real eigen value
            S theta_i = alpha.at(idx,0).real()/beta.at(idx,0);
            Block<S> AxVri{dim,1};
            lapacke::gemm(dim,1,dim,
                          A_save.get_ptr(), A_save.get_leading_dim(),
                          vr.get_vect(idx), vr.get_leading_dim(),
                          AxVri.get_ptr(), AxVri.get_leading_dim(),
                          S(1),S(0));
            Block<S> BxVri{dim,1};
            lapacke::gemm(dim,1,dim,
                          B_save.get_ptr(), B_save.get_leading_dim(),
                          vr.get_vect(idx), vr.get_leading_dim(),
                          BxVri.get_ptr(), BxVri.get_leading_dim(),
                          theta_i, S(0));
            //Substraction :
            for (int i=0; i<dim; ++i)
                AxVri.at(i,0) -= BxVri.at(i,0);

            diff.push_back(AxVri.get_norm(0));
            idx+=1;
        }
    }

    for (auto& d : diff)
        std::cout<<"\t"<<d<<"\n";
    return 0;
}
