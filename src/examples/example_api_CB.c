#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <time.h>

#include "fabulous.h"
#define MARK_AS_USED(var_)   ((void)(var_))

/**
 * \file example_api.c
 * \brief Example C-Api
 */

/*!
 * \example example_api.c
 */

/***************** Users Structures ******************/

/**
 * \brief Example1: structure for storing the matrix
 */
typedef struct {
    int dim; /**< \brief dimension of (square) matrix */
    double *datas; /**< \brief matrix data: array of dim*dim doubles */
} Matrix;

/***************** Define CALLBACKS ******************/

/** \brief Matrix x BlockOfVector product CALLBACK  */
static int64_t mvp(  void *user_env, int N,
                     const void *p_alpha, const void *XX, int ldx,
                     const void *p_beta, void *BB, int ldb)
{
    Matrix *mat = user_env;
    int dim = mat->dim;
    double *B = BB;
    const double *X = XX;
    const double alpha = *(const double*)p_alpha;
    const double beta = *(const double*)p_beta;

    for (int j = 0; j < N; ++j)
        for (int i = 0; i < dim; ++i)
            B[j*ldb+i] *= beta;

    for (int j = 0; j < N; ++j)
        for (int k = 0; k < dim; ++k)
            for (int i = 0; i < dim; ++i)
                B[j*ldb+i] += alpha * mat->datas[k*dim+i] * X[j*ldx+k];

    return (2L + 2L*dim)*N*dim;
}

/** \brief Dot product CALLBACK */
static int64_t dot_product(void *user_env,
                           int M, int N,
                           const void *A_, int lda,
                           const void *B_, int ldb,
                           void *C_, int ldc)
{
    Matrix *mat = user_env;
    int dim = mat->dim;
    const double *A = A_;
    const double *B = B_;
    double *C = C_;

    for (int j = 0; j < N; ++j)
        for (int i = 0; i < M; ++i)
            C[j*ldc+i] = 0.0;

    for (int j = 0; j < N; ++j)
        for (int i = 0; i < M; ++i)
            for (int k = 0; k < dim; ++k)
                C[j*ldc+i] += A[i*lda+k] * B[j*ldb+k];
    return 2L*N*M*dim;
}

/******************* Helper functions *********************/

/** \brief Helper to display matrix to standard output */
static void display_matrix(Matrix *mat)
{
    int dim = mat->dim;
    for (int i = 0; i < dim; ++i) {
        for(int j = 0; j < dim; ++j)
            printf("%e\t", mat->datas[j*dim+i]);
        printf("\n");
    }
}

/** \brief allocate a matrix buffer and fill it with random value */
static double* create_random_matrix(int M, int N)
{
    double *m = malloc(M*N*sizeof*m);
    for (int j = 0; j < N; ++j)
        for (int i = 0; i < M; ++i)
            m[j*M+i] = (((double)rand()) / (double) RAND_MAX) * 10;
    return m;
}

/** \brief allocate a matrix buffer and fill it with zeros */
static double *alloc_zero_matrix(int M, int N)
{
    double *m = calloc(M*N, sizeof*m);
    return m;
}

/********************* Main program ***************************/

int main(int argc, char *argv[])
{
    int dim = 100;
    if (argc > 1)
        dim = atoi(argv[1]);

    /* int seed = time(NULL); */
    /* srand(seed); */

    //Create Matrix
    Matrix *my_matrix = malloc(sizeof*my_matrix);
    my_matrix->dim = dim;
    my_matrix->datas = create_random_matrix(dim, dim);

    int nbRHS = 5;
    // Create Right Hand Sides and Initial solutions:
    double *RHS = create_random_matrix(dim, nbRHS);
    double *X0  = alloc_zero_matrix(dim, nbRHS);

    // display_matrix(my_matrix);
    MARK_AS_USED(display_matrix);

    fabulous_handle handle;
    handle = fabulous_create(FABULOUS_REAL_DOUBLE, dim, my_matrix);

    // Setup callback:
    fabulous_set_mvp(&mvp, handle);
    fabulous_set_dot_product(&dot_product, handle);

    // Setup parameters:
    double tolerance[1] = { 1e-3 };
    fabulous_set_parameters(
        3*dim, // Maximum number of Matrix X Vector product
        50,    // maximum size of base before restart
        tolerance, /* maximum value for the Backward Error
                    * to consider the solution has converged */
        1,         /* size of tolerance array */
        handle  /* handle as returned by fabulous_init */
    );

    fabulous_set_ortho_process(FABULOUS_MGS, FABULOUS_RUHE, 2, handle);

    // call solve function:
    int nb_mvp = fabulous_solve_IB(nbRHS, RHS, dim, X0, dim, handle);
    printf("Number of Matrix X Vector products without computational blocking: %d\n", nb_mvp);

    int compututional_blocking = 2;
    fabulous_set_advanced_parameters(
        compututional_blocking, // maximum number of kept direction to add to basis
        0, // want to get real residual on callbacks ?
        10, // maximum lenght for user logs
        0, // quiet ?
        handle
    );

    free(X0);
    X0 = alloc_zero_matrix(dim, nbRHS);

    nb_mvp = fabulous_solve_IB(nbRHS, RHS, dim, X0, dim, handle);
    printf("Number of Matrix X Vector products with computational blocking: %d\n", nb_mvp);

    fabulous_destroy(handle);

    free(RHS);
    free(X0);
    free(my_matrix->datas);
    free(my_matrix);

    return EXIT_SUCCESS;
}
