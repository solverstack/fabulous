/*!
 * \file example_gcro.cpp
 * \brief Example for fabulous use; dense matrix, not distributed
 */

/*!
 * \example example_gcro.cpp
 */

#include <fabulous.hpp>

#include <utility>

#include "../common/MatrixMarketLoader.hpp"
#include "../common/RandomMatrixLoader.hpp"

namespace fa = ::fabulous;
using S = std::complex<double>;
using Block = fa::Block<S>;

/*! \brief example_gcro matrix */
class Matrix
{
private:
    Block _data;

    void load_matrix(const std::string &filename)
    {
        fa::MatrixMarketLoader mml;
        mml.LoadMatrix(_data, filename);
    }

public:
    using value_type = std::complex<double>;
    using primary_type = double;

    explicit Matrix(const std::string &filename)
    {
        load_matrix(filename);
    }

    /* ---------------------------------------------------------------- */
    /* --------------- methods for fabulous core: --------------------- */
    /* ---------------------------------------------------------------- */

    int size() const { return _data.get_nb_row(); }

    // B := A * X
    int64_t operator()(const Block &X, Block &B, S alpha=S{1.0}, S beta=S{0.0}) const
    {
        int M = B.get_nb_row();
        int N = B.get_nb_col();
        int K = X.get_nb_row();
        fa::lapacke::gemm(
            M, N, K,
            _data.get_ptr(), _data.get_leading_dim(),
            X.get_ptr(), X.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha, beta
        );
        return fa::lapacke::flops::gemm<S>(M, N, K);
    }

}; // end class Matrix

int main()
{
    /* get the young1c matrix for this example at
     ftp://math.nist.gov/pub/MatrixMarket2/Harwell-Boeing/acoust/young1c.mtx.gz
     */
    Matrix A{"../data/young1c.mtx"};
    std::ofstream file{"LogExample_gcro.txt", std::ios::out}; // Saving convergence history
    std::vector<double> epsilon{1e-4};
    const int dim = A.size();
    const int max_mvp = 10000;
    const int max_krylov_space = 90;
    const int nrhs = 6;
    const int nb_eigen_pair = 5;
    fa::noop_placeholder _;
    Block Uk{}; // Start with an empty deflation space
    Block X{dim, nrhs};
    Block X2{dim, nrhs};
    Block X3{dim, nrhs};
    Block B{dim, nrhs};
    Block B2{dim, nrhs};
    Block B3{dim, nrhs};

    fa::RandomMatrixLoader rnl;
    rnl.LoadMatrix(B);
    rnl.LoadMatrix(B2);
    rnl.LoadMatrix(B3);

    auto param = fa::parameters(max_mvp, max_krylov_space);
    auto ortho = fa::OrthoType::RUHE + fa::OrthoScheme::MGS;
    auto restart = fa::deflated_restart(nb_eigen_pair, S{0.0});

    // IB-BGCRO-DR algorithm
    auto algo = fa::bgcro::ibdr();


    // First solve
    auto eq = fa::equation(A, _, _, X, B, epsilon);
    auto log = fa::bgcro::solve(eq, algo, param, Uk, ortho, restart);
    // The deflation space is no longer empty (if at least a restart occured)
    std::cerr << "The deflated space is of size : " << Uk.get_nb_col() << '\n';

    log.print("first solve gcro", file); // this prints comma-separated columns with information about convergence

    // Second solve
    auto eq2 = fa::equation(A, _, _, X2, B2, epsilon);
    auto log2 = fa::bgcro::solve(eq2, algo, param, Uk, ortho, restart);
    // The deflation space has been updated according to the second solve

    log2.print("second solve gcro", file);

    // Third solve, we think that the deflation is a good enought aproximation
    // We don't need to modify it, thus we declare it constant with the extra parameter
    // Uk_const set to true
    auto eq3 = fa::equation(A, _, _, X3, B3, epsilon);
    auto log3 = fa::bgcro::solve(eq3, algo, param, Uk, ortho, restart, /* Uk_const */ true);
    // The deflation space has not been updated

    log3.print("third solve gcro", file);



    //
    // Same experiments with BGMRES to compare
    //
    X.zero();
    X2.zero();
    X3.zero();

    // IB-GMRES-DR algorithm
    auto algo_gmres = fa::bgmres::ibdr();

    // First solve
    log = fa::bgmres::solve(eq, algo_gmres, param, ortho, restart);
    log.print("first solve gmres", file);

    // Second solve
    log2 = fa::bgmres::solve(eq2, algo_gmres, param, ortho, restart);
    log2.print("second solve gmres", file);

    // Third solve
    log3 = fa::bgmres::solve(eq3, algo_gmres, param, ortho, restart);
    log3.print("third solve gmres", file);

    return 0;
}
