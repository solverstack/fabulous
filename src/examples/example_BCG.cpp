/*!
 * \file example_CB.cpp
 * \brief Example for fabulous use; dense matrix, not distributed
 */

/*!
 * \example example_CB.cpp
 */

#include <fabulous.hpp>

#include <utility>

#include "../common/MatrixMarketLoader.hpp"
#include "../common/RandomMatrixLoader.hpp"

namespace fa = ::fabulous;
using S = double;
using Block = fa::Block<S>;

/*! \brief example_gcro matrix */
class Matrix
{
private:
    Block _data;

    void load_matrix(const std::string &filename)
    {
        fa::MatrixMarketLoader mml;
        mml.LoadMatrix(_data, filename);
    }

public:
    using value_type = std::complex<double>;
    using primary_type = double;

    explicit Matrix(const std::string &filename)
    {
        load_matrix(filename);
    }

    /* ---------------------------------------------------------------- */
    /* --------------- methods for fabulous core: --------------------- */
    /* ---------------------------------------------------------------- */

    int size() const { return _data.get_nb_row(); }

    // B := A * X
    int64_t operator()(const Block &X, Block &B, S alpha=S{1.0}, S beta=S{0.0}) const
    {
        int M = B.get_nb_row();
        int N = B.get_nb_col();
        int K = X.get_nb_row();
        fa::lapacke::gemm(
            M, N, K,
            _data.get_ptr(), _data.get_leading_dim(),
            X.get_ptr(), X.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha, beta
        );
        return fa::lapacke::flops::gemm<S>(M, N, K);
    }

}; // end class Matrix


void display_results(const fabulous::Logger &log, const Matrix &A, Block &X, const Block &B, const bool is_cb){
    Block R = B.copy();
    A(X, R, 1.0, -1.0);

    auto cnorm_r = R.cwise_norm();
    auto cnorm_b = B.cwise_norm();
    if(is_cb)
        std::cout << "  CB :";
    else
        std::cout << "  No CB :";
    std::cout << "\n    mvps : " << log.get_nb_mvp() <<"\n    right-hand-sides backward errors:  |";
    for(int i = 0; i < B.get_nb_col(); i++)
        std::cout << " " << cnorm_r[i] / cnorm_b[i] << " |";
    std::cout << '\n';
    X.zero();
}


int main()
{
    /* get the bcsstk14 matrix for this example at
     ftp://math.nist.gov/pub/MatrixMarket2/Harwell-Boeing/bcsstruc2/bcsstk14.mtx.gz
     */
    Matrix A{"../data/bcsstk27.mtx"};
    std::vector<double> epsilon{1e-4};
    const int dim = A.size();
    const int max_mvp = 10000;
    const int max_krylov_space = 200;
    const int computational_blocking = 2;
    const int nrhs = 6;
    fa::noop_placeholder _;
    Block X{dim, nrhs};
    Block B{dim, nrhs};

    fa::RandomMatrixLoader rnl;
    rnl.LoadMatrix(B);

    auto param = fa::parameters(max_mvp, max_krylov_space);
    param.set_quiet(true);

    auto eq = fa::equation(A, _, _, X, B, epsilon);

    // BF-BCG algorithm
    auto algo_bcg = fa::bcg::std();
    // BF-BCG algorithm (small variant on the BF implementation)
    auto algo_bcg_v2 = fa::bcg::v2();

    std::cout << "BF-BCG:\n";
    auto log = fa::bcg::solve(eq, algo_bcg, param);
    display_results(log, A, X, B, false); // Also reset X
    std::cout << "BF-BCG (variant):\n";
    log = fa::bcg::solve(eq, algo_bcg_v2, param);
    display_results(log, A, X, B, false);

    // Set computational blocking
    param.set_max_kept_direction(computational_blocking);


    std::cout << "BF-BCG:\n";
    log = fa::bcg::solve(eq, algo_bcg, param);
    display_results(log, A, X, B, true);
    std::cout << "BF-BCG (variant):\n";
    log = fa::bcg::solve(eq, algo_bcg_v2, param);
    display_results(log, A, X, B, true);

    return 0;
}
