/**
 * \file example1.cpp
 * \brief Example for fabulous use; dense matrix, not distributed
 */

/*!
 * \example example3.cpp
 */

#include <fabulous.hpp>

#include "../common/MatrixMarketLoader.hpp"
#include "../common/RandomMatrixLoader.hpp"

namespace fa = ::fabulous;
using S = std::complex<double>;
using Block = fa::Block<S>;

/*! \brief example3 matrix */
class Matrix
{
private:
    Block _data;

    void load_matrix(const std::string &filename)
    {
        fa::MatrixMarketLoader mml;
        mml.LoadMatrix(_data, filename);
    }

public:
    using value_type = std::complex<double>;
    using primary_type = double;

    explicit Matrix(const std::string &filename)
    {
        load_matrix(filename);
    }

    /* ---------------------------------------------------------------- */
    /* --------------- methods for fabulous core: --------------------- */
    /* ---------------------------------------------------------------- */

    int size() const { return _data.get_nb_row(); }

    // B := A * X
    int64_t operator()(const Block &X, Block &B, S alpha=S{1.0}, S beta=S{0.0}) const
    {
        const int M = B.get_nb_row();
        const int N = B.get_nb_col();
        const int K = X.get_nb_row();
        fa::lapacke::gemm(
            M, N, K,
            _data.get_ptr(), _data.get_leading_dim(),
            X.get_ptr(), X.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha, beta
        );
        return fa::lapacke::flops::gemm<S>(M, N, K);
    }

}; // end class Matrix

/*! \brief example3 variable precond (bgmres) */
template<class Matrix, class P>
class Precond
{
private:
    const Matrix &_A;
    const int _max_mvp;
    const int _max_space;
    const P _tolerance;

public:
    Precond(const Matrix &A, int max_mvp, int max_space, P tolerance):
        _A{A}, _max_mvp{max_mvp}, _max_space{max_space}, _tolerance{tolerance}
    {
    }

    operator bool() const { return true; }

    int64_t operator()(const Block &B, Block &X) const
    {
        Block B2 = B.copy();
        std::vector<P> epsilon{_tolerance};

        fa::noop_placeholder _;
        auto eq = fa::equation(_A, _, _, X, B2, epsilon);
        auto param = fa::parameters(_max_mvp, _max_space);
        param.set_quiet(true);

        auto ortho = fa::OrthoType::BLOCK + fa::OrthoScheme::CGS;
        auto restart = fa::classic_restart();
        auto algo = fa::bgmres::std();
        fa::bgmres::solve(eq, algo, param, ortho, restart);

        return 0;
    }
};

int main()
{
    /* get the young1c matrix for this example at
     ftp://math.nist.gov/pub/MatrixMarket2/Harwell-Boeing/acoust/young1c.mtx.gz
     */
    Matrix A{"../data/young1c.mtx"};
    std::vector<double> epsilon{1e-4};
    const int dim = A.size();
    const int max_mvp = 10000;
    const int max_krylov_space = 90;
    const int nrhs = 6;
    Block X{dim, nrhs};
    Block B{dim, nrhs};

    Precond<Matrix,double> M{A, max_mvp/10, max_krylov_space/2, 0.95};

    fa::RandomMatrixLoader rnl;
    rnl.LoadMatrix(B);

    fa::noop_placeholder _;
    auto eq = fa::equation(A, M, _, X, B, epsilon);
    auto param = fa::parameters(max_mvp, max_krylov_space);
    auto ortho = fa::OrthoType::BLOCK + fa::OrthoScheme::MGS;
    auto algo = fa::bgcr::std();
    auto log = fa::bgcr::solve(eq, algo, param, ortho);

    log.print("young1c"); // this prints comma-separated columns with information about convergence
    return 0;
}
