#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include <assert.h>

#include "fabulous.h"
#include "fabulous/ext/cblas.h"

#define MARK_AS_USED(var_)   ((void)(var_))

/**
 * \file example_api2.c
 * \brief Example2: This test case solve the system provided by G. Sylvand.
 */

/*!
 * \example example_api2.c
 */

/***************** Users Structures ******************/

/**
 * \brief Example2: structure for storing the matrix
 */
typedef struct user_matrix {
    int dim1; /**< \brief number of row */
    int dim2; /**< \brief number of column */
    double complex *datas; /** \brief matrix data array of dim*dim doubles complex */
} Matrix;

/**
 * \brief Example2: structure for storing the preconditionner
 */
typedef struct user_right_precond {
    int dim; /**< \brief preconditioner size */
    double complex *datas; /**< \brief preconditionner data */
} RightPrecond;

/**
 * \brief Example2: environement structure to be passed to callbacks
 */
typedef struct user_env {
    Matrix *mat;  /**< \brief the user matrix */
    RightPrecond *right_precond;  /**< \brief the user preconditionner */
} Env;

/***************** Define CALLBACKS ******************/

/** \brief Matrix x BlockOfVector product CALLBACK  */
static int64_t mvp(  void *user_env, int N,
                     const void *alpha, const void *p_X, int ldx,
                     const void *beta, void *p_B, int ldb)
{
    Env *env = user_env;
    Matrix *mat = env->mat;
    int dim = mat->dim1;
    const double _Complex *X = p_X;
    double _Complex *B = p_B;

    cblas_zgemm(
        CblasColMajor, CblasNoTrans, CblasNoTrans,
        dim, N, dim,
        (const double _Complex*)alpha, mat->datas, dim,
        X, ldx,
        (const double _Complex*)beta, B, ldb
    );
    return (8L*dim + 12L)* N * dim;
}

/** \brief Right precond CALLBACK */
static int64_t right_precond(  void *user_env, int N,
                               const void *XX, int ldx,
                               void *BB, int ldb        )
{
    Env *env = user_env;
    RightPrecond *my_rpc = env->right_precond;

    int dim = my_rpc->dim;
    const double complex *X = XX;
    double complex *B = BB;

    for (int j = 0; j < N; ++j)
        for (int i = 0; i < dim; ++i)
            B[j*ldb+i] = X[j*ldx+i] * my_rpc->datas[i];
    return 6L*N*dim;
}

/** \brief Dot product CALLBACK */
static int64_t dot_product( void *user_env, int M, int N,
                            const void *AA, int lda,
                            const void *BB, int ldb,
                            void *CC, int ldc               )
{
    Env *env = user_env;
    int dim = env->mat->dim1;
    const double complex *A = AA;
    const double complex *B = BB;
    double complex *C = CC;
    double complex one = 1.0;
    double complex zero = 0.0;

    cblas_zgemm(
        CblasColMajor, CblasConjTrans, CblasNoTrans,
        M, N, dim,
        &one, A, lda,
        B, ldb,
        &zero, C, ldc
    );
    return 8L * dim * N * dim;
}

/******************* Helper functions *********************/

enum iw_file_format_data_type {
    IW_FILE_FORMAT_FLOAT          = 0,
    IW_FILE_FORMAT_DOUBLE         = 1,
    IW_FILE_FORMAT_COMPLEX_FLOAT  = 2,
    IW_FILE_FORMAT_COMPLEX_DOUBLE = 3,
};

/** \brief this function is a IW File format parser */
static void read_IW_file(Matrix *mat, FILE *file, const char *filename)
{
    int32_t ari;

    size_t ret = fread(&ari, sizeof ari, 1, file);
    if (ret != 0) printf("fread returns %ld\n", ret);

    if (ari != IW_FILE_FORMAT_COMPLEX_DOUBLE) { // Check complex double format
        fprintf(stderr, "ERROR: '%s': Unrecognized file format\n", filename);
        exit(EXIT_FAILURE);
    }

    int32_t dim[2];
    ret = fread(dim, sizeof dim[0], 2, file);
    if (ret != 0) printf("fread returns %ld\n", ret);
    printf("Mat is %dx%d\n", dim[0], dim[1]);

    int32_t size[2]; // will contains SizeofEl and 0
    ret = fread(size, sizeof size[0], 2, file);
    if (ret != 0) printf("fread returns %ld\n", ret);
    assert( size[0] == sizeof(double complex) );

    printf("\tSizeOfEl : %d, and Zero :%d\n", size[0], size[1]);
    if (size[1] != 0)
        fprintf(stderr, "WARNING: '%s' file format could be incoherent\n", filename);

    mat->datas = malloc(size[0]*dim[0]*dim[1]); // Allocate enough memory
    mat->dim1 = dim[0];     // set dimensions
    mat->dim2 = dim[1];

    // Fill our buffer
    ret = fread(mat->datas, size[0], dim[0]*dim[1], file);
    if (ret != 0) printf("fread returns %ld\n", ret);
}

/**
 * \brief this function is a wrapper for the IW file format parser read_IW_file()
 */
static Matrix* load_matrix_from_file(const char *filename)
{
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        fprintf(stderr, "Cannot open file '%s': %s\n", filename, strerror(errno));
        exit(EXIT_FAILURE);
    }

    Matrix *m = calloc(1, sizeof*m);
    read_IW_file(m, file, filename);
    fclose(file);
    return m;
}

/** \brief allocate and initialize the user environement structure */
static Env* create_user_env(Matrix *A, RightPrecond *rpc)
{
    Env *env = calloc(1, sizeof*env);
    env->mat = A;
    env->right_precond = rpc;
    return env;
}

/** \brief allocate and initialize the right preconditioner structure */
static RightPrecond* create_right_precond(Matrix *mat)
{
    RightPrecond *rpc = calloc(1, sizeof*rpc);

    rpc->datas = malloc(mat->dim1 * sizeof rpc->datas[0]);
    rpc->dim = mat->dim1;

    // Fill with the diag of Mat
    for (int j = 0; j < rpc->dim; ++j)
        rpc->datas[j] = 1.0 / (mat->datas[j*(mat->dim1)+j]);
    return rpc;
}

/** \brief write down the convergence graph (minResidualNorm, maxResidualNorm, timestamp) */
static void write_down_convergence( const char *filename,
                                    int nbLog, const double *historic )

{
    FILE *conv_file = fopen(filename, "w");
    if (conv_file == NULL) {
        fprintf(stderr, "Cannot open file '%s': %s\n", filename, strerror(errno));
        exit(EXIT_FAILURE);
    }
    fprintf(conv_file, "Ite\tMin\tMax\tTime\n");
    for (int i = 0; i < nbLog; ++i)
        fprintf(conv_file, "%d\t%e\t%e\t%e\n",
                i, historic[3*i], historic[3*i+1], historic[3*i+2]);
    fclose(conv_file);
}

/********************* Main program ***************************/

int main(int argc, char *argv[])
{
    //Open Matrix file
    if (argc < 3) {
        fprintf(stderr, "Need to provide 2 files as parameters: Matrice and RHS\n");
        return EXIT_FAILURE;
    }

    Matrix *mat = load_matrix_from_file(argv[1]);  // Load the matrix
    Matrix *RHS = load_matrix_from_file(argv[2]);  // Load the RHS
    RightPrecond *rpc = create_right_precond(mat); // Create right precond
    Env *env = create_user_env(mat, rpc); // Create env

    assert( mat->dim1 == mat->dim2 ); // matrix must be squared
    assert( mat->dim1 == RHS->dim1 ); // same number of lines

    fabulous_handle handle;     // Init library:
    handle = fabulous_create(FABULOUS_COMPLEX_DOUBLE, mat->dim1, env);

    // Set the callbacks:
    fabulous_set_mvp(&mvp, handle);
    fabulous_set_dot_product(&dot_product, handle);
    fabulous_set_rightprecond(&right_precond, handle);

    double tolerance[1] = { 1e-4 };
    fabulous_set_parameters(
        10000,      // maximum number of Matrix x Vector product
        400,        // maximum size of base before restart
        tolerance,  /* tolerance (Backward Error), as an array   */
        1,          /* size of tolerance array
                     * Possible value for size are:
                     *   1: this means same tolerance for all the RHS vectors,
                     *   nbRHS: if (size != 1)
                     *           size must be equal to the number of RHS vectors
                     */
        handle  // the fabulous handle as returned by fabulous_create(...)
    );
    fabulous_set_ortho_process(FABULOUS_MGS, FABULOUS_RUHE, 2, handle);

    int nbRHS = RHS->dim2;
    // Create Initial Solution Block
    double complex *X0 = calloc(nbRHS*RHS->dim1, sizeof X0[0]);

    int nb_mvp = fabulous_solve_IB( // Solve:
        nbRHS,
        RHS->datas, RHS->dim1,
        X0, RHS->dim1,
        handle
    );
    printf("Nb matrix vector product: %d\n", nb_mvp);

    // Get the convergence history
    int nb_iter;
    const double *historic = fabulous_get_logs(&nb_iter, handle);
    write_down_convergence("./ConvergenceHisto.txt", nb_iter, historic);
    fabulous_print_log_filename("convergence_histo2.txt", "example_api2", handle);

    // Free the handle
    fabulous_destroy(handle);

    free(rpc->datas);
    free(rpc);
    free(X0);
    free(env);
    free(RHS->datas);
    free(RHS);
    free(mat->datas);
    free(mat);
    return EXIT_SUCCESS;
}
