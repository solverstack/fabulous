/**
 * \file example1.cpp
 * \brief Example for fabulous use; dense matrix, not distributed
 */

/*!
 * \example example4.cpp
 */

#include <fabulous.hpp>

#include "../common/MatrixMarketLoader.hpp"
#include "../common/RandomMatrixLoader.hpp"

namespace fa = fabulous;
using S = double;
using Block = fa::Block<S>;

/*! \brief example3 random symmetric positive definite matrix */
class RandomSpdMatrix
{
private:
    Block _data;

public:
    using value_type = std::complex<double>;
    using primary_type = double;

    explicit RandomSpdMatrix(int N):
        _data{N, N}
    {
        fa::RandomMatrixLoader rnld{/*generate_spd_matrix=*/true};
        rnld.LoadMatrix(_data);
    }

    void display(const std::string &name="A") const
    {
        _data.display(name);
    }

    /* ---------------------------------------------------------------- */
    /* --------------- methods for fabulous core: --------------------- */
    /* ---------------------------------------------------------------- */

    int size() const { return _data.get_nb_row(); }

    // B := A * X
    int64_t operator()(const Block &X, Block &B, S alpha=S{1.0}, S beta=S{0.0}) const
    {
        const int M = B.get_nb_row();
        const int N = B.get_nb_col();
        const int K = X.get_nb_row();
        fa::lapacke::gemm(
            M, N, K,
            _data.get_ptr(), _data.get_leading_dim(),
            X.get_ptr(), X.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha, beta
        );
        return fa::lapacke::flops::gemm<S>(M, N, K);
    }

}; // end class user_matrix

/*! \brief example4 variable precond (bcg) */
template<class Matrix, class P>
class Precond
{
private:
    const Matrix &_A;
    const int _max_mvp;
    const P _tolerance;

public:
    Precond(const Matrix &A, int max_mvp, P tolerance):
        _A{A}, _max_mvp{max_mvp}, _tolerance{tolerance}
    {
    }

    operator bool() const { return true; }

    int64_t operator()(const Block &B, Block &X) const
    {
        Block B2 = B.copy();
        std::vector<P> epsilon{_tolerance};

        fa::noop_placeholder _;
        auto eq = fa::equation(_A, _, _, X, B2, epsilon);
        auto param = fa::parameters(_max_mvp);
        auto algo = fa::bcg::v2();

        param.set_quiet(true);
        fa::bcg::solve(eq, algo, param);

        return 0;
    }
};

int main()
{
    const int dim = 1000;
    const int max_mvp = 10000;
    const int max_space = 90;
    const int nrhs = 6;

    std::vector<double> epsilon{1e-4};
    Block X{dim, nrhs};
    Block X_exact{dim, nrhs};
    Block B{dim, nrhs};

    fa::RandomMatrixLoader rnl;
    rnl.LoadMatrix(X_exact);

    RandomSpdMatrix A{dim};
    Precond<RandomSpdMatrix,double> M{A, 1000, 1e-1};

    A(X_exact, B); // B <- A*X_exact

    fa::noop_placeholder _;
    auto eq = fa::equation(A, M, _, X, B, epsilon);
    auto param = fa::parameters(max_mvp, max_space);
    auto ortho = fa::OrthoType::BLOCK + fa::OrthoScheme::MGS;
    auto algo = fa::bgcr::std();
    auto log = fa::bgcr::solve(eq, algo, param, ortho);

    std::ofstream file{"LogExample4.txt", std::ios::out};
    log.print("random_spd", file); // this prints comma-separated columns with information about convergence
    return 0;
}
