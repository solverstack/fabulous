/*!
 * \file example1.cpp
 * \brief Example for fabulous use; dense matrix, not distributed
 */

/*!
 * \example example1.cpp
 */

#include <fabulous.hpp>

#include <utility>

#include "../common/MatrixMarketLoader.hpp"
#include "../common/RandomMatrixLoader.hpp"

namespace fa = ::fabulous;
using S = std::complex<double>;
using Block = fa::Block<S>;

/*! \brief example1 matrix */
class Matrix
{
private:
    Block _data;

    void load_matrix(const std::string &filename)
    {
        fa::MatrixMarketLoader mml;
        mml.LoadMatrix(_data, filename);
    }

public:
    using value_type = std::complex<double>;
    using primary_type = double;

    explicit Matrix(const std::string &filename)
    {
        load_matrix(filename);
    }

    /* ---------------------------------------------------------------- */
    /* --------------- methods for fabulous core: --------------------- */
    /* ---------------------------------------------------------------- */

    int size() const { return _data.get_nb_row(); }

    // B := A * X
    int64_t operator()(const Block &X, Block &B, S alpha=S{1.0}, S beta=S{0.0}) const
    {
        int M = B.get_nb_row();
        int N = B.get_nb_col();
        int K = X.get_nb_row();
        fa::lapacke::gemm(
            M, N, K,
            _data.get_ptr(), _data.get_leading_dim(),
            X.get_ptr(), X.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha, beta
        );
        return fa::lapacke::flops::gemm<S>(M, N, K);
    }

}; // end class Matrix

int main()
{
    /* get the young1c matrix for this example at
     ftp://math.nist.gov/pub/MatrixMarket2/Harwell-Boeing/acoust/young1c.mtx.gz
     */
    Matrix A{"../../data/young1c.mtx"};
    std::vector<double> epsilon{1e-4};
    const int dim = A.size();
    const int max_mvp = 10000;
    const int max_krylov_space = 90;
    const int nrhs = 6;
    const int nb_eigen_pair = 5;
    Block X{dim, nrhs};
    Block B{dim, nrhs};

    fa::RandomMatrixLoader rnl;
    rnl.LoadMatrix(B);

    fa::noop_placeholder _;
    auto eq = fa::equation(A, _, _, X, B, epsilon);
    auto param = fa::parameters(max_mvp, max_krylov_space);
    auto ortho = fa::OrthoType::BLOCK + fa::OrthoScheme::MGS;
    auto restart = fa::deflated_restart(nb_eigen_pair, S{0.0});
    auto algo = fa::bgmres::ibdr();
    auto log = fa::bgmres::solve(eq, algo, param, ortho, restart);
    log.print("young1c"); // this prints comma-separated columns with information about convergence
    return 0;
}
