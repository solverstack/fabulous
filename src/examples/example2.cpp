/**
 * \file example1.cpp
 * \brief Example for fabulous use; dense matrix, not distributed
 */

/*!
 * \example example2.cpp
 */

#include <fabulous.hpp>

#include "../common/MatrixMarketLoader.hpp"
#include "../common/RandomMatrixLoader.hpp"

namespace fa = fabulous;
using S = double;
using Block = fa::Block<S>;

/*! \brief example2 callback */
template<class Matrix>
class Callback
{
private:
    const Matrix &_matrix;
    const Block  &_X_exact;
public:
    Callback(const Matrix &matrix, const Block &X_exact):
        _matrix{matrix}, _X_exact{X_exact}
    {
    }

    template<class Logger>
    void operator()(int iter, Logger &logger, const Block &X, const Block &R) const
    {
        (void) iter;
        (void) R;

        /* We use the callback to debug and check that
         * the A-norm of (X-Xexact) is decreasing */

        int M = _X_exact.get_nb_row();
        int N = _X_exact.get_nb_col();
        Block XDiff{M, N};
        Block AXDiff{M, N};
        Block Anorm{N, N};
        FABULOUS_ASSERT( X.get_nb_col() == _X_exact.get_nb_col() );

        for (int j = 0; j < N; ++j) {
            for (int i = 0; i < M; ++i) {
                XDiff(i, j) = X(i, j) - _X_exact(i, j);
            }
        }
        _matrix(XDiff, AXDiff); // AXDiff <- A*XDiff
        fabulous::lapacke::Tgemm(N, N, M,
                                 XDiff.get_ptr(), XDiff.get_leading_dim(),
                                 AXDiff.get_ptr(), AXDiff.get_leading_dim(),
                                 Anorm.get_ptr(), Anorm.get_leading_dim());
        // Anorm <- XDiff^{T} * A * Xdiff

        auto min_max = min_max_norm(Anorm);
        auto min = std::get<0>(min_max);
        auto max = std::get<1>(min_max);
        logger.set_iteration_user_data(0, min, 0);
        logger.set_iteration_user_data(1, max, 0);
    }

}; // end class Callback

/*! \brief example2 random symmetric positive definite matrix */
class RandomSpdMatrix
{
private:
    Block _data;

public:
    using value_type   = typename Block::value_type;
    using primary_type = typename Block::primary_type;

    explicit RandomSpdMatrix(int N):
        _data{N, N}
    {
        fa::RandomMatrixLoader rnld{/*generate_spd_matrix=*/true};
        rnld.LoadMatrix(_data);
    }

    void display(const std::string &name="A") const
    {
        _data.display(name);
    }

    /* ---------------------------------------------------------------- */
    /* --------------- methods for fabulous core: --------------------- */
    /* ---------------------------------------------------------------- */

    int size() const { return _data.get_nb_row(); }

    // B := A * X
    int64_t operator()(const Block &X, Block &B,
                       value_type alpha=value_type{1.0},
                       value_type beta=value_type{0.0}) const
    {
        int M = B.get_nb_row();
        int N = B.get_nb_col();
        int K = X.get_nb_row();
        fa::lapacke::gemm(
            M, N, K,
            _data.get_ptr(), _data.get_leading_dim(),
            X.get_ptr(), X.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha, beta
        );
        return fa::lapacke::flops::gemm<S>(M, N, K);
    }

    // [Q,R] := QR(Q)
    int64_t QRFacto(Block &Q, Block &R) const
    {
        // default implementation: ( distributed QR; uses DotProduct )
        //fa::qr::InPlaceQRFactoMGS_User(Q, R, *this);

        // default implementation: (not distributed)
        return fa::qr::InPlaceQRFacto(Q, R);
    }

    // C := A^{H} * B
    int64_t DotProduct(int M, int N,
                       const S *A, int lda,
                       const S *B, int ldb,
                       S *C, int ldc) const
    {
        int dim = _data.get_nb_row();
        fa::lapacke::Tgemm(M, N, dim, A, lda, B, ldb, C, ldc);
        return fa::lapacke::flops::gemm<S>(M, N, dim);
    }

}; // end class user_matrix

int main()
{
    int dim = 1000;
    int max_mvp = 10000;
    int nrhs = 100;

    std::vector<double> epsilon{1e-6};
    Block X{dim, nrhs};
    Block X_exact{dim, nrhs};
    Block B{dim, nrhs};

    fa::RandomMatrixLoader rnl;
    rnl.LoadMatrix(X_exact);

    RandomSpdMatrix A{dim};
    Callback<RandomSpdMatrix> CB{A, X_exact};

    A(X_exact, B); // B <- A*X_exact

    std::vector<double>  n = B.cwise_norm();
    std::vector<double> sc = fa::array_inverse(n);
    X_exact.cwise_scale(sc);
    /* X_exact is scaled with B norms because
     * X passed in the callback is like this too */

    fa::noop_placeholder _;
    auto eq = fa::equation(A, _, CB, X, B, epsilon);
    auto param = fa::parameters(max_mvp);
    param.set_real_residual(true);

    auto algo = fa::bcg::v2();
    auto log = fa::bcg::solve(eq, algo, param);

    std::ofstream file{"LogExample2.txt", std::ios::out};
    log.print("random_spd", file); // this prints comma-separated columns with information about convergence
    return 0;
}
