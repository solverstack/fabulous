#ifndef FABULOUS_G_API_VECTOR_HPP
#define FABULOUS_G_API_VECTOR_HPP

#include <cassert>
#include <vector>
#include <type_traits>
#include <memory>


namespace fabulous {
namespace api_g {
template<class S> class Vector;
}
}

#include "fabulous.h"
#include "fabulous/utils/Error.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/QR.hpp"

namespace fabulous {
namespace api_g {

using VoidPtr = std::shared_ptr<void>;


/*! \brief C++ wrapper for user vectors
 * \note the vector storage and method implementations are entirely up to the user
 */
template<class S>
class Vector
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;

private:
    fabulous_g_vector_operation_t _operations;
    void *_vec;
    VoidPtr _ptr;

public:
    /*********************************************************/

    explicit Vector() {}

    explicit Vector(const fabulous_g_vector_operation_t &ops, void *vec):
        _operations(ops),
        _vec{vec}
    {
    }

    void qr(Vector &q, Block<S> &r)
    {
        if (q._vec != _vec) {
            q.copy(*this);
        }
        //FABULOUS_DEBUG("QR custom vector");
        if ( r.get_leading_dim() > r.get_nb_row() ) {
            Block<S> tmp{r.get_nb_row(), r.get_nb_col() };
            _operations.qr(q._vec, tmp.get_ptr());
            r.copy(tmp);
        } else {
            _operations.qr(q._vec, r.get_ptr());
        }
    }

    void trsm(Vector &q, Block<S> &r)
    {
        if (q._vec != _vec) {
            q.copy(*this);
        }

        if ( r.get_leading_dim() > r.get_nb_row() ) {
            Block<S> tmp{r.get_nb_row(), r.get_nb_col() };
            _operations.trsm(q._vec, tmp.get_ptr());
            r.copy(tmp);
        } else {
            _operations.trsm(q._vec, r.get_ptr());
        }
    }

    void cwise_scale(const std::vector<primary_type> &coef)
    {
        //FABULOUS_DEBUG("CWISE SCALE custom vector");
        const int n = this->get_nb_col();
        FABULOUS_ASSERT( (int)coef.size() == n );
        _operations.cwise_scale(_vec, (void*)coef.data() );
    }

    std::vector<primary_type> cwise_norm() const
    {
        //FABULOUS_DEBUG("CWISE NORM custom vector");
        std::vector<primary_type> vec_norm;
        const int n = this->get_nb_col();
        vec_norm.resize(n);
        std::fill(vec_norm.begin(), vec_norm.end(), primary_type{0.0});
        _operations.cwise_norm( _vec, (void*)vec_norm.data() );
        return vec_norm;
    }

    void *get_vect() const
    {
        return _vec;
    }

    /*********************************************************/
    // FOLLOWING IS REQUIRED BY FABULOUS CORE:

    /* \brief basic vector creation constructor (for fabulous core)
     *   vector are always constructed from another given other vector
     *   so that they can retained its dimension and/or dot product properties
     *   in a transparent way from fabulous core point of view
     */
    Vector(const Vector &o, int nb_col):
        _operations(o._operations)
    {
        //FABULOUS_DEBUG("CREATE custom vector");
        _operations.create_resize(o._vec, &_vec, nb_col);
        auto destroy = _operations.destroy;
        _ptr.reset(_vec, [=](void *d) {
                //FABULOUS_DEBUG("DESTROY custom vector");
                destroy(d);
            }
        );
    }

    int get_nb_col() const
    {
        int n;
        _operations.get_nb_col(_vec, &n);
        return n;
    }

    int get_nb_local_row() const
    {
        return -1;
    }

    void dot(const Vector &b, Block<S> &c) const
    {
        //FABULOUS_DEBUG("DOT custom vector");
        if ( c.get_leading_dim() != c.get_nb_row() ) {
            FABULOUS_ASSERT( c.get_leading_dim() > c.get_nb_row() );
            Block<S> tmp{c.get_nb_row(), c.get_nb_col()};
            FABULOUS_ASSERT( c.get_nb_row() == get_nb_col() );
            FABULOUS_ASSERT( c.get_nb_col() == b.get_nb_col() );

            _operations.dot(_vec, b._vec, tmp.get_ptr());
            c.copy(tmp);
        } else {
            _operations.dot(_vec, b._vec, c.get_ptr());
        }
    }

    void dot(const Vector &b, S &c) const
    {
        _operations.dot(_vec, b._vec, &c);
    }

    void axpy(const Vector &b, const Block<S> &s, S alpha=S{1.0})
    {
        //FABULOUS_DEBUG("AXPY custom vector");
        FABULOUS_ASSERT( alpha == S{1.0} || alpha == S{-1.0} );
        const Block<S> *seq = &s;
        Block<S> tmp{};
        if ( s.get_leading_dim() != s.get_nb_row() ) {
            FABULOUS_ASSERT( s.get_leading_dim() > s.get_nb_row() );
            tmp = s.copy();
            seq = &tmp;
        }
        _operations.axpy( _vec, b._vec, (void*)seq->get_ptr(), (void*)&alpha );
    }


    void axpy(const Vector &b, const S &s, S alpha=S{1.0})
    {
        //FABULOUS_DEBUG("AXPY custom vector");
        FABULOUS_ASSERT( alpha == S{1.0} || alpha == S{-1.0} );
        _operations.axpy( _vec, b._vec, (void*)&s, (void*)&alpha );
    }

    void zero()
    {
        //FABULOUS_DEBUG("ZERO custom vector");
        _operations.zero(_vec);
    }

    void cwise_axpy(const Vector &b, S alpha=S{1.0})
    {
        //FABULOUS_DEBUG("CWISE AXPY custom vector");
        _operations.cwise_axpy(_vec, b._vec, (void*) &alpha);
    }

    void copy(const Vector &o)
    {
        //FABULOUS_DEBUG("COPY custom vector");
        _operations.copy(o._vec, _vec);
    }

    Vector copy()
    {
        Vector cp{*this, this->get_nb_col()};
        _operations.copy(this->_vec, cp._vec);
        return cp;
    }
    Vector sub_vector(const int offset, const int nb_vect) const {
        (void) offset;
        (void) nb_vect;
        FABULOUS_ASSERT(false);
        //FABULOUS_TRHOW(Unsupported, "should not be called");
	return *this;
    }
    Vector& operator*= (const S& scal){
        (void) scal;
        FABULOUS_ASSERT(false);
        return *this;
    }
    Vector get_bvect(const int offset) const {
	return this->sub_vector(offset, 1);
    }

}; // end class Vector


  template<class Scalar, typename P = typename fabulous::Block<Scalar>::primary_type>
    P snorm(const Vector<Scalar> &ref){
      (void) ref;
      FABULOUS_ASSERT(false);
      return P{0.0};
    }
  template<class Scalar>
    void sscale(Vector<Scalar> &ref, Scalar a){
      ref *= a;
    }
} // end namespace api_g
} // end namespace fabulous

#endif // FABULOUS_G_API_VECTOR_HPP
