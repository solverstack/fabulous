
#include "fabulous.h"
#include "ApiEngine.hpp"
#include "fabulous/utils/Utils.hpp"

using namespace fabulous::api_g;

namespace {
thread_local ::std::string s_last_errmsg="Success";
}

namespace fabulous {
namespace api_g {

void set_errmsg(const std::string &errmsg)
{
    s_last_errmsg = errmsg;
}

}
}

FABULOUS_BEGIN_C_DECL

#define FABULOUS_G_API_ENGINE(handle_)           \
    reinterpret_cast<ApiEngineI*>(handle_)

#define FABULOUS_G_LOGGER(handle_)                       \
    reinterpret_cast<::fabulous::Logger*>(handle_)

#define FABULOUS_G_HANDLE(engine_)               \
    reinterpret_cast<fabulous_g_handle>(engine_)

FABULOUS_EXPORT
fabulous_g_handle fabulous_g_create(fabulous_g_arithmetic ari, void *user_env)
{
    ApiEngineI *engine = nullptr;

    switch (ari) {
    case FABULOUS_G_REAL_FLOAT:
        engine = new ApiEngine<float>(user_env);
        break;
    case FABULOUS_G_REAL_DOUBLE:
        engine = new ApiEngine<double>(user_env);
        break;
    case FABULOUS_G_COMPLEX_FLOAT:
        engine = new ApiEngine<std::complex<float>>(user_env);
        break;
    case FABULOUS_G_COMPLEX_DOUBLE:
        engine = new ApiEngine<std::complex<double>>(user_env);
        break;
    default:
        s_last_errmsg = "Invalid arithmetic parameter";
        break;
    }
    return FABULOUS_G_HANDLE(engine);
}

#define TRY_CATCH_VOID(disp_, method_, ...)     \
    do {                                        \
        try {                                   \
            (disp_)->method_(__VA_ARGS__);      \
            return 0;                           \
        } catch (::fabulous::Error &err) {      \
            s_last_errmsg.assign(err.what());   \
            return - err.get_error_code();      \
        }                                       \
    } while(0)

#define TRY_CATCH_INT(disp_, method_, ...)              \
    do {                                                \
        try {                                           \
            return (disp_)->method_(__VA_ARGS__);       \
        } catch (::fabulous::Error &err) {              \
            s_last_errmsg.assign(err.what());           \
            return -err.get_error_code();               \
        }                                               \
    }while(0)

FABULOUS_EXPORT
const char *fabulous_g_last_error_str(void)
{
    static thread_local ::std::string s_errmsg;
    s_errmsg = s_last_errmsg;
    return s_errmsg.c_str();
}

FABULOUS_EXPORT
const char *fabulous_g_error_type_str(int errcode)
{
    static thread_local ::std::string s_errmsg;
    s_errmsg = ::fabulous::error_type(errcode);
    return s_errmsg.c_str();
}

FABULOUS_EXPORT
void fabulous_g_set_operations(fabulous_g_mvp_t user_mvp,
                              fabulous_g_rightprecond_t user_rpc,
                              fabulous_g_callback_t user_callback,
                              const fabulous_g_vector_operation_t *vector_ops,
                              fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    disp->set_operations(user_mvp, user_rpc, user_callback, vector_ops);
}

FABULOUS_EXPORT
int fabulous_g_set_parameters(int max_mvp, int max_space_size,
                             void *tolerance, int nb_tol,
                             fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, set_parameters, max_mvp, max_space_size, tolerance, nb_tol);
}

FABULOUS_EXPORT
int fabulous_g_set_advanced_parameters(int max_kept_direction,
                                      int real_residual,
                                      int logger_user_data_size,
                                      int quiet,
                                      fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, set_advanced_parameters,
                   max_kept_direction, real_residual,
                   logger_user_data_size, quiet);
}

FABULOUS_EXPORT
int fabulous_g_set_ortho_process(fabulous_g_orthoscheme scheme,
                                fabulous_g_orthotype type,
                                int nb_iter,fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, set_ortho_process, scheme, type, nb_iter);
}

FABULOUS_EXPORT
int fabulous_g_solve(void *X, void *B, fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve, X, B);
}

FABULOUS_EXPORT
int fabulous_g_solve_GCR(void *X, void *B, fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_GCR, X, B);
}

FABULOUS_EXPORT
int fabulous_g_get_Uk_const_last_solve(fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    return disp->get_Uk_const_last_solve();
}

FABULOUS_EXPORT
int fabulous_g_solve_GCRO(void *X, void *B, void **Uk, int nb_eigen_pair,
                          void *target, int Uk_const,
                          fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_GCRO, X, B, Uk, nb_eigen_pair, target, Uk_const);
}

FABULOUS_EXPORT
int fabulous_g_solve_IB(void *X, void *B,
                       fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_IB, X, B);
}

FABULOUS_EXPORT
int fabulous_g_solve_DR(void *X, void *B,
                       int nb_eigen_pair, void *target,
                       fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_DR, X, B, nb_eigen_pair, target);
}

FABULOUS_EXPORT
int fabulous_g_solve_IBDR(void *X, void *B,
                         int nb_eigen_pair, void *target,
                         fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_IBDR, X, B, nb_eigen_pair, target);
}

FABULOUS_EXPORT
int fabulous_g_solve_QR(void *X, void *B,
                       fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_QR, X, B);
}

FABULOUS_EXPORT
int fabulous_g_solve_QRIB(void *X, void *B,
                         fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_QRIB, X, B);
}

FABULOUS_EXPORT
int fabulous_g_solve_QRDR(void *X, void *B,
                         int nb_eigen_pair, void *target,
                         fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_QRDR, X, B, nb_eigen_pair, target);
}

FABULOUS_EXPORT
int fabulous_g_solve_QRIBDR(void *X, void *B,
                           int nb_eigen_pair, void *target,
                           fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_QRIBDR, X, B, nb_eigen_pair, target);
}

FABULOUS_EXPORT
const double *fabulous_g_get_logs(int *size, fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    try {
        return disp->get_logs(size);
    } catch (::fabulous::Error &err) {
        s_last_errmsg = err.what();
        return NULL;
    }
}

FABULOUS_EXPORT
int fabulous_g_set_iteration_user_data(unsigned id, double fp, int64_t integ, fabulous_g_handle handle)
{
    try {
        ::fabulous::Logger *logger = FABULOUS_G_LOGGER(handle);
        logger->set_iteration_user_data(id, fp, integ);
        return 0;
    } catch (::fabulous::Error &err) {
        s_last_errmsg = err.what();
        return -err.get_error_code();
    }
}

FABULOUS_EXPORT
int fabulous_g_print_log_file(void *file, const char *log_id, fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, print_log_file, (FILE*)file, log_id);
}

FABULOUS_EXPORT
int fabulous_g_print_log_filename(const char *filename, const char *log_id, fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, print_log_filename, filename, log_id);
}

FABULOUS_EXPORT
void fabulous_g_destroy(fabulous_g_handle handle)
{
    ApiEngineI *disp = FABULOUS_G_API_ENGINE(handle);
    delete disp;
}

FABULOUS_EXPORT
void fabulous_g_set_output_colored(int enable_color)
{
    ::fabulous::set_output_colored(enable_color != 0);
}

FABULOUS_END_C_DECL
