#ifndef FABULOUS_G_API_ENGINE_HPP
#define FABULOUS_G_API_ENGINE_HPP

#include <fstream>
#include <sstream>
#include <cstring>

namespace fabulous {

/*! \brief generic vector c-api implementation */
namespace api_g {
struct ApiEngineI;
template<class S> class ApiEngine;
}
}

#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"

#include "fabulous/orthogonalization/Orthogonalizer.hpp"

#include "fabulous/algo/BGMRes.hpp"
#include "fabulous/algo/BGCR.hpp"
#include "fabulous/algo/BGCRO.hpp"
#include "fabulous/algo/AlgoType.hpp"
#include "fabulous/algo/Equation.hpp"

#include "fabulous.h"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "Precond.hpp"
#include "Callback.hpp"

namespace fabulous {
namespace api_g {

/**
 * \brief This interface is used to operate on the handle given by the user
 * without knowing which arithmetic is used.
 */
struct ApiEngineI
{
    virtual ~ApiEngineI(){}
    virtual void set_operations(fabulous_g_mvp_t user_mvp,
                                fabulous_g_rightprecond_t user_rpc,
                                fabulous_g_callback_t user_callback,
                                const fabulous_g_vector_operation_t *vector_ops) = 0;

    virtual void set_parameters(int max_mvp, int max_krylov_space_size,
                                void *tolerance, int nb_tol) = 0;
    virtual void set_advanced_parameters(int max_kept_direction,
                                         int real_residual,
                                         int logger_user_data_size,
                                         int quiet) = 0;
    virtual void set_ortho_process(fabulous_g_orthoscheme scheme,
                                   fabulous_g_orthotype type, int nb_iter) = 0;

    virtual int solve(void *X, void *B) = 0;
    virtual int solve_GCR(void *X, void *B) = 0;

    virtual int solve_GCRO(void *X, void *B, void** Uk,
                           int nb_eigen_pair, void *target,
                           int Uk_const) = 0;

    virtual int solve_IB(void *X, void *B) = 0;
    virtual int solve_DR(void *X, void *B,
                         int nb_eigen_pair, void *target) = 0;
    virtual int solve_IBDR(void *X, void *B,
                           int nb_eigen_pair, void *target) = 0;

    virtual int solve_QR(void *X, void *B) = 0;
    virtual int solve_QRIB(void *X, void *B) = 0;
    virtual int solve_QRDR(void *X, void *B,
                           int nb_eigen_pair, void *target) = 0;
    virtual int solve_QRIBDR(void *X, void *B,
                             int nb_eigen_pair, void *target) = 0;

    virtual const double *get_logs(int *size) = 0;
    virtual void print_log_filename(const char *filename, const char *log_id) const = 0;
    virtual void print_log_file(FILE *file, const char *log_id) const = 0;

    // For bgcro only
    virtual int get_Uk_const_last_solve(void) const = 0;

}; // end struct ApiEngineI

/**
 * \brief Main Engine of the Library. Each method of the API except for
 * the init function is a member function of both this class and its
 * parent's one.
 */
template<class S>
class ApiEngine : public ApiEngineI
{
public:
    using value_type = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
    using P = primary_type;

private:
    Matrix _matrix;
    Precond _precond;
    Callback _callback;
    fabulous_g_vector_operation_t _operations;

    Parameters _params;
    std::vector<P> _tolerance;
    OrthoScheme _ortho_scheme;
    OrthoType _ortho_type;
    int _ortho_nb_iter;
    Logger _logger;
    bool _Uk_const_last_solve;

public:
    ApiEngine(void *user_env):
        _matrix{user_env},
        _precond{user_env},
        _callback{user_env},
        _tolerance{},
        _ortho_scheme{OrthoScheme::MGS/*default value*/},
        _ortho_type{OrthoType::RUHE/*default value*/},
        _ortho_nb_iter{2},
        _Uk_const_last_solve{false}
    {
    }

    void set_operations(fabulous_g_mvp_t user_mvp,
                        fabulous_g_rightprecond_t user_rpc,
                        fabulous_g_callback_t user_callback,
                        const fabulous_g_vector_operation_t *vector_ops) override
    {
        _matrix.set_mvp(user_mvp);
        _precond.set_precond(user_rpc);
        _callback.set_callback(user_callback);
        _operations = *vector_ops;
    }

    void set_parameters(int max_mvp, int max_space,
                        void *tolerance, int nb_tolerance) override
    {
        P *pTolerance = reinterpret_cast<P*>(tolerance);
        _tolerance.clear();
        _tolerance.reserve(nb_tolerance);
        _tolerance.assign(pTolerance, pTolerance+nb_tolerance);

        _params.max_mvp = max_mvp;
        _params.max_space = max_space;
    }

    void set_advanced_parameters(int max_kept_direction,
                                 int real_residual,
                                 int logger_user_data_size,
                                 int quiet) override
    {
        _params.set_max_kept_direction(max_kept_direction);
        _params.set_real_residual(real_residual != 0);
        _params.set_logger_user_data_size(logger_user_data_size);
        _params.set_quiet(quiet != 0);
    }

    void set_ortho_process(fabulous_g_orthoscheme scheme,
                           fabulous_g_orthotype type, int nb_iter) override
    {
        switch (scheme) {
        case FABULOUS_G_MGS:  _ortho_scheme = OrthoScheme::MGS;  break;
        case FABULOUS_G_CGS:  _ortho_scheme = OrthoScheme::CGS;  break;
        case FABULOUS_G_IMGS: _ortho_scheme = OrthoScheme::IMGS; break;
        case FABULOUS_G_ICGS: _ortho_scheme = OrthoScheme::ICGS; break;
        default:
            FABULOUS_THROW(Parameter, "Parameter ortho_scheme is invalid");
            break;
        }
        switch (type) {
        case FABULOUS_G_RUHE:  _ortho_type = OrthoType::RUHE;  break;
        case FABULOUS_G_BLOCK: _ortho_type = OrthoType::BLOCK; break;
        default:
            FABULOUS_THROW(Parameter, "Parameter ortho_type is invalid");
            break;
        }
        if (nb_iter < 2 && (scheme == FABULOUS_G_IMGS || scheme == FABULOUS_G_ICGS)) {
            FABULOUS_THROW(Parameter, "the iter parameter must be greater than 2 or equal to 2");
        }
        _ortho_nb_iter = nb_iter;
    }

    int get_Uk_const_last_solve(void) const override { return _Uk_const_last_solve; }


private:
    template<class Algo>
    int call_solve(Algo algo, void *X_, void *B_)
    {
        Vector<S> X{_operations, X_};
        Vector<S> B{_operations, B_};

        auto eq = equation(_matrix, _precond, _callback, X, B, _tolerance);
        auto ortho = orthogonalization(_ortho_scheme, _ortho_type, _ortho_nb_iter);
        auto restart = classic_restart();

        bgmres::BGMRes<S> bgmres;
        const int mvp = bgmres.solve( eq, algo, _params, ortho, restart );
        _logger = bgmres.get_logger();
        return mvp;
    }

    template<class Algo>
    int call_solveDR(Algo algo, void *X_, void *B_, int nb_eigen_pair, S target)
    {
        Vector<S> X{_operations, X_};
        Vector<S> B{_operations, B_};

        auto eq = equation(_matrix, _precond, _callback, X, B, _tolerance);
        auto restart = deflated_restart(nb_eigen_pair, target);
        auto ortho = orthogonalization(_ortho_scheme, _ortho_type, _ortho_nb_iter);

        bgmres::BGMRes<S> bgmres;
        const int mvp = bgmres.solve(eq, algo, _params, ortho, restart);
        _logger = bgmres.get_logger();
        return mvp;
    }


    template<class Algo>
    int call_solveGCRO(Algo algo, void *X_, void *B_, void **Uk_, int nb_eigen_pair, S target, bool Uk_const = false)
    {
        Vector<S> X{_operations, X_};
        Vector<S> B{_operations, B_};
        Vector<S> Uk{_operations, *Uk_};

        auto eq = equation(_matrix, _precond, _callback, X, B, _tolerance);
        auto restart = deflated_restart(nb_eigen_pair, target);
        auto ortho = orthogonalization(_ortho_scheme, _ortho_type, _ortho_nb_iter);

        bgcro::BGCRO<S> bgcro{Uk_const};
        const int mvp = bgcro.solve(eq, algo, _params, ortho, restart, Uk);
        _logger = bgcro.get_logger();

        _Uk_const_last_solve = bgcro.get_Uk_const();
        // If deflation space is declared constant and remained constant
        // do not work on it
        // Otherwise allocate and copy values of Uk
        if(Uk.get_nb_col() > 0 && !_Uk_const_last_solve) _operations.create_resize(&Uk, Uk_, Uk.get_nb_col());
        return mvp;
    }

    template<class Algo>
    int call_solveGCR(Algo algo, void *X_, void *B_)
    {
        (void) algo;
        (void) B_;
        (void) X_;
        return 0;
        // Block<S> X{_dim, X_};
        // Block<S> B{_dim, B_};
        // auto eq = equation(_matrix, _precond, _callback, X, B, _tolerance);
        // auto ortho = orthogonalization(_ortho_scheme, _ortho_type, _ortho_nb_iter);

        // bgcr::BGCR<S> bgcr;
        // const int mvp = bgcr.solve(eq,algo, _params, ortho);
        // _logger = bgcr.get_logger();
        // return mvp;
    }

public:
    int solve(void *X, void *B) override
    {
        return call_solve(bgmres::std(), X, B);
    }

    int solve_GCR(void *X, void *B) override
    {
        return call_solveGCR(bgcr::std(), X, B);
    }

    int solve_GCRO(void *X, void *B, void** Uk, int nb_eigen_pair, void *target, int Uk_const) override
    {
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveGCRO(bgcro::ibdr(), X, B, Uk, nb_eigen_pair, Target, (Uk_const != 0));
    }

    int solve_IB(void *X, void *B) override
    {
        return call_solve(bgmres::ib(), X, B);
    }

    int solve_DR(void *X, void *B, int nb_eigen_pair, void *target) override
    {
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveDR(bgmres::std(), X, B, nb_eigen_pair, Target);
    }

    int solve_IBDR(void *X, void *B, int nb_eigen_pair, void *target) override
    {
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveDR(bgmres::ibdr(), X, B, nb_eigen_pair, Target);
    }

    int solve_QR(void *X, void *B) override
    {
        return call_solve(bgmres::qr(), X, B);
    }

    int solve_QRIB(void *X, void *B) override
    {
        return call_solve(bgmres::qribdr(), X, B);
    }

    int solve_QRDR(void *X, void *B, int nb_eigen_pair, void *target) override
    {
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveDR(bgmres::qrdr(), X, B, nb_eigen_pair, Target);
    }

    int solve_QRIBDR(void *X, void *B, int nb_eigen_pair, void *target) override
    {
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveDR(bgmres::qribdr(), X, B, nb_eigen_pair, Target);
    }

    const double *get_logs(int *size) override
    {
        *size = _logger.get_nb_iterations();
        return _logger.write_down_array();
    }

    void print_log_file(FILE *file, const char *log_id) const override
    {
        if (file != NULL) {
            _logger.print(file, log_id);
        } else {
            FABULOUS_ERROR("invalid argument");
        }
    }

    void print_log_filename(const char *filename, const char *log_id) const override
    {
        std::ofstream file{filename, std::ios::out};
        if (file.is_open()) {
            _logger.print(std::string{log_id}, file);
        } else {
            std::stringstream ss;
            ss << "Cannot open '" << filename << "': '" << strerror(errno)<<"'";
            error(ss.str());
        }
    }

}; // end class ApiEngine

} // end namespace api_g
} // end namespace fabulous

#endif // FABULOUS_G_API_ENGINE_HPP
