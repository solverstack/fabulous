#ifndef FABULOUS_G_API_CALLBACK_HPP
#define FABULOUS_G_API_CALLBACK_HPP

#include <cassert>

namespace fabulous {
class Callback;
}

#include "fabulous.h"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/QR.hpp"
#include "Vector.hpp"

namespace fabulous {
namespace api_g {

/*! \brief C++ wrapper over the user generic callback. */
class Callback
{
private:
    void *_user_env;
    fabulous_g_callback_t _callback;

public:
    explicit Callback(void *user_env):
        _user_env{user_env},
        _callback{nullptr}
    {
    }

    void set_callback(fabulous_g_callback_t callback)
    {
        _callback = callback;
    }

    template<class S>
    void operator()(int iter, Logger &logger, const Vector<S> &X, const Vector<S> &R) const
    {
        if ( _callback != nullptr ) {
            FABULOUS_ASSERT( X.get_nb_col() == R.get_nb_col() );
            _callback(_user_env, iter,   X.get_vect(), R.get_vect(), (fabulous_g_handle) &logger );
        }
    }

}; // end class Callback

} // end namespace api_g
} // end namespace fabulous

#endif // FABULOUS_G_API_CALLBACK_HPP
