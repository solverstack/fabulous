#ifndef FABULOUS_G_API_MATRIX_HPP
#define FABULOUS_G_API_MATRIX_HPP

#include <cassert>

namespace fabulous {
namespace api_g {
class Matrix;
}
}

#include "fabulous.h"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/QR.hpp"
#include "Vector.hpp"

namespace fabulous {
namespace api_g {

/*! \brief C++ wrapper over the user matrix-vector product callback. */
class Matrix
{
private:
    void *_user_env;
    fabulous_g_mvp_t _user_mvp;

    // S at(int, int) { FABULOUS_G_THROW(Unsupported, "should not be reached"); return S{0.0}; }

public:
    explicit Matrix(void *user_env):
        _user_env{user_env},
        _user_mvp{nullptr}
    {
    }

    void set_mvp(fabulous_g_mvp_t user_mvp)
    {
        _user_mvp = user_mvp;
    }

    template<class S>
    int64_t operator()( const Vector<S> &IN, Vector<S> &OUT,
                        S alpha = S{1.0}, S beta = S{0.0}) const
    {
        if ( _user_mvp == nullptr ) {
            FABULOUS_ERROR("User matrix vector product is not set!");
            FABULOUS_ERROR("Have you called fabulous_g_set_mvp() ?!");
            FABULOUS_THROW(NotImplemented, "Invalid user matrix product callback");
        }
        // FABULOUS_DEBUG("MULT custom vector alpha="<<alpha<<" beta="<<beta);
        return _user_mvp( _user_env, &alpha, IN.get_vect(), &beta, OUT.get_vect() );
    }

}; // end class Matrix

} // end namespace api_g
} // end namespace fabulous

#endif // FABULOUS_G_API_MATRIX_HPP
