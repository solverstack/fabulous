#ifndef FABULOUS_G_API_PRECOND_HPP
#define FABULOUS_G_API_PRECOND_HPP

#include <cassert>

namespace fabulous {
namespace api_g {
class Precond;
}
}

#include "fabulous.h"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/QR.hpp"
#include "Vector.hpp"

namespace fabulous {
namespace api_g {

/*! \brief C++ wrapper over the user preconditioner callback. */
class Precond
{
private:
    void *_user_env;
    fabulous_g_rightprecond_t _user_rpc;

public:
    explicit Precond(void *user_env):
        _user_env{user_env},
        _user_rpc{nullptr}
    {
    }

    void set_precond(fabulous_g_rightprecond_t user_rpc)
    {
        _user_rpc = user_rpc;
    }

    operator bool() const { return _user_rpc != nullptr;  }

    template<class S>
    int64_t operator()(const Vector<S> &IN, Vector<S> &OUT) const
    {
        FABULOUS_ASSERT( _user_rpc != nullptr );
        // FABULOUS_DEBUG("PRECOND custom vector");
        return _user_rpc(   _user_env, IN.get_vect(), OUT.get_vect() );
    }

}; // end class Precond

} // end namespace api_g
} // end namespace fabulous

#endif // FABULOUS_G_API_PRECOND_HPP
