#ifndef FABULOUS_API_ENGINE_HPP
#define FABULOUS_API_ENGINE_HPP

#include <fstream>
#include <sstream>
#include <cstring>

namespace fabulous {

/*! \brief basic c-api implementation */
namespace api {
struct ApiEngineI;
template<class S> class ApiEngine;
}
}

#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"

#include "fabulous/orthogonalization/Orthogonalizer.hpp"

#include "fabulous/algo/BGMRes.hpp"
#include "fabulous/algo/BGCRO.hpp"
#include "fabulous/algo/BGCR.hpp"
#include "fabulous/algo/AlgoType.hpp"
#include "fabulous/algo/Equation.hpp"

#include "fabulous.h"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "Precond.hpp"
#include "Callback.hpp"
#include "Dot.hpp"

namespace fabulous {
namespace api {

/*!
 * \brief This interface is used to operate on the handle given by the user
 * without knowing which arithmetic is used.
 */
struct ApiEngineI
{
    virtual ~ApiEngineI(){}
    virtual void set_mvp(fabulous_mvp_t user_mvp) = 0;
    virtual void set_rightprecond(fabulous_rightprecond_t user_rpc) = 0;
    virtual void set_dot_product(fabulous_dot_t user_dot) = 0;
    virtual void set_callback(fabulous_callback_t callback) = 0;
    virtual void set_parameters(int max_mvp, int max_krylov_space_size,
                                void *tolerance, int nb_tol) = 0;
    virtual void set_advanced_parameters(int max_kept_direction,
                                         int real_residual,
                                         int logger_user_data_size,
                                         int quiet) = 0;

    virtual void set_ortho_process(fabulous_orthoscheme scheme,
                                   fabulous_orthotype type, int nb_iter) = 0;

    virtual int solve(int nrhs, void *B, int ldb, void *X, int ldx) = 0;
    virtual int solve_GCR(int nrhs, void *B, int ldb, void *X, int ldx) = 0;

    virtual int solve_GCRO(int nrhs, void *B, int ldb, void *X, int ldx,
                           void **Uk, int *ldu, int *ku, int k, void *target, int Uk_const) = 0;

    virtual int solve_IB(int nrhs, void *B, int ldb, void *X, int ldx) = 0;
    virtual int solve_DR(int nrhs, void *B, int ldb, void *X, int ldx,
                         int nb_eigen_pair, void *target) = 0;
    virtual int solve_IBDR(int nrhs, void *B, int ldb, void *X, int ldx,
                           int nb_eigen_pair, void *target) = 0;

    virtual int solve_QR(int nrhs, void *B, int ldb, void *X, int ldx) = 0;
    virtual int solve_QRIB(int nrhs, void *B, int ldb, void *X, int ldx) = 0;
    virtual int solve_QRDR(int nrhs, void *B, int ldb, void *X, int ldx,
                           int nb_eigen_pair, void *target) = 0;
    virtual int solve_QRIBDR(int nrhs, void *B, int ldb, void *X, int ldx,
                             int nb_eigen_pair, void *target) = 0;

    virtual const double *get_logs(int *size) = 0;
    virtual void print_log_filename(const char *filename, const char *log_id) const = 0;
    virtual void print_log_file(FILE *file, const char *log_id) const = 0;

    // For bgcro only
    virtual int get_Uk_const_last_solve(void) const = 0;

}; // end struct ApiEngineI

/**
 * \brief Main Engine of the Library.
 * Each method of the API except for the init function is
 * a member function of both this class and its parent's one.
 */
template<class S>
class ApiEngine : public ApiEngineI
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
    using P = primary_type;

private:
    int _dim;
    Matrix _matrix;
    Precond _precond;
    Callback _callback;
    Dot _dot;

    Parameters _params;
    std::vector<P> _tolerance;
    OrthoScheme _ortho_scheme;
    OrthoType _ortho_type;
    int _ortho_nb_iter;
    Logger _logger;
    bool _Uk_const_last_solve;

public:
    ApiEngine(int dim, void *user_env):
        _dim{dim},
        _matrix{user_env},
        _precond{user_env},
        _callback{user_env},
        _dot{user_env},
        _tolerance{},
        _ortho_scheme{OrthoScheme::MGS/*default value*/},
        _ortho_type{OrthoType::RUHE/*default value*/},
        _ortho_nb_iter{2},
        _Uk_const_last_solve{false}
    {
    }

    /*!
     * \brief This function set the user Dot product
     */
    void set_dot_product(fabulous_dot_t user_dot) override
    {
        _dot.set_dot_product(user_dot);
    }

    /*!
     * \brief This function allow to set a right pre conditionner
     */
    void set_rightprecond(fabulous_rightprecond_t user_rpc) override
    {
        _precond.set_rpc(user_rpc);
    }

    void set_mvp(fabulous_mvp_t user_mvp) override
    {
        _matrix.set_mvp(user_mvp);
    }

    void set_callback(fabulous_callback_t callback) override
    {
        _callback.set_callback(callback);
    }

    void set_parameters(int max_mvp, int max_space,
                        void *tolerance, int nb_tolerance) override
    {
        P *pTolerance = reinterpret_cast<P*>(tolerance);
        FABULOUS_ASSERT( tolerance != nullptr );
        FABULOUS_ASSERT( pTolerance != nullptr );

        _tolerance.clear();
        _tolerance.reserve(nb_tolerance);
        _tolerance.assign(pTolerance, pTolerance+nb_tolerance);
        _params.max_mvp = max_mvp;
        _params.max_space = max_space;
    }

    void set_advanced_parameters(int max_kept_direction,
                                 int real_residual,
                                 int logger_user_data_size,
                                 int quiet) override
    {
        _params.set_max_kept_direction(max_kept_direction);
        _params.set_real_residual(real_residual != 0);
        _params.set_logger_user_data_size(logger_user_data_size);
        _params.set_quiet(quiet != 0);
    }

    void set_ortho_process(fabulous_orthoscheme scheme,
                           fabulous_orthotype type, int nb_iter) override
    {
        switch (scheme) {
        case FABULOUS_MGS:  _ortho_scheme = OrthoScheme::MGS;  break;
        case FABULOUS_CGS:  _ortho_scheme = OrthoScheme::CGS;  break;
        case FABULOUS_IMGS: _ortho_scheme = OrthoScheme::IMGS; break;
        case FABULOUS_ICGS: _ortho_scheme = OrthoScheme::ICGS; break;
        default:
            FABULOUS_THROW(Parameter, "Parameter ortho_scheme is invalid");
            break;
        }
        switch (type) {
        case FABULOUS_RUHE:  _ortho_type = OrthoType::RUHE;  break;
        case FABULOUS_BLOCK: _ortho_type = OrthoType::BLOCK; break;
        default:
            FABULOUS_THROW(Parameter, "Parameter ortho_type is invalid");
            break;
        }
        if (nb_iter < 2 && (scheme == FABULOUS_IMGS || scheme == FABULOUS_ICGS)) {
            FABULOUS_THROW(Parameter, "the iter parameter must be greater than 2 or equal to 2");
        }
        _ortho_nb_iter = nb_iter;
    }

    int get_Uk_const_last_solve(void) const override { return _Uk_const_last_solve; }

private:
    template<class Algo>
    int call_solve(Algo algo, int nrhs, S *B_, int ldb, S *X_, int ldx)
    {
        Block<S> X_block{_dim, nrhs, ldx, X_};
        Block<S> B_block{_dim, nrhs, ldb, B_};
        Vector<S> X{_dot};
        Vector<S> B{_dot};
        X.set_block(X_block);
        B.set_block(B_block);

        auto eq = equation(_matrix, _precond, _callback, X, B, _tolerance);
        auto ortho = orthogonalization(_ortho_scheme, _ortho_type, _ortho_nb_iter);
        auto restart = classic_restart();

        bgmres::BGMRes<S> bgmres;
        const int mvp = bgmres.solve( eq, algo, _params, ortho, restart );
        _logger = bgmres.get_logger();
        return mvp;
    }

    template<class Algo>
    int call_solveDR(Algo algo, int nrhs, S *B_, int ldb, S *X_, int ldx,
                     int nb_eigen_pair, S target)
    {
        Block<S> X_block{_dim, nrhs, ldx, X_};
        Block<S> B_block{_dim, nrhs, ldb, B_};
        Vector<S> X{_dot};
        Vector<S> B{_dot};
        X.set_block(X_block);
        B.set_block(B_block);

        auto eq = equation(_matrix, _precond, _callback, X, B, _tolerance);
        auto restart = deflated_restart(nb_eigen_pair, target);
        auto ortho = orthogonalization(_ortho_scheme, _ortho_type, _ortho_nb_iter);

        bgmres::BGMRes<S> bgmres;
        const int mvp = bgmres.solve(eq, algo, _params, ortho, restart);
        _logger = bgmres.get_logger();
        return mvp;
    }

    template<class Algo>
    int call_solveGCR(Algo algo, int nrhs, S *B_, int ldb, S *X_, int ldx)
    {
        Block<S> X_block{_dim, nrhs, ldx, X_};
        Block<S> B_block{_dim, nrhs, ldb, B_};
        Vector<S> X{_dot};
        Vector<S> B{_dot};
        X.set_block(X_block);
        B.set_block(B_block);
        auto eq = equation(_matrix, _precond, _callback, X, B, _tolerance);
        auto ortho = orthogonalization(_ortho_scheme, _ortho_type, _ortho_nb_iter);

        bgcr::BGCR<S> bgcr;
        const int mvp = bgcr.solve(eq,algo, _params, ortho);
        _logger = bgcr.get_logger();
        return mvp;
    }

    template<class Algo>
    int call_solveGCRO(Algo algo, int nrhs, S *B_, int ldb, S *X_, int ldx,
                       S **Uk_, int *ldu, int *ku, int nb_eigen_pair, S target,
                       const bool Uk_const = false)
    {
        FABULOUS_ASSERT(Uk_ != nullptr);
        FABULOUS_ASSERT(ldu != nullptr);
        FABULOUS_ASSERT(ku != nullptr);
        Block<S> X_block{_dim, nrhs, ldx, X_};
        Block<S> B_block{_dim, nrhs, ldb, B_};
        Vector<S> X{_dot};
        Vector<S> B{_dot};
        X.set_block(X_block);
        B.set_block(B_block);

        Vector<S> Uk{_dot};
        bool Uk_start_empty = (*Uk_ == nullptr);

        if(Uk_start_empty){
            Block<S> Uk_block{};
            Uk.set_block(Uk_block);
            *ku = -1;
            *ldu = -1;
        } else {
            Block<S> Uk_block{_dim, *ku, *ldu, *Uk_};
            Uk.set_block(Uk_block);
        }

        const int ku_before = *ku;

        auto eq = equation(_matrix, _precond, _callback, X, B, _tolerance);
        auto restart = deflated_restart(nb_eigen_pair, target);
        auto ortho = orthogonalization(_ortho_scheme, _ortho_type, _ortho_nb_iter);

        bgcro::BGCRO<S> bgcro{Uk_const};
        const int mvp = bgcro.solve(eq, algo, _params, ortho, restart, Uk);
        _logger = bgcro.get_logger();

        _Uk_const_last_solve = bgcro.get_Uk_const();
        // If deflation space is declared constant and remained constant
        // do not work on it
        if(_Uk_const_last_solve) return mvp;

        *ku = Uk.get_nb_col();
        FABULOUS_ASSERT(*ku == nb_eigen_pair || *ku == nb_eigen_pair + 1);

        // Allocate Uk for the user if it was the first solve
        if(Uk.get_nb_col() > 0 && Uk_start_empty){
            *ldu = Uk.get_leading_dim();
            const int buff_size = sizeof(S) * (*ldu) * (nb_eigen_pair + 1); // Maximum size
            const int curr_size = sizeof(S) * (*ldu) * (*ku); // Current size
            *Uk_ = (S*) malloc(buff_size);
            memcpy(*Uk_, Uk.get_ptr(), curr_size);
        }
        if(*ku == nb_eigen_pair && *ku < ku_before){ // Set the last vector to 0
            std::fill(*Uk_ + (*ldu) * nb_eigen_pair, *Uk_ + (*ldu) * (nb_eigen_pair + 1), 0);
        }
        return mvp;
    }

public:
    int solve(int nrhs, void *B, int ldb, void *X, int ldx) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        return call_solve(bgmres::std(), nrhs, B_, ldb, X_, ldx);
    }

    int solve_GCR(int nrhs, void *B, int ldb, void *X, int ldx) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        return call_solveGCR(bgcr::std(), nrhs, B_, ldb, X_, ldx);
    }

    int solve_GCRO(int nrhs, void *B, int ldb, void *X, int ldx,
                   void **Uk, int *ldu, int *ku, int nb_eigen_pair, void *target, int Uk_const) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        S **Uk_ = reinterpret_cast<S**>(Uk);
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveGCRO(bgcro::ibdr(), nrhs, B_, ldb, X_, ldx, Uk_, ldu, ku, nb_eigen_pair, Target, (Uk_const != 0));
    }

    int solve_IB(int nrhs, void *B, int ldb, void *X, int ldx) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        return call_solve(bgmres::ib(), nrhs, B_, ldb, X_, ldx);
    }

    int solve_DR(int nrhs, void *B, int ldb, void *X, int ldx,
                 int nb_eigen_pair, void *target) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveDR(bgmres::std(), nrhs, B_, ldb, X_, ldx, nb_eigen_pair, Target);
    }

    int solve_IBDR(int nrhs, void *B, int ldb, void *X, int ldx,
                   int nb_eigen_pair, void *target) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveDR(bgmres::ibdr(), nrhs, B_, ldb, X_, ldx, nb_eigen_pair, Target);
    }

    int solve_QR(int nrhs, void *B, int ldb, void *X, int ldx) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        return call_solve(bgmres::qr(), nrhs, B_, ldb, X_, ldx);
    }

    int solve_QRIB(int nrhs, void *B, int ldb, void *X, int ldx) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        return call_solve(bgmres::qribdr(), nrhs, B_, ldb, X_, ldx);
    }

    int solve_QRDR(int nrhs, void *B, int ldb, void *X, int ldx,
                   int nb_eigen_pair, void *target) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveDR(bgmres::qrdr(), nrhs, B_, ldb, X_, ldx, nb_eigen_pair, Target);
    }

    int solve_QRIBDR(int nrhs, void *B, int ldb, void *X, int ldx,
                     int nb_eigen_pair, void *target) override
    {
        S *B_ = reinterpret_cast<S*>(B);
        S *X_ = reinterpret_cast<S*>(X);
        P Target;
        if (target != nullptr)
            Target = *reinterpret_cast<P*>(target);
        else
            Target = P{0.0};
        return call_solveDR(bgmres::qribdr(), nrhs, B_, ldb, X_, ldx, nb_eigen_pair, Target);
    }

    const double *get_logs(int *size) override
    {
        *size = _logger.get_nb_iterations();
        return _logger.write_down_array();
    }

    void print_log_file(FILE *file, const char *log_id) const override
    {
        if (file != NULL) {
            _logger.print(file, log_id);
        } else {
            FABULOUS_ERROR("invalid argument");
        }
    }

    void print_log_filename(const char *filename, const char *log_id) const override
    {
        std::ofstream file{filename, std::ios::out};
        if (file.is_open()) {
            _logger.print(std::string{log_id}, file);
        } else {
            std::stringstream ss;
            ss << "Cannot open '" << filename << "': '" << strerror(errno)<<"'";
            error(ss.str());
        }
    }

}; // end class ApiEngine

} // end namespace api
} // end namespace fabulous

#endif // FABULOUS_API_ENGINE_HPP
