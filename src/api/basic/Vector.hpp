#ifndef FABULOUS_API_VECTOR_HPP
#define FABULOUS_API_VECTOR_HPP

#include <cassert>
#include <vector>
#include <string>

namespace fabulous {
namespace api {

template<class S> class Vector;

}
}

#include "fabulous.h"
#include "fabulous/data/BlockOps.hpp"
#include "fabulous/data/Block.hpp"

#include "fabulous/kernel/QR.hpp"
#include "Dot.hpp"

namespace fabulous {
namespace api {

/*! \brief C++ wrapper for user vectors
 * \note the vector are stored in memory in a COLUMN_MAJOR (lapack-style) layout
 */
template<class S>
class Vector
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;

private:
    using P = primary_type;

    Dot _dot;
    Block<S> _data;

public:
    /*********************************************************/
    // SPECIFIC FOR IN CORE DISTRIBUTED (or not) API

    explicit Vector(): _dot{} {}

    explicit Vector(const Dot &dot):
        _dot{dot}
    {
    }

    const Block<S>& as_block() const
    {
        return _data;
    }

    const Block<S>& get_block() const
    {
        return _data;
    }
    Block<S>& get_block()
    {
        return _data;
    }

    void set_block(const Block<S> &block)
    {
        _data = block;
    }

    /*********************************************************/
    // FOLLOWING IS REQUIRED BY FABULOUS CORE:

    /* \brief basic vector creation constructor (for fabulous core)
     *   vector are always constructed from another given other vector
     *   so that they can retained its dimension and/or dot product properties
     *   in a transparent way from fabulous core point of view
     */
    Vector(const Vector &o, int nb_col, const std::string &name="vec"):
        _dot{o._dot},
        _data{o._data.get_nb_row(), nb_col, name}
    {
    }

    int get_nb_col() const { return _data.get_nb_col(); }
    // int get_nb_row() const { return _data.get_nb_row(); }

    void dot(const Vector &b, Block<S> &c) const
    {
        FABULOUS_ASSERT(  c.get_nb_row() == this->get_nb_col() );
        FABULOUS_ASSERT(  c.get_nb_col() == b.get_nb_col() );

        _dot( _data.get_nb_col(), b._data.get_nb_col(),
              _data.get_ptr(), _data.get_leading_dim(),
              b._data.get_ptr(), b._data.get_leading_dim(),
              c.get_ptr(), c.get_leading_dim()   );
    }

    void dot(const Vector &b, S &c) const
    {
        FABULOUS_ASSERT( this->get_nb_col() == 1 );
        FABULOUS_ASSERT( b.get_nb_col() == 1 );

        _dot( 1, 1,
              _data.get_ptr(), _data.get_leading_dim(),
              b._data.get_ptr(), b._data.get_leading_dim(),
              &c, 1   );
    }

    void axpy(const Vector &b, const Block<S> &s, S alpha=S{1.0})
    {
        _data.axpy(b._data, s, alpha);
    }

    void axpy(const Vector &b, S &s, S alpha=S{1.0})
    {
        _data.axpy(b._data, s, alpha);
    }

    void cwise_axpy(const Vector &b, S alpha=S{1.0})
    {
        _data += b._data*alpha;
    }

    std::vector<P> cwise_norm() const
    {
        const int N = _data.get_nb_col();
        std::vector<P> norms(N);
        for (int j = 0; j < N; ++j) {
            norms[j] = _data.norm(j, _dot);
            if (norms[j] == 0.0) {
                FABULOUS_WARNING("B["<<j<<"] (RHS column "<<j<<") have norm 0.0");
                norms[j] = 1.0;
            }
        }
        return norms;
    }

    void cwise_scale(const std::vector<P> &s)
    {
        _data.cwise_scale(s);
    }

    void copy(const Vector &o)
    {
        _data.copy(o._data);
    }

    Vector copy()
    {
        Vector cp{*this, this->get_nb_col()};
        cp._data = this->_data.copy();
        return cp;
    }

    void qr(Vector &q, Block<S> &r)
    {
        if ( q._data.get_ptr() != _data.get_ptr() ) {
            q.copy(*this);
        }
        ::fabulous::qr::InPlaceQRFactoMGS_User(q._data, r, q._dot);
    }

    void trsm(Vector &x, Block<S> &r)
    {
        if ( x._data.get_ptr() != _data.get_ptr() ) {
            x.copy(*this);
        }
        x._data.trsm(x._data, r);
    }

    void zero()
    {
        _data.zero();
    }

    Vector sub_vector(int offset, int width) const
    {
        Vector<S> v{_dot};
        v.set_block(_data.sub_vector(offset, width));
        return v;
    }

    int get_local_dim() const { return _data.get_nb_row();  }
    int get_leading_dim() const {  return _data.get_leading_dim(); }
    S *get_ptr() { return _data.get_ptr(); }
    S* get_vect(int i = 0) { return _data.get_vect(i); }
    const S *get_ptr() const { return _data.get_ptr(); }
    const S* get_vect(int i = 0) const { return _data.get_vect(i); }

    /*         */
    template<class U>
    void sscale(U val)
    {
        _data *= val;
    }

}; // end class Vector

// the following two function are needed for RUHE orthogonalization

template<class S>
auto snorm(const Vector<S> &v) -> typename Block<S>::primary_type
{
    S buf = S{0.0};
    Block<S> block{1, 1, 1, &buf};
    v.dot(v, block);
    return std::sqrt(fabulous::real(buf));
}

template<class S, class P>
void sscale(Vector<S> &v, P val)
{
    v.sscale(val);
}


} // end namespace api
} // end namespace fabulous

#endif // FABULOUS_API_VECTOR_HPP
