#ifndef FABULOUS_API_PRECOND_HPP
#define FABULOUS_API_PRECOND_HPP

#include <cassert>

namespace fabulous {
namespace api {
class Precond;
}
}

#include "fabulous.h"
#include "fabulous/utils/Error.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/QR.hpp"
#include "Vector.hpp"

namespace fabulous {
namespace api {

/*! \brief C++ wrapper over the user preconditioner callback. */
class Precond
{
private:
    void *_user_env;
    fabulous_rightprecond_t _user_rpc;

public:
    explicit Precond(void *user_env):
        _user_env{user_env},
        _user_rpc{nullptr}
    {
    }

    void set_rpc(fabulous_rightprecond_t user_rpc)
    {
        _user_rpc = user_rpc;
    }

    operator bool() const { return _user_rpc != nullptr;  }

    template<class S>
    int64_t operator()(const api::Vector<S> &IN, api::Vector<S> &OUT) const
    {
        FABULOUS_ASSERT ( _user_rpc != nullptr );
        return _user_rpc(
            _user_env, IN.get_nb_col(),
            IN.get_block().get_ptr(), IN.get_block().get_leading_dim(),
            OUT.get_block().get_ptr(), OUT.get_block().get_leading_dim()
        );
    }

}; // end class Precond

} // end namespace api
} // end namespace fabulous

#endif // FABULOUS_PRECOND_API_HPP
