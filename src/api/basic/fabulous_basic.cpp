#include "fabulous.h"

#include "fabulous/utils/Traits.hpp"

#include "ApiEngine.hpp"
#include "fabulous/utils/Utils.hpp"

using namespace fabulous;
using namespace fabulous::api;

namespace {
thread_local ::std::string s_last_errmsg="Success";
}

#if __GNUC__
# define FABULOUS_EXPORT __attribute__((visibility("default")))
// #elif _WIN32
// # define FABULOUS_EXPORT __declspec(dllexport)
#else
# define FABULOUS_EXPORT
#endif

FABULOUS_BEGIN_C_DECL

#define FABULOUS_API_ENGINE(handle_)            \
    reinterpret_cast<ApiEngineI*>(handle_)

#define FABULOUS_LOGGER(handle_)                \
    reinterpret_cast<Logger*>(handle_)

#define FABULOUS_HANDLE(engine_)                \
    reinterpret_cast<fabulous_handle>(engine_)

FABULOUS_EXPORT
fabulous_handle fabulous_create(
    fabulous_arithmetic ari, int dim, void *user_env)
{
    ApiEngineI *engine = nullptr;
    if (dim < 0) {
        s_last_errmsg = "Invalid dimension parameter";
        return NULL;
    }

    switch (ari) {
    case FABULOUS_REAL_FLOAT:
        engine = new ApiEngine<float>(dim, user_env);
        break;
    case FABULOUS_REAL_DOUBLE:
        engine = new ApiEngine<double>(dim, user_env);
        break;
    case FABULOUS_COMPLEX_FLOAT:
        engine = new ApiEngine<std::complex<float>>(dim, user_env);
        break;
    case FABULOUS_COMPLEX_DOUBLE:
        engine = new ApiEngine<std::complex<double>>(dim, user_env);
        break;
    default:
        s_last_errmsg = "Invalid arithmetic parameter";
        break;
    }
    return FABULOUS_HANDLE(engine);
}

#define TRY_CATCH_VOID(disp_, method_, ...)     \
    do {                                        \
        try {                                   \
            (disp_)->method_(__VA_ARGS__);      \
            return 0;                           \
        } catch (::fabulous::Error &err) {      \
            s_last_errmsg.assign(err.what());   \
            return - err.get_error_code();      \
        }                                       \
    } while(0)

#define TRY_CATCH_INT(disp_, method_, ...)              \
    do {                                                \
        try {                                           \
            return (disp_)->method_(__VA_ARGS__);       \
        } catch (::fabulous::Error &err) {              \
            s_last_errmsg.assign(err.what());           \
            return -err.get_error_code();               \
        }                                               \
    }while(0)

FABULOUS_EXPORT
const char *fabulous_last_error_str(void)
{
    static thread_local ::std::string s_errmsg;
    s_errmsg = s_last_errmsg;
    return s_errmsg.c_str();
}

FABULOUS_EXPORT
const char *fabulous_error_type_str(int errcode)
{
    static thread_local ::std::string s_errmsg;
    s_errmsg = ::fabulous::error_type(errcode);
    return s_errmsg.c_str();
}

FABULOUS_EXPORT
void fabulous_set_mvp(fabulous_mvp_t user_mvp, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    disp->set_mvp(user_mvp);
}

FABULOUS_EXPORT
void fabulous_set_rightprecond(fabulous_rightprecond_t user_rpc,
                              fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    disp->set_rightprecond(user_rpc);
}

FABULOUS_EXPORT
void fabulous_set_dot_product(fabulous_dot_t user_dot, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    disp->set_dot_product(user_dot);
}

FABULOUS_EXPORT
void fabulous_set_callback(fabulous_callback_t callback, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    disp->set_callback(callback);
}

FABULOUS_EXPORT
int fabulous_set_parameters(int max_mvp, int max_space_size,
                            void *tolerance, int nb_tol,
                            fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, set_parameters, max_mvp, max_space_size, tolerance, nb_tol);
}

FABULOUS_EXPORT
int fabulous_set_advanced_parameters(int max_kept_direction,
                                     int real_residual,
                                     int logger_user_data_size,
                                     int quiet,
                                     fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, set_advanced_parameters,
                   max_kept_direction, real_residual, logger_user_data_size, quiet);
}

FABULOUS_EXPORT
int fabulous_set_ortho_process(fabulous_orthoscheme scheme,
                               fabulous_orthotype type,
                               int nb_iter,fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, set_ortho_process, scheme, type, nb_iter);
}

FABULOUS_EXPORT
int fabulous_solve(int nrhs, void *B, int ldb, void *X, int ldx, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve, nrhs, B, ldb, X, ldx);
}

FABULOUS_EXPORT
int fabulous_solve_GCR(int nrhs, void *B, int ldb, void *X, int ldx, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_GCR, nrhs, B, ldb, X, ldx);
}

FABULOUS_EXPORT
int fabulous_get_Uk_const_last_solve(fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    return disp->get_Uk_const_last_solve();
}

FABULOUS_EXPORT
int fabulous_solve_GCRO(int nrhs, void *B, int ldb, void *X, int ldx,
                        void **Uk, int *ldu, int *ku, int nb_eigen_pair, void *target,
                        int Uk_const, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_GCRO, nrhs, B, ldb, X, ldx, Uk, ldu, ku, nb_eigen_pair, target, Uk_const);
}

FABULOUS_EXPORT
int fabulous_solve_IB(int nrhs, void *B, int ldb, void *X, int ldx,
                      fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_IB, nrhs, B, ldb, X, ldx);
}

FABULOUS_EXPORT
int fabulous_solve_DR(int nrhs, void *B, int ldb, void *X, int ldx,
                      int nb_eigen_pair, void *target,
                      fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_DR, nrhs, B, ldb, X, ldx, nb_eigen_pair, target);
}

FABULOUS_EXPORT
int fabulous_solve_IBDR(int nrhs, void *B, int ldb, void *X, int ldx,
                        int nb_eigen_pair, void *target,
                        fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_IBDR, nrhs, B, ldb, X, ldx, nb_eigen_pair, target);
}

FABULOUS_EXPORT
int fabulous_solve_QR(int nrhs, void *B, int ldb, void *X, int ldx,
                      fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_QR, nrhs, B, ldb, X, ldx);
}

FABULOUS_EXPORT
int fabulous_solve_QRIB(int nrhs, void *B, int ldb, void *X, int ldx,
                        fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_QRIB, nrhs, B, ldb, X, ldx);
}

FABULOUS_EXPORT
int fabulous_solve_QRDR(int nrhs, void *B, int ldb, void *X, int ldx,
                        int nb_eigen_pair, void *target,
                        fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_QRDR, nrhs, B, ldb, X, ldx, nb_eigen_pair, target);
}

FABULOUS_EXPORT
int fabulous_solve_QRIBDR(int nrhs, void *B, int ldb, void *X, int ldx,
                          int nb_eigen_pair, void *target,
                          fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_INT(disp, solve_QRIBDR, nrhs, B, ldb, X, ldx, nb_eigen_pair, target);
}

FABULOUS_EXPORT
const double *fabulous_get_logs(int *size, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    try {
        return disp->get_logs(size);
    } catch (::fabulous::Error &err) {
        s_last_errmsg = err.what();
        return NULL;
    }
}

FABULOUS_EXPORT
int fabulous_set_iteration_user_data(unsigned id, double fp, int64_t integ, fabulous_handle handle)
{
    try {
        Logger *logger = FABULOUS_LOGGER(handle);
        logger->set_iteration_user_data(id, fp, integ);
        return 0;
    } catch (::fabulous::Error &err) {
        s_last_errmsg = err.what();
        return -err.get_error_code();
    }
}

FABULOUS_EXPORT
int fabulous_print_log_file(void *file, const char *log_id, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, print_log_file, (FILE*)file, log_id);
}

FABULOUS_EXPORT
int fabulous_print_log_filename(const char *filename, const char *log_id, fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    TRY_CATCH_VOID(disp, print_log_filename, filename, log_id);
}

FABULOUS_EXPORT
void fabulous_destroy(fabulous_handle handle)
{
    ApiEngineI *disp = FABULOUS_API_ENGINE(handle);
    delete disp;
}

FABULOUS_EXPORT
void fabulous_set_output_colored(int enable_color)
{
    ::fabulous::set_output_colored(enable_color != 0);
}

FABULOUS_END_C_DECL
