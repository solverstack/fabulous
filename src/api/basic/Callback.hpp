#ifndef FABULOUS_API_CALLBACK_HPP
#define FABULOUS_API_CALLBACK_HPP

#include <cassert>

namespace fabulous {
namespace api {
class Callback;
}
}

#include "fabulous.h"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/QR.hpp"
#include "Vector.hpp"

namespace fabulous {
namespace api {

/*! \brief C++ wrapper over the user generic callback. */
class Callback
{
private:
    void *_user_env;
    fabulous_callback_t _callback;

public:
    explicit Callback(void *user_env):
        _user_env{user_env},
        _callback{nullptr}
    {
    }

    void set_callback(fabulous_callback_t callback)
    {
        _callback = callback;
    }

    template<class S>
    void operator()(int iter, Logger &logger, const api::Vector<S> &X, const api::Vector<S> &R) const
    {
        if ( _callback != nullptr ) {
            FABULOUS_ASSERT( X.get_nb_col() == R.get_nb_col() );
            const int NRHS = X.get_nb_col();
            _callback(_user_env, iter, NRHS,
                      X.get_block().get_ptr(), X.get_block().get_leading_dim(),
                      R.get_block().get_ptr(), R.get_block().get_leading_dim(),
                      (fabulous_handle) &logger );
        }
    }

}; // end class Callback

} // end namespace api
} // end namespace fabulous

#endif // FABULOUS_API_CALLBACK_HPP
