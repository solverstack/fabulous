#ifndef FABULOUS_API_MATRIX_HPP
#define FABULOUS_API_MATRIX_HPP

#include <cassert>

namespace fabulous {
namespace api {
class Matrix;
}
}

#include "fabulous.h"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/QR.hpp"
#include "Vector.hpp"

namespace fabulous {
namespace api {

/*! \brief C++ wrapper over the user matrix-vector product callback. */
class Matrix
{
private:
    void *_user_env;
    fabulous_mvp_t _user_mvp;

    // S at(int, int) { FABULOUS_THROW(Unsupported, "should not be reached"); return S{0.0}; }

public:
    explicit Matrix(void *user_env):
        _user_env{user_env},
        _user_mvp{nullptr}
    {
    }

    void set_mvp(fabulous_mvp_t user_mvp)
    {
        _user_mvp = user_mvp;
    }

    template<class S>
    int64_t operator()( const api::Vector<S> &IN, api::Vector<S> &OUT,
                        S alpha = S{1.0}, S beta = S{0.0}) const
    {
        if ( _user_mvp == nullptr ) {
            FABULOUS_ERROR("User matrix vector product is not set!");
            FABULOUS_ERROR("Have you called fabulous_set_mvp() ?!");
            FABULOUS_THROW(NotImplemented, "Invalid user matrix product callback");
        }

        return _user_mvp(
            _user_env, IN.get_nb_col(),
            &alpha, IN.get_block().get_ptr(), IN.get_block().get_leading_dim(),
            &beta, OUT.get_block().get_ptr(), OUT.get_block().get_leading_dim()
        );
    }

}; // end class Matrix

} // end namespace api
} // end namespace fabulous

#endif // FABULOUS_MATRIX_API_HPP
