#ifndef FABULOUS_API_DOT_HPP
#define FABULOUS_API_DOT_HPP

#include <cassert>

namespace fabulous {
class Dot;
}

#include "fabulous.h"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/QR.hpp"

namespace fabulous {
namespace api {

/*! \brief C++ wrapper over the user dot product callback. */
class Dot
{
private:
    void *_user_env;
    fabulous_dot_t _user_dot;

public:
    explicit Dot():
        _user_env{nullptr},
        _user_dot{nullptr} {}


    explicit Dot(void *user_env):
        _user_env{user_env},
        _user_dot{nullptr}
    {
    }

    void set_dot_product(fabulous_dot_t user_dot)
    {
        _user_dot = user_dot;
    }

    template<class S>
    int64_t operator()(const int M, const int N,
                       const S *a, int lda,
                       const S *b, int ldb,
                       /**/  S *c, int ldc) const
    {
        if ( _user_dot == nullptr ) {
            FABULOUS_ERROR("User dot vector product is not set!");
            FABULOUS_ERROR("Have you called fabulous_set_dot_product() ?!");
            FABULOUS_THROW(NotImplemented, "Invalid user dot product callback");
        }
        return _user_dot( _user_env, M, N, a, lda, b, ldb,  c, ldc );
    }

}; // end class Dot

} // end namespace api
} // end namespace fabulous

#endif // FABULOUS_API_DOT_HPP
