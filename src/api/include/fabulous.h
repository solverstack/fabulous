#ifndef FABULOUS_H
#define FABULOUS_H

#undef FABULOUS_BEGIN_C_DECL
#undef FABULOUS_END_C_DECL
#ifdef __cplusplus
# define FABULOUS_BEGIN_C_DECL   extern "C" {
# define FABULOUS_END_C_DECL     }
#else
# define FABULOUS_BEGIN_C_DECL
# define FABULOUS_END_C_DECL
#endif

#if __GNUC__
# define FABULOUS_EXPORT __attribute__((visibility("default")))
// #elif _WIN32
// # define FABULOUS_EXPORT __declspec(dllexport)
#else
# define FABULOUS_EXPORT
#endif

#define FABULOUS_H_IN_INCLUDE_ 1
#include <fabulous/fabulous_basic.h>
#include <fabulous/fabulous_generic_vectors.h>
#undef FABULOUS_H_IN_INCLUDE_

#endif // FABULOUS_H
