/* authors:
 * Cyrille Piacibello: cyrille.piacibello@inria.fr
 * Luc Giraud: luc.giraud@inria.fr
 * Thomas Mijieux: thomas.mijieux@inria.fr
 */

#ifndef FABULOUS_BASIC_H
#define FABULOUS_BASIC_H

#ifndef FABULOUS_H_IN_INCLUDE_
#error "Never include this header directly use "fabulous.h" instead"
#endif

/**
 * \file fabulous.h Header for using the fabulous library.
 *
 * \brief Block Krylov iterative solver API.
 *
 * This Library is implementing Block Krylov General Minimum Residual iterative solver
 * (denoted as BGMRES) which means that this library can solve linear equations
 * with multiple right hand sides.
 *
 * This library also implements several algorithm variants that may improve the
 * performance of the library (Incremental QR factorization, IB-BGMRES,
 * BGMRES-DR, and IB-BGMRES-DR.)
 *
 * BCG: Hao Ji Yaohang Li
 *      A breakdown-free block conjugate gradient method. \n
 *      Springer Science+Business Media Dordrecht 2016
 *
 * BF-BCG: Hao Ji Yaohang Li
 *         A breakdown-free block conjugate gradient method. \n
 *         Springer Science+Business Media Dordrecht 2016
 *
 * IB-BGMRES:  M. Robbé and M. Sadkane. \n
 *               Exact and inexact breakdowns in the block GMRES method. \n
 *               Linear Algebra and its Applications, 419:265–285, 2006
 *
 * BGMRES-DR: R. B. Morgan. \n
 *              Restarted block GMRES with deflation of eigenvalues. \n
 *              Applied Numerical Mathematics, 54(2):222–236, 2005.
 *
 * IB-BGMRES-DR: E. Agullo, L. Giraud, Y.-F. Jing \n
 *                 Block GMRES method with inexact breakdowns and deflated restarting \n
 *                 SIAM Journal on %Matrix Analysis and Applications
 *                 35, 4, November 2014, p. 1625–1651
 *
 * IB-BGCRO-DR: Luc Giraud, Yan-Fei Jing, Yanfei Xiang.
 *              A block minimum residual norm subspace solver for sequences of multiple left and right-hand side linear systems.
 *              [Research Report] RR-9393, Inria Bordeaux Sud-Ouest. 2021, pp.60. hal-03146213v2
 *
 * The %Matrix x Vector product is not handled by the library and must be implemented
 * as a callback by the user. \n
 * This allow the user to implement a algorithm adapted to his/her problem. \n
 * (whether the matrix is sparse/dense or have a specific structure)
 *
 * The user must also implements a DotProduct callback. This allow the user to
 * use the library in a distributed fashion. \n
 * (The input solution and right hand sides may be distributed with block lines style)
 * If the user intend to distribute its vector over several nodes, \n
 * the the callbacks must be adapted to perform the necessary
 * communications and/or reductions.
 *
 */

#include <stdint.h>

FABULOUS_BEGIN_C_DECL

/**
 * \brief opaque handle for the fabulous library
 */
typedef void *fabulous_handle;

/**
 * \brief The different arithmetics available
 */
enum fabulous_arithmetic {
    FABULOUS_REAL_FLOAT     = 0, /*!< correspond to 'float' type */
    FABULOUS_REAL_DOUBLE    = 1, /*!< correspond to 'double' type */
    FABULOUS_COMPLEX_FLOAT  = 2, /*!< correspond to 'float complex' type */
    FABULOUS_COMPLEX_DOUBLE = 3, /*!< correspond to 'double complex' type */
};

/**
 * \brief  The different available orthogonalization schemas
 */
enum fabulous_orthoscheme {
    FABULOUS_MGS  = 0, /*!< Modified Gram-Schmidt */
    FABULOUS_IMGS = 1, /*!< Iterated Modified Gram-Schmidt */
    FABULOUS_CGS  = 2, /*!< Classical Gram-Schmidt */
    FABULOUS_ICGS = 3, /*!< Iteratied Classical Gram-Schmidt */
};

/**
 * \brief  The different available orthogonalization types
 */
enum fabulous_orthotype {
    FABULOUS_RUHE  = 0, /*!< RUHE  variant (vector-by-vector) */
    FABULOUS_BLOCK = 1, /*!< BLOCK variant (block-by-block) */
};

typedef enum fabulous_arithmetic fabulous_arithmetic;
typedef enum fabulous_orthoscheme fabulous_orthoscheme;
typedef enum fabulous_orthotype fabulous_orthotype;

/**
 * \brief User defined function to compute
 * product between user's %Matrix (A) and %Matrix given in argument (X)
 *
 * Operation to be performed:  B := A * X
 *
 * \param user_data same pointer that was passed to fabulous_create()
 * \param N number of vector in the matrices X and B.

 * \param[in] X matrix of size 'dim' x N to be read.
 * Values are stored in Column Major layout (as in Fortran.)
 * \param ldx leading dimension of X
 *
 * \param[out] B matrix of size 'dim' x N to be written by the user.
 * Values are stored in Column Major layout.
 * \param ldb leading dimension of B
 *
 * \return numbers of flops performed by the operation
 *        for (optionnal) perfomance analysis
 *
 * \note dim is the parameter that was passed to fabulous_create()
 */
typedef int64_t (*fabulous_mvp_t)(void *user_data, int N,
                                  const void *alpha, const void *X, int ldx,
                                  const void *beta, void *B, int ldb);

/**
 * \brief User defined function to compute product between
 * the user's right-preconditioner M and a matrix X
 *
 * Operation to be performed: B := M*X
 *
 * \param user_data same pointer that was passed to fabulous_create()
 *
 * \param N number of vector inside the matrices B and X
 *
 * \param[in] X %Matrix of size 'dim' x N  to be read.
 * Values are stored in Column Major layout (as in Fortran)
 * \param ldx leading dimension of X
 *
 * \param[out] B matrix of size 'dim' x N to be written by the user.
 * Values are stored in Column Major layout.
 * \param ldb leading dimension of B
 * \return numbers of flops performed by the operation
 *        for (optionnal) perfomance analysis
 *
 * \note dim is the parameter that was passed to fabulous_create()
 */
typedef int64_t (*fabulous_rightprecond_t)(void *user_data, int N,
                                           const void *X, int ldx,
                                           void *B, int ldb);

/**
 * \brief Function to perform multiple dot products between two sets of vectors
 *          C = A^{H} * B
 *
 * \param user_data same pointer that was passed to fabulous_create()
 *
 * \param M number of vectors in A
 * \param N number of vectors  in B
 * \param[in] A matrix of size ( 'dim' x M ), to be read by the user
 * \param lda leading dimension of A
 *
 * \param[in] B matrix of size ( 'dim' x N ), to be read by the user
 * \param ldb leading dimension of B
 *
 * \param[out] C matrix of size ( M x N ) to be filled with the result by the user
 * \param ldc leading dimension of C
 * \return numbers of flops performed by the operation
 *        for (optionnal) perfomance analysis
 *
 * \note dim is the parameter that was passed to fabulous_create()
 */
typedef int64_t (*fabulous_dot_t)(void *user_data, int M, int N,
                                  const void *A, int lda,
                                  const void *B, int ldb,
                                  void *C, int ldc);

/**
 * \brief Function to be called once at the end of each iteration
 *
 * \param user_data same pointer that was passed to fabulous_create()
 *
 * \param NRHS number of vectors in X and R
 *      In some case it may be 0 (For instance, in GMRES, the actual residual
 *      and solution are not computed on each iteration by default,
 *      because there no short term recurrence to compute them.
 *      This behaviour can be changed, see fabulous_set_advanced_parameters()
 *      (parameter 'real_residual'))
 *
 * \param[in] X either NULL or a matrix of size ( 'dim' x NRHS ) corresponding to solution vector
 * \param ldx leading dimension of X
 *
 * \param[in] R either NULL or a matrix of size ( 'dim' x NRHS ) corresponding to residual vector
 * \param ldr leading dimension of R
 *
 * \param handle can be used by the user as a parameter to fabulous_set_iteration_user_data()
 * \note this handle may be different from the solver handle
 *     but the user may not call fabulous_destroy() on this handle
 *     because it is internal to the library
 *
 * \note dim is the parameter that was passed to fabulous_create()
 */
typedef void (*fabulous_callback_t)(void *user_data,
                                    int iter, int NRHS,
                                    const void *X, int ldx,
                                    const void *R, int ldr,
                                    fabulous_handle handle);

/**
 * \brief Create a library handle.
 *
 * \param ari the arithmetic used (float/double) (real/complex)
 * \param dim Local dimension of the problem to be solved.
 *   This parameter is very important as it will be the assumed number of lines
 *   for all input matrices in the user callbacks. \n
 *   It is possible that 'dim' does not match the full size of the matrix,
 *   and the library can work in a distributed fashion. \n
 *   To achieve the distributed version of the library the user must implements
 *   the callback to perform the necessary communications.
 *
 * \param user_data a pointer that will be provided to each callback.
 *  The user may use this pointer to store extra informations needed by
 *  the callbacks (see examples.)
 *
 * \return a fabulous_handle opaque object, it is needed to make calls
 * to other functions of the library or NULL if an error occured
 */
fabulous_handle fabulous_create(fabulous_arithmetic ari, int dim, void *user_data);

/**
 * \brief free internal ressources held by the opaque fabulous_handle.
 * After calling this function on a handle, no other library function may be called.
 *
 * \param handle the fabulous opaque handle to be destroyed
 */
void fabulous_destroy(fabulous_handle handle);

/**
 * \brief Set the callback to compute product between user's %Matrix and a block of vector.
 *
 * \param user_mvp matrix vector product callback (function pointer),
 *        refer to documentation to get the prototype
 *
 * \param handle the fabulous opaque handle
 *
 * \note MVP stand for %Matrix Vector Product
 */
void fabulous_set_mvp(fabulous_mvp_t user_mvp, fabulous_handle handle);

/**
 * \brief Set the callback to compute the product between
 * user's right-preconditionner and a block of vector
 *
 * \param user_rpc callback(function pointer), refer to documentation to get the prototype
 *        To disable the use of a right preconditionner,
 *        the user may pass NULL as the first argument instead of a valid function pointer.
 *
 * \param handle the fabulous library opaque handle
 *
 * \note If this function is never called, it is assumed no preconditioner is used.
 *
 */
void fabulous_set_rightprecond(fabulous_rightprecond_t user_rpc, fabulous_handle handle);

/**
 * \brief set the solver's parameters
 *
 * \param max_mvp Maximum number of matrix vector product to be done

 * \param max_krylov_space_size Maximum size allowed for Krylov Search Space.

 * \param[in] tolerance an array of scalar containing the tolerance threshold
 *        used to consider convergence is reached (normalized backward error)
 *   - the type of value in the array must match the correct type to represent
 *     the module of the Arithetic used:
 *     for instance if the handle was created with FABULOUS_REAL_DOUBLE
 *     or FABULOUS_COMPLEX_DOUBLE, then tolerance must be an array of double \n
 *   - otherwise\n
 *         tolerance must be an array of float
 *
 * \param nb_tolerance size of the tolerance array \n
 *  - if nb_tolerance == 1 \n
 *        then tolerance[0] will be used as the threshold for all vectors. \n
 *  - otherwise \n
 *        nb_tolerance must match the parameter nrhs (number of right hand sides)
 *        passed in the fabulous_solve_XX() family of function \n
 *  - if nb_tolerance is neither equal to 1 nor nrhs \n
 *        this is an error and the behaviour is undefined.
 *
 * \param handle the fabulous library opaque handle
 * \return 0 if the call was successful or a negative value to indicate an error
 */
int fabulous_set_parameters(int max_mvp, int max_krylov_space_size,
                            void *tolerance, int nb_tolerance,
                            fabulous_handle handle);

/**
 * \brief set the solver's advanced parameters
 *
 * \param max_kept_direction maximum number of kept direction per iteration
 *    This parameter is meaningful only for IB variants,
 *    otherwise it is not taken into account. You can set this parameter to -1 if
 *    you want it to match the number of right hand sides
 *
 * \param real_residual In GMRES algorithms, compute X and R at each iteration
 *   such that the user can access them in CallBack
 *
 * \param logger_user_data_size number of slots for user data when calling
 *              fabulous_set_iteration_user_data()
 *
 * \param quiet boolean value to limit fabulous verbosity
 *
 * \param handle the fabulous library opaque handle
 * \return 0 if the call was successful or a negative value to indicate an error
 *
 * \warning Using real_residual parameter set to true may slow down BGMRES a lot!
 *          it is designed to be used for debugging purposes
 */
int fabulous_set_advanced_parameters(int max_kept_direction,
                                     int real_residual,
                                     int logger_user_data_size,
                                     int quiet,
                                     fabulous_handle handle);

/**
 * \brief Set the orthogonalization schema
 *
 * \param scheme one of the following:
 \verbatim
 FABULOUS_MGS  -> Modified Gram Schmidt (default value)
 FABULOUS_CGS  -> Classical Gram Schmidt
 FABULOUS_IMGS -> Iterated Modified Gram Schmidt
 FABULOUS_ICGS -> Iterated Classical Gram Schmidt
 \endverbatim
 *
 * \param type  one of the following:
 \verbatim
 FABULOUS_RUHE  -> RUHE variant (vector-by-vector)
 FABULOUS_BLOCK -> BLOCK variant (block-by-block)
 \endverbatim
 *
 * \param nb_iter number of iteration for Iterated Schemas (IMGS and ICGS); Must be positive integer >= 2
 * \param handle the fabulous library opaque handle
 *
 * The choice of the schema will have influence on the numerical quality
 * of the orthogonalization coefficients.
 * A better orthogonalization schema may improve the convergence speed in terms
 * of number of iteration but it may also slow the speed of the algorithm if you
 * use the library in a distributed fashion since it requires a greater amount of
 * synchronization.
 *
 \verbatim
 less synchronizations -------------> more synchronizations
 CLASSICAL Gram-Schidt -------------> MODIFIED Gram-Schmidt
 \endverbatim
 *
 * The iterated version of these algorithms denote the facts than a schema
 * may be repeated several times (the default is two time) to improve the numerical
 * quality. Of the course the number of synchronization is also multiplied by the amount
 * of times the schema is repeated.
 *
 * \note if no call to this function is made, FABULOUS_MGS will be used
 * as the default value for the orthoproc parameter
 * \return 0 if the call was successful or a negative value to indicate an error
 */
int fabulous_set_ortho_process(fabulous_orthoscheme scheme,
                               fabulous_orthotype type,
                               int nb_iter, fabulous_handle handle);

/**
 * \brief Set the user dot product.
 *
 * It is needed for computation of norms and orthogonalization coefficents
 * in the orthogonalization schemas.
 *
 * \param user_dot user dot product callback (function pointer)
 * \param handle the fabulous opaque handle
 */
void fabulous_set_dot_product(fabulous_dot_t user_dot, fabulous_handle handle);

/**
 * \brief Set the user callback
 *
 * Set the user callback to be called once in each iteration.
 *
 * \note Setting a user callback is NOT mandatory for the fabulous library to work.
 *    If this function is never called, this is not a problem.
 *
 * \param callback user callback (function pointer)
 * \param handle the fabulous opaque handle
 */
void fabulous_set_callback(fabulous_callback_t callback, fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X as initial solution guess using
 * Block General Minimum Residual (BGMRES) algorithm.
 *
 * \param nrhs number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout)
 * \param ldx leading dimension of X
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve(int nrhs, void *B, int ldb, void *X, int ldx, fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X as initial solution guess using
 * Block General Conjugate Residual (BGCR) algorithm.
 *
 * \param nrhs number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout)
 * \param ldx leading dimension of X
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_GCR(int nrhs, void *B, int ldb, void *X, int ldx, fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X as initial solution guess using
 * Block General Conjugate Residual O... (BGCRO) algorithm, starting with an empty
 * deflation space Uk.
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param[in,out] Uk deflation space (Column major layout)
 * \param[in,out] ldu deflation space leading dimension
 * \param[in,out] ku deflation space true size
 * \param k number of eigen pair used in Deflated Restarting (size of Uk)
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_g_create()
 * \param Uk_const Indicate if the deflation space considered a good enough approximation
 *  and thus does not need to be updated (if set to true, the deflation space will not be updated)
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_GCRO(int nrhs, void *B, int ldb, void *X, int ldx,
                        void **Uk, int *ldu, int *ku, int k,
                        void *target, int Uk_const, fabulous_handle handle);

/**
 * \brief Get the constant property of the latest deflation space used for GCRO.
 *
 * \return true if the latest deflation space used for GCRO was constant, false otherwise
 */
int fabulous_get_Uk_const_last_solve(fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with detection of Inexact Breakdowns
 * (IB-BGMRES)
 *
 * \param nrhs number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout).
 * \param ldx leading dimension of X
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_IB(int nrhs, void *B, int ldb, void *X, int ldx,
                      fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Deflated restarting
 * (BGMRES-DR)
 *
 * \param nrhs (p) number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout)
 * \param ldx leading dimension of X
 * \param k number of eigen pair used in Deflated Restarting
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_create()
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_DR(int nrhs, void *B, int ldb, void *X, int ldx,
                      int k, void *target,
                      fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Inexact Breakdown and Deflated restarting
 * (IB-BGMRES-DR)
 *
 * \param nrhs (p) number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout)
 * \param ldx leading dimension of X
 * \param k number of eigen pair used in Deflated Restarting
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_create()
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_IBDR(int nrhs, void *B, int ldb, void *X, int ldx,
                        int k, void *target,
                        fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm (BGMRES)
 * with Incremental QR factorization.
 *
 * \param nrhs number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout)
 * \param ldx leading dimension of X
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_QR(int nrhs, void *B, int ldb, void *X, int ldx,
                      fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Inexact Breakdown
 * and Incremental QR factorization
 * (QR-IB-BGMRES)
 *
 * \param nrhs (p) number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout)
 * \param ldx leading dimension of X
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_QRIB(int nrhs, void *B, int ldb, void *X, int ldx,
                        fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Deflated restarting
 * and Incremental QR factorization
 * (BGMRES-DR)
 *
 * \param nrhs (p) number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout)
 * \param ldx leading dimension of X
 * \param k number of eigen pair used in Deflated Restarting
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_create()
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_QRDR(int nrhs, void *B, int ldb, void *X, int ldx,
                        int k, void *target,
                        fabulous_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Deflated restarting,
 * Inexact Breakdown and Incremental QR factorization
 * (QR-IB-BGMRES-DR)
 *
 * \param nrhs (p) number of right hand side
 * \param[in] B Right Hand Side (Column major layout)
 * \param ldb leading dimension of B
 * \param[in,out] X initial solution guess (Column major layout)
 * \param ldx leading dimension of X
 * \param k number of eigen pair used in Deflated Restarting
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_create()
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_solve_QRIBDR(int nrhs, void *B, int ldb, void *X, int ldx,
                          int k, void *target,
                          fabulous_handle handle);

/**
 * \brief Get the convergence history
 *
 * This function return the convergence history (in terms of backward error)
 * as an array containing 3 double for each iteration step:
 *
 *      {MIN over the vectors; MAX over the vectors;  TIMESTAMP}.
 *
 * \param[out] size filled with the size of the returned array
 * \param handle the fabulous opaque handle
 *
 * \return the array containing the convergence history as described above.
 *
 * \warning The returned array is valid until fabulous_destroy()
 * is called on the handle or another solve call is performed.
 * If you want to access the data after calling fabulous_destroy() or a solve function,
 * you need to copy the data
 */
const double* fabulous_get_logs(int *size, fabulous_handle handle);



/**
 * \brief Allow the user to add its own data into fabulous internal logger
 *
 *  This function can be called only inside the user callback of type fabulous_callback_t
 *  which was set with the fabulous_set_user_callback() function
 *
 * \param id Number identifying the user data.
 *    Must be an integer greater or equal than 0 and less than the maximum
 *    number of user data slots availables (The default is 4).
 *    This maximum number of slots can be
 *    changed with the fabulous_set_advanced_parameters() function.
 * \param fp floating point user data
 * \param integ integer user data
 * \param handle the last parameter of the callback of type fabulous_callback_t
 *
 * \return 0 on success or a negative value to indicate an error
 */
int fabulous_set_iteration_user_data(unsigned id, double fp, int64_t integ, fabulous_handle handle);

/**
 * \brief Print the convergence history with advanced details
 *
 * \param file must be a valid pointer of type (FILE*) returned by fopen or similar
 * \param log_id a string to identify the log
 * \param handle the fabulous opaque handle
 *
 * \return 0 on success or a negative value to indicate an error
 */
int fabulous_print_log_file(void *file, const char *log_id, fabulous_handle handle);

/**
 * \brief Print the convergence history with advanced details
 *
 * \param filename the output filename
 * \param log_id a string to identify the log
 * \param handle the fabulous opaque handle
 *
 * \return 0 on success or a negative value to indicate an error
 */
int fabulous_print_log_filename(const char *filename, const char *log_id, fabulous_handle handle);

/**
 * \brief get the error description as a string
 *
 * \return the string as a c-style NUL terminated string
 *  The user must not free the value returned by this function as it is stored statically.
 *  The returned string must not be modified by the user but it may be modified by
 *  subsequent call to fabulous_last_error_str()
 */
const char *fabulous_last_error_str(void);

/**
 * \brief get the error number type description as a string
 *
 * \return the string as a c-style NUL terminated string
 *  The user must not free the value returned by this function as it is stored statically.
 *  The returned string must not be modified by the user but it may be modified by
 *  subsequent call to fabulous_error_type_str()
 */
const char *fabulous_error_type_str(int errcode);

/**
 * \brief enable or disable color in fabulous output
 * \param enable_color boolean value to indicate whether colors should be enabled
 *
 * \note the effect is global and does not depend on any handle
 */
void fabulous_set_output_colored(int enable_color);

FABULOUS_END_C_DECL

#endif // FABULOUS_BASIC_H
