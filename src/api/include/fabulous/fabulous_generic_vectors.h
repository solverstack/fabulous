/* authors:
 * Cyrille Piacibello: cyrille.piacibello@inria.fr
 * Luc Giraud: luc.giraud@inria.fr
 * Thomas Mijieux: thomas.mijieux@inria.fr
 */

#ifndef FABULOUS_GENERIC_VECTORS_H
#define FABULOUS_GENERIC_VECTORS_H

#ifndef FABULOUS_H_IN_INCLUDE_
#error "Never include this header directly use "fabulous.h" instead"
#endif

/**
 * \file fabulous.h Header for using the fabulous library.
 *
 * \brief Block Krylov iterative solver API.
 *
 * This Library is implementing Block Krylov General Minimum Residual iterative solver
 * (denoted as BGMRES) which means that this library can solve linear equations
 * with multiple right hand sides.
 *
 * This library also implements several algorithm variants that may improve the
 * performance of the library (Incremental QR factorization, IB-BGMRES,
 * BGMRES-DR, and IB-BGMRES-DR.)
 *
 * BCG: Hao Ji Yaohang Li
 *      A breakdown-free block conjugate gradient method. \n
 *      Springer Science+Business Media Dordrecht 2016
 *
 * BF-BCG: Hao Ji Yaohang Li
 *         A breakdown-free block conjugate gradient method. \n
 *         Springer Science+Business Media Dordrecht 2016
 *
 * IB-BGMRES:  M. Robbé and M. Sadkane. \n
 *               Exact and inexact breakdowns in the block GMRES method. \n
 *               Linear Algebra and its Applications, 419:265–285, 2006
 *
 * BGMRES-DR: R. B. Morgan. \n
 *              Restarted block GMRES with deflation of eigenvalues. \n
 *              Applied Numerical Mathematics, 54(2):222–236, 2005.
 *
 * IB-BGMRES-DR: E. Agullo, L. Giraud, Y.-F. Jing \n
 *                 Block GMRES method with inexact breakdowns and deflated restarting \n
 *                 SIAM Journal on %Matrix Analysis and Applications
 *                 35, 4, November 2014, p. 1625–1651
 *
 * IB-BGCRO-DR: Luc Giraud, Yan-Fei Jing, Yanfei Xiang.
 *              A block minimum residual norm subspace solver for sequences of multiple left and right-hand side linear systems.
 *              [Research Report] RR-9393, Inria Bordeaux Sud-Ouest. 2021, pp.60. hal-03146213v2
 *
 * The %Matrix x Vector product is not handled by the library and must be implemented
 * as a callback by the user. \n
 * This allow the user to implement a algorithm adapted to his/her problem. \n
 * (whether the matrix is sparse/dense or have a specific structure)
 *
 * The user must also implements a DotProduct callback. This allow the user to
 * use the library in a distributed fashion. \n
 * (The input solution and right hand sides may be distributed with block lines style)
 * If the user intend to distribute its vector over several nodes, \n
 * the the callbacks must be adapted to perform the necessary
 * communications and/or reductions.
 *
 */

#include <stdint.h>

FABULOUS_BEGIN_C_DECL

/**
 * \brief opaque handle for the fabulous library
 */
typedef void *fabulous_g_handle;

/**
 * \brief The different arithmetics available
 */
enum fabulous_g_arithmetic {
    FABULOUS_G_REAL_FLOAT     = 0, /*!< correspond to 'float' type */
    FABULOUS_G_REAL_DOUBLE    = 1, /*!< correspond to 'double' type */
    FABULOUS_G_COMPLEX_FLOAT  = 2, /*!< correspond to 'float complex' type */
    FABULOUS_G_COMPLEX_DOUBLE = 3, /*!< correspond to 'double complex' type */
};

/**
 * \brief  The different available orthogonalization schemas
 */
enum fabulous_g_orthoscheme {
    FABULOUS_G_MGS  = 0, /*!< Modified Gram-Schmidt */
    FABULOUS_G_IMGS = 1, /*!< Iterated Modified Gram-Schmidt */
    FABULOUS_G_CGS  = 2, /*!< Classical Gram-Schmidt */
    FABULOUS_G_ICGS = 3, /*!< Iteratied Classical Gram-Schmidt */
};

/**
 * \brief  The different available orthogonalization types
 */
enum fabulous_g_orthotype {
    FABULOUS_G_RUHE  = 0, /*!< RUHE  variant (vector-by-vector) */
    FABULOUS_G_BLOCK = 1, /*!< BLOCK variant (block-by-block) */
};

typedef enum fabulous_g_arithmetic fabulous_g_arithmetic;
typedef enum fabulous_g_orthoscheme fabulous_g_orthoscheme;
typedef enum fabulous_g_orthotype fabulous_g_orthotype;

/**
 * \brief User defined function to compute
 * product between user's %Matrix (A) and %Matrix given in argument (X)
 *
 * Operation to be performed:  B := A * X
 *
 * \param user_data same pointer that was passed to fabulous_g_create()
 * \param N number of vector in the matrices X and B.

 * \param[in] X matrix of size 'dim' x N to be read.
 * Values are stored in Column Major layout (as in Fortran.)
 * \param ldx leading dimension of X
 *
 * \param[out] B matrix of size 'dim' x N to be written by the user.
 * Values are stored in Column Major layout.
 * \param ldb leading dimension of B
 *
 * \return numbers of flops performed by the operation
 *        for (optionnal) perfomance analysis
 *
 * \note dim is the parameter that was passed to fabulous_g_create()
 */
typedef int64_t (*fabulous_g_mvp_t)(void *user_data,
                                   const void *alpha, const void *X,
                                   const void *beta, void *B);

/**
 * \brief User defined function to compute product between
 * the user's right-preconditioner M and a matrix X
 *
 * Operation to be performed: B := M*X
 *
 * \param user_data same pointer that was passed to fabulous_g_create()
 *
 * \param N number of vector inside the matrices B and X
 *
 * \param[in] X %Matrix of size 'dim' x N  to be read.
 * Values are stored in Column Major layout (as in Fortran)
 * \param ldx leading dimension of X
 *
 * \param[out] B matrix of size 'dim' x N to be written by the user.
 * Values are stored in Column Major layout.
 * \param ldb leading dimension of B
 * \return numbers of flops performed by the operation
 *        for (optionnal) perfomance analysis
 *
 * \note dim is the parameter that was passed to fabulous_g_create()
 */
typedef int64_t (*fabulous_g_rightprecond_t)(void *user_data, const void *X, void *B);

/**
 * \brief Function to be called once at the end of each iteration
 *
 * \param user_data same pointer that was passed to fabulous_g_create()
 *
 * \param[in] X either NULL or a matrix of size ( 'dim' x NRHS ) corresponding to solution vector
 * \param[in] R either NULL or a matrix of size ( 'dim' x NRHS ) corresponding to residual vector
 * \param logger can be used by the user as a parameter to fabulous_g_set_iteration_user_data()
 * \note this handle may be different from the solver handle
 *     but the user may not call fabulous_g_destroy() on this handle
 *     because it is internal to the library
 *
 * \note dim is the parameter that was passed to fabulous_g_create()
 */
typedef void (*fabulous_g_callback_t)(void *user_data, int iter,
                                     const void *X, const void *R,
                                     fabulous_g_handle logger);

typedef struct fabulous_g_vector_operation fabulous_g_vector_operation_t;

struct fabulous_g_vector_operation {
    void (*create_resize)(const void *v_i, void **v_o, int n);
    void (*copy)(const void *v_src, void *v_dest);
    void (*destroy)(void *v);
    void (*get_nb_col)(const void *v, int *n);

    void (*qr)(void *q, void *r);
    void (*trsm)(void *x, void *r); // solve xR = b, with R upper-triangular
    void (*dot)(const void *v1, const void *v2, void *c);
    void (*axpy)(void *y, const void *x, const void *a, const void *alpha);
    void (*zero)(void *vec);

    void (*cwise_norm)(const void *v, void *n);
    void (*cwise_scale)(void *v, const void *s);
    void (*cwise_axpy)(void *y, const void *x, const void *alpha);
};

/**
 * \brief Create a library handle.
 *
 * \param ari the arithmetic used (float/double) (real/complex)
 *
 * \param user_data a pointer that will be provided to each callback.
 *  The user may use this pointer to store extra informations needed by
 *  the callbacks (see examples.)
 *
 * \return a fabulous_g_handle opaque object, it is needed to make calls
 * to other functions of the library or NULL if an error occured
 */
fabulous_g_handle fabulous_g_create(fabulous_g_arithmetic ari, void *user_data);

/**
 * \brief free internal ressources held by the opaque fabulous_g_handle.
 * After calling this function on a handle, no other library function may be called.
 *
 * \param handle the fabulous opaque handle to be destroyed
 */
void fabulous_g_destroy(fabulous_g_handle handle);

/**
 * \brief Set method for
 *
 * \param user_mvp matrix vector product callback (function pointer),
 *        refer to documentation to get the prototype
 * \param user_rpc right preconditioner (function pointer),
 *        refer to documentation to get the prototype
 * \param user_callback user callback (function pointer),
 *        refer to documentation to get the prototype
 * \param vector_ops operations to handle vectors
 *        refer to documentation to get the members
 *
 * \param handle the fabulous opaque handle
 *
 * \note MVP stand for %Matrix Vector Product
 */
void fabulous_g_set_operations(fabulous_g_mvp_t user_mvp,
                              fabulous_g_rightprecond_t user_rpc,
                              fabulous_g_callback_t user_callback,
                              const fabulous_g_vector_operation_t *vector_ops,
                              fabulous_g_handle handle);

/**
 * \brief set the solver's parameters
 *
 * \param max_mvp Maximum number of matrix vector product to be done

 * \param max_krylov_space_size Maximum size allowed for Krylov Search Space.

 * \param[in] tolerance an array of scalar containing the tolerance threshold
 *        used to consider convergence is reached (normalized backward error)
 *   - the type of value in the array must match the correct type to represent
 *     the module of the Arithetic used:
 *     for instance if the handle was created with FABULOUS_G_REAL_DOUBLE
 *     or FABULOUS_G_COMPLEX_DOUBLE, then tolerance must be an array of double \n
 *   - otherwise\n
 *         tolerance must be an array of float
 *
 * \param nb_tolerance size of the tolerance array \n
 *  - if nb_tolerance == 1 \n
 *        then tolerance[0] will be used as the threshold for all vectors. \n
 *  - otherwise \n
 *        nb_tolerance must match the parameter nrhs (number of right hand sides)
 *        passed in the fabulous_g_solve_XX() family of function \n
 *  - if nb_tolerance is neither equal to 1 nor nrhs \n
 *        this is an error and the behaviour is undefined.
 *
 * \param handle the fabulous library opaque handle
 * \return 0 if the call was successful or a negative value to indicate an error
 */
int fabulous_g_set_parameters(int max_mvp, int max_krylov_space_size,
                             void *tolerance, int nb_tolerance,
                             fabulous_g_handle handle);

/**
 * \brief set the solver's advanced parameters
 *
 * \param max_kept_direction maximum number of kept direction per iteration
 *    This parameter is meaningful only for IB variants,
 *    otherwise it is not taken into account. You can set this parameter to -1 if
 *    you want it to match the number of right hand sides
 *
 * \param real_residual In GMRES algorithms, compute X and R at each iteration
 *   such that the user can access them in CallBack
 *
 * \param logger_user_data_size number of slots for user data when calling
 *              fabulous_g_set_iteration_user_data()
 *
 * \param quiet boolean to know if solver must not produce any output
 *
 * \param handle the fabulous library opaque handle
 * \return 0 if the call was successful or a negative value to indicate an error
 *
 * \warning Using real_residual parameter set to true may slow down BGMRES a lot!
 *          it is designed to be used for debugging purposes
 */
int fabulous_g_set_advanced_parameters(int max_kept_direction,
                                       int real_residual,
                                       int logger_user_data_size,
                                       int quiet,
                                       fabulous_g_handle handle);

/**
 * \brief Set the orthogonalization schema
 *
 * \param scheme one of the following:
 \verbatim
 FABULOUS_G_MGS  -> Modified Gram Schmidt (default value)
 FABULOUS_G_CGS  -> Classical Gram Schmidt
 FABULOUS_G_IMGS -> Iterated Modified Gram Schmidt
 FABULOUS_G_ICGS -> Iterated Classical Gram Schmidt
 \endverbatim
 *
 * \param type one of the following:
 \verbatim
 FABULOUS_G_RUHE  -> RUHE variant (vector-by-vector)
 FABULOUS_G_BLOCK -> BLOCK variant (block-by-block)
 \endverbatim
 *
 * \param nb_iter number of iteration for Iterated Schemas (IMGS and ICGS); Must be positive integer >= 2
 * \param handle the fabulous library opaque handle
 *
 * The choice of the schema will have influence on the numerical quality
 * of the orthogonalization coefficients.
 * A better orthogonalization schema may improve the convergence speed in terms
 * of number of iteration but it may also slow the speed of the algorithm if you
 * use the library in a distributed fashion since it requires a greater amount of
 * synchronization.
 *
 \verbatim
 less synchronizations -------------> more synchronizations
 CLASSICAL Gram-Schidt -------------> MODIFIED Gram-Schmidt
 \endverbatim
 *
 * The iterated version of these algorithms denote the facts than a schema
 * may be repeated several times (the default is two time) to improve the numerical
 * quality. Of the course the number of synchronization is also multiplied by the amount
 * of times the schema is repeated.
 *
 * \note if no call to this function is made, FABULOUS_G_MGS will be used
 * as the default value for the orthoproc parameter
 * \return 0 if the call was successful or a negative value to indicate an error
 */
int fabulous_g_set_ortho_process(fabulous_g_orthoscheme scheme,
                                fabulous_g_orthotype type,
                                int nb_iter, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X as initial solution guess using
 * Block General Minimum Residual (BGMRES) algorithm.
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve(void *X, void *B, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X as initial solution guess using
 * Block General Conjugate Residual (BGCR) algorithm.
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_GCR(void *X, void *B, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X as initial solution guess using
 * Block General Conjugate Residual O... (BGCRO) algorithm, starting with an empty
 * deflation space Uk.
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param[in,out] Uk deflation space (Column major layout)
 * \param k number of eigen pair used in Deflated Restarting (size of Uk)
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_g_create()
 * \param Uk_const Indicate if the deflation space considered is a good enough approximation
 *  and thus does not need to be updated (if set to true, the deflation space will not be updated)
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_GCRO(void *X, void *B, void **Uk, int k, void *target, int Uk_const, fabulous_g_handle handle);

/**
 * \brief Get the constant property of the latest deflation space used for GCRO.
 *
 * \return true if the latest deflation space used for GCRO was constant, false otherwise
 */
int fabulous_g_get_Uk_const_last_solve(fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with detection of Inexact Breakdowns
 * (IB-BGMRES)
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout).
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_IB(void *X, void *B, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Deflated restarting
 * (BGMRES-DR)
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param k number of eigen pair used in Deflated Restarting
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_g_create()
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_DR(void *X, void *B, int k, void *target, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Inexact Breakdown and Deflated restarting
 * (IB-BGMRES-DR)
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param k number of eigen pair used in Deflated Restarting
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_g_create()
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_IBDR(void *X, void *B, int k, void *target, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm (BGMRES)
 * with Incremental QR factorization.
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_QR(void *X, void *B, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Inexact Breakdown
 * and Incremental QR factorization
 * (QR-IB-BGMRES)
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_QRIB(void *X, void *B, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Deflated restarting
 * and Incremental QR factorization
 * (BGMRES-DR)
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param k number of eigen pair used in Deflated Restarting
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_g_create()
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_QRDR(void *X, void *B, int k, void *target, fabulous_g_handle handle);

/**
 * \brief Solve the system AX=B starting with X0
 * using Block General Minimum Residual algorithm with Deflated restarting,
 * Inexact Breakdown and Incremental QR factorization
 * (QR-IB-BGMRES-DR)
 *
 * \param[in] B Right Hand Side (Column major layout)
 * \param[in,out] X initial solution guess (Column major layout)
 * \param k number of eigen pair used in Deflated Restarting
 * \param target targeted eigen value: the eigen values used will be the k eigen values
 *  whose are the closest to target. \n
 *  The type of target must match the arithmetic chosen in fabulous_g_create()
 * \param handle the fabulous opaque handle
 * \return number of matrix vector product performed or a negative value to indicate an error
 */
int fabulous_g_solve_QRIBDR(void *X, void *B, int k, void *target, fabulous_g_handle handle);

/**
 * \brief Get the convergence history
 *
 * This function return the convergence history (in terms of backward error)
 * as an array containing 3 double for each iteration step:
 *
 *      {MIN over the vectors; MAX over the vectors;  TIMESTAMP}.
 *
 * \param[out] size filled with the size of the returned array
 * \param handle the fabulous opaque handle
 *
 * \return the array containing the convergence history as described above.
 *
 * \warning The returned array is valid until fabulous_g_destroy()
 * is called on the handle or another solve call is performed.
 * If you want to access the data after calling fabulous_g_destroy() or a solve function,
 * you need to copy the data
 */
const double* fabulous_g_get_logs(int *size, fabulous_g_handle handle);

/**
 * \brief Allow the user to add its own data into fabulous internal logger
 *
 *  This function can be called only inside the user callback of type fabulous_g_callback_t
 *  which was set with the fabulous_g_set_user_callback() function
 *
 * \param id Number identifying the user data.
 *    Must be an integer greater or equal than 0 and less than the maximum
 *    number of user data slots availables (The default is 4).
 *    This maximum number of slots can be
 *    changed with the fabulous_g_set_advanced_parameters() function.
 * \param fp floating point user data
 * \param integ integer user data
 * \param logger the last parameter of the callback of type fabulous_g_callback_t
 *
 * \return 0 on success or a negative value to indicate an error
 */
int fabulous_g_set_iteration_user_data(unsigned id, double fp, int64_t integ, fabulous_g_handle logger);

/**
 * \brief Print the convergence history with advanced details
 *
 * \param file must be a valid pointer of type (FILE*) returned by fopen or similar
 * \param log_id a string to identify the log
 * \param handle the fabulous opaque handle
 *
 * \return 0 on success or a negative value to indicate an error
 */
int fabulous_g_print_log_file(void *file, const char *log_id, fabulous_g_handle handle);

/**
 * \brief Print the convergence history with advanced details
 *
 * \param filename the output filename
 * \param log_id a string to identify the log
 * \param handle the fabulous opaque handle
 *
 * \return 0 on success or a negative value to indicate an error
 */
int fabulous_g_print_log_filename(const char *filename, const char *log_id, fabulous_g_handle handle);

/**
 * \brief get the error description as a string
 *
 * \return the string as a c-style NUL terminated string
 *  The user must not free the value returned by this function as it is stored statically.
 *  The returned string must not be modified by the user but it may be modified by
 *  subsequent call to fabulous_g_last_error_str()
 */
const char *fabulous_g_last_error_str(void);

/**
 * \brief get the error number type description as a string
 *
 * \return the string as a c-style NUL terminated string
 *  The user must not free the value returned by this function as it is stored statically.
 *  The returned string must not be modified by the user but it may be modified by
 *  subsequent call to fabulous_g_error_type_str()
 */
const char *fabulous_g_error_type_str(int errcode);


/**
 * \brief enable or disable color in fabulous output
 * \param enable_color boolean value to indicate whether colors should be enabled
 *
 * \note the effect is global and does not depend on any handle
 */
void fabulous_g_set_output_colored(int enable_color);

FABULOUS_END_C_DECL

#endif // FABULOUS_GENERIC_VECTORS_H
