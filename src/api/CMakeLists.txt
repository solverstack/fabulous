#################
# c api library #
#################

add_library(fabulous_c_api
    include/fabulous.h

    basic/fabulous_basic.cpp
    basic/ApiEngine.hpp
    basic/Matrix.hpp
    basic/Vector.hpp
    basic/Dot.hpp
    basic/Precond.hpp
    include/fabulous/fabulous_basic.h

    generic_vectors/fabulous_generic_vectors.cpp
    generic_vectors/ApiEngine.hpp
    generic_vectors/Matrix.hpp
    generic_vectors/Callback.hpp
    generic_vectors/Precond.hpp
    generic_vectors/Vector.hpp
    include/fabulous/fabulous_generic_vectors.h
    )

set_target_properties(fabulous_c_api PROPERTIES
    OUTPUT_NAME "fabulous"
    VERSION   ${fabulous_VERSION}
    SOVERSION ${fabulous_VERSION_MAJOR}
    )

target_include_directories(fabulous_c_api PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
    )
target_link_libraries( fabulous_c_api PRIVATE fabulous_cpp )

target_compile_definitions(fabulous_c_api PRIVATE
    ${FABULOUS_DEFINITIONS}
    )

target_compile_options(fabulous_c_api PRIVATE
    ${FABULOUS_CXX_FLAGS}
    )

#######################
# fortran api library #
#######################

if(FABULOUS_BUILD_Fortran_API)

add_library(fabulous_fortran_api
    basic/fabulous_mod.F90
    )

set_target_properties(fabulous_fortran_api PROPERTIES
    OUTPUT_NAME "fabulous_fortran"
    VERSION   ${fabulous_fortran_VERSION}
    SOVERSION ${fabulous_fortran_VERSION_MAJOR}
    )

target_include_directories(fabulous_fortran_api PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
    )

target_link_libraries(fabulous_fortran_api PRIVATE fabulous_c_api)

target_compile_definitions(fabulous_fortran_api PRIVATE
    ${FABULOUS_DEFINITIONS}
    )

target_compile_options(fabulous_fortran_api PRIVATE
    ${FABULOUS_Fortran_FLAGS}
    )

# install(
#     TARGETS fabulous_fortran_api
#     EXPORT FabulousConfig
#     FILES "${CMAKE_CURRENT_BINARY_DIR}/fabulous_mod.mod"
#     DESTINATION lib
#     )

    # # fortran interface
    # target_sources(fabulous_c_api PRIVATE
    #     basic/fabulous_mod.F90
    #     )
    # target_compile_options(fabulous_c_api PRIVATE
    #     # $<$<COMPILE_LANGUAGE:Fortran>:${FABULOUS_Fortran_FLAGS}>
    #     ${FABULOUS_Fortran_FLAGS}
    #     )
    # install(
    #     FILES "${CMAKE_CURRENT_BINARY_DIR}/fabulous_mod.mod"
    #     DESTINATION include
    #     )

endif(FABULOUS_BUILD_Fortran_API)

###########
# install #
###########

if (FABULOUS_BUILD_Fortran_API)

    # export target
    install(EXPORT fabulous_fortran_apiTargets
            NAMESPACE FABULOUS::
            DESTINATION lib/cmake/fabulous
            )
    install(TARGETS fabulous_fortran_api
            EXPORT fabulous_fortran_apiTargets
            ARCHIVE DESTINATION lib
            LIBRARY DESTINATION lib
            )
    install(FILES "${CMAKE_CURRENT_BINARY_DIR}/fabulous_mod.mod" DESTINATION include)

endif(FABULOUS_BUILD_Fortran_API)


# export target
install(EXPORT fabulous_c_apiTargets
        NAMESPACE FABULOUS::
        DESTINATION lib/cmake/fabulous
        )
install(TARGETS fabulous_c_api
        EXPORT fabulous_c_apiTargets
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib
        )
install(DIRECTORY include/ DESTINATION include)
