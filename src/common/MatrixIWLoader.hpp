#ifndef FABULOUS_MATRIX_IW_LOADER_HPP
#define FABULOUS_MATRIX_IW_LOADER_HPP

#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <cstdint>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <complex>

#include "fabulous/utils/Error.hpp"
#include "BadMatrix.hpp"

namespace fabulous {

/*! \brief IW file format arithmetic */
template<class T>
struct IW_matrix_type
{
    enum { value = -1 };
};

#define FABULOUS_DEFINE_IW_MATRIX_TYPE(type_, value_)   \
    template<>                                          \
    struct IW_matrix_type<type_> {                      \
        typedef type_ type;                             \
        enum { value = value_};                         \
        static constexpr const char *name = #type_;     \
    }

FABULOUS_DEFINE_IW_MATRIX_TYPE(float,                0);
FABULOUS_DEFINE_IW_MATRIX_TYPE(double,               1);
FABULOUS_DEFINE_IW_MATRIX_TYPE(std::complex<float>,  2);
FABULOUS_DEFINE_IW_MATRIX_TYPE(std::complex<double>, 3);

/*! \brief Load matrix in Innovations Works (binary) file format */
class MatrixIWLoader
{
private:
    template<class Matrix>
    void LoadMatrix(Matrix &A, std::ifstream &file, bool check_square = true)
    {
        using S = typename Matrix::value_type;

        if (!file.is_open()) {
            std::cerr << "file not opened!" << std::endl;
            exit(EXIT_FAILURE);
        }
        int32_t header[5];
        file.read((char*)header, sizeof header);
        int type = header[0];
        int M = header[1];
        int N = header[2];
        if (type != IW_matrix_type<S>::value) {
            FABULOUS_THROW(
                Parameter,
                "Input file arithmetic does not match template parameter 'Matrix' arithmetic."
            );
        }
        std::cerr << "Type: '" << IW_matrix_type<S>::name
                  << "' sizes=("<<M<<", "<<N<<")\n";

        if (check_square && N != M) {
            std::cerr<< "N != M :: "<<N<<" != "<<M<<"\n"
                     << "Matrix is not square !\n"
                     << "Exiting\n";
            exit(EXIT_FAILURE);
        }
        A.resize(M, N, M*N);

        std::vector<S> vec;
        vec.resize(M*N);

        file.read((char*) vec.data(), sizeof(S)*N*M);
        for (int j = 0; j < N; ++j)
            for (int i = 0; i < M; ++i)
                A.at(i, j) = vec[j*N+i];
    }

public:
    template<class Matrix>
    void LoadMatrix(Matrix &A, const std::string &filename, bool check_square = true)
    {
        if ( is_bad_matrix_t<Matrix>::value ) {
             FABULOUS_THROW(Parameter, "Use BadMatrix with BadLoader only");
        }
        std::ifstream file{filename, std::ios::binary};
        if (!file) {
            std::stringstream ss;
            ss << filename <<": " << strerror(errno);
            fabulous::fatal_error(ss.str());
            return;
        }
        std::cerr<<"Loading IW matrix: '"<<filename<<"'\n";
        LoadMatrix(A, file, check_square);
        file.close();
    }

}; // end class MatrixIWLoader

} // end namespace fabulous

#endif // FABULOUS_MATRIX_IW_LOADER_HPP
