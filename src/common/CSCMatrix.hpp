#ifndef FABULOUS_CSC_MATRIX_HPP
#define FABULOUS_CSC_MATRIX_HPP

#include <memory>
#include <vector>

namespace fabulous {
template<class> class CSCMatrix;
}

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/utils/Arithmetic.hpp"

#include "./sparse.hpp"

namespace fabulous {

/**
 * Matrix Class: need to implement MatBlockVect()
 * useRightPreCond(), preCondBlockVect() and DotProduct()
 * member function in order to works with BGMRes algorithm
 *
 * An abstract class could be used to enforce that.
 */
template<class S>
class CSCMatrix
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;

private:
    std::vector<S> _values;
    std::vector<int> _indices;
    std::vector<int> _row_ptr;
    int _last_j;

public:
    S& at(int i, int j)
    {
        if (j < _last_j) {
            FABULOUS_THROW(Parameter, "matrix was not filled in CSC order");
        }
        _values.emplace_back(S{0.0});
        _indices.emplace_back(i+1);
        if (j > _last_j) {
            if (j > _last_j+1) {
                FABULOUS_THROW(Parameter, "empty column in source matrix");
            }
            // FABULOUS_DEBUG("newline idx="<<_row_ptr.back());
            _row_ptr.emplace_back(_row_ptr.back());
        }
        ++ _row_ptr.back();
        // FABULOUS_DEBUG("last_ptr="<<_row_ptr.back());
        _last_j = j;

        // FABULOUS_DEBUG("i="<<i<<" j="<<j);
        return _values.back();
    }

    const S& at(int, int) const
    {
        FABULOUS_THROW(Unsupported, "random access");
        static S dummy;
        return dummy;
    }

    CSCMatrix(int=0, bool=false):
        _last_j{0}
    {
    }

    void resize(int m, int n, int nnz)
    {
        FABULOUS_DEBUG("resize");
        FABULOUS_DEBUG("m="<<m<<" n="<< n<<" nnz="<<nnz);
        FABULOUS_ASSERT( m == n );

        _values.clear();
        _indices.clear();
        _row_ptr.clear();

        _values.reserve(nnz);
        _indices.reserve(nnz);

        FABULOUS_DEBUG("m+1="<<m+1);
        _row_ptr.reserve(m+1);
        _row_ptr.emplace_back(1);
        _row_ptr.emplace_back(1);
    }

    primary_type get_inf_norm() const
    {
        FABULOUS_THROW(Unsupported, "matrix norm");
        return -1.0;
    }

    /* ---------------------------------------------------------------- */
    /* --------------- CALLBACKS -------------------------------------- */
    /* ---------------------------------------------------------------- */
    int size() const { return _row_ptr.size()-1; }
    int get_nb_row() const { return size(); }
    int get_nb_col() const { return size(); }

    int64_t operator()(const Block<S> &X, Block<S> &B,
                       S alpha = S{1.0}, S beta = S{0.0}) const
    {
        FABULOUS_ASSERT( B.get_nb_col() == X.get_nb_col() );
        FABULOUS_ASSERT( X.get_nb_row() == size() );
        FABULOUS_ASSERT( B.get_nb_row() == size() );
        FABULOUS_ASSERT( _row_ptr.capacity() == _row_ptr.size() );

        int M = size();
        int N = X.get_nb_col();
        int K = M;

        sparse::cscmm(
            M, N, K, alpha,
            _values.data(), _indices.data(),
            _row_ptr.data(), _row_ptr.data()+1,
            X.get_ptr(), X.get_leading_dim(),
            beta, B.get_ptr(), B.get_leading_dim()
        );
        FABULOUS_WARNING("nb_flops not computed for sparse mvp");
        return 0;
    }

    int64_t QRFacto(Block<S> &Q, Block<S> &R) const
    {
        //return qr::InPlaceQRFactoMGS_User(Q, R, *this);
        return qr::InPlaceQRFacto(Q, R);
    }

    int64_t DotProduct(int M, int N,
                       const S *A, int lda,
                       const S *B, int ldb,
                       S *C, int ldc) const
    {
        int dim = size();
        lapacke::Tgemm(M, N, dim, A, lda, B, ldb, C, ldc, S{1.0}, S{0.0});
        return lapacke::flops::gemm<S>(M, N, dim);
    }

}; // end class UserInputMatrix

} // end namespace fabulous

#endif // FABULOUS_USER_INPUT_MATRIX_HPP
