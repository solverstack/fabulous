#ifndef FABULOUS_USER_INPUT_MATRIX_HPP
#define FABULOUS_USER_INPUT_MATRIX_HPP

#include <memory>

namespace fabulous {
template<class> class BasicDenseMatrix;
template<class> class BasicDiagDominantPrecond;
}

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/kernel/flops.hpp"
#include "fabulous/utils/Arithmetic.hpp"

namespace fabulous {

/*! \brief dense matrix (in-memory lapack column major layout) */
template<class S>
class BasicDenseMatrix
{
public:
    using value_type   = typename Arithmetik<S>:: value_type;
    using primary_type = typename Arithmetik<S>:: primary_type;

private:
    using P = primary_type;

    Block<S> _data;

public:

    BasicDenseMatrix():
        _data{}
    {
    }

    explicit BasicDenseMatrix(int dim):
        _data{dim, dim}
    {
    }

    void resize(int m, int n, int /*nnz*/)
    {
        FABULOUS_ASSERT(m == n);
        _data = Block<S>{m, n};
    }

    void resize(int n)
    {
        if (n == _data.get_nb_row())
            return;
        _data.realloc(n, n);
    }

    int size() const { return _data.get_nb_row(); }
    int get_nb_row() const { return _data.get_nb_row(); }
    int get_nb_col() const { return _data.get_nb_col(); }
    S &at(int i, int j) { return _data.at(i, j); }
    const S &at(int i, int j) const { return _data.at(i, j); }
    void display() const { _data.display(); }

    // debug
    primary_type get_inf_norm() const
    {
        int M = _data.get_nb_row();
        int N = _data.get_nb_col();

        primary_type max = primary_type{0.0};
        for (int j = 0; j < N; ++j) {
            P n = P{0.0};
            for (int i = 0; i < M; ++i)
                n += std::norm(_data.at(i, j));
            max = std::max(n, max);
        }
        return std::sqrt(max);
    }

    void display_first_block(int block_size)
    {
        Block<S> first = _data.sub_block(0, 0, block_size, block_size);
        first.display();
    }

    /* ---CALLBACK-------------------------- */

    int64_t operator()(const Block<S> &X, Block<S> &B,
                       S alpha = S{1.0}, S beta = S{0.0}) const
    {
        FABULOUS_ASSERT( B.get_nb_col() == X.get_nb_col() );
        FABULOUS_ASSERT( X.get_nb_row() == _data.get_nb_row() );
        FABULOUS_ASSERT( B.get_nb_row() == _data.get_nb_row() );

        int M = _data.get_nb_row();
        int N = X.get_nb_col();
        lapacke::gemm(
            M, N, M,
            _data.get_ptr(), _data.get_leading_dim(),
            X.get_ptr(), X.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha, beta
        );
        return lapacke::flops::gemm<S>(M, N, M);
    }

}; // end class BasicDenseMatrix


/*! \brief basic preconditioner for diagonally dominant matrix */
template<class Matrix>
class BasicDiagDominantPrecond
{
private:
    const Matrix &_A;
    bool _enabled;
public:
    BasicDiagDominantPrecond(const Matrix &A, bool enabled=false):
        _A{A},
        _enabled{enabled}
    {
    }

    operator bool() const { return _enabled; }

    /**
     * \brief Preconditioner for diagonally dominant matrix
     *
     * This preconditionner works well for diagonally dominant matrices.
     * M^{-1} == diagonal_matrix(inverse_of_diagonal_element(A))
     * M^{-1} * A ~= I
     */
    template<class S>
    int64_t operator()(const Block<S> &input, Block<S> &output) const
    {
        if (!_enabled) {
            FABULOUS_THROW(Internal, "should no be there");
        }
        FABULOUS_ASSERT(input.get_ptr() != output.get_ptr() );
        for (int j = 0; j < output.get_nb_col(); ++j)
            for (int i = 0; i < _A.get_nb_row(); ++i)
                output.at(i, j) = input.at(i, j) * (S{1.0} / _A.at(i, i));
        FABULOUS_WARNING("nb_flops not computed for preconditionner");
        return 0;
    }

};

} // end namespace fabulous

#endif // FABULOUS_USER_INPUT_MATRIX_HPP
