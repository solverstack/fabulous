#ifndef FABULOUS_TEST_HPP
#define FABULOUS_TEST_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "fabulous/data/Block.hpp"
#include "fabulous/algo/Equation.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/ConvergenceCheck.hpp"
#include "fabulous/utils/Meta.hpp"
#include "fabulous/algo/solve.hpp"
#include "fabulous/algo/AlgoType.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"

#define MARK_AS_USED(var_) ((void)(var_))

namespace fabulous {

template<class P>
struct TestRet
{
    double elapsed; /**< elapsed time */
    int nb_mvp; /**< number of matrix vector product done */
    std::pair<P, P> minmax; /**< forward error */
    int max_mvp; /**< max matrix vector product scheduled */
};

template<class P>
void print_test_ret(const TestRet<P> &r, const std::string &test_name)
{
    std::cerr << test_name<<":\n"
              << "\tElapsed time:\t"<<r.elapsed << " seconds for "
              << r.nb_mvp<<"/"<<r.max_mvp<<" iterations\n"
              << "\tForward error:\n"
              << "\t\tMin:\t"<<r.minmax.first<<"\n"
              << "\t\tMax:\t"<<r.minmax.second<<"\n";
}

template<class Matrix, class S, class P = typename Arithmetik<S>::primary_type>
void check_solution(Matrix &A, Block<S> &X, Block<S> &B, const Block<S> &X_Exact)
{
    const int dim   = X.get_nb_row();
    const int nbRHS = X.get_nb_col();

    Block<S> AX{dim, nbRHS};
    Block<S> R{dim, nbRHS};
    Block<S> X_diff{dim, nbRHS};

    A.MatBlockVect(X, AX);
    R.copy(AX);

    for (int j = 0; j < nbRHS; ++j) {
        for (int i = 0; i < dim; ++i) {
            R.at(i, j) = B.at(i, j) - R.at(i, j);
            X_diff.at(i,j) = X_Exact.at(i, j) - X.at(i, j);
        }
    }

    for (int j = 0; j < nbRHS; ++j) {
        std::cerr<<"Metrics on "<<j<<"\n";
        P ForwardError = X_diff.get_norm(j);
        std::cerr <<"["<<j<<"]\tforward error (on X)  \t" << ForwardError <<"\n";

        P BackwardError = R.get_norm(j);
        std::cerr <<"["<<j<<"]\tbackward error  \t" << BackwardError <<"\n";

        P ForwardErrorNormalized = ForwardError / X_Exact.get_norm(j);
        std::cerr <<"["<<j<<"]\tnormalized forward error \t" << ForwardErrorNormalized <<"\n";

        P BackwardErrorNormalized = BackwardError / B.get_norm(j);
        std::cerr <<"["<<j<<"]\tNormalized backward error\t" << BackwardErrorNormalized << "\n\n";
    }
}

/* *********************** helpers *************************** */

template<class Algo, class Restart>
std::string get_formatted_filename(
    const std::string &label, OrthoParam ortho,
    Algo algo, Restart restart, bool append_suffix)
{
    std::stringstream name;
    name << label;
    if (append_suffix) {
        name <<"_" <<ortho.get_scheme()<<"_"<<ortho.get_type()<<"_"
             << algo << get_type(restart);
    }
    return name.str();
}

template<class Logger, class Algo, class Restart>
void print_logger_to_file(
    const Logger &logger, const std::string &label, OrthoParam ortho,
    Algo algo, Restart restart, bool append_suffix)
{
    std::stringstream fpath, fpath_kernel, fpath_memory;
    std::string name;
    name = get_formatted_filename(label, ortho, algo, restart, append_suffix);
    fpath << "./" << name << ".res";
    fpath_kernel << "./" << name << ".kernel.txt";
    fpath_memory << "./" << name << ".memory.txt";

    std::ofstream file{fpath.str(), std::ios::out};
    if (file.is_open()) {
        logger.print(name, file);
        file.close();
    } else {
        std::stringstream ss;
        ss << "Cannot open '" << fpath.str() << "': '" << strerror(errno)<<"'";
        error(ss.str());
    }

    #if FABULOUS_KERNEL_LOGGING
    ::fabulous::write_kernel_log(fpath_kernel.str());
    #endif

    #if FABULOUS_MEMORY_LOGGING
    ::fabulous::write_memory_log(fpath_memory.str());
    auto &pk = ::fabulous::get_peak_record();
    std::cout << "Peak = "<<pk.peak_MB<<" MB  :: "<<pk.peak_GB<<" GB\n";
    #endif
}

template<class Logger, class Algo, class Restart>
void print_logger_to_stdout(
    const Logger &logger, const std::string &label, OrthoParam ortho,
    Algo algo, Restart restart, bool append_suffix)
{
    std::string name;
    name = get_formatted_filename(label, ortho, algo, restart, append_suffix);
    logger.print(name, ::std::cout);
}

/* *************************** TEST ********************************** */

template<class Algo, class Restart, class Matrix, class Precond,
         class S, class P = typename Arithmetik<S>::primary_type  >
TestRet<P> run_test_BGMRES_filelog( const std::string &label,
                                    const Matrix &A, const Precond &M,
                                    Block<S> &RHS, const Block<S> &XExact,
                                    const std::vector<P> &epsilon, Algo algo,
                                    Parameters param,
                                    OrthoParam ortho, Restart restart,
                                    bool append_suffix=true )
{
    FABULOUS_ASSERT( XExact.get_nb_row() == RHS.get_nb_row() );
    FABULOUS_ASSERT( XExact.get_nb_col() == RHS.get_nb_col() );

    const int dim   = RHS.get_nb_row();
    const int nbRHS = RHS.get_nb_col();
    Timer time;
    Block<S> X{dim, nbRHS};

    time.start();
    noop_placeholder _;
    auto eq = equation(A, M, _, X, RHS, epsilon);
    auto logger = bgmres::solve(eq, algo, param, ortho, restart);
    time.stop();

    double elapsed = time.get_length();
    int nb = logger.get_nb_mvp();
    std::cerr << "Return value / max_mvp : " << nb << "/" << param.max_mvp << std::endl;

    print_logger_to_file(logger, label, ortho, algo, restart, append_suffix);

    auto minmax = compare_blocks_normalized(XExact, X);
    return TestRet<P>{elapsed, nb, minmax, param.max_mvp};
}

template<class Algo, class Restart, class Matrix, class Precond,
         class S, class P = typename Arithmetik<S>::primary_type  >
TestRet<P> run_test_BGCRO_filelog( const std::string &label,
                                    const Matrix &A, const Precond &M,
                                    Block<S> &RHS, const Block<S> &XExact,
                                    const std::vector<P> &epsilon, Algo algo,
                                    Parameters param,
                                    OrthoParam ortho, Restart restart,
                                    bool append_suffix=true )
{
    FABULOUS_ASSERT( XExact.get_nb_row() == RHS.get_nb_row() );
    FABULOUS_ASSERT( XExact.get_nb_col() == RHS.get_nb_col() );

    const int dim   = RHS.get_nb_row();
    const int nbRHS = RHS.get_nb_col();
    Timer time;
    Block<S> X{dim, nbRHS};

    time.start();
    noop_placeholder _;
    auto eq = equation(A, M, _, X, RHS, epsilon);
    Block<S> Uk{};
    auto logger = bgcro::solve(eq, algo, param, Uk, ortho, restart);
    logger = bgcro::solve(eq, algo, param, Uk, ortho, restart);
    time.stop();

    double elapsed = time.get_length();
    int nb = logger.get_nb_mvp();
    std::cerr << "Return value / max_mvp : " << nb << "/" << param.max_mvp << std::endl;

    print_logger_to_file(logger, label, ortho, algo, restart, append_suffix);

    auto minmax = compare_blocks_normalized(XExact, X);
    return TestRet<P>{elapsed, nb, minmax, param.max_mvp};
}

template<class Algo, class Matrix, class Precond,
         class S, class P = typename Arithmetik<S>::primary_type  >
TestRet<P>  run_test_BGCR_filelog(
    const std::string &label,
    const Matrix &A, const Precond &M,
    Block<S> &RHS, Block<S> &XExact, const std::vector<P> &epsilon,
    Algo algo, Parameters param,
    OrthoParam ortho, bool append_suffix=true )
{
    FABULOUS_ASSERT( XExact.get_nb_row() == RHS.get_nb_row() );
    FABULOUS_ASSERT( XExact.get_nb_col() == RHS.get_nb_col() );

    const int dim   = RHS.get_nb_row();
    const int nbRHS = RHS.get_nb_col();
    Timer time;
    Block<S> X{dim, nbRHS};

    time.start();
    noop_placeholder _;
    auto eq = equation(A, M, _, X, RHS, epsilon);
    auto logger = bgcr::solve(eq, algo, param, ortho);
    time.stop();

    double elapsed = time.get_length();
    int nb = logger.get_nb_mvp();
    std::cerr << "Return value / max_mvp : " << nb << "/" << param.max_mvp << std::endl;

    print_logger_to_file(logger, label, ortho, algo, classic_restart(), append_suffix);

    auto minmax = compare_blocks_normalized(XExact, X);
    return TestRet<P>{elapsed, nb, minmax, param.max_mvp};
}

template<class Algo, class Matrix, class Precond,
         class S, class P = typename Arithmetik<S>::primary_type  >
TestRet<P> run_test_BCG_filelog(
    const std::string &label,
    const Matrix &A, const Precond &M,
    Block<S> &RHS, Block<S> &XExact, const std::vector<P> &epsilon,
    Algo algo, Parameters param, bool append_suffix=true )
{
    FABULOUS_ASSERT( XExact.get_nb_row() == RHS.get_nb_row() );
    FABULOUS_ASSERT( XExact.get_nb_col() == RHS.get_nb_col() );

    const int dim   = RHS.get_nb_row();
    const int nbRHS = RHS.get_nb_col();

    Block<S> X{dim, nbRHS};
    Timer time;

    time.start();
    noop_placeholder _;
    auto eq = equation(A, M, _, X, RHS, epsilon);
    auto logger = bcg::solve(eq, algo, param);
    time.stop();

    double elapsed = time.get_length();
    int nb = logger.get_nb_mvp();
    std::cerr << "Return value / max_mvp : " << nb << "/" << param.max_mvp << std::endl;

    auto ortho = orthogonalization();
    print_logger_to_file(logger, label, ortho, algo, classic_restart(), append_suffix);

    auto minmax = compare_blocks_normalized(XExact, X);
    return TestRet<P>{elapsed, nb, minmax, param.max_mvp};
}

} // end namespace fabulous

#endif // FABULOUS_TEST_HPP
