#ifndef FABULOUS_SPARSE_MKL_HPP
#define FABULOUS_SPARSE_MKL_HPP

#ifdef FABULOUS_USE_MKL_SPARSE

#include <complex>

#define MKL_Complex16 std::complex<double>
#define MKL_Complex8 std::complex<float>
#include <mkl_spblas.h>

#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Arithmetic.hpp"

namespace fabulous {
namespace sparse {

inline void cscmm(MKL_INT M, MKL_INT N, MKL_INT K,
                  float alpha,
                  const float *values, const MKL_INT *indices,
                  const MKL_INT *ptr_begin, const MKL_INT *ptr_end,
                  const float *B, MKL_INT ldb,
                  float beta, float *C, MKL_INT ldc)
{
    static const char desca[6] = { 'G', 0, 0, 'F', 0, 0 };
    mkl_scscmm(
        "NoTrans", &M, &N, &K, &alpha, desca,
        values, indices, ptr_begin, ptr_end,
        B, &ldb, &beta, C, &ldc
    );
}

inline void cscmm(MKL_INT M, MKL_INT N, MKL_INT K,
                  double alpha,
                  const double *values, const MKL_INT *indices,
                  const MKL_INT *ptr_begin, const MKL_INT *ptr_end,
                  const double *B, MKL_INT ldb,
                  double beta, double *C, MKL_INT ldc)
{
    static const char desca[6] = { 'G', 0, 0, 'F', 0, 0 };
    mkl_dcscmm(
        "NoTrans", &M, &N, &K, &alpha, desca,
        values, indices, ptr_begin, ptr_end,
        B, &ldb, &beta, C, &ldc
    );
}

inline void cscmm(MKL_INT M, MKL_INT N, MKL_INT K,
                  std::complex<float> alpha,
                  const std::complex<float> *values, const MKL_INT *indices,
                  const MKL_INT *ptr_begin, const MKL_INT *ptr_end,
                  const std::complex<float> *B, MKL_INT ldb,
                  std::complex<float> beta, std::complex<float> *C, MKL_INT ldc)
{
    static const char desca[6] = { 'G', 0, 0, 'F', 0, 0 };
    mkl_ccscmm(
        "NoTrans", &M, &N, &K, &alpha, desca,
        values, indices, ptr_begin, ptr_end,
        B, &ldb, &beta, C, &ldc
    );
}

inline void cscmm(MKL_INT M, MKL_INT N, MKL_INT K,
                  std::complex<double> alpha,
                  const std::complex<double> *values, const MKL_INT *indices,
                  const MKL_INT *ptr_begin, const MKL_INT *ptr_end,
                  const std::complex<double> *B, MKL_INT ldb,
                  std::complex<double> beta, std::complex<double> *C, MKL_INT ldc)
{
    static const char desca[6] = { 'G', 0, 0, 'F', 0, 0 };
    mkl_zcscmm(
        "NoTrans", &M, &N, &K, &alpha, desca,
        values, indices, ptr_begin, ptr_end,
        B, &ldb, &beta, C, &ldc
    );
}

inline void coomm(MKL_INT M, MKL_INT N, MKL_INT K,
                  float alpha,
                  const float *values,
                  const MKL_INT *rows, const MKL_INT *cols, MKL_INT nnz,
                  const float *B, MKL_INT ldb,
                  float beta, float *C, MKL_INT ldc)
{
    static const char desca[6] = { 'G', 0, 0, 'F', 0, 0 };
    mkl_scoomm("NoTrans", &M, &N, &K, &alpha, desca,
               values, rows, cols, &nnz, B, &ldb, &beta, C, &ldc);
}

inline void coomm(MKL_INT M, MKL_INT N, MKL_INT K,
                  double alpha,
                  const double *values,
                  const MKL_INT *rows, const MKL_INT *cols, MKL_INT nnz,
                  const double *B, MKL_INT ldb,
                  double beta, double *C, MKL_INT ldc)
{
    static const char desca[6] = { 'G', 0, 0, 'F', 0, 0 };
    mkl_dcoomm("NoTrans", &M, &N, &K, &alpha, desca,
               values, rows, cols, &nnz, B, &ldb, &beta, C, &ldc);
}

inline void coomm(MKL_INT M, MKL_INT N, MKL_INT K,
                  std::complex<float> alpha,
                  const std::complex<float> *values,
                  const MKL_INT *rows, const MKL_INT *cols, MKL_INT nnz,
                  const std::complex<float> *B, MKL_INT ldb,
                  std::complex<float> beta, std::complex<float> *C, MKL_INT ldc)
{
    static const char desca[6] = { 'G', 0, 0, 'F', 0, 0 };
    mkl_ccoomm("NoTrans", &M, &N, &K, &alpha, desca,
               values, rows, cols, &nnz, B, &ldb, &beta, C, &ldc);
}

inline void coomm(MKL_INT M, MKL_INT N, MKL_INT K,
                  std::complex<double> alpha,
                  const std::complex<double> *values,
                  const MKL_INT *rows, const MKL_INT *cols, MKL_INT nnz,
                  const std::complex<double> *B, MKL_INT ldb,
                  std::complex<double> beta, std::complex<double> *C, MKL_INT ldc)
{
    static const char desca[6] = { 'G', 0, 0, 'F', 0, 0 };
    mkl_zcoomm("NoTrans", &M, &N, &K, &alpha, desca,
               values, rows, cols, &nnz, B, &ldb, &beta, C, &ldc);
}

} // end namespace sparse
} // end namespace fabulous

#endif // FABULOUS_USE_MKL_SPARSE

#endif // FABULOUS_SPARSE_MKL_HPP
