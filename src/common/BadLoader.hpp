#ifndef FABULOUS_BAD_LOADER_H
#define FABULOUS_BAD_LOADER_H

#include <type_traits>
#include <iostream>
#include <string>
#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Meta.hpp"
#include "fabulous/utils/Traits.hpp"
#include "BadMatrix.hpp"

namespace fabulous {

/*! \brief Loader for BadMatrix */
template<class S>
class BadLoader
{
private:
    S _subDiag;
    S _diag;
    S _superDiag;

public:
    BadLoader(S subDiag = -2.99, S diag = 4.0, S superDiag = -1.0):
        _subDiag{subDiag},
        _diag{diag},
        _superDiag{superDiag}
    {
    }

    template<template <class> class Matrix, class U>
    void LoadMatrix(Matrix<U> &A, const std::string &filename)
    {
        if (not is_bad_matrix_t<Matrix<U>>::value ) {
            FABULOUS_THROW(Unsupported,
                           "This loader only works for BadMatrix. "
                           "Use in combination with -k BAD option");
        }
        if (not std::is_same<U, S>::value ) {
            FABULOUS_THROW(Parameter, "Loader And Matrix type mismatch");
        }
        std::cerr << "BAD-LOADER: Loading: " << filename << "\n";
        A.at(0, 0) = _subDiag;
        A.at(1, 0) = _diag;
        A.at(2, 0) = _superDiag;
    }
};

template<class S, class = enable_if_t<is_real_t<S>::value>>
BadLoader<S> bad_loader()
{
    return BadLoader<S>(-2.99, 4.0, -1.0);
}

template<class S, class = enable_if_t<is_complex_t<S>::value>, class=void>
BadLoader<S> bad_loader()
{
    return BadLoader<S>(
        S{-2.99, 1.0},
        S{ 4.0, -2.0},
        S{-1.0,  1.0}
    );
}

} // end namespace fabulous

#endif // FABULOUS_BAD_LOADER_H
