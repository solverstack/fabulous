#ifndef FABULOUS_SPARSE_HPP
#define FABULOUS_SPARSE_HPP

#ifdef FABULOUS_USE_MKL_SPARSE
# include "./sparse_mkl.hpp"
#else

#include <fabulous/utils/Error.hpp>

namespace fabulous {
namespace sparse {

/* the OMP parallelizations in this file may be not good;
 * (sparse operation are memory bound, so bad parallelizations may be useless)
 * one may want to study the memory access pattern
 * and rewrite the algorithm/parallelization
 *
 * (No testing, or thorough checking have been performed on this kernels,
 *  they even may be incorrect)
 */

template<class S>
void cscmm(int M, int N, int K,
           S alpha,
           const S *values, const int *indices,
           const int *ptr_begin, const int *ptr_end,
           const S *B, int ldb,
           S beta, S *C, int ldc)
{
    #pragma omp parallel
    {
        #pragma omp for schedule(static, 1000) collapse(2)
        for (int j = 0; j < N; ++j) {
            for (int i = 0; i < M; i++) {
                C[j*ldc+i] = beta * C[j*ldc+i];
            }
        }
        #pragma omp for schedule(static, 1000)
        for (int j = 0; j < N; ++j) {
            for (int k = 0; k < K; ++k) {
                for (int p = ptr_begin[k]; p < ptr_end[k]; ++p) {
                    int i = indices[p] - 1;
                    C[j*ldc+i] = C[j*ldc+i] + alpha * values[p] * B[j*ldb+k];
                }
            }
        }
    }
}

template<class S>
void coomm(int M, int N, int K,
           S alpha,
           const S *values,
           const int *rows, const int *cols, int nnz,
           const S *B, int ldb,
           S beta, S *C, int ldc)
{
    FABULOUS_ASSERT(M == K);

    #pragma omp parallel
    {
        #pragma omp for schedule(static, 1000) collapse(2)
        for (int j = 0; j < N; ++j) {
            for (int i = 0; i < M; i++) {
                C[j*ldc+i] = beta * C[j*ldc+i];
            }
        }
        #pragma omp for schedule(static, 1000)
        for (int j = 0; j < N; ++j) {
            for (int p = 0; p < nnz; ++p) {
                int i = rows[p] - 1;
                int k = cols[p] - 1;
                C[j*ldc+i] = C[j*ldc+i] + alpha * values[p] * B[j*ldb+k];
            }
        }
    }
}

} // end namespace sparse
} // end namespace fabulous


#endif // FABULOUS_USE_MKL_SPARSE

#endif // FABULOUS_SPARSE_HPP
