#ifndef FABULOUS_COO_MATRIX_HPP
#define FABULOUS_COO_MATRIX_HPP

#include <memory>
#include <vector>
#include <complex>

namespace fabulous {
template<class> class COOMatrix;
}

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "./sparse.hpp"

namespace fabulous {

/**
 * Matrix Class: need to implement MatBlockVect()
 * useRightPreCond(), preCondBlockVect() and DotProduct()
 * member function in order to works with BGMRes algorithm
 *
 * An abstract class could be used to enforce that.
 */
template<class S>
class COOMatrix
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;

private:
    int _m;
    std::vector<int> _row;
    std::vector<int> _col;
    std::vector<S> _val;

public:
    S& at(int i, int j)
    {
        _val.emplace_back(S{0.0});
        _row.emplace_back(i+1);
        _col.emplace_back(j+1);
        return _val.back();
    }

    const S& at(int, int) const
    {
        FABULOUS_THROW(Unsupported, "");
        static S dummy;
        return dummy;
    }

    COOMatrix(int=0, bool=false)
    {
    }

    void resize(int m, int n, int nnz)
    {
        FABULOUS_DEBUG("resize");
        FABULOUS_ASSERT(m == n);

        FABULOUS_DEBUG("m="<<m<<" n="<< n<<" nnz="<<nnz);
        FABULOUS_ASSERT( m == n );

        _m = m;
        _val.clear();
        _row.clear();
        _col.clear();

        _val.reserve(nnz);
        _row.reserve(nnz);
        _col.reserve(nnz);
    }

    primary_type get_inf_norm() const
    {
        FABULOUS_THROW(Unsupported, "");
        return -1.0;
    }

    /* ---------------------------------------------------------------- */
    /* --------------- CALLBACKS -------------------------------------- */
    /* ---------------------------------------------------------------- */
    int size() const { return _m; }
    int get_nb_row() const { return size(); }
    int get_nb_col() const { return size(); }

    int64_t operator()(const Block<S> &X, Block<S> &B,
                       S alpha = S{1.0}, S beta = S{0.0}) const
    {
        FABULOUS_ASSERT( B.get_nb_col() == X.get_nb_col() );
        FABULOUS_ASSERT( X.get_nb_row() == size() );
        FABULOUS_ASSERT( B.get_nb_row() == size() );

        int M = size();
        int N = X.get_nb_col();
        int K = M;
        int NNZ = _val.size();

        sparse::coomm(
            M, N, K, alpha,
            _val.data(), _row.data(), _col.data(), NNZ,
            X.get_ptr(), X.get_leading_dim(),
            beta, B.get_ptr(), B.get_leading_dim()
        );
        FABULOUS_WARNING("nb_flops not computed for sparse mvp");
        return 0;
    }

    int64_t QRFacto(Block<S> &Q, Block<S> &R) const
    {
        //return qr::InPlaceQRFactoMGS_User(Q, R, *this);
        return qr::InPlaceQRFacto(Q, R);
    }

    int64_t DotProduct(int M, int N,
                       const S *A, int lda,
                       const S *B, int ldb,
                       S *C, int ldc) const
    {
        int dim = size();
        lapacke::Tgemm(M, N, dim, A, lda, B, ldb, C, ldc, S{1.0}, S{0.0});
        return lapacke::flops::gemm<S>(M, N, dim);
    }


}; // end class UserInputMatrix

} // end namespace fabulous

#endif // FABULOUS_USER_INPUT_MATRIX_HPP
