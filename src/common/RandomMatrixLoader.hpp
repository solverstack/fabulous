#ifndef FABULOUS_RANDOM_MATRIX_LOADER_HPP
#define FABULOUS_RANDOM_MATRIX_LOADER_HPP

#include <random>
#include <complex>
#include <iostream>

#include "fabulous/data/Block.hpp"
#include "fabulous/utils/Traits.hpp"

namespace fabulous {

/*! \brief Fill matrix with random values */
class RandomMatrixLoader
{
private:
    std::mt19937_64 _gen; // Mersenne twister 19937
    bool _spd;

private:
    template<class S,
             class Distribution,
             class = enable_if_t<is_real_t<S>::value> >
    S rnd(Distribution &dis)
    {
        return S{dis(_gen)};
    }

    template<class S,
             class Distribution,
             class = enable_if_t<is_complex_t<S>::value>,
             class = void >
    S rnd(Distribution &dis)
    {
        return S{dis(_gen), dis(_gen)};
    }

    template<class Matrix>
    void randomize(Matrix &A)
    {
        using S = typename Matrix::value_type;
        using P = typename Matrix::primary_type;

        std::normal_distribution<P> dis{};
        int M = A.get_nb_row();
        int N = A.get_nb_col();

        for (int j = 0; j < N; ++j)
            for (int i = 0; i < M; ++i)
                A.at(i, j) = rnd<S>(dis);
    }

public:
    explicit RandomMatrixLoader(bool spd = false):
        _gen{0},
        _spd{spd}
    {
    }

    template<class Matrix>
    void LoadMatrix(Matrix &A, const std::string &name="")
    {
        if (name != "") {
            std::cerr << "Loading randomly '" << name << "'\n";
        } else {
            std::cerr << "Random loader ("
                      << A.get_nb_row()
                      << "x"
                      << A.get_nb_col()
                      << ")" << (_spd ? " [SPD]": "") << "\n";
        }

        if (_spd) {
            using S = typename Matrix::value_type;
            int M = A.get_nb_row();
            FABULOUS_ASSERT( A.get_nb_row() == A.get_nb_col() );
            Block<S> A1{M, M};
            Block<S> A2{M, M};
            randomize(A1);
            lapacke::Tgemm(
                M, M, M,
                A1.get_ptr(), A1.get_leading_dim(),
                A1.get_ptr(), A1.get_leading_dim(),
                A2.get_ptr(), A2.get_leading_dim()
            );
            for (int j = 0; j < M; ++j)
                for (int i = 0; i < M; ++i)
                    A.at(i, j) = A2(i, j);
        } else {
            randomize(A);
        }
    }
}; // end class RandomMatrixLoader

} // end namespace fabulous

#endif // FABULOUS_RANDOM_MATRIX_LOADER_HPP
