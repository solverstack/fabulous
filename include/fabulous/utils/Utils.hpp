#ifndef FABULOUS_UTILS_HPP
#define FABULOUS_UTILS_HPP

#include <algorithm>
#include <limits>
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <complex>
#include <type_traits>
#include <utility>
#include <tuple>
#include <cstdint>


#define FABULOUS_DISABLE_TEMPLATE_INSTANTIATION(Type_S_)        \
    static_assert(                                              \
        sizeof(Type_S_) != sizeof(Type_S_),                     \
        "this template is disabled"                             \
    );                                                          \

namespace fabulous {

#define FABULOUS_DELETE_COPY_MOVE(KLASS_)       \
    FABULOUS_DELETE_COPY(KLASS_);               \
    FABULOUS_DELETE_MOVE(KLASS_)                \

#define FABULOUS_DELETE_COPY(KLASS_)            \
    KLASS_& operator=(const KLASS_&) = delete;  \
    KLASS_(const KLASS_&) = delete

#define FABULOUS_DELETE_MOVE(KLASS_)            \
    KLASS_& operator=(KLASS_&&) = delete;       \
    KLASS_(KLASS_&&) = delete


template<class T, std::size_t N>
constexpr std::size_t array_size(const T (&)[N])
{
    return N;
}

/***************************************/
/********** string split ***************/

/**
 * \brief split a string on a specific separator
 *
 * \param line the string to be splitted
 * \param sep the separator
 */
inline std::vector<std::string> split(const std::string &line, char sep = ' ')
{
    std::stringstream ss{line};
    std::string token;
    std::vector<std::string> res;

    while (std::getline(ss, token, sep)) {
        // empty token each time there is several separators following each other :
        if (token != "") {
            res.emplace_back(token);
        }
    }
    return res;
}

/* ******************************************************* */
/* ******************** Real Part ************************ */

template< class S,
          class P = typename S::value_type,
          class = typename std::enable_if<
              std::is_same<S, std::complex<P>>::value>::type >
inline P real(S a) { return a.real(); }

template< class S,
          class = typename std::enable_if<
              std::is_same<S, float>::value ||
              std::is_same<S, double>::value>::type >
inline S real(S a) { return a; }



/* ******************************************************* */
/* ******************** Conj ************************ */

template< class S,
          class P = typename S::value_type,
          class = typename std::enable_if<
              std::is_same<S, std::complex<P>>::value>::type >
inline S conj(S a) { return std::conj(a); }

template< class S,
          class = typename std::enable_if<
              std::is_same<S, float>::value ||
              std::is_same<S, double>::value>::type >
inline S conj(S a) { return a; }


/* ******************************************************* */
/* ******************** array inverse ******************** */

/** \brief compute the inverse of each entry in an array of scalars. */
template<class P>
std::vector<P> array_inverse(const std::vector<P> &arr)
{
    std::vector<P> inv;
    std::transform(
        arr.begin(), arr.end(),
        std::back_inserter(inv),
        [](const P &a)-> P { return P{1.0}/a; }
    );
    return inv;
}

/* ******************************************************* */
/* ******************** is nan ******************** */
template<class P>
bool isnan(const std::complex<P> &v)
{
    return std::isnan(v.real()) || std::isnan(v.imag());
}

inline bool isnan(const float &a)
{
    return std::isnan(a);
}

inline bool isnan(const double &a)
{
    return std::isnan(a);
}


template<class Vector, class P>
std::tuple<P,P,bool> min_max_conv(const Vector &v, const std::vector<P> &epsilon)
{
    bool conv = true;
    P min = std::numeric_limits<P>::max();
    P max = P{0.0};
    std::vector<P> norm_v = v.cwise_norm();

    for (unsigned i = 0; i < norm_v.size(); ++i) {
        const P & n_i = norm_v[i];
        const auto &epsilon_i = (epsilon.size() > 1) ? epsilon[i] : epsilon.front();

        min = std::min( n_i, min );
        max = std::max( n_i, max );
        if (n_i > epsilon_i) {
            conv = false;
        }
    }
    return std::make_tuple(min, max, conv);
}

template<class Vector, class P = typename Vector::primary_type>
std::pair<P,P> min_max_norm(const Vector &v)
{
    P min = std::numeric_limits<P>::max();
    P max = P{0.0};
    std::vector<P> norm_v = v.cwise_norm();

    for (unsigned i = 0; i < norm_v.size(); ++i) {
        const P & n_i = norm_v[i];

        min = std::min( n_i, min );
        max = std::max( n_i, max );
    }
    return std::make_pair(min, max);
}

template<class> class Block;

template<class Vector, class Precond, class Base, class S>
void X_plus_MVY(Vector &X, const Precond &M, Base &base_V, const Block<S> &Y)
{
    const int nrhs = Y.get_nb_col();
    if ( M ) {
        Vector MVY{X, nrhs};
        Vector VY{X, nrhs};
        VY.zero();
        BASE_AXPY(VY, base_V, Y); // VY <- base_V * Y;
        M(VY, MVY); // MVY <- M*VY
        X.cwise_axpy(MVY);
    } else {
        BASE_AXPY(X, base_V, Y);
    }
}

/**
 * \brief Test if the current solution is accurate enough.
 *
 * It will works only if the RHS are scaled,
 * (i.e each column vector of B has a norm of 1).
 * If the epsilon conditon is completed, at the end, X0 contains
 * Base*Y + X0, or Precond*Base*Y + X0 if a right precond is used.
 *
 * \param A Matrix provided by the user
 * \param M Preconditioner provided by the user
 * \param X0 initial guess
 * \param B RHS (each column vector of B must be of norm 1)
 * \param base_V current base
 * \param Y current solution of least square problem
 * \param epsilon target accuracy
 */
template<class Matrix, class Precond,
         class Vector, class Base,
         class S, class P  >
bool check_current_solution(const Matrix &A, const Precond &M,
                            const Vector &X0, const Vector &B,
                            const Base &base_V, const Block<S> &Y,
                            const std::vector<P> &epsilon)
{
    const int nrhs = B.get_nb_col();
    Vector X{X0, nrhs};
    X.copy(X0);
    X_plus_MVY(X, M, base_V, Y);

    Vector R{X0, nrhs};     // Then ||A*X - B||
    R.copy(B);
    A(X, R, S{-1.0}, S{1.0}); // R <- B - A*X

    auto MinMaxConv = min_max_conv(R, epsilon);
    std::cerr<<"\tReal Residual      :"
             <<" Min " << std::get<0>(MinMaxConv)
             <<"\tMax "<< std::get<1>(MinMaxConv) << "\n";

    bool convergence = std::get<2>(MinMaxConv);
    if (convergence) {
        std::cerr<<"CONVERGENCE_SUCCESS !\n";
    } else {
        std::cerr<<"CONVERGENCE_FAILURE !\n";
    }
    return convergence;
}


} // end namespace fabulous

#endif // FABULOUS_UTILS_HPP
