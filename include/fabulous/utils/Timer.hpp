#ifndef FABULOUS_TIMER_HPP
#define FABULOUS_TIMER_HPP

#include <chrono>
/*! \brief The Maphys++ library contains special timers that disapear at compilation if unused */
#ifndef FABULOUS_USE_MAPHYSPP_TIMER

#define TIMER_TRACE            ;
#define TIMER_MVP              ;
#define TIMER_ORTHO            ;
#define TIMER_RESTART          ;
#define TIMER_LSQR             ;
#define TIMER_COMPUTE_SOLUTION ;

#else // FABULOUS_USE_MAPHYSPP_TIMER

#ifndef TIMER_LEVEL_MAX
#define TIMER_LEVEL_MAX 101
#endif

#ifndef TIMER_LEVEL_MIN
#define TIMER_LEVEL_MIN 0
#endif

#include <Timer.hpp>
/*! \brief Timer to created an exhaustive chronology of function calls */
#define TIMER_TRACE            timer::Timer<TIMER_ALL>  t((std::string)(__func__));
/*! \brief Timer for matrix vector products */
#define TIMER_MVP              timer::Timer<TIMER_MAIN> t("mvp");
/*! \brief Timer for orthogonalisation process */
#define TIMER_ORTHO            timer::Timer<TIMER_MAIN> t("orthogonalisation");
/*! \brief Timer for restart process */
#define TIMER_RESTART          timer::Timer<TIMER_MAIN> t("restart");
/*! \brief Timer for the solve of the least square problem */
#define TIMER_LSQR             timer::Timer<TIMER_MAIN> t("least square");
/*! \brief Timer for the solution update */
#define TIMER_COMPUTE_SOLUTION timer::Timer<TIMER_MAIN> t("update solution");

#define TIMER_MAIN 100
#define TIMER_ALL 1000

#endif // FABULOUS_USE_MAPHYSPP_TIMER

namespace fabulous {
struct Timer;
}

#include "fabulous/utils/Error.hpp"

namespace fabulous {

/*! \brief time measurement facility (stopwatch) */
struct Timer
{
public:
    using Clock = std::chrono::steady_clock;
    using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;
    using Duration = std::chrono::duration<double>;

private:
    bool _running;
    Clock _clock;
    TimePoint _last_start;
    Duration _length;

private:
    static double duration_to_seconds(Duration d)
    {
        auto m = std::chrono::duration_cast<std::chrono::microseconds>(d);
        return m.count() / 1000000.0;
    }

    static double duration_to_micro_seconds(Duration d)
    {
        auto m = std::chrono::duration_cast<std::chrono::nanoseconds>(d);
        return m.count() / 1000.0;
    }

public:
    Timer():
        _running{false},
        _length{Duration::zero()}
    {
    }


    /*! \brief reset the stopwatch duration to 0 */
    void reset()
    {
        _running = false;
        _length = Duration::zero();
    }

    /*! \brief start the stopwatch */
    void start()
    {
        if (_running) {
            FABULOUS_THROW(Internal, "this timer is already started");
        }
        _last_start = _clock.now();
        _running = true;
    }

    /*! \brief stop the stopwatch */
    void stop()
    {
        if (!_running) {
            FABULOUS_THROW(Internal, "this timer is not running");
        }
        TimePoint tsp = _clock.now();
        _length += (tsp - _last_start);
        _running = false;
    }

    /*! \brief get the stopwatch time in seconds */
    double get_length()
    {
        if (_running) {
            TimePoint tsp = _clock.now();
            Duration length = _length + (tsp - _last_start);
            return duration_to_seconds(length);
        }
        return duration_to_seconds(_length);
    }

    /*! \brief get the stopwatch time in micro-seconds */
    double get_micro_length()
    {
        if (_running) {
            TimePoint tsp = _clock.now();
            Duration length = _length + (tsp - _last_start);
            return duration_to_micro_seconds(length);
        }
        return duration_to_micro_seconds(_length);
    }

}; // end class Timer

} // end namespace fabulous

#endif // FABULOUS_TIMER_HPP
