#ifndef FABULOUS_META_HPP
#define FABULOUS_META_HPP

#include <complex>

namespace fabulous {
template<class T> struct Type;
}

#include "fabulous/utils/Traits.hpp"

namespace fabulous {

/**
 * \brief meta-wrapper for a c++ %type
 */
template <class T>
struct Type
{
    using type = T;

    template<class U>
    constexpr bool operator==(const Type<U>&) const { return std::is_same<T,U>::value; }

    template<class U>
    constexpr bool operator!=(const Type<U>&) const { return !std::is_same<T,U>::value; }
};

/**
 * \brief return Type of parameter
 */
template<class T>
Type<T> get_type(const T&)
{
    return Type<T>{};
}

template<class U>
constexpr bool is_complex(const Type<U>&)
{
    return is_complex_t<U>::value;
}

template<class U>
constexpr bool is_real(const Type<U>&)
{
    return is_real_t<U>::value;
}

} // end namespace fabulous

#endif // FABULOUS_META_HPP
