#ifndef FABULOUS_LOGGER_HPP
#define FABULOUS_LOGGER_HPP

#include <iostream>
#include <cstdio>
#include <vector>
#include <fstream>
#include <inttypes.h>


namespace fabulous {
struct LogEntry;
class Logger;
}

#include "./Utils.hpp"
#include "./Timer.hpp"
#include "./Color.hpp"
#include "./Error.hpp"
#include "./Traits.hpp"
#include "fabulous/kernel/flops.hpp"

namespace fabulous {

/*! \brief Log information about memory allocation */
struct MemoryLogEntry
{
    std::string name;
    int M;
    int N;
    bool alloc;
    size_t elem_size;
    size_t size_B;
    double size_MB;
    double size_GB;

    MemoryLogEntry(const std::string &name_, bool alloc_, int M_, int N_, size_t elem_size_):
        name{name_},
        M{M_}, N{N_}, alloc{alloc_},
        elem_size{elem_size_}
    {
        size_B = elem_size * M * N;
        size_MB = size_B / (1000. * 1000.);
        size_GB = size_MB / 1000.;
    }
};

/*! \brief Log information about kernel calls */
struct KernelLogEntry
{
    std::string name;
    int M;
    int N;
    int K;
    int IB;
    double flops;
    double micro_seconds;
    KernelLogEntry(const std::string &name_,
                   int M_, int N_, int K_, int IB_,
                   double flops_, double micro_seconds_):
        name{name_},
        M{M_}, N{N_}, K{K_}, IB{IB_},
        flops{flops_}, micro_seconds{micro_seconds_}
    {
    }
};

/*! \brief Store information about memory allocated by the library */
struct MemoryPeakRecord
{
    size_t peak;
    double peak_MB;
    double peak_GB;

    size_t current;
    double current_MB;
    double current_GB;

    MemoryPeakRecord():
        peak{0}, peak_MB{0.0}, peak_GB{0.0},
        current{0}, current_MB{0.0}, current_GB{0.0}
    {
    }
};

using KernelLog = std::vector<KernelLogEntry>;
using MemoryLog = std::vector<MemoryLogEntry>;

inline KernelLog &get_kernel_log()
{
    static KernelLog log;
    return log;
}

inline MemoryLog &get_memory_log()
{
    static MemoryLog log;
    return log;
}

inline MemoryPeakRecord &get_peak_record()
{
    static MemoryPeakRecord rec;
    return rec;
}

inline void write_kernel_log(const std::string name)
{
    std::ofstream file{name, std::ios::out};
    if (file.is_open()) {
        file << "name,M,N,K,IB,flops,micro_seconds\n";
        const auto &log = get_kernel_log();
        for (auto &e : log) {
            file << e.name <<","<<e.M<<","<<e.N<<","<<e.K<<","
                 <<e.IB<<","<<e.flops<<","<<e.micro_seconds<<"\n";
        }
    } else {
        std::stringstream ss;
        ss << "Cannot open '" << name << "': '" << strerror(errno)<<"'";
        error(ss.str());
    }
}

inline void write_memory_log(const std::string name)
{
    std::ofstream file{name, std::ios::out};
    if (file.is_open()) {
        file << "name,M,N,alloc_free,elem_size,size_B,size_MB,size_GB\n";
        const auto &log = get_memory_log();
        for (auto &e : log) {
            file << e.name <<","
                 <<e.M<<","<<e.N<<","
                 << (e.alloc ? "ALLOC" : "FREE" ) << ","
                 <<e.elem_size<<","
                 <<e.size_B<<","<<e.size_MB<<","<<e.size_GB<<"\n";
        }
    } else {
        std::stringstream ss;
        ss << "Cannot open '" << name << "': '" << strerror(errno)<<"'";
        error(ss.str());
    }
}

#define FABULOUS_KERNEL_LOG(Kernel_, M_, N_, K_, IB_, Flops_, Time_)    \
    do {                                                                \
        const std::string name = #Kernel_;                              \
        if ( name == "gemm" || name == "dot"                            \
             || name == "axpy" || name == "scal") {                     \
            break /*filter a bit */;                                    \
        }                                                               \
        ::fabulous::get_kernel_log().emplace_back(                      \
            name, (M_), (N_), (K_), (IB_), (Flops_), (Time_));          \
    }while(0)

#ifdef FABULOUS_DEBUG_MODE

# ifndef FABULOUS_KERNEL_LOGGING
#  define FABULOUS_KERNEL_LOGGING 1
# endif

# ifndef FABULOUS_MEMORY_LOGGING
#  define FABULOUS_MEMORY_LOGGING 1
# endif

#endif

#if FABULOUS_KERNEL_LOGGING
# define FABULOUS_BEGIN_KERNEL_PERF                     \
    {   ::fabulous::Timer _timer_kern_perf_tmp__;       \
    _timer_kern_perf_tmp__.start()                      \

# define FABULOUS_END_KERNEL_PERF(Kernel_, S_, M_, N_, K_, IB_)         \
    _timer_kern_perf_tmp__.stop();                                      \
    FABULOUS_KERNEL_LOG(                                                \
        Kernel_, (M_), (N_), (K_), (IB_),                               \
        (::fabulous::lapacke::flops::Kernel_<S_>((M_),(N_),(K_))),      \
        (_timer_kern_perf_tmp__.get_micro_length())  );   }

#else
# define FABULOUS_BEGIN_KERNEL_PERF      {
# define FABULOUS_END_KERNEL_PERF(...)   }
#endif

#if FABULOUS_MEMORY_LOGGING
# define FABULOUS_LOG_ALLOC(name, M, N, esize)          \
    do {                                                \
        ::fabulous::get_memory_log()                    \
            .emplace_back((name),true,(M),(N),(esize)); \
        auto &pk = ::fabulous::get_peak_record();       \
        pk.current += esize*M*N;                        \
        pk.peak = std::max(pk.current, pk.peak);        \
        pk.current_MB = pk.current / (1000. * 1000.);   \
        pk.current_GB = pk.current_MB / 1000.;          \
        pk.peak_MB = pk.peak / (1000. * 1000.);         \
        pk.peak_GB = pk.peak_MB / 1000.;                \
    } while(0)

# define FABULOUS_LOG_FREE(name, M, N, esize)                   \
    do {                                                        \
        ::fabulous::get_memory_log()                            \
            .emplace_back((name),false,(M),(N),(esize));        \
        auto &pk = ::fabulous::get_peak_record();               \
        pk.current -= esize*M*N;                                \
        pk.current_MB = pk.current / (1000. * 1000.);           \
        pk.current_GB = pk.current_MB / 1000.;                  \
    } while(0)

#else
# define FABULOUS_LOG_ALLOC(...)
# define FABULOUS_LOG_FREE(...)
#endif

#ifndef FABULOUS_MAX_USER_LOG_SIZE
# define FABULOUS_MAX_USER_LOG_SIZE 4
#endif

/*! \brief Store timing information about a step of an iteration */
struct LogIterationStep
{
private:
    Timer _timer;
public:
    union {
        double time; /*!< time spent in the given step */
        double fp; /*!< generic floating point information */
    };
    union {
        int64_t flops; /*!< number of flops done in the given step */
        int64_t integ; /*!< generic integer information */
    };

    inline void reset()
    {
        _timer.reset();
        time = 0.0;
        flops = 0;
    }

    inline void begin()
    {
        _timer.start();
    }

    inline void end(int64_t nb_flops)
    {
        _timer.stop();
        flops += nb_flops;
        time = _timer.get_length();
    }

    inline LogIterationStep():
        _timer{},
        time{0.0},
        flops(0L)
    {
    }
};

/*! \brief container with information about one iteration */
struct LogEntry
{
public:
    int global_iteration; /*!< global iteration taking restart into account */
    int restart_num;  /*!< number of restart this is iteration is in */
    int local_iteration; /*!< current iteration in arnoldi procedure */
    int krylov_space_size; /*!< Index of Current iteration*/
    int nb_mvp; /*!< Total Number of mat vect computed*/
    int current_block_size; /*!< Number of vectors to extend the space*/
    double min; /*!< Arnoldi Minimal norm reached at this iteration*/
    double max; /*!< Arnoldi Maximum norm reached at this iteration*/

    double total_time; /*!< total time for the whole solve */
    double iter_time; /*!< total time for this iteration */

    LogIterationStep mvp;
    LogIterationStep precond;
    LogIterationStep ortho;
    LogIterationStep facto;
    LogIterationStep least_square;
    LogIterationStep ib;
    std::vector<LogIterationStep> user;

    static constexpr const char *log_header =
        "global_iteration,restart_num,local_iteration,"
        "krylov_space_size,nb_mvp,current_block_size,"
        "minRes,maxRes,total_time,iter_time,"
        "mvp_time,mvp_flops,precond_time,precond_flops,"
        "ortho_time,ortho_flops,facto_time,facto_flops,"
        "least_square_time,least_square_flops,ib_time,ib_flops,name";

    void print_header(std::ostream &o = std::cout) const
    {
        o << log_header;
        for (unsigned i = 0; i < user.size(); ++i) {
            o << ",user" << i << "_fp,"
              << "user" << i << "_integ";
        }
        o << "\n";
    }

    void print_header(FILE *o) const
    {
        fprintf(o, "%s", log_header);
        for (unsigned i = 0; i < user.size(); ++i) {
            fprintf(o, ",user%u_fp,user%u_integ", i, i);
        }
        fprintf(o, "\n");
    }

    void print(const std::string &name, std::ostream &o = std::cout) const
    {
        o << global_iteration<<","<< restart_num<<","<<local_iteration<<","
          << krylov_space_size<<","<< nb_mvp<<","<< current_block_size<<","
          << min<<","<< max<<","<< total_time << "," << iter_time <<","

          << mvp.time    << ","  << mvp.flops<<","
          << precond.time<< "," << precond.flops<<","

          << ortho.time << "," << ortho.flops <<","
          << facto.time << "," << facto.flops<<","

          << least_square.time << "," << least_square.flops<<","
          << ib.time           << "," << ib.flops <<","<< name;

        for (unsigned i = 0; i < user.size(); ++i) {
            o << ","
              << user[i].fp <<","
              << user[i].integ;
        }
        o << "\n";
    }

    void print(FILE *o, const char *name) const
    {
        fprintf(
            o,
            "%d,%d,"    // glob_iter, local_iter
            "%d,%d,%d," // space, mvp, block_size
            "%g,%g,%g,%g," // min, max, total_time, iter_time,
            "%g,%" PRId64 "," // mvp
            "%g,%" PRId64 "," // precond
            "%g,%" PRId64 "," // ortho
            "%g,%" PRId64 "," // facto
            "%g,%" PRId64 "," // least_square
            "%g,%" PRId64 "," // ib
            "%s\n", // name
            global_iteration, local_iteration,
            krylov_space_size, nb_mvp, current_block_size,
            min, max, total_time, iter_time,
            mvp.time, mvp.flops,
            precond.time, precond.flops,
            ortho.time, ortho.flops,
            facto.time, facto.flops,
            least_square.time, least_square.flops,
            ib.time, ib.flops,
            name
        );

        for (unsigned i = 0; i < user.size(); ++i) {
            fprintf(o, ",%g,%" PRId64 , user[i].fp, user[i].integ);
        }
        fprintf(o, "\n");
    }
};

/*! \brief container for informations during the solve at each iteration. */
class Logger
{
private:

    double _total_time; /*!< total time (over multiple restart) */

    Timer _global_timer;
    Timer _iterations_timer;

    LogIterationStep _mvp;
    LogIterationStep _precond;
    LogIterationStep _ortho;
    LogIterationStep _facto;
    LogIterationStep _least_square;
    LogIterationStep _ib;
    std::vector<LogIterationStep> _user;

    int _global_iteration;
    int _restart_num;
    int _total_mvp;
    int _last_mvp;
    bool _convergence; /*!< whether convergence was reached */
    bool _quiet; /*!< no output if true */

    std::vector<LogEntry> _iterations;
    std::vector<double> _array;

public:
    Logger():
        _total_time{0.0},
        _user(FABULOUS_MAX_USER_LOG_SIZE),
        _global_iteration{0},
        _restart_num{0},
        _total_mvp{0},
        _last_mvp{0},
        _convergence{false},
        _quiet{false}
    {
    }

    /*!
     * calling this function after the solver has been started
     * yield undefined behaviour
     */
    void resize_user_data(unsigned size)
    {
        _user.resize(size);
    }

    void set_quiet(bool val)
    {
        _quiet = val;
    }

    int get_nb_iterations() const { return _iterations.size(); }
    int get_nb_mvp() const { return _total_mvp; }

    void reset()
    {
        global_reset();
        iteration_reset();
    }

    void global_reset()
    {
        _global_iteration = 0;
        _total_mvp = 0;
        _last_mvp = 0;
        _restart_num = 0;

        _iterations.clear();
        _array.clear();
        _global_timer.reset();
    }

    void iteration_reset()
    {
        _iterations_timer.reset();
        _mvp.reset();
        _precond.reset();
        _ortho.reset();
        _facto.reset();
        _least_square.reset();
        _ib.reset();

        for (unsigned i = 0; i < _user.size(); ++i) {
            _user[i].reset();
        }
    }

    const LogEntry &get_entry(int i) const { return _iterations.at(i); }
    const std::vector<LogEntry> &get_iterations() const { return _iterations; }
    double get_total_time() const { return _total_time; }

    /* -------------------- notify BEGIN --------------------------------------*/

    void notify_iteration_begin(int iteration_id, int size_krylov_space)
    {
        if (!_quiet) {
            std::cerr<<"\nStart of Iteration ["
                     <<Color::note<<iteration_id<<" :: "
                     <<size_krylov_space<<Color::reset<<"]\n";
        }
        if (_global_iteration == 0) {
            global_reset();
            _global_timer.start();
        }
        if (iteration_id == 0) {
            _last_mvp = 0;
        }
        iteration_reset();
        _iterations_timer.start();
    }

    void notify_mvp_begin()          { _mvp.begin(); }
    void notify_precond_begin()      { _precond.begin(); }
    void notify_ortho_begin()        { _ortho.begin(); }
    void notify_facto_begin()        { _facto.begin(); }
    void notify_least_square_begin() { _least_square.begin(); }
    void notify_ib_begin()           { _ib.begin(); }

    /* -------------------- notify END --------------------------------------*/

    void notify_mvp_end(int64_t flops) { _mvp.end(flops); }
    void notify_precond_end(int64_t flops) { _precond.end(flops); }
    void notify_ortho_end(int64_t flops) {  _ortho.end(flops); }
    void notify_facto_end(int64_t flops) { _facto.end(flops); }
    void notify_least_square_end(int64_t flops) { _least_square.end(flops); }
    void notify_ib_end(int64_t flops) { _ib.end(flops);  }

    /**
     * \brief Add an iteration to the list.
     */
    template<class P, class = enable_if_t<is_real_t<P>::value>>
    void notify_iteration_end(int iter_id, int size_krylov_space, int mvp,
                              std::tuple<P, P, bool> min_max_conv)
    {
        _total_time = _global_timer.get_length();
        _iterations_timer.stop();
        double iter_time = _iterations_timer.get_length();
        auto min = std::get<0>(min_max_conv);
        auto max = std::get<1>(min_max_conv);

        int mvp_diff = mvp - _last_mvp;
        _last_mvp = mvp;
        _total_mvp += mvp_diff;

        ++ _global_iteration;
        if (iter_id == 0) {
            ++ _restart_num;
        }
        _iterations.push_back(
            LogEntry{ _global_iteration, _restart_num, iter_id, size_krylov_space,
                    _total_mvp, mvp_diff, (double)min, (double)max, _total_time, iter_time,
                    _mvp, _precond, _ortho, _facto, _least_square, _ib, _user }
        );
        if (std::isnan(min) || std::isnan(max)) {
            FABULOUS_THROW(Numeric, "residual norm is NAN (Not A Number IEEE 754)");
        }

        if (!_quiet) {
            FABULOUS_NOTE(
                "\t\t "<<Color::red<<"MIN="<<Color::reset<<std::scientific<<min
                <<"\t"<<Color::red<<"MAX="<<Color::reset<<std::scientific<<max
            );
            std::cerr<< "End of Iteration ["<<Color::note<<iter_id<<" :: "
                     << size_krylov_space<<Color::reset<<"]\n\n";
        }
    }

    void set_iteration_user_data(unsigned idx, double fp_val, int64_t integ_val)
    {
        if ( idx >= _user.size() ) {
            FABULOUS_THROW(Parameter, "Out of range user log slot");
        }
        _user[idx].fp = fp_val;
        _user[idx].integ = integ_val;
    }

    /**
     * \brief Write down the datas logged as an array of double
     */
    const double *write_down_array()
    {
        int size = _iterations.size();
        _array.resize(3*size);

        for (int i = 0; i < size; ++i) {
            _array[3*i + 0] = _iterations[i].min;
            _array[3*i + 1] = _iterations[i].max;
            _array[3*i + 2] = _iterations[i].iter_time;
        }
        return _array.data();
    }

    void print(std::string name="log", std::ostream& o = std::cerr) const
    {
        if ( _iterations.size() > 0) {
            _iterations.front().print_header(o);
            for (auto&&it : _iterations)
                it.print(name, o);
        }
    }

    void print(FILE *o, const char *name="log") const
    {
        if ( _iterations.size() > 0) {
            _iterations.front().print_header(o);
            for (auto&&it : _iterations)
                it.print(o, name);
        }
    }

    void set_convergence(bool convergence)
    {
        _convergence = convergence;
    }

    bool get_convergence() const
    {
        return _convergence;
    }

}; // end class Logger

} // end namespace fabulous

#endif // FABULOUS_LOGGER_HPP
