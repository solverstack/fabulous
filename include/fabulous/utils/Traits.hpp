#ifndef FABULOUS_TRAITS_HPP
#define FABULOUS_TRAITS_HPP

#include <type_traits>
#include <complex>
#include <utility>

namespace fabulous {

template<bool B, class T=void>
using enable_if_t = typename std::enable_if<B,T>::type;

template<class ARNOLDI, class HESSENBERG> struct arnoldiXhessenberg;
template<class HESSENBERG, class RESTARTER> struct hessenbergXrestarter;
namespace bgmres {
template<class> class ClassicRestarter;
template<class> class DeflatedRestarter;
}

namespace bgmres {
template<class HESSENBERG, class S> class ArnoldiIB;
template<class HESSENBERG, class S> class ArnoldiDR;
template<class HESSENBERG, class S> class ArnoldiIBDR;

template<class> class HessDR;
template<class> class HessQR;
template<class> class HessQRDR;
template<class> class HessIB;
template<class> class HessQRIB;
template<class> class HessIBDR;
template<class> class HessQRIBDR;

#ifdef FABULOUS_USE_CHAMELEON
template<class> class HessDR_CHAM;
template<class> class HessQR_CHAMSUB;
template<class> class HessIB_CHAM;
#endif

} // end namespace bgmres
namespace bgcro {
template<class> class DeflatedRestarter;
}

namespace bgcro {
template<class HESSENBERG, class S> class ArnoldiIBGCRO;

template<class> class HessGCRO;

} // end namespace bgcro
} // end namespace fabulous

namespace fabulous {

/**
 * \brief boolean field 'value' is set whether ARNOLDI is compatible with HESSENBERG 
 * and whether HESSENBERG is compatible with RESTART in the gcro algorithm case
 */

template<class S>
struct hessenbergXrestarter<bgcro::HessGCRO<S>, bgmres::ClassicRestarter<S>>: public std::true_type {};
template<class S>
struct hessenbergXrestarter<bgcro::HessGCRO<S>, bgcro::DeflatedRestarter<S>>: public std::true_type {};
template<class S>
struct arnoldiXhessenberg<bgcro::ArnoldiIBGCRO<bgcro::HessGCRO<S>, S>, bgcro::HessGCRO<S>>: public std::true_type {};

/**
 * \brief boolean field 'value' is set whether ARNOLDI is compatible with HESSENBERG
 */
template<class ARNOLDI, class HESSENBERG>
struct arnoldiXhessenberg : public std::false_type {};

#define FABULOUS_ARNOLDI_X_HESSENBERG(ARNOLDI, HESSENBERG)              \
    template<class S>                                                   \
    struct arnoldiXhessenberg<bgmres::ARNOLDI<bgmres::HESSENBERG<S>, S>, bgmres::HESSENBERG<S>> \
        : public std::true_type {}                                      \

FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiDR, HessDR);
FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiDR, HessQR);
FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiDR, HessQRDR);

#ifdef FABULOUS_USE_CHAMELEON
FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiDR, HessDR_CHAM);
FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiDR, HessQR_CHAMSUB);
FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiIB, HessIB_CHAM);
#endif

FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiIB,   HessIB);
FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiIBDR, HessQRIB);
FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiIBDR, HessIBDR);
FABULOUS_ARNOLDI_X_HESSENBERG(ArnoldiIBDR, HessQRIBDR);

/**
 * \brief boolean field 'value' is set whether HESSENBERG is compatible with RESTARTER
 */
template<class HESSENBERG, class RESTART>
struct hessenbergXrestarter : public std::false_type {};

#define FABULOUS_HESSENBERG_X_RESTARTER(HESSENBERG, RESTARTER)          \
    template<class S>                                                   \
    struct hessenbergXrestarter<bgmres::HESSENBERG<S>, bgmres::RESTARTER<S>> \
        : public std::true_type {}                                      \

FABULOUS_HESSENBERG_X_RESTARTER(HessDR, ClassicRestarter);
FABULOUS_HESSENBERG_X_RESTARTER(HessDR, DeflatedRestarter);
FABULOUS_HESSENBERG_X_RESTARTER(HessQRDR, ClassicRestarter);
FABULOUS_HESSENBERG_X_RESTARTER(HessQRDR, DeflatedRestarter);

FABULOUS_HESSENBERG_X_RESTARTER(HessQRIB, ClassicRestarter);

FABULOUS_HESSENBERG_X_RESTARTER(HessIBDR, ClassicRestarter);
FABULOUS_HESSENBERG_X_RESTARTER(HessIBDR, DeflatedRestarter);

FABULOUS_HESSENBERG_X_RESTARTER(HessQRIBDR, ClassicRestarter);
FABULOUS_HESSENBERG_X_RESTARTER(HessQRIBDR, DeflatedRestarter);

FABULOUS_HESSENBERG_X_RESTARTER(HessQR, ClassicRestarter);
FABULOUS_HESSENBERG_X_RESTARTER(HessIB, ClassicRestarter);

#ifdef FABULOUS_USE_CHAMELEON

FABULOUS_HESSENBERG_X_RESTARTER(HessDR_CHAM, ClassicRestarter);
FABULOUS_HESSENBERG_X_RESTARTER(HessDR_CHAM, DeflatedRestarter);

FABULOUS_HESSENBERG_X_RESTARTER(HessQR_CHAMSUB, ClassicRestarter);
FABULOUS_HESSENBERG_X_RESTARTER(HessIB_CHAM, ClassicRestarter);

#endif // FABULOUS_USE_CHAMELEON

/**
 * \brief tell if hessenberg handle Inexact Breakdown
 *
 * This class have a boolean const static member 'value'
 * telling wether the Hessenberg handle Inexact Breakdown.
 */
template<class Hessenberg> struct handle_ib_t : public std::false_type {};
template<class S> struct handle_ib_t<bgmres::HessIB<S>>      : public std::true_type {};
template<class S> struct handle_ib_t<bgmres::HessQRIB<S>>    : public std::true_type {};
template<class S> struct handle_ib_t<bgmres::HessIBDR<S>>    : public std::true_type {};
template<class S> struct handle_ib_t<bgmres::HessQRIBDR<S>>  : public std::true_type {};
#ifdef FABULOUS_USE_CHAMELEON
template<class S> struct handle_ib_t<bgmres::HessIB_CHAM<S>> : public std::true_type {};
#endif // FABULOUS_USE_CHAMELEON

/**
 * \brief trait to check is argument type is a supported complex arithmetic type
 */
template<class S> struct is_complex_t : public std::false_type {};
template<> struct is_complex_t<std::complex<float> >  : public std::true_type {};
template<> struct is_complex_t<std::complex<double> > : public std::true_type {};

/**
 * \brief trait to check is argument type is a supported real arithmetic type
 */
template<class S> struct is_real_t  : public std::false_type {};
template<> struct is_real_t<float>  : public std::true_type {};
template<> struct is_real_t<double> : public std::true_type {};

/*! \brief trait to test the presence of sub_vector() method */
struct has_sub_vector_test {
    template<typename T>
    static auto test(const T &v) -> decltype(v.sub_vector(0,0), std::true_type());

    template<typename>
    static auto test(... ) -> std::false_type;
};

/*! \brief trait to test the presence of get_local_dim() method */
struct has_get_local_dim_test {
    template<typename T>
    static auto test(const T &v) -> decltype(v.get_local_dim(), std::true_type());
    template<typename>
    static auto test(... ) -> std::false_type;
};

/*! \brief trait to test the presence of get_ptr() method */
struct has_get_ptr_test {
    template<typename T>
    static auto test(const T &v) -> decltype(v.get_ptr(), std::true_type());
    template<typename>
    static auto test(... ) -> std::false_type;
};

/*! \brief trait to test the presence of snorm() function */
struct has_snorm_test {
    template<typename T>
    static auto test(const T &v) -> decltype(snorm(v), std::true_type());
    template<typename>
    static auto test(... ) -> std::false_type;
};

/*! \brief trait to test the presence of sscale() function */
struct has_sscale_test {
    template<typename T>
    static auto test(T v) -> decltype(sscale(v, 0.0), std::true_type());
    template<typename>
    static auto test(... ) -> std::false_type;
};

/*! \brief trait to test the presence of sub_vector() method */
template<class T>
struct has_sub_vector_t : decltype(has_sub_vector_test::test<T>(std::declval<T>()))
{
};

/*! \brief trait to test the presence of get_local_dim() method */
template<class T>
struct has_get_local_dim_t : decltype(has_get_local_dim_test::test<T>(std::declval<T>()))
{
};

/*! \brief trait to test the presence of get_ptr() method */
template<class T>
struct has_get_ptr_t : decltype(has_get_ptr_test::test<T>(std::declval<T>()))
{
};

/*! \brief trait to test the presence of snorm() function */
template<class T>
struct has_snorm_t : decltype(has_snorm_test::test<T>(std::declval<T>()))
{
};

/*! \brief trait to test the presence of sscale() function */
template<class T>
struct has_sscale_t : decltype(has_sscale_test::test<T>(std::declval<T>()))
{
};

/*! \brief trait to distinguish between vectors that must use Base and BlockPW
 and vectors that must use BaseGeneric and GenericPW  */
template<class T>
struct is_optimizable_t
{
    static constexpr bool value = (has_sub_vector_t<T>::value and
                                   has_get_ptr_t<T>::value and
                                   has_get_local_dim_t<T>::value    );
};

} // end namespace fabulous

#endif // FABULOUS_TRAITS_HPP
