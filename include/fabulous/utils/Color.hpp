#ifndef FABULOUS_COLOR_HPP
#define FABULOUS_COLOR_HPP

#include <string>

namespace fabulous {

#define FABULOUS_COLOR_LIST(COLOR)              \
    COLOR(RED,      red,      91 )              \
    COLOR(BLUE,     blue,     34 )              \
    COLOR(GREEN,    green,    92 )              \
    COLOR(YELLOW,   yellow,   93 )              \
    COLOR(ERROR,    error,    01;31 )           \
    COLOR(WARNING,  warning,  01;35 )           \
    COLOR(IB_MARK2, ib_mark2, 01;93 )           \
    COLOR(IB_MARK3, ib_mark3, 01;92 )           \
    COLOR(NOTE,     note,     01;36 )           \
    COLOR(BOLD,     bold,     01 )              \
    COLOR(DEBUG,    debug,    93 )              \
    COLOR(RESET,    reset,    0  )              \

/* ******************* COLOR CLASS ********************* */

#define FABULOUS_COLOR_TO_STRING(name_capital_, name_, value_)          \
    static const constexpr char *name_capital_ = "\033["#value_"m";

#define FABULOUS_COLOR_TO_PTR_DECLARE(name_capital_, name_, value_)     \
    static const char *name_;

/**
 * \brief Template class in order to have static color variables in headers
 */
template<class>
struct ColorT
{
    FABULOUS_COLOR_LIST(FABULOUS_COLOR_TO_STRING)
    FABULOUS_COLOR_LIST(FABULOUS_COLOR_TO_PTR_DECLARE)
};

#define FABULOUS_COLOR_TO_PTR_DEFINE(name_capital_, name_, value_)      \
    template<class T> const char *ColorT<T>::name_ = ColorT::name_capital_;

FABULOUS_COLOR_LIST(FABULOUS_COLOR_TO_PTR_DEFINE)

/**
 * \brief Helper class to print colored ouput
 */
struct Color : public ColorT<void>
{
};

/* ******************* TOGGLE COLOR OUTPUT ********************* */

#define FABULOUS_COLOR_ENABLE(name_capital_, name_, value_)     \
    Color::name_ = Color::name_capital_;

#define FABULOUS_COLOR_DISABLE(name_capital_, name_, value_)    \
    Color::name_ = "";

/**
 * \brief toggle the color output
 *
 * \param enable_color boolean indicating the state of the colored output: <br/>
 *   - true: output is colored <br/>
 *   - false: output is not colored
 */
inline void set_output_colored(bool enable_color)
{
    if (enable_color)
    { FABULOUS_COLOR_LIST(FABULOUS_COLOR_ENABLE) }
    else
    { FABULOUS_COLOR_LIST(FABULOUS_COLOR_DISABLE) }
}

} // end namespace fabulous

#endif // FABULOUS_COLOR_HPP
