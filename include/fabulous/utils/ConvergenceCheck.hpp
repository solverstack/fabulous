#ifndef FABULOUS_CONVERGENCE_CHECK_HPP
#define FABULOUS_CONVERGENCE_CHECK_HPP

#include <limits>

#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Arithmetic.hpp"

#include "fabulous/kernel/blas.hpp"

namespace fabulous {

/* ******************************************************* */
/* *********** convergence debug helper ****************** */

template<class Matrix, class Vector>
auto eval_current_solution(const Matrix &A, const Vector &B, const Vector &X)
{
    using S = typename Vector::value_type;
    const int n = B.get_nb_col();
    Vector R{X, n};
    R.copy(B);
    A(X, R, S{-1.0}, S{1.0}); // Then R <- B - A*X
    return min_max_norm(R);
}

template<class Matrix, class Vector>
auto eval_current_solution2(const Matrix &A, const Vector &B, const Vector &X)
{
    auto min_max_norm = eval_current_solution(A, B, X);
    std::cerr << "Real residual : (min, max) = ";
    std::cerr << std::get<0>(min_max_norm) << ',' << std::get<1>(min_max_norm) << '\n';
}

/**
 * \brief This function compare two blocks of vectors of the same size
 *
 * Display the min and max of frobenius norm overs the vectors' differences
 *
 * \param block1 the first block
 * \param block2 the second block
 */
template<class S, class P = typename Arithmetik<S>::primary_type>
auto compare_blocks(const Block<S> &block1, const Block<S> &block2)
{
    P min = std::numeric_limits<P>::max();
    P max = P{0.0};

    if (block1.get_nb_col() != block2.get_nb_col()) {
        FABULOUS_FATAL_ERROR("col count of blocks not compatible\n");
        return std::pair<P,P>{-1.0, -1.0};
    }
    if (block1.get_nb_row() != block2.get_nb_row()) {
        FABULOUS_FATAL_ERROR("row count of blocks not compatible\n");
        return std::pair<P,P>{-1.0, -1.0};
    }

    for (int j = 0; j < block1.get_nb_col(); ++j) {
        P norm2_diff{0.0};
        for (int i = 0; i < block1.get_nb_row(); ++i) {
            norm2_diff += std::norm( block1.at(i, j) - block2.at(i, j) );
        }
        P norm_diff = std::sqrt(norm2_diff);

        min = std::min(norm_diff, min);
        max = std::max(norm_diff, max);
    }
    return std::make_pair(min, max);
}


/**
 * \brief This function compare two blocks of vectors of the same size
 *
 * Display the min and max of frobenius norm overs the vectors' relative differences
 *
 * \param block1 the first block
 * \param block2 the second block
 */
template<class S, class P = typename Arithmetik<S>::primary_type>
auto compare_blocks_normalized(const Block<S> &block1, const Block<S> &block2)
{
    P min = std::numeric_limits<P>::max();
    P max = P{0.0};

    if (block1.get_nb_col() != block2.get_nb_col()) {
        FABULOUS_FATAL_ERROR("col count of blocks not compatible\n");
        return std::pair<P,P>{-1.0, -1.0};
    }
    if (block1.get_nb_row() != block2.get_nb_row()) {
        FABULOUS_FATAL_ERROR("row count of blocks not compatible\n");
        return std::pair<P,P>{-1.0, -1.0};
    }

    for (int j = 0; j < block1.get_nb_col(); ++j) {
        P norm2_X{0.0};
        P norm2_diff{0.0};
        for (int i = 0; i < block1.get_nb_row(); ++i) {
            norm2_X    += std::norm( block1.at(i, j) );
            norm2_diff += std::norm( block1.at(i, j) - block2.at(i, j) );
        }
        P norm_diff = std::sqrt(norm2_diff);
        P norm_X = std::sqrt(norm2_X);
        P rnorm = norm_diff / norm_X;

        min = std::min(rnorm, min);
        max = std::max(rnorm, max);
    }
    return std::make_pair(min, max);
}


/**
 * \brief This function test the arnoldi equality : A*Vm = Vm+1 Hm
 *
 * \param A Input Matrix, need to implement
 * \param base Current base (correspond to V in above formula)
 * \param hess Current hessenberg, given through parent references.
 * \param nbRHS number of right hand side
 */
template<class Matrix, class Hessenberg, class S>
void check_arnoldi_formula(const Matrix &A, Base<S> &base, Hessenberg &hess, int nbRHS)
{
    int nm = hess.get_nb_vect();
    int dim = base.get_nb_row();
    FABULOUS_ASSERT( base.get_nb_vect() >= nm + nbRHS );

    Block<S> hessblock = hess.get_hess_block();
    Block<S> Vm = base.sub_block(0, 0, dim, nm);
    Block<S> VmP1 = base.sub_block(0, 0, dim, nm+nbRHS);
    Block<S> Hm1 = hessblock.sub_block(0, 0, nm+nbRHS, nm);

    FABULOUS_DEBUG("Vm.get_nb_col()="<<Vm.get_nb_col());
    FABULOUS_DEBUG("VmP1.get_nb_col()="<<VmP1.get_nb_col());

    // hess.debug_dump_true_HM(Hm1, nb_vect, nb_vect-block_size);

    Block<S> AVm{dim, nm}, VmP1Hm1{dim, nm};
    A(Vm, AVm);

    lapacke::gemm(VmP1Hm1.get_nb_row(), VmP1Hm1.get_nb_col(), VmP1.get_nb_col(),
                  VmP1.get_ptr(), VmP1.get_leading_dim(),
                  Hm1.get_ptr(), Hm1.get_leading_dim(),
                  VmP1Hm1.get_ptr(), VmP1Hm1.get_leading_dim());

    using P = typename Block<S>::primary_type;
    static const P N = A.get_inf_norm();
    auto MM = compare_blocks(AVm, VmP1Hm1);

    FABULOUS_DEBUG(Color::red << " >CHECK FORMULA< :: ("
                   <<MM.first<<", "<<MM.second<<")"<<Color::reset);
    FABULOUS_DEBUG(Color::red << " >CHECK FORMULA NORMALIZED< :: ("
                   <<MM.first/N<<", "<<MM.second/N<<")"<<Color::reset);
}

template<class Matrix, class Hessenberg, class S>
void check_arnoldi_formula_IB(const Matrix &A, Base<S> &base,
                              Hessenberg &FF, Block<S> &WP)
{
    int nm = FF.get_nb_vect();
    int dim = base.get_nb_row();
    int nbRHS = WP.get_nb_col();

    FABULOUS_ASSERT( base.get_nb_vect() >= nm );

    Block<S> Vm = base.sub_block(0, 0, dim, nm);
    Block<S> AVM{dim, nm};
    A(Vm, AVM);

    Block<S> VPW{dim, nm+nbRHS};
    VPW.copy(Vm);
    Block<S> vPW = VPW.sub_block(0, nm, dim, nbRHS);
    vPW.copy(WP);

    VPW.check_ortho("VPW");
    Block<S> VPWF{dim, nm};
    Block<S> hessblock = FF.get_hess_block();
    Block<S> F = hessblock.sub_block(0, 0, nm+nbRHS, nm);
    lapacke::gemm(VPWF.get_nb_row(), VPWF.get_nb_col(), VPW.get_nb_col(),
                  VPW.get_ptr(), VPW.get_leading_dim(),
                  F.get_ptr(), F.get_leading_dim(),
                  VPWF.get_ptr(), VPWF.get_leading_dim() );

    using P = typename Arithmetik<S>::primary_type;
    const P N = A.get_inf_norm();

    auto MM = compare_blocks(AVM, VPWF);
    FABULOUS_DEBUG(Color::red << " >CHECK FORMULA< :: ("
                   <<MM.first<<", "<<MM.second<<")"<<Color::reset);
    FABULOUS_DEBUG(Color::red << " >CHECK FORMULA NORMALIZED< :: ("
                   <<MM.first/N<<", "<<MM.second/N<<")"<<Color::reset);

}




template<class S, class P = typename Arithmetik<S>::primary_type>
void ortho_display(const Block<S> &block1, const Block<S> &block2, const std::string name = "")
{
    std::vector<P> esp;
    esp.push_back(P{1e-7});
    std::cerr.precision(3);
    std::cerr << std::scientific;
    Block<S> tmp{block1.get_nb_col(), block2.get_nb_col()};
    block1.dot(block2, tmp);
    auto mm = min_max_conv(tmp, esp);
    std::cerr << "Orthogonality " << name << " : " << std::get<0>(mm) << ',' << std::get<1>(mm) << '\n';
}

template<class S, class P = typename Arithmetik<S>::primary_type>
void ortho2_display(const Block<S> &block1, const std::string name = "")
{
    std::vector<P> esp;
    esp.push_back(P{1e-8});
    std::cerr.precision(3);
    std::cerr << std::scientific;
    const int n = block1.get_nb_col();
    Block<S> tmp{n, n};
    block1.dot(block1, tmp);
    for(int i = 0; i < n; i++) tmp(i, i) -= S{1.0};
    auto mm = min_max_conv(tmp, esp);
    std::cerr << "Orthogonality " << name << " : " << std::get<0>(mm) << ',' << std::get<1>(mm) << '\n';
}



template<class Matrix, class Hessenberg, class VectorPW, class S, class Precond, class Vector>
void check_arnoldi_formula_IB2(const Matrix &A, Base<Block<S>> &base,
                              Hessenberg &FF, VectorPW &WP,
                               Vector &Uk, Vector &Ck, const Precond &M)
{
    int nm = base.get_nb_vect();
    int dim = WP.concat_PW().get_nb_row();
    int nbRHS = WP.concat_PW().get_nb_col();
    const int k = Uk.get_nb_col();

    Vector Vm = base.sub_vector(0, nm);
    Vector AVM{dim, k + nm};
    Vector AVM2 = AVM.sub_block(0, k, dim, nm);
    if(k > 0){
        Vector AVM1 = AVM.sub_block(0, 0, dim, k);
        Vector MUk{};
        if ( M ) {
            MUk = Block<S>{Uk, k};
            M(Uk, MUk);
        } else {
            MUk = Uk;
        }
        A(MUk, AVM1);
        //ortho_display(Uk, Vm, "Uk^H * Vm ");
        //if(nm > nbRHS) ortho_display(Uk, base.sub_vector(nbRHS, nm), "Uk^H * V1:m ");
    }
    Vector MVm{};
    if ( M ) {
        MVm = Vector{Vm, nm};
        M(Vm, MVm);
    } else {
        MVm = Vm;
    }
    A(MVm, AVM2);

    Vector VPW{dim, k + nm + nbRHS};
    Vector P = WP.get_P();
    if(P.get_nb_col() > 0) {
        ortho2_display(P, "P^H * P ");
        ortho_display(P, WP.get_W(), "P^H * W ");
    }
    if(k > 0){
        ortho2_display(Ck, "Ck^H * Ck ");
        VPW.sub_block(0, 0, dim, k).copy(Ck);
        if(P.get_nb_col() > 0) {
            ortho_display(P, Ck, "P^H * Ck ");
        }
        ortho_display(WP.get_W(), Ck, "W^H * Ck ");
        ortho_display(Ck, Vm, "Ck^H * Vm ");
    }
    if(P.get_nb_col() > 0) {
        ortho_display(P, Vm, "P^H * Vm ");
    }
    ortho_display(WP.get_W(), Vm, "W^H * Vm ");
    ortho2_display(Vm, "Vm^H * Vm ");
    ortho2_display(WP.get_W(), "W^H * W ");
    VPW.sub_block(0, 0, dim, k).copy(Ck);
    VPW.sub_block(0, k, dim, nm).copy(Vm);
    VPW.sub_block(0, k + nm, dim, nbRHS).copy(WP.concat_PW());

    //VPW.check_ortho("VPW");
    Vector VPWF{dim, k + nm};
    Vector Fm = FF.get_F();
    VPWF.axpy(Fm, VPW);

    auto MM = compare_blocks(AVM, VPWF);
    std::cerr << "A * M * [Uk, Vm] = [Ck, Vm, PW] * Fm : ";
    std::cerr << std::get<0>(MM) << ',' << std::get<0>(MM) << '\n';
}

} // end namespace fabulous

#endif // FABULOUS_CONVERGENCE_CHECK_HPP
