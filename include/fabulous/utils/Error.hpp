#ifndef FABULOUS_ERROR_HPP
#define FABULOUS_ERROR_HPP

#include <iostream>
#include <sstream>
#include <string>
#include <exception>
#include <cstdio>
#include <execinfo.h>

#include <cstdlib>
#include <cstring>

#include "fabulous/utils/Color.hpp"
#include "fabulous/utils/Utils.hpp"

namespace fabulous {

#ifndef FABULOUS_USE_BACKTRACE
# define FABULOUS_USE_BACKTRACE 1
#endif

#define FABULOUS_MAX_BACKTRACE_SIZE 20

#define FABULOUS_ERROR_LIST(ERROR)                                      \
    ERROR(GENERIC,            Generic,           generic)               \
    ERROR(KERNEL,             Kernel,            kernel)                \
    ERROR(NUMERIC,            Numeric,           numeric)               \
    ERROR(NOT_IMPLEMENTED,    NotImplemented,    not_implemented)       \
    ERROR(INTERNAL,           Internal,          internal)              \
    ERROR(ASSERTION,          Assertion,         assertion)             \
    ERROR(PARAMETER,          Parameter,         parameter)             \
    ERROR(INPUT,              Input,             input)                 \
    ERROR(UNSUPPORTED,        Unsupported,       unsupported_operation) \
    ERROR(INVALID_ARITHMETIC, InvalidArithmetic, invalid_arithmetic)    \
    ERROR(FILE,               File,              file)                  \


#define FABULOUS_DEFINE_ERROR_ENUM_DECLARATOR_(CAPITAL_, CamelCase_, lower_case_) \
    FABULOUS_ERROR_##CAPITAL_,

enum error_code {
    FABULOUS_SUCCESS = 0,
    FABULOUS_ERROR_LIST(FABULOUS_DEFINE_ERROR_ENUM_DECLARATOR_)
};

#define FABULOUS_DEFINE_ERROR_STRING_CASE_(CAPITAL_, CamelCase_, lower_case_) \
    case FABULOUS_ERROR_##CAPITAL_:                                     \
    case -FABULOUS_ERROR_##CAPITAL_:                                    \
    return #lower_case_;                                                \
    break;

inline ::std::string error_type(const int errcode)
{
    switch ( errcode ) {
        FABULOUS_ERROR_LIST(FABULOUS_DEFINE_ERROR_STRING_CASE_)
    default:
            return std::string("Invalid error code ") + std::to_string(errcode);
        break;
    };
}

/*! \brief fabulous exception base class */
class Error : public ::std::exception
{
private:
    std::string _name;
    std::string _what;
    error_code _errcode;
    std::string _what_b;

    #if FABULOUS_USE_BACKTRACE
    int _backtrace_size;
    void *_backtrace[FABULOUS_MAX_BACKTRACE_SIZE];
    #endif

public:    /* was protected but there is a bug in ICC */
    Error() = delete;
    Error(const Error&) = default;
    Error(Error&&) = default;
    Error(const std::string &name, const std::string &what, error_code errcode):
        _name{name},
        _what{what},
        _errcode{errcode},
        _what_b{what}
    {
        #if FABULOUS_USE_BACKTRACE
        _backtrace_size = ::backtrace(_backtrace, array_size(_backtrace));
        #endif
        static ::std::stringstream ss;
        ss << _name << ": " << _what;
        _what_b = ss.str();
    }

public:
    const char *what() const noexcept override final
    {
        return _what_b.c_str();
    }

    int get_error_code()
    {
        return static_cast<int>(_errcode);
    }


    #if FABULOUS_USE_BACKTRACE
    void print_backtrace()
    {
        int f_no = ::fileno(stderr);
        ::backtrace_symbols_fd(_backtrace, _backtrace_size, f_no);
        if (_backtrace_size == FABULOUS_MAX_BACKTRACE_SIZE) {
            std::fprintf(stderr, " ... \n");
        }
    }
    #endif
};

#define FABULOUS_DECLARE_ERROR_(CAPITAL_, CamelCase_, lower_case_)      \
    class CamelCase_##Error : public ::fabulous::Error                  \
    {                                                                   \
      public:                                                           \
        explicit CamelCase_##Error(const ::std::string &what=""):       \
            Error{#CAPITAL_ "_ERROR", what, FABULOUS_ERROR_##CAPITAL_}  \
        {                                                               \
        }                                                               \
    };                                                                  \

FABULOUS_ERROR_LIST(FABULOUS_DECLARE_ERROR_)

/** \brief display an error message */
inline void error(const ::std::string &errstr, const ::std::string &errprefix = "error: ")
{
    std::cerr << Color::error << errprefix
              << Color::reset << errstr << "\n";
}

/** \brief display an warning message */
inline void warning(const std::string &errstr)
{
    std::cerr << Color::warning << "warning: "
              << Color::reset << errstr << "\n";
}

/** \brief display an debug message */
inline void debug(const std::string &errstr)
{
    #ifdef FABULOUS_DEBUG_MODE
    std::cerr << Color::debug << "DEBUG: "
              << Color::reset << errstr << "\n";
    #else
    (void) errstr;
    #endif
}

/** \brief display an note message */
inline void note(const std::string &errstr)
{
    std::cerr << Color::note << "note: "
              << Color::reset << errstr << "\n";
}

/** \brief display an error message and exit with an error code */
inline void fatal_error(const std::string &errstr, int error_code = EXIT_FAILURE)
{
    error(errstr, "fatal error: ");
    #ifdef FABULOUS_DEBUG_MODE
    abort();
    #endif
    exit(error_code);
}

/** \brief strip directory name */
inline const char *basename(const char *str)
{
    return strrchr(str, '/')  ? strrchr(str, '/')+1 : str;
}

#define FABULOUS_CONCAT_(errstr_, ACTION)                       \
    do {                                                        \
        std::stringstream fabulous_ss_err_impl_;                \
        fabulous_ss_err_impl_                                   \
            << ::fabulous::Color::green                         \
            << ::fabulous::basename(__FILE__) <<":" << __LINE__ \
            <<"["<< __func__ << "]: "                           \
            <<  ::fabulous::Color::reset << errstr_;            \
        ACTION(fabulous_ss_err_impl_.str());                    \
    }while(0)                                                   \

#define FABULOUS_ERROR(errstr_)                         \
    FABULOUS_CONCAT_(errstr_, ::fabulous::error)
#define FABULOUS_WARNING(errstr_)                       \
    FABULOUS_CONCAT_(errstr_, ::fabulous::warning)
#define FABULOUS_DEBUG(errstr_)                         \
    FABULOUS_CONCAT_(errstr_, ::fabulous::debug)
#define FABULOUS_FATAL_ERROR(errstr_)                   \
    FABULOUS_CONCAT_(errstr_, ::fabulous::fatal_error)
#define FABULOUS_NOTE(errstr_)                  \
    FABULOUS_CONCAT_(errstr_, ::fabulous::note)

#ifdef FABULOUS_DEBUG_MODE

#define FABULOUS_THROW(ErrorName_, errstr_)                     \
    FABULOUS_FATAL_ERROR( #ErrorName_ "Error: " << errstr_ )

#else // FABULOUS_DEBUG_MODE

#define FABULOUS_THROW(ErrorName_, errstr_)                             \
    FABULOUS_CONCAT_(errstr_, throw ::fabulous::ErrorName_##Error)

#endif // FABULOUS_DEBUG_MODE


#ifdef FABULOUS_NO_ASSERTIONS

#define FABULOUS_ASSERT( X ) ((void) (X))

#else

#define FABULOUS_ASSERT( cond_ )                                        \
    do{                                                                 \
        if (!(cond_)) {                                                 \
            FABULOUS_THROW(Assertion, "ASSERT( "#cond_" ) FAILED!");    \
        }                                                               \
    }while(0)                                                           \

#endif // FABULOUS_NO_ASSERTIONS

} // end namespace fabulous

#endif // FABULOUS_ERROR_HPP
