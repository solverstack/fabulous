#ifndef FABULOUS_CHAMELEON_utils_HPP
#define FABULOUS_CHAMELEON_utils_HPP

#include <memory>
#include <cstdio>
#include <chameleon.h>

namespace fabulous {
typedef std::shared_ptr<CHAM_desc_t> DescPtr;
typedef std::shared_ptr<CHAMELEON_sequence_t> SeqPtr;
struct DescDeleter;
struct SeqDeleter;
}

#include "fabulous/utils/Error.hpp"

namespace fabulous {
/**
 * \brief Deleter class for shared_ptr on CHAM_desc_t object
 */
struct DescDeleter
{
    inline void operator()(CHAM_desc_t *desc)
    {
        //FABULOUS_DEBUG("call descriptor destroy");
        CHAMELEON_Desc_Destroy(&desc);
    }
};

/**
 * \brief Deleter class for shared_ptr on CHAMELEON_sequence_t object
 */
struct SeqDeleter
{
    inline void operator()(CHAMELEON_sequence_t *seq)
    {
        FABULOUS_DEBUG("call sequence destroy");
        CHAMELEON_Sequence_Destroy(seq);
    }
};

/**
 * \brief Dump a CHAM_desc_t object to standard output for debugging purposes.
 */
inline void dump_descriptor(const CHAM_desc_t &desc)
{
    printf("mb: %d\n"
           "nb: %d\n"
           "bsiz: %d\n"
           "lm: %d\n"
           "ln: %d\n"
           "lmt: %d\n"
           "lnt: %d\n"
           "i: %d\n"
           "j: %d\n"
           "m: %d\n"
           "n: %d\n"
           "mt: %d\n"
           "nt: %d\n"
           "id: %d\n"
           "occurences: %d\n"
           "use_mat: %d\n"
           "alloc_mat: %d\n"
           "register_mat: %d\n",
           desc.mb,  desc.nb,
           desc.bsiz,
           desc.lm,  desc.ln,
           desc.lmt, desc.lnt,
           desc.i,   desc.j,
           desc.m,   desc.n,
           desc.mt,  desc.nt,
           desc.id,
           desc.occurences,
           desc.use_mat,
           desc.alloc_mat,
           desc.register_mat   );
}

/**
 * \brief Dump a CHAM_desc_t object to standard output for debugging purposes.
 */
inline void dump_descriptor(const CHAM_desc_t *desc)
{
    FABULOUS_ASSERT( desc != nullptr );
    dump_descriptor(*desc);
}

} // end namespace fabulous

#endif // FABULOUS_CHAMELEON_utils_HPP
