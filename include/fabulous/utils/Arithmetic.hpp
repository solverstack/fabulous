#ifndef FABULOUS_ARITHMETIC_HPP
#define FABULOUS_ARITHMETIC_HPP

#include <complex>
#include <type_traits>

#ifdef FABULOUS_USE_CHAMELEON
# include <chameleon.h>
#endif // FABULOUS_USE_CHAMELEON

#include "fabulous/utils/Traits.hpp"

namespace fabulous {

#define FABULOUS_ARITHMETIC_LIST(ARITHMETIC)                    \
    ARITHMETIC(                                                 \
        REAL_FLOAT, float, float, 0,                            \
        ChamRealFloat, ChamTrans, s, or, 'T'                  \
    )                                                           \
    ARITHMETIC(                                                 \
        REAL_DOUBLE, double, double, 1,                         \
        ChamRealDouble, ChamTrans, d, or, 'T'                 \
    )                                                           \
    ARITHMETIC(                                                 \
        COMPLEX_FLOAT, std::complex<float>, float, 2,           \
        ChamComplexFloat, ChamConjTrans, c, un, 'C'           \
    )                                                           \
    ARITHMETIC(                                                 \
        COMPLEX_DOUBLE, std::complex<double>, double, 3,        \
        ChamComplexDouble, ChamConjTrans, z, un, 'C'          \
    )                                                           \

/* **************** Arithmetic CLASS ********************** */

#ifdef FABULOUS_USE_CHAMELEON
# define FABULOUS_ARITHMETIC_CHAMELEON_TYPE(morse_type_, morse_trans_)  \
    static const constexpr int morse_type = morse_type_;/**<MORSE type */ \
    static const constexpr int mtrans = morse_trans_; /**<MORSE trans */
#else
# define FABULOUS_ARITHMETIC_CHAMELEON_TYPE(morse_type_, morse_trans_)
#endif // FABULOUS_USE_CHAMELEON

#define FABULOUS_DISABLE_INVALID_ARITHMETIC(Type_S_)    \
    static_assert(                                      \
        ::fabulous::Arithmetik<Type_S_>::value,         \
        "Invalid arithmetic: "                          \
        "Only float, double, std::complex<float>, and " \
        "std::complex<double> precisions will work!"    \
    )                                                   \

/**
 * \brief Global constants that depends on the Arithmetic used
 */
template<class S>
struct Arithmetik : public std::false_type
{
};

#define FABULOUS_DEFINE_ARITHMETIC(name_, type_, primary_type_, value_, \
                                   morse_type_,  morse_trans_,          \
                                   _7, _8, la_trans_)                   \
    template <>                                                         \
    struct Arithmetik<type_> : public std::true_type {                  \
        using value_type = type_ ;  /**< Scalar value */                \
        using primary_type = primary_type_;                             \
        FABULOUS_ARITHMETIC_CHAMELEON_TYPE(morse_type_, morse_trans_)   \
        static const constexpr char ltrans = la_trans_; /**< LAPACK trans */ \
        static const constexpr char *name = #name_; /**< Arithmetic string */ \
        static const constexpr char *type_name = #type_; /**< type string */ \
    };

FABULOUS_ARITHMETIC_LIST(FABULOUS_DEFINE_ARITHMETIC)

template<class S>
std::ostream& operator<<(std::ostream &o, const Arithmetik<S>&)
{
    o << Arithmetik<S>::name <<" ("<<Arithmetik<S>::type_name<<")";
    return o;
}

} // end namespace fabulous

#endif // FABULOUS_ARITHMETIC_HPP
