#ifndef FABULOUS_DYNAMIC_TAU_FACTOR_HPP
#define FABULOUS_DYNAMIC_TAU_FACTOR_HPP

#include <vector>

namespace fabulous {
template<class> class DynamicTauFactor;
}

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/tpqrt.hpp"
#include "fabulous/kernel/qrf.hpp"

namespace fabulous {

template<class S>
using DynamicTauVector = std::vector<DynamicTauFactor<S>>;

template<class S>
inline Block<S> OTHER_tp_A_block(int I, Block<S> &block, const Block<S> &V)
{
    const int M = V.get_nb_col();
    const int N = block.get_nb_col();
    return block.sub_block( I, 0, M, N );
}

template<class S>
inline Block<S> OTHER_tp_B_block(int I, Block<S> &block, const Block<S> &V)
{
    const int M = V.get_nb_row();
    const int N = block.get_nb_col();
    return block.sub_block( I, 0, M, N );
}

template<class S>
inline Block<S> OTHER_qrf_block(int I, Block<S> &block, int M)
{
    const int N = block.get_nb_col();
    return block.sub_block( I, 0, M, N );
}

/*! \brief Remember all information needed to apply the
 * update counterpart of a GEQRF, TSQRT or TTQRT factorization */
template<class S>
class DynamicTauFactor
{
public:
    enum facto_type_t {
        NONE = 0,
        PANEL, // GEQRT
        TS, // TSQRT
        TT, // TTQRT
    };
private:
    std::vector<S> _buf;
    const facto_type_t _type; /*!< either ts or diag_panel */
    const int _line; /*!< Block line in hessenberg where to apply this udpate */
    const int _line2; /*!< Block line in hessenberg where to apply this udpate (tpqrt case second line) */
    const int _ib; /*!< inner blocking size used for tpqrt kernels */
    const Block<S> _V; /*!< the block containing householder vector (A or V) */

public:
    const S* data() const { return _buf.data(); }
    std::vector<S>& get_buf() { return _buf; }

    DynamicTauFactor(facto_type_t type, Block<S> &AV, int line, int line2, int ib):
        _buf{},
        _type{type},
        _line{line},
        _line2{line2},
        _ib{ib},
        _V{AV}
    {
    }

    int64_t apply(char trans, Block<S> &block) const
    {
        int64_t flops = 0;

        switch ( _type ) {
        case DynamicTauFactor<S>::PANEL: { // diag block qr update
            const Block<S> &V = _V;
            Block<S> C = OTHER_qrf_block(_line, block, V.get_nb_row());
            const S *tau = data();
            const int M = C.get_nb_row();
            const int N = C.get_nb_col();
            const int K = V.get_nb_col();

            FABULOUS_ASSERT( V.get_nb_row() == C.get_nb_row() );
            //FABULOUS_DEBUG("UNMQR update on ("<<_line<<") height="<<M<<" width="<<N);
            const int err = lapacke::unmqr( trans, M, N, K,
                                            V.get_ptr(), V.get_leading_dim(), tau,
                                            C.get_ptr(), C.get_leading_dim()       );
            flops += lapacke::flops::unmqr<S>(M, N, K);
            if (err != 0) {
                FABULOUS_THROW(Kernel, "unmqr 'step' err="<<err);
            }
        } break;
        case DynamicTauFactor<S>::TS: { // triangular over square(rectangular) qr update
            const Block<S> &V = _V;
            Block<S> A = OTHER_tp_A_block(_line, block, V);
            Block<S> B = OTHER_tp_B_block(_line2, block, V);
            const S *T = data();
            const int M = B.get_nb_row();
            const int N = B.get_nb_col();
            const int K = V.get_nb_col();
            //FABULOUS_DEBUG("TSMQR update on ("<<_line<<";"<<_line2<<") height="<<M<< " width="<<N);

            FABULOUS_ASSERT( A.get_nb_row() == K );
            FABULOUS_ASSERT( A.get_nb_col() == N );
            FABULOUS_ASSERT( V.get_nb_row() == M );

            const int err = lapacke::tsmqrt(trans, M, N, K, _ib,
                                            V.get_ptr(), V.get_leading_dim(),
                                            T, _ib,
                                            A.get_ptr(), A.get_leading_dim(),
                                            B.get_ptr(), B.get_leading_dim()    );
            flops += lapacke::flops::tsmqrt<S>(M, N, K);
            if (err != 0) {
                FABULOUS_THROW(Kernel, "tpmqrt err="<<err);
            }
        } break;
        case DynamicTauFactor<S>::TT: { // triangular over triangular qr update
            const Block<S> &V = _V;
            Block<S> A = OTHER_tp_A_block(_line, block, V);
            Block<S> B = OTHER_tp_B_block(_line2, block, V);
            const S *T = data();
            const int M = B.get_nb_row();
            const int N = B.get_nb_col();
            const int K = V.get_nb_col();
            //FABULOUS_DEBUG("TTMQR update on ("<<_line<<";"<<_line2<<") height="<<M<<" width="<<N);

            FABULOUS_ASSERT( V.get_nb_row() == V.get_nb_col() );
            FABULOUS_ASSERT( A.get_nb_row() == V.get_nb_row() );
            FABULOUS_ASSERT( A.get_nb_col() == N );

            const int err = lapacke::ttmqrt(trans, M, N, K, _ib,
                                            V.get_ptr(), V.get_leading_dim(),
                                            T, _ib,
                                            A.get_ptr(), A.get_leading_dim(),
                                            B.get_ptr(), B.get_leading_dim()    );
            flops += lapacke::flops::ttmqrt<S>(M, N, K);
            if (err != 0) {
                FABULOUS_THROW(Kernel, "tpmqrt err="<<err);
            }
        } break;
        default:
            FABULOUS_THROW(Internal, "unexpected internal error");
            break;
        }
        return flops;
    }
};

} // end namespace fabulous

#endif // FABULOUS_DYNAMIC_TAU_FACTOR_HPP
