#ifndef FABULOUS_HESS_DR_HPP
#define FABULOUS_HESS_DR_HPP

namespace fabulous {
namespace bgmres {
template<class S> class HessDR;
}
}

#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/gels.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/utils/Logger.hpp"

namespace fabulous {
namespace bgmres {

/**
 * \brief The Hessenberg in the standard BGMres case
 *
 * Format: <br/>
 \verbatim
 | X X X |
 | X X X |
 |   X X |
 |     X |
 \endverbatim
 *
 * @warning the whole matrix is allocated, its size is driven by the restart parameter.
 */
template<class S>
class HessDR
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
private:
    using P = primary_type;
private:
    Logger &_logger;
    const int _nbRHS;  /*!< number of right hand sides */
    int _nb_vect; /*!< number of vector */
    int _nb_eigen_pair; /*!< number of eigen pair in deflated_restarting */

    Block<S> _data; /*!< the hessenberg data */
    Block<S> _data_tmp; /*!< copy of the hessenberg data for solve purpose */
    Block<S> _R1; /*!< right hand sides of projected problem */
    bool _solution_computed; /*!< whether least square solution is computed  */
    Block<S> _Y_buf; /*!< least square solution buffer */
    Block<S> _Y; /*!< least square solution */

public:
    HessDR(int max_krylov_space_size, int nbRHS, Logger &logger):
        _logger{logger},
        _nbRHS{nbRHS},
        _nb_vect{0},
        _nb_eigen_pair{0},
        _data{ max_krylov_space_size+nbRHS, max_krylov_space_size, "hessenberg_dr"},
        _data_tmp{ max_krylov_space_size+nbRHS, max_krylov_space_size, "hessenberg_dr_tmp"},
        _R1{},
        _solution_computed{false},
        _Y_buf{max_krylov_space_size+_nbRHS, _nbRHS},
        _Y{}
    {
    }

    void display(std::string name = "")
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        Block<S> sub = _data.sub_block( 0, 0, M, N );
        sub.display(name);
    }

    void display_hess_bitmap(const std::string &name = "")
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        std::cout << name<<" (M="<<M<<"; N="<<N<<")\n";
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                bool b = _data.at(i, j) != S{0.0};
                if (i < _nb_eigen_pair || j < _nb_eigen_pair)
                    std::cout << Color::red;
                std::cout << (b ? ". " : "  ") << Color::reset;
            }
            std::cout<<"\n";
        }
        std::cout<<std::endl;
    }


    void set_rhs(Block<S> &R1)
    {
        _R1 = R1;
    }

    Block<S> get_H()
    {
        const int M = _nb_vect;
        const int N = _nbRHS;
        const int J = _nb_vect - N;
        return _data.sub_block( 0, J, M, N );
    }

    Block<S> get_R()
    {
        const int N = _nbRHS;
        const int I = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, N, N );
    }

    Block<S> get_H1new()
    {
        FABULOUS_ASSERT( _nb_vect == _nb_eigen_pair );
        const int M = _nb_eigen_pair + _nbRHS;
        const int N = _nb_eigen_pair;
        return _data.sub_block( 0, 0, M, N );
    }

    /**
     * \brief \f$ \mathscr{F} \f$: the whole projected matrix (Block Hessenberg)
     * \return \f$ \mathscr{F} \f$
     */
    Block<S> get_F()
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        return _data.sub_block( 0, 0, M, N );
    }

    /*!
     * \brief \f$ \mathscr{F} \f$: copy of the whole projected matrix for solve purpose
     * \return \f$ \mathscr{F} \f$
     */
    Block<S> get_F_tmp()
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        Block<S> tmp = _data_tmp.sub_block( 0, 0, M, N );
        Block<S>   F = _data.sub_block( 0, 0, M, N );
        tmp.copy(F);
        return tmp;
    }

    Block<S> alloc_least_square_sol()
    {
        TIMER_TRACE;
        const int M = _nb_vect;
        const int N = _nbRHS;
        _Y = _Y_buf.sub_block( 0, 0, M, N );
        if (!_solution_computed) {
            const int M2 = _nb_vect + _nbRHS;
            Block<S> YY = _Y_buf.sub_block( 0 ,0, M2, N );
            YY.zero();
        }
        return _Y;
    }

    void solve_least_square(Block<S> &Y)
    {
        TIMER_LSQR;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );
        if (_solution_computed) {
            return;
        }

        _logger.notify_least_square_begin();
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        Block<S> YY = _Y_buf.sub_block( 0, 0, M, _nbRHS );
        YY.copy(_R1);
        Block<S> F = get_F_tmp();
        int err = lapacke::gels(
            M, N, _nbRHS,
            F.get_ptr(),  F.get_leading_dim(),
            YY.get_ptr(), YY.get_leading_dim()
        );
        if (err != 0) {
            FABULOUS_THROW(Kernel, "gels (least square) err="<<err);
        }

        int64_t flops = lapacke::flops::gels<S>(M, N, _nbRHS);
        _solution_computed = true;
        _logger.notify_least_square_end( flops );
    }

    void compute_proj_residual(Block<S> &LS, const Block<S> &Y)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );

        const int M = _nb_vect + _nbRHS;
        const int N = _nbRHS;
        const int K = _nb_vect;

        FABULOUS_ASSERT( LS.get_nb_row() == M );
        FABULOUS_ASSERT( LS.get_nb_col() == _R1.get_nb_col() );
        FABULOUS_ASSERT( LS.get_nb_row() >= _R1.get_nb_row() );
        FABULOUS_ASSERT( Y.get_nb_row() == K );
        FABULOUS_ASSERT( Y.get_nb_col() == N );

        const Block<S> F = get_F();
        LS.copy(_R1);
        LS -= F * Y;
    }

    std::tuple<P,P,bool>
    check_least_square_residual(const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        Block<S> Y = alloc_least_square_sol();
        solve_least_square(Y);

        _logger.notify_least_square_begin();
        const int I = _nb_vect;
        const int N = _nbRHS;
        Block<S> residual = _Y_buf.sub_block( I, 0, N, N );
        auto MinMaxConv = min_max_conv(residual, epsilon);
        int64_t flops = N * lapacke::flops::dot<S>(N);
        _logger.notify_least_square_end(flops);
        return MinMaxConv;
    }

    void notify_ortho_end() const {/*nop*/}

    Block<S> get_hess_block()
    {
        return get_F();
    }

    /**
     * \brief Increment cursor
     */
    void increase(int block_size)
    {
        FABULOUS_ASSERT( block_size == _nbRHS );
        _nb_vect += block_size;
        _solution_computed = false;
    }

    void reserve_DR(int nb_eigen_vector)
    {
        _nb_vect = nb_eigen_vector;
        _nb_eigen_pair = nb_eigen_vector;
    }

    int get_nb_vect() const { return _nb_vect; }
    int get_nb_hess_line() const { return _nb_vect + _nbRHS; }

}; // end class HessDR

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_HESS_DR_HPP
