#ifndef FABULOUS_HESS_QR_IB_DR_HPP
#define FABULOUS_HESS_QR_IB_DR_HPP

#include <memory>
#include <cassert>

namespace fabulous {
namespace bgmres {
template<class> class HessQRIBDR;
}
}

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/qrf.hpp"
#include "fabulous/kernel/tpqrt.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/hessenberg/DynamicTauFactor.hpp"

namespace fabulous {
namespace bgmres {

/*!
 * \brief Hessenberg for IB-BGMRes version.
 *
 * This hold the matrix denoted as \f$\mathscr{F}\f$ in the paper.
 */
template<class S>
class HessQRIBDR
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
private:
    using P = primary_type;
private:
    Logger &_logger;
    const int _nbRHS;  /*!< number of right hand sides */
    const int _max_krylov_space_size; /*!< size of allocated number of columns */
    int _nb_block_col; /*!< number of block columns */
    int _nb_vect; /*!< number of vector inside the hessenberg */
    int _nb_eigen_pair; /*!< number of eigen pair in deflated_restarting */

    bool _restarted; /*!< whether the arnoldi procedure already
                      * restarted once or not */
    int _ib_col; /*!< -1 if IB not happened or abscissa of column where IB start */
    std::vector<int> _block_size; /*!< size of each block */

    Block<S> _data; /*!< the factorized hessenberg data */
    Block<S> _data_tmp; /*!< copy of hessenberg for solve purpose */
    Block<S> _orig; /*!< unfactorized hessenberg, used for restarting */
    bool _last_column_factorized; /*!< whether last column was factorized  */

    DynamicTauVector<S> _tau;
    Block<S> _phi_buf; /*!< buffer for phi */
    Block<S> _phi; /*!< needed to compute the rhs for Least square.
                                     Ref: IB BGMRes Dr : Page 10. */
    bool _phi_initialized; /*!< whether phi was initialized and must be used to update RHS */

    Block<S> _lambda1; /*!< initial value of RHS (projected problem) */
    Block<S> _Lambda; /*!< rhs of least square problem */

    bool _solution_computed; /*!< whether solution (for current size) was computed */
    Block<S> _Y_buf; /*!< least square solution */
    Block<S> _Y; /*!< least square solution */

public:
    /*!
     * \brief create \f$\mathscr{F}\f$
     * \param max_krylov_space_size maximum size of Krylov search space
     * \param nbRHS number of right hand side
     * \param logger object logging the application trace and floating point operations count
     */
    HessQRIBDR(int max_krylov_space_size, int nbRHS, Logger &logger):
        _logger{logger},
        _nbRHS{nbRHS},
        _max_krylov_space_size{max_krylov_space_size},
        _nb_block_col{0},
        _nb_vect{0},
        _nb_eigen_pair{0},
        _restarted{false},
        _ib_col{-1},
        _data{max_krylov_space_size+nbRHS, max_krylov_space_size,
                "hessenberg_qribdr"},
        _data_tmp{max_krylov_space_size+nbRHS, max_krylov_space_size,
                "hessenberg_qribdr_tmp"},
        _orig{max_krylov_space_size+nbRHS, max_krylov_space_size,
                "hessenberg_qribdr_orig"},
        _last_column_factorized{false},
        _phi_buf{},
        _phi{},
        _phi_initialized{false},
        _lambda1{},
        _Lambda{max_krylov_space_size+_nbRHS, _nbRHS},
        _solution_computed{false},
        _Y_buf{max_krylov_space_size+_nbRHS, _nbRHS},
        _Y{}
    {
    }

private:


    /*!
     * \brief \f$ \mathbb{H}_j \f$: the bottom part of \f$ \mathscr{F} \f$
     * \return \f$ \mathbb{H}_j \f$
     */
    Block<S> get_Hj()
    {
        const int I = _nb_vect;
        const int M = _nbRHS;
        const int N = _nb_vect;
        return _data.sub_block( I, 0, M, N );
    }

    /*!
     * \brief \f$ \mathscr{F} \f$: the whole projected matrix
     * \return \f$ \mathscr{F} \f$
     */
    Block<S> get_F()
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        return _data.sub_block( 0, 0, M, N );
    }


    /*!
     * \brief \f$ \mathscr{F} \f$: copy of the whole projected matrix for solve purpose
     * \return \f$ \mathscr{F} \f$
     */
    Block<S> get_F_tmp()
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        Block<S> tmp = _data_tmp.sub_block( 0, 0, M, N );
        Block<S>   F = _data.sub_block( 0, 0, M, N );
        tmp.copy(F);
        return tmp;
    }

    /**
     * see IB-BGMRES-DR 3.1.4 "right hand sides of the least square problem"
     *                  proposition 3.   equation (25)
     *  for the computation made in this function
     */
    int64_t compute_Lambda_restarted_phi(Block<S> &Lambda)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( Lambda.get_nb_row() == _nb_vect + _nbRHS );
        FABULOUS_ASSERT( Lambda.get_nb_row() == _phi.get_nb_row() );

        const int k = _nb_eigen_pair;
        const int p = _nbRHS;
        const int M = Lambda.get_nb_row();
        int64_t flops = 0;

        // restart update method :: Lambda1: (k+p, p)
        const Block<S> phi_1 = _phi.sub_block( 0, 0,   k, p );
        const Block<S> phi_2 = _phi.sub_block( k, 0, M-k, p );
        const Block<S> l1_1  = _lambda1.sub_block( 0, 0, k, p );
        const Block<S> l1_2  = _lambda1.sub_block( k, 0, p, p );
        Block<S> L_1  = Lambda.sub_block( 0, 0,   k, p );
        Block<S> L_2  = Lambda.sub_block( k, 0, M-k, p );

        L_1.copy(l1_1);
        L_1 += phi_1 * l1_2;
        L_2 = phi_2 * l1_2;

        flops += lapacke::flops::gemm<S>(k, p, p);
        flops += lapacke::flops::gemm<S>(Lambda.get_nb_row()-k, p, p);
        return flops;
    }

    int64_t compute_Lambda_ibr0_phi(Block<S> &Lambda)
    {
        TIMER_TRACE;
        int64_t flops = 0;
        // breakdown on R0 restart method  :: Lambda1: (p, p)
        // \Lambda_j <- \Phi_j * \Lambda_1
        Lambda = _phi * _lambda1;
        flops += lapacke::flops::gemm<S>(Lambda.get_nb_row(), _nbRHS, _nbRHS);
        return flops;
    }

    int64_t compute_Lambda_phi(Block<S> &Lambda)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( _phi_initialized );
        if ( _restarted ) {
            return compute_Lambda_restarted_phi(Lambda);
        } else {
            return compute_Lambda_ibr0_phi(Lambda);
        }
    }

    /*!
     * \brief Compute \f$\Lambda_{j}\f$ using \f$\Phi_j\f$ and \f$\Lambda_1\f$
     *
     * \param[out] Lambda \f$ \Lambda_j = \begin{cases}
     \Phi_j * \Lambda_1 & \text{if IB on R0 (IB-BGMRES-DR, eq 25)} \\
     \left[ \begin{array}[cc] \\
     I_{p_1} & \Phi_j \\
     0_{(n_j+p-p1)\times p1} &
     \end{array}\right] * \Lambda_1 & \text{if restarted procedure (annex eq 2.6)}
     \end{cases} \f$
     */
    int64_t compute_fully_updated_Lambda( Block<S> &Lambda,
                                          DynamicTauVector<S> &disposable_tau )
    {
        TIMER_TRACE;
        int64_t flops = 0;
        if (!_phi_initialized) {
            // no inexact breakdown on R0 or no restart
            // NO FIRST/SECOND UPDATE
            // => We use Lambda computed incrementally (as in Classic Incremental QR)
            //FABULOUS_DEBUG("using INCREMENTAL Lambda");
            Lambda.copy(_Lambda);
        } else {
            //FABULOUS_DEBUG("using FULL UPDATE Lambda");
            // Other wise apply full TRIPLE UPDATE on _lambda1 (relative high cost)
            // FIRST UPDATE ( phi )
            flops += compute_Lambda_phi( Lambda );
            // SECOND UPDATE ( incremental QR )
            Timer mqr_update_lambda_time;
            mqr_update_lambda_time.start();
            for (auto &tau : _tau) {
                flops += tau.apply(Arithmetik<S>::ltrans, Lambda);
            }
            mqr_update_lambda_time.stop();
            FABULOUS_DEBUG("mqr_update_time="<<mqr_update_lambda_time.get_length());
        }

        // THIRD UPDATE (temporary factorization)
        for (auto &tau : disposable_tau) {
            flops += tau.apply(Arithmetik<S>::ltrans, Lambda);
        }
        return flops;
    }

    /*!
     * \brief Check if there is room to another iteration
     */
    bool check_room(int block_size) const
    {
        return (_nb_vect + block_size <= _max_krylov_space_size);
    }

    void factorize_last_column()
    {
        TIMER_TRACE;
        if (_last_column_factorized) {
            return;
        }
        const int N = _block_size.back();
        const int M = _nb_vect + _nbRHS;
        const int J = _nb_vect - N;

        Block<S> last_column = _data.sub_block( 0, J, M, N );
        _logger.notify_facto_begin();

        int64_t flops = 0;
        Timer update_time;
        /* STEP 1: Loop over each block in last column to
         apply already computed Householders reflectors */
        //FABULOUS_DEBUG("_tau.size = "<<_tau.size());
        update_time.start();
        const char trans = Arithmetik<S>::ltrans;
        for (auto &tau : _tau) {
            flops += tau.apply(trans, last_column);
        }
        update_time.stop();
        FABULOUS_DEBUG("update_time="<<update_time.get_length());
        /* STEP 2: Call GEQRF on the very last block of last col */

        Timer qrf_time;
        qrf_time.start();
        Block<S> A = _data.sub_block( J, J, N, N );
        _tau.emplace_back(DynamicTauFactor<S>::PANEL, A, J, -1, -1);
        std::vector<S> &tau = _tau.back().get_buf();
        int err = lapacke::geqrf( N, N,
                                  A.get_ptr(), A.get_leading_dim(), tau);
        // FABULOUS_DEBUG("GEQRF J="<<J<<" height="<<M<< " width="<<N);
        if (err != 0) {
            FABULOUS_THROW(Kernel, "geqrf 'last block' err="<<err);
        }
        flops += lapacke::flops::geqrf<S>(N, N);
        if (!_phi_initialized) {
            //FABULOUS_DEBUG("apply on _Lambda");
            flops += _tau.back().apply(Arithmetik<S>::ltrans, _Lambda);
        }
        qrf_time.stop();
        FABULOUS_DEBUG("qrf_time="<<qrf_time.get_length());

        _last_column_factorized = true;
        _logger.notify_facto_end( flops );
    }

    int64_t factorize_last_line(int nb_kept_direction)
    {
        TIMER_TRACE;
        int64_t flops = 0;
        const bool ib_happened = (nb_kept_direction != _nbRHS);
        if (ib_happened || (_restarted && _nb_block_col == 1)) {
            // TS on dense block line
            Timer tsqrt_time;
            if (   ib_happened &&_ib_col == -1 ) {
                const int N = _block_size.back();
                _ib_col = _nb_vect - N;
            }

            tsqrt_time.start();
            // handle incremental qr
            const int M = nb_kept_direction;
            const int N = (_ib_col >= 0) ? (_nb_vect - _ib_col) : _nb_vect;
            const int I1 = _ib_col >= 0 ? _ib_col : 0;
            const int I2 = _nb_vect;
            const int J = _ib_col >= 0 ? _ib_col : 0;
            const int IB = (N <= 64) ? ( N ) : ( 32 ) ;

            Block<S> A = _data.sub_block( I1, J, N, N );
            Block<S> V = _data.sub_block( I2, J, M, N );

            _tau.emplace_back(DynamicTauFactor<S>::TS, V, I1, I2, IB);
            std::vector<S> &tau = _tau.back().get_buf();
            lapacke::tsqrt(M, N, IB,
                           A.get_ptr(), A.get_leading_dim(),
                           V.get_ptr(), V.get_leading_dim(), tau );
            flops += lapacke::flops::tsqrt<S>(M, N);
            tsqrt_time.stop();
            FABULOUS_DEBUG("tsqrt_time="<<tsqrt_time.get_length());
        } else {
            // TT with the only triangle in last column
            Block<S> Hj = this->get_Hj(); // Hj is last block line
            const int N = _block_size.back();
            const int J = Hj.get_nb_col() - N;
            Block<S> V = Hj.sub_block( 0, J, N, N );
            Block<S> H = this->get_H(); // H is last block column
            const int I = H.get_nb_row() - N;
            Block<S> A = H.sub_block( I, 0, N, N );
            Timer ttqrt_time;
            ttqrt_time.start();
            // handle incremental qr
            const int IB = (N <= 64) ? ( N ) : ( 32 ) ;

            _tau.emplace_back(DynamicTauFactor<S>::TT, V, I, I+N, IB);
            std::vector<S> &tau = _tau.back().get_buf();
            lapacke::ttqrt(N, IB,
                           A.get_ptr(), A.get_leading_dim(),
                           V.get_ptr(), V.get_leading_dim(), tau );
            flops += lapacke::flops::ttqrt<S>(N);
            ttqrt_time.stop();
            FABULOUS_DEBUG("ttqrt_time="<<ttqrt_time.get_length());
        }

        if (!_phi_initialized) {
            //FABULOUS_DEBUG("apply on _Lambda");
            flops += _tau.back().apply(Arithmetik<S>::ltrans, _Lambda);
        }
        return flops;
    }

    int64_t pre_solve_factorization( Block<S> &F,
                                     DynamicTauVector<S> &disposable_tau )
    {
        TIMER_TRACE;
        int64_t flops = 0;
        const bool ib_happened = ( _block_size.back() != _nbRHS );
        if (ib_happened || (_restarted && _nb_block_col == 1))  {
            const int M = _nbRHS;
            const int N = (_ib_col >= 0) ? (_nb_vect - _ib_col) : _nb_vect;
            const int I1 = _ib_col >= 0 ? _ib_col : 0;
            const int I2 = _nb_vect;
            const int J = _ib_col >= 0 ? _ib_col : 0;
            const int IB = (N <= 64) ? ( N ) : ( 32 ) ;

            Timer tsqrt_time;
            tsqrt_time.start();
            Block<S> A = F.sub_block( I1, J, N, N );
            Block<S> V = F.sub_block( I2, J, M, N );
            disposable_tau.emplace_back(DynamicTauFactor<S>::TS, V, I1, I2, IB);
            std::vector<S> &T = disposable_tau.back().get_buf();

            int err = lapacke::tsqrt(
                M, N, IB,
                A.get_ptr(), A.get_leading_dim(),
                V.get_ptr(), V.get_leading_dim(), T
            );
            flops += lapacke::flops::tsqrt<S>(M, N);
            if (err != 0) {
                FABULOUS_THROW(Kernel, "tpqrt err="<<err);
            }
            tsqrt_time.stop();
            FABULOUS_DEBUG("tsqrt_time="<<tsqrt_time.get_length());
        } else {
            Timer ttqrt_time;
            ttqrt_time.start();

            const int N = _block_size.back();
            const int IB = (N <= 64) ? ( N ) : ( 32 ) ;
            const int J = _nb_vect - N;
            const int I1 = _nb_vect - N;
            const int I2 = _nb_vect;
            Block<S> A = F.sub_block( I1, J, N, N );
            Block<S> V = F.sub_block( I2, J, N, N );

            disposable_tau.emplace_back(DynamicTauFactor<S>::TT, V, I1, I2, IB);
            std::vector<S> &T = disposable_tau.back().get_buf();
            int err = lapacke::ttqrt(
                N, IB,
                A.get_ptr(), A.get_leading_dim(),
                V.get_ptr(), V.get_leading_dim(), T
            );
            flops += lapacke::flops::ttqrt<S>(N);
            if (err != 0) {
                FABULOUS_THROW(Kernel, "tpqrt err="<<err);
            }
            ttqrt_time.stop();
            FABULOUS_DEBUG("ttqrt_time="<<ttqrt_time.get_length());
        }
        return flops;
    }

public /* DEBUG*/:

    /*!
     * \brief display a dot for each non zero coefficient in matrix
     *
     *  Used for debug purpose
     */
    void display_hess_extended_bitmap(std::string name = "")
    {
        std::cout<<name<<"\n";
        for (int j=0; j<_nb_vect+_nbRHS; ++j) {
            int col = 0;
            for (unsigned int i=0; i<_block_size.size(); ++i) {
                for (int t=0; t<_block_size.at(i); ++t) {
                    auto m = std::norm(_data.at(j, col));
                    if ( m != primary_type{0.0} )
                        std::cout<<"."<<" ";
                    else
                        std::cout<<" "<<" ";
                    col++;
                }
                std::cout<<"| ";
            }
            std::cout<<std::endl;
        }
        std::cout<<std::endl;
    }


public:
    /*! \brief number of vector in \f$\mathscr{F}\f$ */
    int get_nb_vect() const { return _nb_vect; }
    /*! \brief number of lines in \f$\mathscr{F}\f$ */
    int get_nb_hess_line() const { return get_nb_vect() + _nbRHS; }

    /* ---- LeastSquare RHS related methods: ---------------------------- */

    /*!
     * \brief set the initial value for the right hand side of the
     *  LeastSquare problem (projected problem)
     *
     * \param[in] lambda initial RHS for projected problem;
     * R part of QR factorization of initial residual R0
     */
    void init_lambda(const Block<S> &lambda)
    {
        // Check
        if (lambda.get_nb_col() != _nbRHS
            || lambda.get_nb_row() != _nbRHS + _nb_eigen_pair) {
            FABULOUS_THROW(
                Internal,
                "Lambda dimensions are wrong while initiating lambda in Hess\n"
            );
        }
        _lambda1 = lambda;
        _Lambda.copy(lambda);
    }

    /*!
     * \brief Initialize \f$ \Phi \f$
     * (refer to Annex, Eq 2.5) (INEXACT BREAKDOWN ON R0)
     *
     * \param p1 number of direction kept in the inexact breakdown on R0
     */
    void init_phi_ibr0(int p1)
    {
        TIMER_TRACE;
        if (_phi_initialized) {
            FABULOUS_THROW(Internal, "This function cannot be called twice!");
        }
        _phi_initialized = true;

        const int N = _lambda1.get_nb_col();
        _phi_buf = Block<S>{_max_krylov_space_size+2*_nbRHS, _nbRHS, "hess_qrib_phi"};
        _phi = _phi_buf.sub_block( 0, 0, N+p1, N );

        for (int i = 0; i < N; ++i) {
            _phi(i, i) = S{1.0};
        }
    }

    /*!
     * \brief Initialize \f$ \Phi \f$ (RESTART)
     * (refer to IB-BGMRES-DR, Section 3.1.4 (Proposition 3))
     *
     * \param p1 number of eigen pair (size of first block) in deflated restarting
     */
    void init_phi_deflated_restart(int k)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT(k == _nb_eigen_pair);
        FABULOUS_ASSERT( _lambda1.get_nb_row() == k+_nbRHS );

        _restarted = true;

        if (_phi_initialized) {
            FABULOUS_THROW(Internal, "This function cannot be called twice!");
        }
        _phi_initialized = true;

        const int N = _lambda1.get_nb_col();
        _phi_buf = Block<S>{_max_krylov_space_size+2*_nbRHS, _nbRHS};
        _phi = _phi_buf.sub_block( 0, 0, N+k, N );

        for (int i = 0; i < N; ++i) {
            _phi(k+i, i) = S{1.0};
        }
    }

    /*!
     * \brief Update \f$\Phi\f$ if IB happened on R0 or restarted procedure
     *
     * \param[in] W1_W2 \f$ [\mathbb{W}_1, \mathbb{W}_2] \f$ packed in one block
     * \param p_jplus1 \f$p_{j+1}\f$ number of directions kept in the last inexact breakdown
     *
     */
    int64_t update_phi(const Block<S> &W1_W2, const int p_jplus1)
    {
        TIMER_TRACE;
        if (!_phi_initialized) {
            return 0;
        }
        /* If _phi has been set, it means that an I.B. happened
         * on R0, and we have a modified RHS for least square
         * that depends on _phi */
        const int NJ = _nb_vect;
        const int M = _phi.get_nb_row() + p_jplus1;

        Block<S> new_phi = _phi_buf.sub_block( 0, 0, M, _nbRHS );

        // Compute newphi(nj+1:nj+p,:) := W1_W2^{H} * oldphi(nj+1:nj+p,:)
        Block<S> updated_window = _phi.sub_block( NJ, 0, _nbRHS, _nbRHS );
        Block<S> tmp = updated_window.copy();
        W1_W2.dot(tmp, updated_window); // updated_window = W1_W2.dot(tmp);

        _phi = new_phi;
        return lapacke::flops::gemm<S>(_nbRHS, _nbRHS, _nbRHS);
    }

    /* ------ IB hessenberg specific methods: ---------------------------- */

    /*!
     * \brief Update \f$ H_j \f$ block into becoming new last row of
     * \f$ \mathscr{L} \f$ and new \f$ G_j \f$ block
     *
     * \f$  L_{j+1,}  =  W_1^{H} *  H_j \f$ <br/>
     * \f$  G_j       =  W_2^{H} *  H_j \f$ <br/>
     *
     * (this can be done in one operation as blocks following each other in memory)
     *
     * \param[in] W1_W2 \f$ \mathbb{W} = [\mathbb{W}_1, \mathbb{W}_2] \f$ ( Directions )
     */
    int64_t update_bottom_line(const Block<S>& W1_W2)
    {
        TIMER_TRACE;
        const int M = _nbRHS;
        const int N = _nb_vect;
        const int K = _nbRHS;

        Block<S> Hj = get_Hj();
        const Block<S> tmp = Hj.copy();
        W1_W2.dot(tmp, Hj); // Hj = W1_W2^{H} * tmp
        return lapacke::flops::gemm<S>(M, N, K);
    }

    void after_bottom_line_update(int nb_kept_direction)
    {
        TIMER_TRACE;
        // FACTORIZATION OF BOTTOM BLOCK LINE
        _logger.notify_facto_begin();

        const int I = _nb_vect;
        const int M = _nbRHS;
        const int N = _nb_vect;
        Block<S> Hj = this->get_Hj();
        Block<S> Hj_ORI = _orig.sub_block( I, 0, M, N );
        /* save Hj to original before QR factorization */
        Hj_ORI.copy(Hj);

        int64_t flops = factorize_last_line(nb_kept_direction);
        _logger.notify_facto_end( flops );
    }

    /* ------ least square related method: ---------------------------- */

    Block<S> alloc_least_square_sol()
    {
        TIMER_TRACE;
        const int M = _nb_vect;
        const int N = _nbRHS;
        _Y = _Y_buf.sub_block( 0, 0, M, N );
        if (!_solution_computed) {
            const int M2 = M + _nbRHS;
            Block<S> YY = _Y_buf.sub_block( 0, 0, M2, N );
            YY.zero();
        }
        return _Y;
    }

    void solve_least_square(Block<S> &Y)
    {
        TIMER_LSQR;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );
        if (_solution_computed) {
            return;
        }
        factorize_last_column();

        _logger.notify_least_square_begin();

        Timer copy_time;
        copy_time.start();
        Block<S> F = get_F_tmp();
        copy_time.stop();
        /* F is copied before factorization and solve because
         * the factorization is only temporary
         * (last p line of F will be overwritten by IB udpate procedure)
         */
        FABULOUS_DEBUG("copy_time="<<copy_time.get_length());

        int64_t flops = 0;
        DynamicTauVector<S> disposable_tau;
        flops += pre_solve_factorization(F, disposable_tau);
        // Y <- \Lambda; to be overwritten with Y (the solution) by "gels" kernel
        const int Myy = _nb_vect + _nbRHS;
        const int Nyy = _nbRHS;
        Block<S> YY = _Y_buf.sub_block( 0, 0, Myy, Nyy );
        flops += compute_fully_updated_Lambda( YY, disposable_tau );

        Timer trsm_time;
        trsm_time.start();
        const int N = _nb_vect;
        int err = lapacke::trsm( N, _nbRHS,
                                 F.get_ptr(), F.get_leading_dim(),
                                 Y.get_ptr(), Y.get_leading_dim() );
        flops += lapacke::flops::trsm<S>(N, _nbRHS);
        if (err != 0) {
            FABULOUS_THROW(Kernel, "trsm err="<<err);
        }
        trsm_time.stop();
        FABULOUS_DEBUG("trsm_time="<<trsm_time.get_length());

        _solution_computed = true;
        _logger.notify_least_square_end( flops );
    }

    /*!
     * \brief Solve Least Square of projected problem \f$ F*Y-\Lambda \f$
     * \return \f$ Y = argmin(||F*Y-\Lambda||) \f$
     * \return \f$ LS = \Lambda - F*Y \f$
     */
    int64_t compute_proj_residual(Block<S> &LS, const Block<S> &Y)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( _Y_buf.get_ptr() == Y.get_ptr() );

        // save \Lambda in LS in order to compute LS in the end
        int64_t flops = 0;
        if (_phi_initialized)  {
            flops += compute_Lambda_phi(LS);
        } else {
            LS.copy(_lambda1);
        }

        const int M = _nb_vect + _nbRHS;
        const int N = _nbRHS;
        const int K = _nb_vect;
        const Block<S> F = _orig.sub_block( 0, 0, M, K );

        LS -= F * Y;
        flops += lapacke::flops::gemm<S>(M, N, K);
        return flops;
    }

    std::tuple<P,P,bool>
    check_least_square_residual(const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        Block<S> Y = alloc_least_square_sol();
        solve_least_square(Y);

        _logger.notify_least_square_begin();
        const int I = _nb_vect;
        const int N = _nbRHS;
        const Block<S> residual = _Y_buf.sub_block( I, 0, N, N );
        auto MinMaxConv = min_max_conv(residual, epsilon);
        int64_t flops = N * lapacke::flops::dot<S>(N);
        _logger.notify_least_square_end(flops);
        return MinMaxConv;
    }

    /* ---- ORTHOGONALIZATION related method: ---------------------------- */

    /*!
     * \brief increase the size of the Hessenberg
     *
     * increment the cursors
     */
    void increase(int block_size)
    {
        FABULOUS_ASSERT( check_room(block_size) );

        _block_size.push_back(block_size);
        ++ _nb_block_col;
        const int ref = _nb_vect;
        _nb_vect += block_size;
        _solution_computed = false;
        _last_column_factorized = false;

        if (   block_size < _nbRHS
               && ! (_restarted && _nb_block_col == 1)
               && _ib_col == -1 ) {
            _ib_col = ref;
            // FABULOUS_DEBUG("SET IB_COL");
        }
    }

    /*!
     * \brief \f$H\f$: last block column of the Hessenberg to be filled by
     * the orthogonalization process
     * \return \f$H\f$
     */
    Block<S> get_H()
    {
        const int N = _block_size.back();
        const int M = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( 0, J, M, N );
    }

    /*
     * Following member function are relative to the |Hj block at the bottom.
     */

    /*!
     * \brief \f$C\f$ part (orthogonalization coefficient of P against W)
     * to be filled during orthogonalization process
     * \return \f$C\f$
     */
    Block<S> get_C()
    {
        const int N = _block_size.back();
        const int M = _nbRHS - N; // nb discarded direction
        const int I = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, M, N );
    }

    /*!
     * \brief \f$D\f$:  R part of QR factorization of \f$ \widetilde{W} \f$
     * to be filled by orthogonalization process
     * \return \f$D\f$
     */
    Block<S> get_D()
    {
        const int N = _block_size.back();
        const int I = _nb_vect + (_nbRHS - N);
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, N, N );
    }

    void notify_ortho_end()
    {
        /* Copy new orthogonalization data of last column
         * into 'original' before QR factorization  */
        const int M = _nb_vect + _nbRHS;
        const int N = _block_size.back();
        const int J = _nb_vect - N;
        Block<S> HR_data = _data.sub_block( 0, J, M, N );
        Block<S> HR_orig = _orig.sub_block( 0, J, M, N );
        HR_orig.copy(HR_data);
    }

    /* ----------- RESTARTING related method: ---------------------------- */

    Block<S> get_F1new()
    {
        FABULOUS_ASSERT( _nb_vect == _nb_eigen_pair );
        const int M = _nb_eigen_pair + _nbRHS;
        const int N = _nb_eigen_pair;
        return _data.sub_block( 0, 0, M, N );
    }

    void reserve_DR(int nb_eigen_pair)
    {
        // CHECK this is called just after hessenberg initialization:
        FABULOUS_ASSERT( _nb_block_col == 0 );
        FABULOUS_ASSERT( _nb_vect == 0 );
        FABULOUS_ASSERT( _block_size.size() == 0 );

        _nb_eigen_pair = nb_eigen_pair;
        _restarted = true;
        increase(nb_eigen_pair);
    }

    Block<S> get_hess_block()
    {
        /* --> return _orig instead of _data
         * (because _data is in factorized form) */
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        return _orig.sub_block( 0, 0, M, N );
    }

}; // end class HessQRIBDR

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_HESS_QR_IB_DR_HPP
