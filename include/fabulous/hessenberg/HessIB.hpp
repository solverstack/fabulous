#ifndef FABULOUS_HESS_IB_HPP
#define FABULOUS_HESS_IB_HPP

#include <memory>

namespace fabulous {
namespace bgmres {
template<class> class HessIB;
}
}

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/gels.hpp"
#include "fabulous/utils/Logger.hpp"

namespace fabulous {
namespace bgmres {

/**
 * \brief Hessenberg for IB-BGMRes version.
 *
 * This hold the matrix denoted as \f$\mathscr{F}\f$ in the paper.
 *
 */
template<class S>
class HessIB
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
private:
    using P = primary_type;
private:
    Logger &_logger;
    const int _nbRHS; /*!< number of right hand sides */
    const int _max_krylov_space_size; /*!< size of allocated number of columns */
    int _nb_vect; /**< number of vector inside the hessenberg */
    int _nb_block_col; /**< number of block columns */

    std::vector<int> _block_size; /**< size of each block */

    Block<S> _data; /*!< the hessenberg data */
    Block<S> _data_tmp; /*!< copy of the hessenberg data for solve purpose */

    // needed to compute the rhs for Least square:
    Block<S> _phi; /**< Ref : IB BGMRes Dr : Page 10. */
    Block<S> _phi_buf; /**< buffer for phi. */
    bool _phi_initialized; /*!< whether phi was initialized and must be used to update RHS */

    Block<S> _lambda1; /**< initial value of RHS (projected problem) */

    bool _solution_computed;
    Block<S> _Y_buf; /*!< least square solution buffer */
    Block<S> _Y; /*!< least square solution */

public:
    /**
     * \brief create \f$\mathscr{F}\f$
     * \param max_krylov_space_size maximum size of Krylov search space
     * \param nbRHS number of right hand side
     * \param logger object logging the application trace and floating point operations count
     */
    HessIB(int max_krylov_space_size, int nbRHS, Logger &logger):
        _logger{logger},
        _nbRHS{nbRHS},
        _max_krylov_space_size{max_krylov_space_size},
        _nb_vect{0},
        _nb_block_col{0},
        _data{_max_krylov_space_size+_nbRHS, _max_krylov_space_size, "hessenberg_ib"},
        _data_tmp{_max_krylov_space_size+_nbRHS, _max_krylov_space_size, "hessenberg_ib_tmp"},
        _phi{},
        _phi_buf{},
        _phi_initialized{false},
        _solution_computed{false},
        _Y_buf{_max_krylov_space_size+_nbRHS, _nbRHS},
        _Y{}
    {
    }

    /** \brief number of vector in \f$\mathscr{F}\f$ */
    int get_nb_vect() const { return _nb_vect; }
    /** \brief number of lines in \f$\mathscr{F}\f$ */
    int get_nb_hess_line() const { return _nb_vect + _nbRHS; }

    /**
     * \brief set the initial value for the right hand side of
     * the LeastSquare problem (projected problem)
     * \param[in] lambda initial RHS for projected problem;
     *            R part of QR factorization of initial residual R0
     */
    void init_lambda(const Block<S> &lambda)
    {
        //Check
        if (lambda.get_nb_col() != _nbRHS || lambda.get_nb_row() != _nbRHS) {
            FABULOUS_THROW(
                Internal,
                "Lambda dimensions are wrong while initiating lambda in Hess\n"
            );
        }
        _lambda1 = lambda;
    }

    /**
     * \brief Initialize \f$ \Phi \f$ (refer to Annex, Eq 2.5)
     * \param p1 number of direction kept in the inexact breakdown on R0
     */
    void init_phi(int p1)
    {
        TIMER_TRACE;
        if (_phi_initialized) {
            FABULOUS_THROW(Internal, "This function cannot be called twice!");
        }
        _phi_initialized = true;

        const int N = _lambda1.get_nb_col();
        _phi_buf = Block<S>{_max_krylov_space_size+2*_nbRHS, _nbRHS, "hess_ib_phi"};
        _phi = _phi_buf.sub_block( 0, 0, N + p1, N );

        for (int i = 0; i < N; ++i) {
            _phi(i, i) = S{1.0};
        }
    }

    /**
     * \brief Update \f$\Phi\f$ if IB happened on R0
     *
     * \param[in] W1_W2 \f$ [\mathbb{W}_1, \mathbb{W}_2] \f$ packed in one block
     * \param p_jplus1 \f$p_{j+1}\f$ number of directions kept in
     *                 the last inexact breakdown
     */
    int64_t update_phi(const Block<S> &W1_W2, int p_jplus1)
    {
        TIMER_TRACE;
        if (!_phi_initialized) {
            return 0;
        }
        /* If _phi has been set, it means that an I.B. happened
         * on R0, and we have a modified RHS for least square
         * that depends on _phi */

        const int NJ = _nb_vect;
        const int M = _phi.get_nb_row() + p_jplus1;

        Block<S> new_phi = _phi_buf.sub_block( 0, 0, M, _nbRHS );

        // Compute newphi(nj+1:nj+p,:) := W1_W2^{H} * _phi(nj+1:nj+p,:)
        Block<S> updated_window = _phi.sub_block( NJ, 0, _nbRHS, _nbRHS );
        Block<S> tmp = updated_window.copy();
        W1_W2.dot(tmp, updated_window); // updated_window = W1_W2.dot(tmp);

        _phi = new_phi;
        return lapacke::flops::gemm<S>(_nbRHS, _nbRHS, _nbRHS);
    }

    /**
     * \brief Compute \f$\Lambda\f$ using \f$\Phi\f$ and \f$\Lambda_1\f$
     *
     * \param[out] Lambda \f$\Lambda = \Phi * \Lambda_1\f$
     */
    int64_t compute_Lambda(Block<S> &Lambda)
    {
        TIMER_TRACE;
        if (!_phi_initialized) { // no inexact breakdown on R0
            Lambda.copy(_lambda1);
            return 0;
        }
        Lambda = _phi * _lambda1;
        return lapacke::flops::gemm<S>(_phi.get_nb_row(), _nbRHS, _nbRHS);
    }

    /**
     * \brief display a dot for each non zero coefficient in matrix
     *
     *  Used for debug purpose
     */
    void display_hess_extended_bitmap(std::string name = "")
    {
        std::cerr<<name<<"\n";
        for (int j=0; j<_nb_vect+_nbRHS; ++j) {
            int col = 0;
            for (unsigned int i=0; i<_block_size.size(); ++i) {
                for (int t=0; t<_block_size[i]; ++t) {
                    auto m = std::norm(_data.at(j, col));
                    if ( m != primary_type{0.0} )
                        std::cerr<<"."<<" ";
                    else
                        std::cerr<<" "<<" ";
                    col++;
                }
                std::cerr<<"| ";
            }
            std::cerr<<std::endl;
        }
        std::cerr<<std::endl;
    }

    /**
     * \brief Check if there is room to another iteration
     */
    bool check_room(int block_size) const
    {
        return (_nb_vect + block_size <= _max_krylov_space_size);
    }

    /**
     * \brief increase the size of the Hessenberg
     *
     * increment the cursors
     */
    void increase(int block_size)
    {
        FABULOUS_ASSERT( check_room(block_size) );

        _block_size.push_back(block_size);
        ++ _nb_block_col;
        _nb_vect += block_size;
        _solution_computed = false;
    }

    /**
     * \brief Update \f$ H_j \f$ block into becoming new last row of
     * \f$ \mathscr{L} \f$ and new \f$ G_j \f$ block
     *
     * \f$  L_{j+1,}  =  W_1^{H} *  H_j \f$ <br/>
     * \f$  G_j       =  W_2^{H} *  H_j \f$ <br/>
     *
     * (this can be done in one operation as blocks following each other in memory)
     *
     * \param[in] W1_W2 \f$ \mathbb{W} = [\mathbb{W}_1, \mathbb{W}_2] \f$
     * ( Directions )
     */
    int64_t update_bottom_line(const Block<S>& W1_W2)
    {
        TIMER_TRACE;
        const int M = _nbRHS;
        const int N = _nb_vect;
        const int K = _nbRHS;

        Block<S> Hj = get_Hj();
        Block<S> tmp = Hj.copy();
        // Save Hj in order to write directly inside F

        W1_W2.dot(tmp, Hj); // Hj = W1_W2^{H} * tmp;
        return lapacke::flops::gemm<S>(M, N, K);
    }

    Block<S> alloc_least_square_sol()
    {
        TIMER_TRACE;
        const int M = _nb_vect;
        const int N = _nbRHS;
        _Y = _Y_buf.sub_block( 0, 0, M, N );
        if (!_solution_computed) {
            const int M2 = _nb_vect + _nbRHS;
            Block<S> YY = _Y_buf.sub_block( 0 ,0, M2, N );
            YY.zero();
        }
        return _Y;
    }

    void solve_least_square(Block<S> &Y)
    {
        TIMER_LSQR;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );
        if (_solution_computed) {
            return;
        }

        _logger.notify_least_square_begin();

        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        Block<S> YY = _Y_buf.sub_block( 0, 0, M, _nbRHS );
        int64_t flops = compute_Lambda(YY); // Y <- \Lambda; to be overwritten with Y in gels
        Block<S> F = get_F_tmp();

        const int err = lapacke::gels( // solve least square
            M, N, _nbRHS, // this function overwrites both F and Lambda
            F.get_ptr(),  F.get_leading_dim(),
            YY.get_ptr(), YY.get_leading_dim()
        );
        if (err != 0) {
            FABULOUS_THROW(Kernel, "gels (least square) err="<<err);
        }
        _solution_computed = true;
        flops += lapacke::flops::gels<S>(M, N, _nbRHS);
        _logger.notify_least_square_end(flops);
    }

    /**
     * \brief Solve Least Square of projected problem \f$ F*Y-\Lambda \f$
     * \return \f$ Y = argmin(||F*Y-\Lambda||) \f$
     * \return \f$ LS = F*Y-\Lambda \f$
     */
    int64_t compute_proj_residual(Block<S> &LS, Block<S> &Y)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );

        const int M = _nb_vect + _nbRHS;
        const int N = _nbRHS;
        const int K = _nb_vect;
        int64_t flops = 0;

        flops = compute_Lambda(LS);// save \Lambda in LS in order to compute LS in the end
        const Block<S> F = get_F();
        LS -= F * Y;
        flops += lapacke::flops::gemm<S>(M, N, K);
        return flops;
    }

    std::tuple<P,P,bool>
    check_least_square_residual(const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        Block<S> Y = alloc_least_square_sol();
        solve_least_square(Y);

        _logger.notify_least_square_begin();
        const int I = _nb_vect;
        const int N = _nbRHS;
        const Block<S> residual = _Y_buf.sub_block( I, 0, N, N );
        auto MinMaxConv = min_max_conv(residual, epsilon);
        int64_t flops = N * lapacke::flops::dot<S>(N);
        _logger.notify_least_square_end(flops);
        return MinMaxConv;
    }

    /**
     * \brief \f$ \mathscr{F} \f$: the whole projected matrix
     * \return \f$ \mathscr{F} \f$
     */
    Block<S> get_F()
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        return _data.sub_block( 0, 0, M, N );
    }

    /**
     * \brief \f$ \mathscr{F} \f$: the whole projected matrix (Block Hessenberg)
     * \return \f$ \mathscr{F} \f$
     */
    Block<S> get_F_tmp()
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        Block<S> tmp = _data_tmp.sub_block( 0, 0, M, N );
        Block<S>   F = _data.sub_block( 0, 0, M, N );
        tmp.copy(F);
        return tmp;
    }

    Block<S> get_hess_block()
    {
        return get_F();
    }

    /**
     * \brief \f$H\f$: last block column of the Hessenberg to be filled by
     * the orthogonalization process
     * \return \f$H\f$
     */
    Block<S> get_H()
    {
        const int N = _block_size.back();
        const int M = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( 0, J, M, N );
    }

    /*
     * Following member function are relative to the |Hj block at the bottom.
     */

    /**
     * \brief \f$ \mathbb{H}_j \f$: the bottom part of \f$ \mathscr{F} \f$
     * \return \f$ \mathbb{H}_j \f$
     */
    Block<S> get_Hj()
    {
        const int I = _nb_vect;
        const int M = _nbRHS;
        const int N = _nb_vect;
        return _data.sub_block( I, 0, M, N );
    }

    /**
     * \brief \f$C\f$ part (orthogonalization coefficient of P against W) to be filled
     * during orthogonalization process
     * \return \f$C\f$
     */
    Block<S> get_C()
    {
        const int N = _block_size.back();
        const int M = _nbRHS - N; // nb direction discarded
        const int I = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, M, N );
    }

    /**
     * \brief \f$D\f$:  R part of QR factorization of \f$ \widetilde{W} \f$
     *  to be filled by orthogonalization process
     * \return \f$D\f$
     */
    Block<S> get_D()
    {
        const int N = _block_size.back();
        const int I = _nb_vect + (_nbRHS - N);
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, N, N );
    }

    void notify_ortho_end() const {/*nop*/}

}; // end class HessIB

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_HESS_IB_HPP
