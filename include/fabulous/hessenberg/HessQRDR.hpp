#ifndef FABULOUS_HESS_QR_DR_HPP
#define FABULOUS_HESS_QR_DR_HPP

namespace fabulous {
namespace bgmres {
template<class> class HessQRDR;
}
}


#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/qrf.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/hessenberg/DynamicTauFactor.hpp"

namespace fabulous {
namespace bgmres {

/**
 * \brief Hessenberg for Incremental QR version with Deflated Restarting
 *
 * Format: <br/>
 \verbatim
 | R X X |
 | H R X |
 |   H R |
 |     H |
 \endverbatim
 * with: <br/>
 * R Triangular Sup <br/>
 * H Householders reflectors (trapezoidal)
 *
 * \warning the whole matrix is allocated, its size is driven by the restart parameter.
 */
template<class S>
class HessQRDR
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
private:
    using P = primary_type;
private:
    Logger &_logger;
    const int _nbRHS; /*!< number of right hand sides */

    int _nb_vect; /*!< number of vector inside the hessenberg */
    int _nb_eigen_pair; /*!< number of eigen pair in deflated_restarting */

    Block<S> _data; /*!< the hessenberg factorized data */
    Block<S> _orig; /*!< unfactorized hessenberg, used for restarting */
    DynamicTauVector<S> _tau; /*!< tau factors for each QR factorization */

    Block<S> _Lambda1; /*!< initial rhs for the least square problem */
    Block<S> _Lambda; /*!< rhs for the least square problem */
    bool _last_column_factorized; /*!< whether last column is factorized */
    bool _solution_computed; /*!< whether solution (for current size) is computed */
    Block<S> _Y_buf; /*!< least square solution buffer */
    Block<S> _Y; /*!< least square solution */

    std::vector<int> _block_size; /*!< size of each block */

private:

    void factorize_last_column()
    {
        TIMER_TRACE;
        if (_last_column_factorized) {
            return;
        }

        _logger.notify_facto_begin();
        int64_t flops = 0;
        const int N = _block_size.back();
        const int J = _nb_vect - N;
        const char Trans = Arithmetik<S>::ltrans;

        { /* STEP 1: Loop over each block in last column to
           apply already computed Householders */

            const int M = _nb_vect + _nbRHS;
            Block<S> last_column = _data.sub_block( 0, J, M, N );

            for (auto &tau : _tau) {
                flops += tau.apply(Trans, last_column);
            }
        }

        { /* STEP 2: Call QR on the two last block of last col */
            const int M = N + _nbRHS;
            Block<S> A = _data.sub_block( J, J, M, N );
            _tau.emplace_back(DynamicTauFactor<S>::PANEL, A, J, -1, -1);
            std::vector<S> &tau = _tau.back().get_buf();
            int err = lapacke::geqrf( M, N,
                                      A.get_ptr(), A.get_leading_dim(), tau);
            flops += lapacke::flops::geqrf<S>(M, N);
            if (err != 0) {
                FABULOUS_THROW(Kernel, "geqrf 'last block' err="<<err);
            }
        }

        { /* STEP 3: Apply Q^H generated at step 2 to last block of RHS */
            flops += _tau.back().apply(Trans, _Lambda);
        }
        _last_column_factorized = true;
        _logger.notify_facto_end(flops);
    }

    Block<S> rebuild_original()
    {   // experimental function, may not works depending on implementation of geqrf/unmqr
        // DO NOT RELY ON THIS FUNCTION
        TIMER_TRACE;
        const int M = _nb_vect+_nbRHS;
        const int N = _nb_vect;

        std::vector<S> newtau;
        int tau_size = 0;
        for (auto &tau : _tau) {
            tau_size += _tau.get_buf().size();
        }
        newtau.resize(tau_size);

        FABULOUS_ASSERT( tau_size == N );
        tau_size = 0;
        for (auto &tau : _tau) {
            const auto &t = _tau.get_buf();
            std::copy(t.begin(), t.end(), newtau.begin()+tau_size);
            tau_size += t.size();
        }

        Block<S> R{M, N};
        // out of place gqr:
        lapacke::lacpy('U', M, N,
                       _data.get_ptr(), _data.get_leading_dim(),
                       R.get_ptr(),  R.get_leading_dim());
        lapacke::unmqr('N', M, N, N,
                       _data.get_ptr(), _data.get_leading_dim(), newtau.data(),
                       R.get_ptr(), R.get_leading_dim() );
        return R;
    }

public:
    HessQRDR(int max_krylov_space_size, int nbRHS, Logger &logger):
        _logger{logger},
        _nbRHS{nbRHS},
        _nb_vect{0},
        _nb_eigen_pair{0},
        _data{max_krylov_space_size+nbRHS, max_krylov_space_size},
        _orig{max_krylov_space_size+nbRHS, max_krylov_space_size},
        _tau{},
        _Lambda{max_krylov_space_size+nbRHS, nbRHS},
        _last_column_factorized{false},
        _solution_computed{false},
        _Y_buf{max_krylov_space_size, nbRHS},
        _Y{},
        _block_size{}
    {
        const int max_nb_iter = (max_krylov_space_size / nbRHS) + 1;
        _tau.reserve( max_nb_iter );
    }

    void set_rhs(const Block<S> &Lambda1)
    {
        _Lambda1 = Lambda1;
        _Lambda.copy(Lambda1);
    }

    Block<S> get_H()
    {
        const int M = _nb_vect;
        const int N = _nbRHS;
        const int J = _nb_vect - N;
        return _data.sub_block( 0, J, M, N );
    }

    Block<S> get_R()
    {
        const int N = _nbRHS;
        const int I = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, N, N );
    }

    Block<S> get_H1new()
    {
        FABULOUS_ASSERT( _nb_vect == _nb_eigen_pair );
        const int M = _nb_eigen_pair + _nbRHS;
        const int N = _nb_eigen_pair;
        return _data.sub_block(0, 0, M, N );
    }

    Block<S> alloc_least_square_sol()
    {
        TIMER_TRACE;
        const int M = _nb_vect;
        const int N = _nbRHS;
        _Y = _Y_buf.sub_block( 0, 0, M, N );
        if (!_solution_computed) {
            _Y.zero();
        }
        return _Y;
    }

    void solve_least_square(Block<S> &Y)
    {
        TIMER_LSQR;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );

        if (_solution_computed) {
            return;
        }
        factorize_last_column();

        _logger.notify_least_square_begin();
        const int M = _nb_vect;
        const Block<S> R      = _data.sub_block  ( 0, 0, M, M );
        const Block<S> Lambda = _Lambda.sub_block( 0, 0, M, _nbRHS);

        Y.copy(Lambda);
        lapacke::trsm(
            M, _nbRHS,
            R.get_ptr(), R.get_leading_dim(),
            Y.get_ptr(), Y.get_leading_dim()
        );
        int64_t flops = lapacke::flops::trsm<S>(M, _nbRHS);
        _solution_computed = true;
        _logger.notify_least_square_end(flops);
    }

    void compute_proj_residual(Block<S> &LS, const Block<S> &Y)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );

        const int M = _nb_vect + _nbRHS;
        const int N = _nbRHS;
        const int K = _nb_vect;
        FABULOUS_ASSERT( Y.get_nb_row()  == K );
        FABULOUS_ASSERT( Y.get_nb_col()  == N );
        FABULOUS_ASSERT( LS.get_nb_row() == M );
        FABULOUS_ASSERT( LS.get_nb_col() == N );
        FABULOUS_ASSERT(LS.get_nb_col() == _Lambda1.get_nb_col());
        FABULOUS_ASSERT(LS.get_nb_row() >= _Lambda1.get_nb_row());

        const Block<S> F = _orig.sub_block( 0, 0, M, K );
        LS.copy(_Lambda1);
        LS -= F * Y;
    }

    void notify_ortho_end()
    {
        /* Copy new orthogonalization data of last column
         * into 'original' before QR factorization
         */
        TIMER_TRACE;
        const int M = _nb_vect + _nbRHS;
        const int N = _block_size.back();
        const int J = _nb_vect - N;
        Block<S> HR_data, HR_original;
        HR_data     = _data.sub_block( 0, J, M, N );
        HR_original = _orig.sub_block( 0, J, M, N );
        HR_original.copy(HR_data);

        factorize_last_column(); /* Needed here to factorize H1new */
    }

    Block<S> get_hess_block()
    {
        /* --> return _orig instead of _data
         * (because _data is in factorized form) */
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        return _orig.sub_block( 0, 0, M, N );
    }

    std::tuple<P,P,bool>
    check_least_square_residual(const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        factorize_last_column();

        _logger.notify_least_square_begin();
        const int I = _nb_vect;
        const int N = _nbRHS;
        const Block<S> residual = _Lambda.sub_block( I, 0, N, N );
        auto MinMaxConv = min_max_conv(residual, epsilon);
        int64_t flops = N * lapacke::flops::dot<S>(N);
        _logger.notify_least_square_end(flops);
        return MinMaxConv;
    }

    void increase(int block_size)
    {
        _nb_vect += block_size;
        _block_size.emplace_back(block_size);
        _last_column_factorized = false;
        _solution_computed = false;
    }

    void reserve_DR(int nb_eigen_pair)
    {
        // CHECK this is called just after hessenberg initialization:
        FABULOUS_ASSERT( _nb_vect == 0 );
        FABULOUS_ASSERT( _block_size.size() == 0 );

        _nb_eigen_pair = nb_eigen_pair;
        increase(nb_eigen_pair);
    }

    int get_nb_vect() const { return _nb_vect; }

}; // end class HessQRDR

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_HESS_QR_DR_HPP
