#ifndef FABULOUS_HESS_QR_CHAMSUB_HPP
#define FABULOUS_HESS_QR_CHAMSUB_HPP

#ifdef FABULOUS_USE_CHAMELEON

#include <unordered_map>
#include <utility>
#include <memory>
#include <tuple>
#include <vector>
#include <string>
#include <sstream>

#include <cassert>
#include <chameleon.h>

namespace fabulous {
namespace bgmres {
template<class S> class HessQR_CHAMSUB;
}
}

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Chameleon.hpp"
#include "fabulous/utils/Logger.hpp"

#include "fabulous/kernel/chameleon.hpp"
#include "fabulous/kernel/flops.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/data/ChamDesc.hpp"

namespace fabulous {
namespace bgmres {

/*!
 * \brief Hessenberg for Incremental QR version with chamelon back-end
 *
 * This implementation use the 'low level' chameleon API (*_Tile_Async)
 *
 * Format: <br/>
 \verbatim
 | R X X |
 | H R X |
 |   H R |
 |     H |
 \endverbatim
 * with: <br/>
 * R Triangular Sup <br/>
 * H Householders reflectors (trapezoidal)
 *
 * \note only the upper hessenberg block are allocated.
 */
template<class S>
class HessQR_CHAMSUB
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
private:
    using P = primary_type;
private:
    Logger &_logger;
    const int _max_nb_iter; /*!< maximum number of iterations */
    const int _mt; /*!< max number of (big) block line (_nbRHSx_nbRHS) in hessenberg */
    const int _nt; /*!< max number of (big) block column (_nbRHSx_nbRHS) in hessenberg */
    const int _nbRHS; /*!< number of right-hand-sides; AND size of one (big) block */
    int _nb_block_col; /*!< number of block columns in current hessenberg */
    int _nb_vect; /*!< number of columns in current hessenberg */

    std::vector<DescPtr> _tau; /*!< tau factors for each QR factorization*/

    HessenbergTileMapper<S> _hessenbergs_tiles; /*!< the hessenbergs tiles */
    ClassicTileMapper<S> _rhs_tiles; /*!< the (least square) right-hand-side tiles */
    LapackTileMapper<S> _tmp_tiles; /*!<  tmp tiles / lapack layout - HR ortho */

    ChamDesc<S> _hess_cont;
    ChamDesc<S> _rhs_cont;
    ChamDesc<S> _tmp_cont;

    SeqPtr _seq; /*!< MORSE sequence object */

    #ifdef FABULOUS_DEBUG_MODE
    Block<S> _orig; /*!< original (not factorized) hessenberg
                         stored as Lapack matrix for debug purpose
                         (more specifically, in the check_arnoldi_formula() function) */
    #endif

    bool _last_column_factorized; /*!< whether last column was factorized */
    bool _solution_computed; /*!< whether solution was computed(for last column) */

    Block<S> _Y; /*!< least square solution */
    Block<S> _HR; /*!< last column as filled by orthogonalization */
    Block<S> _lambda1; /*!< initial rhs for projected problem */
    Block<S> _residual; /*!< temporary buffer to eval residual norm */

public:

    HessQR_CHAMSUB(int max_krylov_space_size, int nbRHS, Logger &logger):
        _logger{logger},
        _max_nb_iter{ (max_krylov_space_size/nbRHS) +1 },
        _mt{ _max_nb_iter+1 },
        _nt{ _max_nb_iter },
        _nbRHS{nbRHS},
        _nb_block_col{0},
        _nb_vect{0},
        _hessenbergs_tiles{_mt, _nt, nbRHS},
        _rhs_tiles{_mt, 2, nbRHS},
        _tmp_tiles{_mt+1, 2, nbRHS},
        _hess_cont{ChamDesc<S>::create_parent_descriptor(_hessenbergs_tiles, _mt, _nt)},
        _rhs_cont {ChamDesc<S>::create_parent_descriptor(_rhs_tiles, _mt, 2)},
        _tmp_cont {ChamDesc<S>::create_parent_descriptor(_tmp_tiles, _mt+1, 2)},
        #ifdef FABULOUS_DEBUG_MODE
        _orig{_mt*nbRHS, _nt*nbRHS},
        #endif
        _last_column_factorized{false},
        _solution_computed{false},
        _Y { _tmp_tiles.get_block().sub_block(0, 0, _mt*nbRHS, nbRHS)},
        _HR{ _tmp_tiles.get_block().sub_block(0, nbRHS, _mt*nbRHS, nbRHS)},
        _lambda1 {_tmp_tiles.get_block().sub_block(_mt*nbRHS,     0, nbRHS, nbRHS)},
        _residual{_tmp_tiles.get_block().sub_block(_mt*nbRHS, nbRHS, nbRHS, nbRHS)}
    {
        _tau.reserve(_max_nb_iter);
        CHAMELEON_sequence_t *sequence = nullptr;
        CHAMELEON_Sequence_Create(&sequence);
        _seq.reset(sequence, SeqDeleter{});
    }

    template<class Block>
    void debug_dump_orig(Block &Hm, int m, int n)
    {
        #ifdef FABULOUS_DEBUG_MODE
        Hm = _orig.sub_block(0, 0, m, n);
        #else
        FABULOUS_THROW(
            Unsupported,
            "You must define the FABULOUS_DEBUG_MODE preprocessor constant to use this function"
        );
        (void) Hm; (void) m; (void) n;
        #endif
    }

private:

    ChamDesc<S> get_sub(ChamDesc<S> &parent, int it, int jt, int mt, int nt)
    {
        return ChamDesc<S>::create_sub_descriptor(parent, it, jt, mt, nt);
    }

    /*!
     * \brief compute qr factorization of the last column of
     * the block hessenberg matrix
     */
    void factorize_last_column()
    {
        if (_last_column_factorized) {
            return;
        }
        _logger.notify_facto_begin();
        CHAMELEON_enum trans = Arithmetik<S>::mtrans;

        namespace fps = lapacke::flops;
        int64_t flops = 0;

        /* STEP 1: Loop over each block in last column to
         * apply already computed Householders
         * This is almost the only loop that can benefit from
         * Chameleon Asynchronicity
         * (and it may require very large nbRHS to actually see a speedup)
         */
        const int KT = _nb_block_col-1;
        for (int i = 0; i < KT; ++i) {
            ChamDesc<S> A = get_sub(_hess_cont, i, i,  2, 1);
            ChamDesc<S> C = get_sub(_hess_cont, i, KT, 2, 1);
            CHAM_desc_t *T = _tau[i].get();

            int err = chameleon::unmqr<S>(trans, A.get(), T, C.get(), _seq.get());
            flops += fps::unmqr<S>(2*_nbRHS, _nbRHS, _nbRHS);
            if (err != 0) {
                FABULOUS_THROW(Kernel, "unmqr 'step' err="<<err);
            }
        }

        /* STEP 2: Call QR on the very last block of last col */
        DescPtr tau;
        ChamDesc<S> A = get_sub(_hess_cont, KT, KT, 2, 1);
        int err = chameleon::geqrf<S>(A.get(), tau, _seq.get());
        flops += fps::geqrf<S>(2*_nbRHS, _nbRHS);
        if (err != 0) {
            FABULOUS_THROW(Kernel, "geqrf 'last block' err="<<err);
        }
        _tau.push_back(tau);

        /* STEP 3: Apply Q^H generated at step 2 to last block of RHS */
        ChamDesc<S> C = get_sub(_rhs_cont, KT, 0, 2, 1);
        err = chameleon::unmqr<S>(trans, A.get(), tau.get(), C.get(), _seq.get());
        flops += fps::unmqr<S>(2*_nbRHS, _nbRHS, _nbRHS);

        if (err != 0) {
            FABULOUS_THROW(Kernel, "unmqr 'RHS' err="<<err);
        }
        _last_column_factorized = true;
        _logger.notify_facto_end(flops);
    }

public:

    /*!
     * \brief set the \f$\Lambda_1\f$ block (which is nbRHS x nbRHS)
     * (RHS of least square problem)
     */
    void set_rhs(Block<S> &lambda1)
    {
        FABULOUS_ASSERT( lambda1.get_nb_col() == _nbRHS );
        FABULOUS_ASSERT( lambda1.get_nb_row() == _nbRHS );

        // lambda1.display("lambda1");
        _lambda1.copy(lambda1);

        ChamDesc<S> LAMBDA_1 = get_sub(_rhs_cont, 0, 0, 1, 1);
        ChamDesc<S> L1_tmp   = get_sub(_tmp_cont, _mt, 0, 1, 1);
        // equivalent to CHAMELEON_Lapack_to_Tile without extra descriptor creation
        // ( because this would 'consume' a mpi tag )
        chameleon::lacpy<S>(ChamUpperLower, L1_tmp.get(), LAMBDA_1.get(), _seq.get());
    }

    Block<S> get_H()
    {
        auto h = _HR.sub_block( 0, 0, _nb_vect, _nbRHS );
        h.zero();
        return h;
    }

    Block<S> get_R()
    {
        auto r = _HR.sub_block( _nb_vect, 0, _nbRHS, _nbRHS );
        r.zero();
        return r;
    }

    Block<S> get_H1new()
    {
        FABULOUS_THROW(Unsupported, "HessChamQR not compatible with DeflatedRestarting");
        return Block<S>{};
    }

    void notify_ortho_end()
    {
        const int KT = _nb_block_col;
        FABULOUS_ASSERT( _nb_vect == KT * _nbRHS );
        // FABULOUS_ASSERT( _la_tmp.get_nb_col() == _nbRHS );
        // FABULOUS_ASSERT( _la_tmp.get_nb_row() == _nb_vect+_nbRHS );

        #ifdef FABULOUS_DEBUG_MODE
        const int N = _nbRHS;
        const int M = _nb_vect + _nbRHS;
        const int J = _nb_vect - N;
        Block<S> HR_orig = _orig.sub_block( 0, J, M, N );
        HR_orig.copy(_HR);
        #endif
        ChamDesc<S> HR_h = get_sub(_hess_cont, 0, KT-1, KT+1, 1);
        ChamDesc<S> HR_t = get_sub(_tmp_cont, 0, 1, KT+1, 1);
        // equivalent to CHAMELEON_Lapack_to_Tile without extra descriptor creation
        // ( because this would 'consume' a mpi tag )
        chameleon::lacpy<S>(ChamUpperLower, HR_t.get(), HR_h.get(), _seq.get());
    }

    Block<S> alloc_least_square_sol()
    {
        return _Y.sub_block(0, 0, _nb_vect, _nbRHS);
    }

    template< class Block >
    void solve_least_square(Block &Y)
    {
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );
        if (_solution_computed) {
            return;
        }
        factorize_last_column();
        _logger.notify_least_square_begin();

        const int K = _nb_block_col;
        ChamDesc<S> R         = get_sub(_hess_cont, 0, 0, K, K);
        ChamDesc<S> Lambda    = get_sub(_rhs_cont, 0, 0, K, 1);
        ChamDesc<S> LambdaTMP = get_sub(_rhs_cont, 0, 1, K, 1);

        // copy (least square RHS (Lambda)) to Lambda_tmp:
        chameleon::lacpy<S>(ChamUpperLower, Lambda.get(), LambdaTMP.get(), _seq.get());

        // compute the solution
        chameleon::trsm<S>(R.get(), LambdaTMP.get(), _seq.get());
        namespace fps = lapacke::flops;
        int flops = fps::trsm<S>(_nb_vect, _nbRHS);

        ChamDesc<S> YY = get_sub(_tmp_cont, 0, 0, _mt, 1);

        // equivalent to CHAMELEON_Tile_to_Lapack without extra descriptor creation
        // ( because this would 'consume' a mpi tag )
        chameleon::lacpy<S>(ChamUpperLower, LambdaTMP.get(), YY.get(), _seq.get());
        CHAMELEON_Sequence_Wait(_seq.get());
        _solution_computed = true;
        _logger.notify_least_square_end(flops);
    }

    void compute_proj_residual(const Block<S>&, const Block<S>&)
    {
        FABULOUS_THROW(Unsupported, "HessChamQR not compatible with DeflatedRestarting");
    }

    std::tuple<P,P,bool>
    check_least_square_residual(const std::vector<P> &epsilon)
    {
        factorize_last_column();

        _logger.notify_least_square_begin();
        const int IT = _nb_block_col;
        FABULOUS_ASSERT( _nb_vect == IT * _nbRHS );
        ChamDesc<S> Lambda = get_sub(_rhs_cont, IT, 0, 1, 1);
        ChamDesc<S> RESI   = get_sub(_tmp_cont, _mt, 1, 1, 1);
        // equivalent to CHAMELEON_Tile_to_Lapack without extra descriptor creation
        // ( because this would 'consume' a mpi tag )
        chameleon::lacpy<S>(ChamUpperLower, Lambda.get(), RESI.get(), _seq.get());
        CHAMELEON_Sequence_Wait(_seq.get());

        auto MinMaxConv = min_max_conv(_residual, epsilon);
        int64_t flops = _nbRHS * lapacke::flops::dot<S>(_nbRHS);
        _logger.notify_least_square_end(flops);
        return MinMaxConv;
    }

    Block<S> get_hess_block()
    {
        FABULOUS_THROW(Unsupported, "HessChamQR not compatible with DeflatedRestarting");
        return Block<S>{};
    }

    void increase(int block_size)
    {
        FABULOUS_ASSERT( block_size == _nbRHS );
        ++ _nb_block_col;
        _nb_vect += _nbRHS;
        _last_column_factorized = false;
        _solution_computed = false;
    }

    void reserve_DR(int)
    {
        FABULOUS_THROW(Unsupported, "HessChamQR not compatible with DeflatedRestarting");
    }

    inline int get_nb_vect() const { return _nb_vect; }

}; // end class HessChamQR

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_USE_CHAMELEON

#endif // FABULOUS_HESS_QR_CHAMSUB_HPP
