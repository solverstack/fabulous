#ifndef FABULOUS_HESS_IB_DR_HPP
#define FABULOUS_HESS_IB_DR_HPP

#include <memory>
#include <cassert>

namespace fabulous {
namespace bgmres {
template<class> class HessIBDR;
}
}

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/gels.hpp"
#include "fabulous/utils/Logger.hpp"

namespace fabulous {
namespace bgmres {

/**
 * \brief Hessenberg for IB-BGMRes version.
 *
 * This hold the matrix denoted as \f$\mathscr{F}\f$ in the paper.
 */
template<class S>
class HessIBDR
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
private:
    using P = primary_type;
private:
    Logger &_logger;
    const int _nbRHS;  /*!< number of right hand sides */
    const int _max_krylov_space_size; /*!< size of allocated number of columns */
    int _nb_block_col; /*!< number of block columns */
    int _nb_vect; /*!< number of vector inside the hessenberg */
    int _nb_eigen_pair; /*!< number of eigen pair in deflated_restarting */

    bool _restarted;
    std::vector<int> _block_size; /*!< size of each block */
    Block<S> _phi_buf; /*!< buffer for phi */
    Block<S> _phi; /*!< needed to compute the rhs for Least square.
                    Ref: IB BGMRes Dr : Page 10. */
    bool _phi_initialized; /*!< whether phi was initialized and must be used to update RHS */

    Block<S> _data; /*!< the hessenberg data */
    Block<S> _data_tmp; /*!< copy of the hessenberg data for solve purpose */

    Block<S> _lambda1; /*!< initial value of RHS (projected problem) */
    Block<S> _Lambda; /*!< rhs of least square problem */

    bool _solution_computed; /*!< whether solution (for current size)
                              * was computed */
    Block<S> _Y_buf; /*!< least square solution */
    Block<S> _Y; /*!< least square solution */

public:
    /**
     * \brief create \f$\mathscr{F}\f$
     * \param max_krylov_space_size maximum size of Krylov search space
     * \param nbRHS number of right hand side
     * \param logger object logging the application trace and floating point operations count
     */
    HessIBDR(int max_krylov_space_size, int nbRHS, Logger &logger):
        _logger{logger},
        _nbRHS{nbRHS},
        _max_krylov_space_size{max_krylov_space_size},
        _nb_block_col{0},
        _nb_vect{0},
        _nb_eigen_pair{0},
        _restarted{false},
        _phi_buf{},
        _phi{},
        _phi_initialized{false},
        _data{_max_krylov_space_size+_nbRHS, _max_krylov_space_size, "hessenberg_ibdr"},
        _data_tmp{_max_krylov_space_size+_nbRHS, _max_krylov_space_size, "hessenberg_ibdr_tmp"},
        _lambda1{},
        _Lambda{},
        _solution_computed{false},
        _Y_buf{_max_krylov_space_size+_nbRHS, _nbRHS},
        _Y{}
    {
    }

    /** \brief number of vector in \f$\mathscr{F}\f$ */
    int get_nb_vect() const { return _nb_vect; }
    /** \brief number of lines in \f$\mathscr{F}\f$ */
    int get_nb_hess_line() const { return get_nb_vect() + _nbRHS; }

    /**
     * \brief set the initial value for the right hand side of the
     *  LeastSquare problem (projected problem)
     *
     * \param[in] lambda initial RHS for projected problem;
     * R part of QR factorization of initial residual R0
     */
    void init_lambda(const Block<S> &lambda)
    {
        // Check
        if (lambda.get_nb_col() != _nbRHS
            || lambda.get_nb_row() != _nbRHS + _nb_eigen_pair) {
            FABULOUS_THROW(
                Internal,
                "Lambda dimensions are wrong while initiating lambda in Hess\n"
            );
        }
        _lambda1 = lambda;
    }

    /**
     * \brief Initialize \f$ \Phi \f$ (refer to Annex, Eq 2.5) (INEXACT BREAKDOWN ON R0)
     * \param p1 number of direction kept in the inexact breakdown on R0
     */
    void init_phi_ibr0(int p1)
    {
        TIMER_TRACE;
        if (_phi_initialized) {
            FABULOUS_THROW(Internal, "This function cannot be called twice!");
        }
        _phi_initialized = true;

        const int N = _lambda1.get_nb_col();
        _phi_buf = Block<S>{_max_krylov_space_size+2*_nbRHS, _nbRHS};
        _phi = _phi_buf.sub_block( 0, 0, N+p1, N );

        for (int i = 0; i < N; ++i) {
            _phi(i, i) = S{1.0};
        }
    }

    /**
     * \brief Initialize \f$ \Phi \f$ (RESTART) (refer to IB-BGMRES-DR, Section 3.1.4 (Proposition 3))
     * \param k number of eigen pair (size of first block) in deflated restarting
     */
    void init_phi_deflated_restart(int k)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT(k == _nb_eigen_pair);
        FABULOUS_ASSERT( _lambda1.get_nb_row() == k+_nbRHS );

        _restarted = true;

        if (_phi_initialized) {
            FABULOUS_THROW(Internal, "This function cannot be called twice!");
        }
        _phi_initialized = true;

        const int N = _lambda1.get_nb_col();
        _phi_buf = Block<S>{_max_krylov_space_size+2*_nbRHS, _nbRHS};
        _phi = _phi_buf.sub_block( 0, 0, N+k, N );

        for (int i = 0; i < N; ++i) {
            _phi(k+i, i) = S{1.0};
        }
    }

    /**
     * \brief Update \f$\Phi\f$ if IB happened on R0 or restarted procedure
     *
     * \param[in] W1_W2 \f$ [\mathbb{W}_1, \mathbb{W}_2] \f$ packed in one block
     * \param p_jplus1 \f$p_{j+1}\f$ number of directions kept in the last inexact breakdown
     *
     */
    int64_t update_phi(const Block<S> &W1_W2, const int p_jplus1)
    {
        TIMER_TRACE;
        if (!_phi_initialized) {
            return 0;
        }
        /* If _phi has been set, it means that an I.B. happened
         * on R0, and we have a modified RHS for least square
         * that depends on _phi */
        const int NJ = _nb_vect;
        const int M = _phi.get_nb_row() + p_jplus1;

        Block<S> new_phi = _phi_buf.sub_block( 0, 0, M, _nbRHS );

        // Compute newphi(nj+1:nj+p,:) := W1_W2^{H} * oldphi(nj+1:nj+p,:)
        Block<S> updated_window = _phi.sub_block( NJ, 0, _nbRHS, _nbRHS );
        Block<S> tmp = updated_window.copy();
        W1_W2.dot(tmp, updated_window);//   updated_window = trans(W1_W2) * tmp;

        _phi = new_phi;
        return lapacke::flops::gemm<S>(_nbRHS, _nbRHS, _nbRHS);
    }

    /**
     * \brief Compute \f$\Lambda_{j}\f$ using \f$\Phi_j\f$ and \f$\Lambda_1\f$
     *
     * \param[out] Lambda \f$ \Lambda_j = \begin{cases}
     \Phi_j * \Lambda_1 & \text{if IB on R0 (IB-BGMRES-DR, eq 25)} \\
     \left[ \begin{array}[cc] \\
     I_{p_1} & \Phi_j \\
     0_{(n_j+p-p1)\times p1} &
     \end{array}\right] * \Lambda_1 & \text{if restarted procedure (annex eq 2.6)}
     \end{cases} \f$
     */
    int64_t compute_Lambda(Block<S> &Lambda)
    {
        TIMER_TRACE;
        if (!_phi_initialized) { // no inexact breakdown on R0
            Lambda.copy(_lambda1);
            return 0;
        }

        if (_restarted) {
            FABULOUS_ASSERT( Lambda.get_nb_row() == _nb_vect + _nbRHS );
            FABULOUS_ASSERT( Lambda.get_nb_row() == _phi.get_nb_row() );

            const int k = _nb_eigen_pair;
            const int p = _nbRHS;
            const int M = Lambda.get_nb_row();
            int64_t flops = 0;

            // restart update method :: Lambda1: (k+p, p)
            const Block<S> phi_1 = _phi.sub_block( 0, 0,   k, p );
            const Block<S> phi_2 = _phi.sub_block( k, 0, M-k, p );
            const Block<S> l1_1  = _lambda1.sub_block( 0, 0, k, p );
            const Block<S> l1_2  = _lambda1.sub_block( k, 0, p, p );
            Block<S> L_1 = Lambda.sub_block( 0, 0,   k, p );
            Block<S> L_2 = Lambda.sub_block( k, 0, M-k, p );

            L_1.copy(l1_1);
            L_1 += phi_1 * l1_2;
            L_2 = phi_2 * l1_2;

            flops += lapacke::flops::gemm<S>(  k, p, p);
            flops += lapacke::flops::gemm<S>(M-k, p, p);
            return flops;
        } else {
            // breakdown on R0 restart method  :: Lambda1: (p, p)
            Lambda = _phi * _lambda1;
            return lapacke::flops::gemm<S>(Lambda.get_nb_row(), _nbRHS, _nbRHS);
        }
    }

    /**
     * \brief display a dot for each non zero coefficient in matrix
     *
     *  Used for debug purpose
     */
    void display_hess_extended_bitmap(std::string name = "")
    {
        std::cerr<<name<<"\n";
        for (int i=0; i<_nb_vect+_nbRHS; ++i) {
            int col = 0;
            for (unsigned int j=0; j<_block_size.size(); ++j) {
                for (int t=0; t<_block_size[j]; ++t) {
                    auto m = std::norm(_data.at(i, col));
                    if ( m != primary_type{0.0} )
                        std::cerr<<"."<<" ";
                    else
                        std::cerr<<" "<<" ";
                    ++col;
                }
                std::cerr<<"| ";
            }
            std::cerr<<std::endl;
        }
        std::cerr<<std::endl;
    }

    /**
     * \brief Check if there is room to another iteration
     */
    bool check_room(int block_size) const
    {
        return (_nb_vect + block_size <= _max_krylov_space_size);
    }

    void notify_ortho_end() const {/*nop*/}

    /**
     * \brief increase the size of the Hessenberg
     *
     * increment the cursors
     */
    void increase(int block_size)
    {
        FABULOUS_ASSERT( check_room(block_size) );

        _block_size.push_back(block_size);
        ++ _nb_block_col;
        _nb_vect += block_size;
        _solution_computed = false;
    }

    /**
     * \brief Update \f$ H_j \f$ block into becoming new last row of
     * \f$ \mathscr{L} \f$ and new \f$ G_j \f$ block
     *
     * \f$  L_{j+1,}  =  W_1^{H} *  H_j \f$ <br/>
     * \f$  G_j       =  W_2^{H} *  H_j \f$ <br/>
     *
     * (this can be done in one operation as blocks following each other in memory)
     *
     * \param[in] W1_W2 \f$ \mathbb{W} = [\mathbb{W}_1, \mathbb{W}_2] \f$
     * ( Directions )
     */
    int64_t update_bottom_line(const Block<S>& W1_W2)
    {
        TIMER_TRACE;
        const int M = _nbRHS;
        const int N = _nb_vect;
        const int K = _nbRHS;

        Block<S> Hj = get_Hj();
        const Block<S> tmp = Hj.copy();
        W1_W2.dot(tmp, Hj); //  Hj = trans(W1_W2) * tmp;
        return lapacke::flops::gemm<S>(M, N, K);
    }

    void after_bottom_line_update(int /*nb_kept_direction*/) const
    {
        /*nop*/
    }

    Block<S> alloc_least_square_sol()
    {
        TIMER_TRACE;
        const int M = _nb_vect;
        const int N = _nbRHS;
        _Y = _Y_buf.sub_block( 0, 0, M, N );
        if (!_solution_computed) {
            const int M2 = _nb_vect + _nbRHS;
            Block<S> YY = _Y_buf.sub_block( 0, 0, M2, N );
            YY.zero();
        }
        return _Y;
    }

    void solve_least_square(Block<S> &Y)
    {
        TIMER_LSQR;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );
        if (_solution_computed) {
            return;
        }

        _logger.notify_least_square_begin();
        int64_t flops = 0;
        const int M = _nb_vect + _nbRHS;
        Block<S> YY = _Y_buf.sub_block( 0, 0, M, _nbRHS );
        // Y <- \Lambda; to be overwritten with Y in gels
        flops += compute_Lambda(YY);

        const int N = _nb_vect;
        Block<S> F = get_F_tmp();       // working copy of F (to be overwritten by kernel)
        const int err = lapacke::gels(  // solve least square
            M, N, _nbRHS,               // this function overwrites both F and Lambda
            F.get_ptr(),  F.get_leading_dim(),
            YY.get_ptr(), YY.get_leading_dim()
        );
        flops += lapacke::flops::gels<S>(M, N, _nbRHS);
        if (err != 0) {
            FABULOUS_THROW(Kernel, "gels (least square) err="<<err);
        }
        _solution_computed = true;
        _logger.notify_least_square_end(flops);
    }

    /**
     * \brief Solve Least Square of projected problem \f$ F*Y-\Lambda \f$
     * \return \f$ Y = argmin(||F*Y-\Lambda||) \f$
     * \return \f$ PR = F*Y-\Lambda \f$
     */
    int64_t compute_proj_residual(Block<S> &LS, const Block<S> &Y)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );

        const int M = _nb_vect + _nbRHS;
        const int N = _nbRHS;
        const int K = _nb_vect;
        int64_t flops = 0;
        flops = compute_Lambda(LS);// save \Lambda in LS in order to compute LS in the end

        const Block<S> F = get_F();
        LS -= F * Y;
        flops += lapacke::flops::gemm<S>(M, N, K);
        return flops;
    }

    std::tuple<P,P,bool>
    check_least_square_residual(const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        Block<S> Y = alloc_least_square_sol();
        solve_least_square(Y);

        _logger.notify_least_square_begin();
        const int I = _nb_vect;
        const int N = _nbRHS;
        const Block<S> residual = _Y_buf.sub_block( I, 0, N, N );
        auto MinMaxConv = min_max_conv(residual, epsilon);
        int64_t flops = N * lapacke::flops::dot<S>(N);
        _logger.notify_least_square_end(flops);
        return MinMaxConv;
    }

    /**
     * \brief \f$ \mathscr{F} \f$: the whole projected matrix
     * \return \f$ \mathscr{F} \f$
     */
    Block<S> get_F()
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        return _data.sub_block( 0, 0, M, N );
    }

    /**
     * \brief \f$ \mathscr{F} \f$: the whole projected matrix (Block Hessenberg)
     * \return \f$ \mathscr{F} \f$
     */
    Block<S> get_F_tmp()
    {
        const int M = _nb_vect + _nbRHS;
        const int N = _nb_vect;
        Block<S> tmp = _data_tmp.sub_block( 0, 0, M, N );
        Block<S>   F = _data.sub_block( 0, 0, M, N );
        tmp.copy(F);
        return tmp;
    }

    Block<S> get_hess_block()
    {
        return get_F();
    }

    /**
     * \brief \f$H\f$: last block column of the Hessenberg to be filled by
     * the orthogonalization process
     * \return \f$H\f$
     */
    Block<S> get_H()
    {
        const int N = _block_size.back();
        const int M = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( 0, J, M, N );
    }

    /*
     * Following member function are relative to the |Hj block at the bottom.
     */

    /**
     * \brief \f$ \mathbb{H}_j \f$: the bottom part of \f$ \mathscr{F} \f$
     * \return \f$ \mathbb{H}_j \f$
     */
    Block<S> get_Hj()
    {
        const int I = _nb_vect;
        const int M = _nbRHS;
        const int N = _nb_vect;
        return _data.sub_block( I, 0, M, N );
    }

    /**
     * \brief \f$C\f$ part (orthogonalization coefficient of P against W)
     * to be filled during orthogonalization process
     * \return \f$C\f$
     */
    Block<S> get_C()
    {
        const int N = _block_size.back();
        const int M = _nbRHS - N; // nb direction discarded
        const int I = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, M, N );
    }

    /**
     * \brief \f$D\f$:  R part of QR factorization of \f$ \widetilde{W} \f$
     *  to be filled by orthogonalization process
     * \return \f$D\f$
     */
    Block<S> get_D()
    {
        const int N = _block_size.back();
        const int I = _nb_vect + (_nbRHS - N);
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, N, N );
    }

    Block<S> get_F1new()
    {
        FABULOUS_ASSERT( _nb_vect == _nb_eigen_pair );
        const int M = _nb_eigen_pair + _nbRHS;
        const int N = _nb_eigen_pair;
        return _data.sub_block( 0, 0, M, N );
    }

    void reserve_DR(int nb_eigen_pair)
    {
        // CHECK this is called just after hessenberg initialization:
        FABULOUS_ASSERT( _nb_block_col == 0 );
        FABULOUS_ASSERT( _nb_vect == 0 );
        FABULOUS_ASSERT( _block_size.size() == 0 );

        _nb_eigen_pair = nb_eigen_pair;
        increase(nb_eigen_pair);
    }

}; // end class HessIBDR

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_HESS_IB_DR_HPP
