#ifndef FABULOUS_HESS_QR_HPP
#define FABULOUS_HESS_QR_HPP

namespace fabulous {
namespace bgmres {
template<class> class HessQR;
}
}


#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/qrf.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/hessenberg/DynamicTauFactor.hpp"

namespace fabulous {
namespace bgmres {

/**
 * \brief Hessenberg for Incremental QR version
 *
 * Format: <br/>
 \verbatim
 | R X X |
 | H R X |
 |   H R |
 |     H |
 \endverbatim
 * with: <br/>
 * R Triangular Sup <br/>
 * H Householders reflectors (trapezoidal)
 *
 * \warning the whole matrix is allocated,
 *          its size is driven by the restart parameter.
 */
template<class S>
class HessQR
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
private:
    using P = primary_type;
private:
    Logger &_logger;
    Block<S> _Lambda; /**< least square RHS */
    const int _nbRHS; /**< number of RHS; size of each Hessenberg's block */
    int _nb_vect; /**< number of columns in current hessenberg */

    Block<S> _data; /*!< the hessenberg factorized data */
    DynamicTauVector<S> _tau; /**< tau factors for each QR factorization */

    bool _last_column_factorized; /**< whether last column was factorized */
    bool _solution_computed; /**< whether solution was computed(for last column) */
    Block<S> _Y_buf; /**< least square solution buffer */
    Block<S> _Y; /**< least square solution */

public:
    HessQR(int max_krylov_space_size, int nbRHS, Logger &logger):
        _logger{logger},
        _Lambda{max_krylov_space_size+nbRHS, nbRHS},
        _nbRHS{nbRHS},
        _nb_vect{0},
        _data{max_krylov_space_size+nbRHS, max_krylov_space_size},
        _last_column_factorized{false},
        _solution_computed{false},
        _Y_buf{max_krylov_space_size, nbRHS},
        _Y{}
    {
        const int max_nb_iter = (max_krylov_space_size / nbRHS) + 1;
        _tau.reserve( max_nb_iter );
    }

private:

    void factorize_last_column()
    {
        TIMER_TRACE;
        if (_last_column_factorized) {
            return;
        }

        _logger.notify_facto_begin();
        int64_t flops = 0;
        const int N = _nbRHS;
        const int J = _nb_vect - N;
        const char Trans = Arithmetik<S>::ltrans;

        { /* STEP 1: Loop over each block in last column to
           * apply already computed Householders */
            const int M = _nb_vect + _nbRHS;
            Block<S> last_column = _data.sub_block( 0, J, M, N );

            for (auto &tau : _tau) {
                flops += tau.apply(Trans, last_column);
            }
        }

        { /* STEP 2: Call QR on the very last block of last col */
            const int M = N + _nbRHS;
            Block<S> A = _data.sub_block( J, J, M, N );
            _tau.emplace_back(DynamicTauFactor<S>::PANEL, A, J, -1, -1);
            std::vector<S> &tau = _tau.back().get_buf();
            int err = lapacke::geqrf( M, N,
                                      A.get_ptr(), A.get_leading_dim(), tau);
            flops += lapacke::flops::geqrf<S>(M, N);
            if (err != 0) {
                FABULOUS_THROW(Kernel, "geqrf 'last block' err="<<err);
            }
        }

        { /* STEP 3: Apply Q^H generated at step 2 to last block of RHS */
            flops += _tau.back().apply(Trans, _Lambda);
        }
        _last_column_factorized = true;
        _logger.notify_facto_end(flops);
    }

public:
    /**
     * \brief set the \f$\Lambda_1\f$ block (which is nbRHS x nbRHS)
     * (RHS of least square problem)
     */
    void set_rhs(Block<S> &Lambda1)
    {
        _Lambda.copy(Lambda1);
    }

    Block<S> get_H() // last column for orthogonalization
    {
        const int N = _nbRHS;
        const int M = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( 0, J, M, N );
    }

    Block<S> get_R() // R part of last column for orthogonalization
    {
        const int N = _nbRHS;
        const int I = _nb_vect;
        const int J = _nb_vect - N;
        return _data.sub_block( I, J, N, N );
    }

    Block<S> get_H1new()
    {
        FABULOUS_THROW(Unsupported, "HessQR not compatible with DeflatedRestarting");
        return Block<S>{};
    }

    Block<S> alloc_least_square_sol()
    {
        TIMER_TRACE;
        const int M = _nb_vect;
        const int N = _nbRHS;
        _Y = _Y_buf.sub_block( 0, 0, M, N );
        if (!_solution_computed) {
            _Y.zero();
        }
        return _Y;
    }

    void solve_least_square(Block<S> &Y)
    {
        TIMER_LSQR;
        FABULOUS_ASSERT( Y.get_ptr() == _Y.get_ptr() );
        FABULOUS_ASSERT( Y.get_nb_row() == _nb_vect );
        if (_solution_computed) {
            return;
        }
        factorize_last_column();

        _logger.notify_least_square_begin();
        const int M = _nb_vect;
        const int NRHS = _nbRHS;
        const Block<S> R      = _data.sub_block  ( 0, 0, M, M    );
        const Block<S> Lambda = _Lambda.sub_block( 0, 0, M, NRHS );

        Y.copy(Lambda);
        lapacke::trsm(
            M, NRHS,
            R.get_ptr(), R.get_leading_dim(),
            Y.get_ptr(), Y.get_leading_dim()
        );
        int64_t flops = lapacke::flops::trsm<S>(M, NRHS);
        _solution_computed = true;
        _logger.notify_least_square_end(flops);
    }

    void compute_proj_residual(const Block<S>&, const Block<S>&)
    {
        FABULOUS_THROW(Unsupported, "HessQR not compatible with DeflatedRestarting");
    }

    std::tuple<P,P,bool>
    check_least_square_residual(const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        factorize_last_column();

        _logger.notify_least_square_begin();
        const int I = _nb_vect;
        const int N = _nbRHS;
        const Block<S> RR = _Lambda.sub_block( I, 0, N, N );
        auto MinMaxConv = min_max_conv(RR, epsilon);
        int64_t flops = N * lapacke::flops::dot<S>(N);
        _logger.notify_least_square_end(flops);

        return MinMaxConv;
    }

    void notify_ortho_end() const {/*nop*/}

    Block<S> get_hess_block()
    {
        FABULOUS_THROW(Unsupported, "HessQR not compatible with DeflatedRestarting");
        return Block<S>{};
    }

    void increase(int block_size)
    {
        FABULOUS_ASSERT( block_size == _nbRHS );
        _nb_vect += _nbRHS;
        _last_column_factorized = false;
        _solution_computed = false;
    }

    void reserve_DR(int)
    {
        FABULOUS_THROW(Unsupported, "HessQR not compatible with DeflatedRestarting");
    }

    int get_nb_vect() const { return _nb_vect; }

}; // end class HessQR

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_HESS_QR_HPP
