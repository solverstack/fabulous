#ifndef FABULOUS_ORTHO_PARAM_HPP
#define FABULOUS_ORTHO_PARAM_HPP

namespace fabulous {
class OrthoParam;
}

#include "fabulous/utils/Error.hpp"
#include "fabulous/orthogonalization/Enum.hpp"

namespace fabulous {


/**
 * \brief Parameters for orthogonalization
 *
 *  wrapper for orthogonalization scheme, orthogonalization type, and iteration count (for IMGS/ICGS)
 */
class OrthoParam
{
protected:
    int _nb_iteration; /*!< nb iteration for ITERATED GRAM SCHMIDT processes */
    OrthoScheme _scheme;
    OrthoType _type;
public:

    OrthoParam &operator+(OrthoType type)
    {
        _type = type;
        return *this;
    }

    OrthoParam &operator+(OrthoScheme scheme)
    {
        _scheme = scheme;
        return *this;
    }

    OrthoType get_type() const { return _type; }
    OrthoScheme get_scheme() const { return _scheme; }

    OrthoParam(OrthoScheme scheme, OrthoType type, int nb_iterations = 2):
        _nb_iteration{nb_iterations},
        _scheme{scheme},
        _type{type}
    {
        if (_nb_iteration < 2 && (_scheme == OrthoScheme::IMGS
                                  || _scheme == OrthoScheme::ICGS))
        {
            FABULOUS_THROW(
                Parameter,
                "orthogonalization number of iterations must be at least one"
            );
        }
    }
}; // end class OrthoParam

/**
 * \brief Helper function to build an OrthoParam object
 */
inline OrthoParam orthogonalization(
    OrthoScheme scheme, OrthoType type = OrthoType::RUHE, int iteration = 2  )
{
    return OrthoParam{scheme, type, iteration};
}

/**
 * \brief Helper function to build an OrthoParam object
 */
inline OrthoParam orthogonalization( int iteration = 2  )
{
    return OrthoParam{OrthoScheme::MGS, OrthoType::RUHE, iteration};
}

inline OrthoParam operator+(OrthoType type, OrthoScheme scheme)
{
    return orthogonalization(scheme, type);
}

inline OrthoParam operator+(OrthoScheme scheme, OrthoType type)
{
    return orthogonalization(scheme, type);
}

/**
 * \brief Pretty print an OrthoParam to a stream
 */
inline std::ostream& operator<<(std::ostream &o, const OrthoParam &ortho)
{
    o << ortho.get_scheme() << " " << ortho.get_type();
    return o;
}

} // end namespace fabulous

#endif // FABULOUS_ORTHO_PARAM_HPP
