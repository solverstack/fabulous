#ifndef ARNOLDI_ORTHO_BLOCK_IB_HPP
#define ARNOLDI_ORTHO_BLOCK_IB_HPP

namespace fabulous {
class Orthogonalizer;
namespace bgmres {
class OrthogonalizerBlockIB;
}
}

#include "fabulous/utils/Traits.hpp"
#include "fabulous/data/Base.hpp"
#include "fabulous/data/BlockPW.hpp"
#include "fabulous/data/BlockPWGeneric.hpp"
#include "fabulous/orthogonalization/BLOCK_common.hpp"
#include "fabulous/kernel/QR.hpp"

namespace fabulous {
namespace bgmres {

/**
 * \brief Orthogonalization methods for BLOCK variants WITH Inexact Breakdown
 */
class OrthogonalizerBlockIB : public OrthogonalizerBlockCOMMON
{
private:
    friend class ::fabulous::Orthogonalizer;

    explicit OrthogonalizerBlockIB(const OrthoParam &param):
        OrthogonalizerBlockCOMMON{param}
    {
    }

    /**
     * \brief Arnoldi Block IB version (WITH Inexact breakdown)
     *
     * \param[in,out] L the projected matrix \f$ \mathscr{L}\f$
     * \param[in] base the current base of the Krylov search space
     * \param[in,out] PW candidate block in Inexact breakdown case
     * \param ortho_scheme the orthogonalization scheme,
     */
    template<class Hessenberg, class Base, class VectorPW>
    int64_t run(Hessenberg &L, Base &base, VectorPW &PW)
    {
        using Vector = typename VectorPW::vector_type;
        Vector &W = PW.get_W(); // candidate part

        L.increase( W.get_nb_col() );
        using S = typename Vector::value_type;
        Block<S> H = L.get_H(); // hessenberg's parts
        Block<S> D = L.get_D(); // hessenberg's parts
        Block<S> C = L.get_C(); // hessenberg's parts

        int64_t nb_flops = dispatch(base, W, H);
        Vector &P = PW.get_P();
        if (P.get_nb_col() > 0) {
            P.dot( W, C );
            W.axpy(P, C, S{-1.0}); // W -= P * C;
        }
        W.qr(W, D);
        // nb_flops += 2*lapacke::flops::gemm<S>(W.get_nb_row(), W.get_nb_col(), P.get_nb_col());
        return nb_flops;
    }

}; // end class OrthogonalizerBlockIB

} // end namespace bgmres
} // end namespace fabulous

#endif // ARNOLDI_ORTHO_BLOCK_IB_HPP
