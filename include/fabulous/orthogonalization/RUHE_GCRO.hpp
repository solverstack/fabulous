#ifndef FABULOUS_ARNOLDI_ORTHO_RUHE_GCRO_HPP
#define FABULOUS_ARNOLDI_ORTHO_RUHE_GCRO_HPP

namespace fabulous {
class Orthogonalizer;
namespace bgmres {
class OrthogonalizerRuheGCRO;
}
}

#include "fabulous/data/Base.hpp"
#include "fabulous/data/BlockPW.hpp"
#include "fabulous/data/BlockPWGeneric.hpp"
#include "fabulous/utils/Meta.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"
#include "fabulous/orthogonalization/Orthogonalizer.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/flops.hpp"

/* **************************** METHODS ******************************** */

namespace fabulous {
namespace bgcro {

/**
 * \brief Orthogonalization methods for RUHE variant WITH Inexact Breakdown
 */
class OrthogonalizerRuheGCRO : public OrthoParam
{
private:
    int64_t _nb_flops;
    friend class ::fabulous::Orthogonalizer;

    explicit OrthogonalizerRuheGCRO(const OrthoParam &param):
        OrthoParam{param}
    {
    }

    /**
     * \brief Arnoldi Ruhe version with CGS ortho for IB
     */
    template<class S, class Vector, class Base>
    void CGS( const Base &base_V,
              Block<S> &H, Block<S> &C, Block<S> &D, // hessenberg
              Vector &P, Vector &W, // candidate
              const Vector &Ck, Block<S> &Bj_j)
    {
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();

        namespace fps = lapacke::flops;

        // Loop over vector in W block
        for (int k = 0; k < W.get_nb_col(); ++k) {
            Vector W_k   = W.sub_vector(k, 1);
            Block<S> h_k = H.get_bvect(k);
            Block<S> C_k = C.get_bvect(k);
            Block<S> D_k = D.get_bvect(k);

            //Ortho against Ck
            if(Ck.get_nb_col() > 0){
                Block<S> Bj_j_k = Bj_j.get_bvect(k);
                Block<S> tmp{Ck.get_nb_col(), 1};
                Ck.dot(W_k, Bj_j_k);
                W_k.axpy(Ck, Bj_j_k, S{-1.0});
                _nb_flops += 2*fps::gemm<S>(dim, 1, Ck.get_nb_col());
            }
            /*if(Ck.get_nb_col() > 0){
                Ck.dot(W, Bj_j);
                W.axpy(Ck, Bj_j, S{-1.0});
                _nb_flops += 2*fps::gemm<S>(dim, 1, Ck.get_nb_col());
            }*/

            { //Compute Ortho against base
                BASE_DOT( base_V, W_k, h_k ); // H = V_m^{H} * W_k
                BASE_AXPY( W_k, base_V, h_k, S{-1.0} ); // W_k -= V * h_k;
                _nb_flops += 2*fps::gemm<S>(dim, 1, nb_vect);
            }
            if (P.get_nb_col() > 0) {
                // Ortho against P
                P.dot( W_k, C_k ); // C_k <- P . W_k;
                W_k.axpy( P, C_k, S{-1.0}); //  W_k <- W_k - P * C_k;
                _nb_flops += 2*fps::gemm<S>(dim, 1, P.get_nb_col());
            }
            if (k > 0) {
                // Ortho against directions in Wj that have already been
                // ortho-gonalized/normalized : D_k without diagonal =
                const Vector W0k = W.sub_vector(0, k);
                Block<S> D_k_0k = D_k.sub_block(0, 0, k, 1);
                W0k.dot( W_k, D_k_0k ); // D_k_0k <- W0k . W_k  |   W_i{i<k} * Wk
                W_k.axpy( W0k, D_k_0k, S{-1.0}); // W_k <- W_k - W_k * D_k
                _nb_flops += 2*fps::gemm<S>(dim, 1, k);
            }
            //Hess[...] = norm(W_k)
            auto n = snorm(W_k);
            _nb_flops += fps::dot<S>(dim);
            D_k(k) = n;
            if (n == 0.0) {
                #ifndef FABULOUS_IB_EXACT_BREAKDOWN_CONTINUE
                FABULOUS_THROW(
                    Numeric,
                    "Rank loss in block candidate for extending the Krylov Space"
                );
                #else
                FABULOUS_WARNING("Rank loss in block candidate for extending the Krylov Space");
                FABULOUS_WARNING("Continuing since IB can address rank loss problems");
                n = 1.0;
                #endif
            }
            sscale(W_k, S{1.0} / n);
            _nb_flops += fps::scal<S>(dim);
        }
    }

    /**
     * \brief Arnoldi Ruhe version with ICGS ortho for IB
     */
    template<class S, class Vector, class Base>
    void ICGS( const Base &base_V,
               Block<S> &H, Block<S> &C, Block<S> &D, // hessenberg
               Vector &P, Vector &W, // candidate
               const Vector &Ck, Block<S> &Bj_j)
    {
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();
        namespace fps = lapacke::flops;

        // Loop over vector in W block
        for (int k = 0; k < W.get_nb_col(); ++k) {
            Vector W_k = W.sub_vector(k, 1);
            Block<S> h_k = H.get_bvect(k);
            Block<S> C_k = C.get_bvect(k);
            Block<S> D_k = D.get_bvect(k);

            for (int t = 0; t < _nb_iteration; ++t) {
                //Ortho against Ck
                if(Ck.get_nb_col() > 0){
                    Block<S> Bj_j_k = Bj_j.get_bvect(k);
                    Block<S> tmp{Ck.get_nb_col(), 1};
                    Ck.dot(W_k, tmp);
                    W_k.axpy(Ck, tmp, S{-1.0});
                    _nb_flops += 2*fps::gemm<S>(dim, 1, Ck.get_nb_col());
                    Bj_j_k += tmp;
                    _nb_flops += fps::axpy<S>(Ck.get_nb_col());
                }
                {  // Compute Ortho with Vm
                    Block<S> tmp{nb_vect, 1};
                    BASE_DOT( base_V, W_k, tmp ); // tmp = V_m^{H} * W_k
                    BASE_AXPY( W_k, base_V, tmp, S{-1.0} ); // W_k <- W_k - Vm * H
                    _nb_flops += 2*fps::gemm<S>(dim, 1, nb_vect);
                    h_k += tmp;
                    _nb_flops += fps::axpy<S>(nb_vect);
                }
                if (P.get_nb_col() != 0 ) {
                    // Compute ortho with P
                    Block<S> tmp{P.get_nb_col(), 1};
                    P.dot( W_k, tmp ); // tmp <- P . W_k
                    W_k.axpy(P, tmp, S{-1.0}); // W_k <- W_k - P * tmp;

                    _nb_flops += 2*fps::gemm<S>(dim, 1, P.get_nb_col());
                    C_k += tmp;
                    _nb_flops += fps::axpy<S>(P.get_nb_col());
                }
                if (k > 0) {
                    //Ortho against directions in Wj that have already been
                    //ortho-gonalized/normalized :
                    Block<S> tmp{k, 1};
                    Block<S> D_k_0k = D_k.sub_block(0, 0, k, 1);
                    const Vector W0k = W.sub_vector(0, k);

                    W0k.dot( W_k, tmp ); // tmp <- W0k . W_k
                    W_k.axpy( W0k, tmp, S{-1.0} ); // W_k <- W_k - W0k * tmp;
                    D_k_0k += tmp;
                    _nb_flops += 2*fps::gemm<S>(dim, 1, k);
                    _nb_flops += fps::axpy<S>(k);
                }
            }
            //Hess[...] = norm(W_k)
            auto n = snorm(W_k);
            _nb_flops += fps::dot<S>(dim);
            D_k(k) = n;
            if (n == 0.0) {
                #ifndef FABULOUS_IB_EXACT_BREAKDOWN_CONTINUE
                FABULOUS_THROW(
                    Numeric,
                    "Rank loss in block candidate for extending the Krylov Space"
                );
                #else
                FABULOUS_WARNING("Rank loss in block candidate for extending the Krylov Space");
                FABULOUS_WARNING("Continuing since IB can address rank loss problems");
                n = 1.0;
                #endif
            }
            sscale(W_k, S{1.0} / n);
            _nb_flops += fps::scal<S>(dim);
        }
    }

    /**
     * \brief Arnoldi Ruhe version with MGS ortho for IB
     */
    template<class S, class Vector, class Base>
    void MGS( const Base &base_V,
              Block<S> &H, Block<S> &C, Block<S> &D, // hessenberg
              Vector &P, Vector &W, // candidate
              const Vector &Ck, Block<S> &Bj_j)
    {
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();
        namespace fps = lapacke::flops;

        // Loop over vector in W block
        for (int k = 0; k < W.get_nb_col(); ++k) {
            Vector W_k = W.sub_vector(k, 1);
            Block<S> h_k = H.get_bvect(k);
            Block<S> C_k = C.get_bvect(k);
            Block<S> D_k = D.get_bvect(k);
            //Ortho against Ck
            if(Ck.get_nb_col() > 0){
                Block<S> Bj_j_k = Bj_j.get_bvect(k);
                for (int n = 0; n < Ck.get_nb_col(); ++n) {
                    Vector Ck_n = Ck.sub_vector(n, 1);
                    Ck_n.dot( W_k, Bj_j_k(n) );
                    W_k.axpy( Ck_n, Bj_j_k(n), S{-1.0} );
                    _nb_flops += 2*fps::gemm<S>(dim, 1, 1);
                }
            }
            //Ortho against base
            for (int n = 0; n < nb_vect; ++n) {
                Vector V_n = base_V.sub_vector(n, 1);
                V_n.dot( W_k, h_k(n) );  // H_k(n) = V_n . W_k
                W_k.axpy( V_n, h_k(n), S{-1.0} ); //  W_k <- W_k - V_n * H_k(n);
                _nb_flops += 2*fps::gemm<S>(dim, 1, 1);
            }

            // Ortho against P
            for (int n = 0; n < P.get_nb_col(); ++n){
                Vector P_n = P.sub_vector(n, 1);
                P_n.dot( W_k, C_k(n) ); // C_k(n) <- P_n . W_k
                W_k.axpy( P_n, C_k(n), S{-1.0} ); //  W_k <- W_k - P_n * C_k(n);
                _nb_flops += 2*fps::gemm<S>(dim, 1, 1);
            }

            //Ortho against already ortho-normalized vectors in W
            for (int n = 0; n < k; ++n) {
                Vector W_n = W.sub_vector(n, 1);
                W_n.dot( W_k, D_k(n) ); // D_k(n) <- W_n . W_k
                W_k.axpy( W_n, D_k(n), S{-1.0} ); // W_k <- W_k - W_n * D_k(n);
                _nb_flops += 2*fps::gemm<S>(dim, 1, 1);
            }
            //Normalization of W_k and storage inside L
            auto n = snorm(W_k);
            _nb_flops += fps::dot<S>(dim);
            D_k(k) = n;
            if (n == 0.0) {
                #ifndef FABULOUS_IB_EXACT_BREAKDOWN_CONTINUE
                FABULOUS_THROW(
                    Numeric,
                    "Rank loss in block candidate for extending the Krylov Space"
                );
                #else
                FABULOUS_WARNING("Rank loss in block candidate for extending the Krylov Space");
                FABULOUS_WARNING("Continuing since IB can address rank loss problems");
                n = 1.0;
                #endif
            }
            sscale(W_k, S{1.0} / n);
            _nb_flops += fps::scal<S>(dim);
        }
    }

    /**
     * \brief Arnoldi Ruhe version with IMGS ortho for IB
     */
    template<class S, class Vector, class Base>
    void IMGS( const Base &base_V,
               Block<S> &H, Block<S> &C, Block<S> &D, // hessenberg
               Vector &P, Vector &W, // candidate
               const Vector &Ck, Block<S> &Bj_j)
    {
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();
        namespace fps = lapacke::flops;

        // Loop over vector in W block
        for (int k = 0; k < W.get_nb_col(); ++k) {
            Vector W_k = W.sub_vector(k, 1);
            Block<S> h_k = H.get_bvect(k);
            Block<S> C_k = C.get_bvect(k);
            Block<S> D_k = D.get_bvect(k);


            //Ortho against Ck
            if(Ck.get_nb_col() > 0){
                Block<S> Bj_j_k = Bj_j.get_bvect(k);
                for (int n = 0; n < Ck.get_nb_col(); ++n) {
                    for (int t = 0; t < _nb_iteration; ++t) {
                        S tmp;
                        Vector Ck_n = Ck.sub_vector(n, 1);
                        Ck_n.dot(W_k, tmp);
                        W_k.axpy(Ck_n, tmp, S{-1.0});
                        _nb_flops += 2*fps::gemm<S>(dim,1,1);
                        Bj_j_k(n) += tmp;
                    }
                }
            }

            // Ortho against base
            for (int n = 0; n < nb_vect; ++n) {
                for (int t = 0; t < _nb_iteration; ++t) {
                    S tmp;
                    Vector V_n = base_V.sub_vector(n, 1);
                    V_n.dot( W_k, tmp ); // tmp = V_n . W_k
                    W_k.axpy( V_n, tmp, S{-1.0} ); // W_k <- W_k - V_n * tmp
                    _nb_flops += 2*fps::gemm<S>(dim,1,1);
                    h_k(n) += tmp;
                }
            }
            // Ortho against P
            for (int n = 0; n < P.get_nb_col(); ++n) {
                for (int t = 0; t < _nb_iteration; ++t) {
                    Vector P_n = P.sub_vector(n, 1);
                    S tmp;
                    P_n.dot( W_k, tmp ); // tmp <- P_n . W_k
                    W_k.axpy( P_n, tmp, S{-1.0} ); // W_k <- W_k - P_n * tmp;
                    C_k(n) += tmp;
                    _nb_flops += 2*fps::gemm<S>(dim, 1, 1);
                }
            }
            //Ortho against already ortho-normalized vectors in W
            for (int n = 0; n < k; ++n) {
                for (int t = 0; t < _nb_iteration; ++t) {
                    S tmp;
                    Vector W_n = W.sub_vector(n, 1);
                    W_n.dot( W_k, tmp ); // tmp <- W_n . W_k
                    W_k.axpy( W_n, tmp, S{-1.0} ); // W_k <- W_k - W_n * tmp;
                    D_k(n) += tmp;
                    _nb_flops += 2*fps::gemm<S>(dim,1,1);
                }
            }
            // Normalization of Wk and storage inside L
            auto n = snorm(W_k);
            _nb_flops += fps::dot<S>(dim);
            D_k(k) = n;
            if (n == 0.0) {
                #ifndef FABULOUS_IB_EXACT_BREAKDOWN_CONTINUE
                FABULOUS_THROW(
                    Numeric,
                    "Rank loss in block candidate for extending the Krylov Space"
                );
                #else
                FABULOUS_WARNING("Rank loss in block candidate for extending the Krylov Space");
                FABULOUS_WARNING("Continuing since IB can address rank loss problems");
                n = 1.0;
                #endif
            }
            sscale(W_k, S{1.0} / n);
            _nb_flops += fps::scal<S>(dim);
        }
    }

    template<class Hessenberg, class Base, class VectorPW, class Vector>
    int64_t run(Hessenberg &L, const Base &base_V, VectorPW &PW, const Vector &Ck)
    {
        _nb_flops = 0;
        Vector &P = PW.get_P();
        Vector &W = PW.get_W();

        L.increase(W.get_nb_col());

        using S = typename Vector::value_type;
        Block<S> H = L.get_H();
        Block<S> C = L.get_C();
        Block<S> D = L.get_D();

        Block<S> Bj_j = (Ck.get_nb_col() > 0) ? L.get_Bj_j() : Block<S>{};
        switch(_scheme) {
        case OrthoScheme:: MGS:  MGS(base_V, H, C, D, P, W, Ck, Bj_j);            break;
        case OrthoScheme::IMGS: IMGS(base_V, H, C, D, P, W, Ck, Bj_j);            break;
        case OrthoScheme:: CGS:  CGS(base_V, H, C, D, P, W, Ck, Bj_j);            break;
        case OrthoScheme::ICGS: ICGS(base_V, H, C, D, P, W, Ck, Bj_j);            break;
        default: FABULOUS_THROW(Parameter, "Invalid orthogonalization scheme\n"); break;
        }

        return _nb_flops;
    }

}; // end class OrthogonalizerRuheIB

} // end namespace bgcro
} // end namespace fabulous

#endif // FABULOUS_ARNOLDI_ORTHO_RUHE_GCRO_HPP
