#ifndef FABULOUS_ORTHO_ENUM_HPP
#define FABULOUS_ORTHO_ENUM_HPP

#include <unordered_map>
#include <iostream>

namespace fabulous {

/*!
 * \brief The two different approach to orthogonalization phase during Arnoldi.
 */
enum class OrthoType {
    RUHE  = 0, /*!< Arnoldi-RUHE: orthogonalization vector by vector*/
    BLOCK = 1, /*!< Arnoldi-BLOCK: Ortho block wise followed by a QR factorization*/
};

/**
 * \brief The different available orthogonalization scheme.
 */
enum class OrthoScheme {
    MGS  = 0, /*!< Modified Gram Schmidt */
    IMGS = 1, /*!< Iterated Modified Gram Schmidt */
    CGS  = 2, /*!< Classical Gram Schmidt */
    ICGS = 3, /*!< Iterated Classical Gram Schmidt */
};

} // end namespace fabulous


/*! \brief C++ std namespace */
namespace std {
/**
 *  \brief specialization of hash for enum class for defect prior c++14
 */
template<>
struct hash<::fabulous::OrthoType>
{
    typedef ::fabulous::OrthoType argument_type;
    typedef size_t result_type;

    result_type operator () (const argument_type& x) const
    {
        using type = typename std::underlying_type<argument_type>::type;
        return std::hash<type>()(static_cast<type>(x));
    }
};

/**
 *  \brief specialization of hash for enum class for defect prior c++14
 */
template<>
struct hash<::fabulous::OrthoScheme>
{
    typedef ::fabulous::OrthoScheme argument_type;
    typedef size_t result_type;

    result_type operator () (const argument_type& x) const
    {
        using type = typename std::underlying_type<argument_type>::type;
        return std::hash<type>()(static_cast<type>(x));
    }
};

} // end namespace std

namespace fabulous {

/* **************** OPERATOR<< OVERLOAD ********************* */

inline std::ostream& operator<<(std::ostream &os, const OrthoType ortho)
{
    static std::unordered_map<OrthoType, std::string>
        arnortho_names = {
        { OrthoType::BLOCK, "BLOCK" },
        { OrthoType::RUHE,  "RUHE"  },
    };

    auto it = arnortho_names.find(ortho);
    if (it == arnortho_names.end())
        os << "No choice made between BLOCK and RUHE";
    else
        os << it->second;
    return os;
}

inline std::ostream& operator<<(std::ostream& os, const OrthoScheme ortho)
{
    static std::unordered_map<OrthoScheme, std::string>
        ortho_choice_names = {
        { OrthoScheme::MGS,  "MGS"  },
        { OrthoScheme::CGS,  "CGS"  },
        { OrthoScheme::IMGS, "IMGS" },
        { OrthoScheme::ICGS, "ICGS" },
    };

    auto it = ortho_choice_names.find(ortho);
    if (it == ortho_choice_names.end())
        os << "Orthogonalization process unknown";
    else
        os << it->second;
    return os;
}

} // end namespace fabulous

#endif // FABULOUS_ORTHO_ENUM_HPP
