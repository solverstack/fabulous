#ifndef ARNOLDI_ORTHO_BLOCK_COMMON_HPP
#define ARNOLDI_ORTHO_BLOCK_COMMON_HPP

namespace fabulous {
namespace bgmres {
class OrthogonalizerBlockCOMMON;
}
}

#include <cassert>

#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/flops.hpp"

namespace fabulous {
namespace bgmres {

/**
 * \brief Orthogonalization methods for BLOCK variants
 */
class OrthogonalizerBlockCOMMON : public OrthoParam
{
private:
    int64_t _nb_flops;

private:
    /**
     * \brief Orthogonalisation process :: Classical Gram Schmidt
     *
     * \param[in] base the current base of the Krylov search space
     * \param[out] H block to write the orthogonalization coeffs
     * \param[in,out] W block to be orthogonalized against current base
     */
    template<class Base, class Vector, class S>
    void CGS(const Base &base_V, Vector &W, Block<S> &H)
    {
        BASE_DOT( base_V, W, H ); // H = V . W
        BASE_AXPY( W, base_V, H, S{-1.0} ); // W -= V * H;

        const int W_size = W.get_nb_col();
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();
        _nb_flops += 2*lapacke::flops::gemm<S>(dim, W_size, nb_vect);
    }

    /**
     * \brief orthogonalisation process :: Classical Gram Schmidt
     *
     * \param[in] base the current base of the Krylov search space
     * \param[in,out] W block to be orthogonalized against current base
     * \param[out] H block to write the orthogonalization coeffs
     */
    template<class Base, class Vector, class S>
    void ICGS(const Base &base_V, Vector &W, Block<S> &H)
    {
        const int nb_vect = base_V.get_nb_vect();
        const int W_size = W.get_nb_col();
        Block<S> tmp{nb_vect, W_size}; // Block tmp to be added to Hess

        BASE_DOT(base_V, W, tmp); // tmp = V.dot( W );
        BASE_AXPY(W, base_V, tmp, S{-1.0}); // W -= V * tmp;
        H += tmp;

        const int dim = base_V.get_local_dim();
        _nb_flops += 2* lapacke::flops::gemm<S>(dim, W_size, nb_vect);
        _nb_flops += W_size * lapacke::flops::axpy<S>(nb_vect);
    }

    /**
     * \brief Orthogonalization process (Modified Gram Schmidt)
     *
     * \param[in] base the current base of the Krylov search space
     * \param[in,out] W block to be orthogonalized against current base
     * \param[out] H block to write the orthogonalization coeffs
     */
    template<class Base, class Vector, class S>
    void MGS(const Base &base_V, Vector &W, Block<S> &H)
    {
        const int dim = base_V.get_local_dim();
        const int W_size = W.get_nb_col();
        const int nb_block = base_V.get_nb_block();

        namespace fps = lapacke::flops;
        FABULOUS_ASSERT( H.get_nb_col() == W.get_nb_col() );
        FABULOUS_ASSERT( H.get_nb_row() == base_V.get_nb_vect() );

        // Loop over different blocks
        int size_block_sum = 0;
        for (int i = 0; i < nb_block; ++i) {
            const Vector &V_i = base_V[i];
            const int size_block = V_i.get_nb_col();

            Block<S> H_ij = H.sub_block(size_block_sum, 0, size_block, W_size);
            size_block_sum += size_block;
            V_i.dot( W, H_ij ); // H_{ij} = V_i^{h} * W
            W.axpy(V_i, H_ij, S{-1.0}); // W -= V_i * H_ij;

            _nb_flops += 2*fps::gemm<S>(dim, W_size, size_block);
        }
    }

    /**
     * \brief Orthogonalization process (Iterated Modified Gram Schmidt)
     *
     * Same as above, but we store the ortho coefficients inside a temporary
     * block in order to be able to call this function twice in case of Iterated processus.
     *
     * \param[in] base the current base of the Krylov search space
     * \param[in,out] W block to be orthogonalized against current base
     * \param[out] H block to write the orthogonalization coeffs
     */
    template<class Base, class Vector, class S>
    void IMGS(const Base &base_V, Vector &W, Block<S> &H)
    {
        const int dim = base_V.get_local_dim();
        const int W_size = W.get_nb_col();
        const int nb_block = base_V.get_nb_block();

        namespace fps = lapacke::flops;
        FABULOUS_ASSERT( H.get_nb_col() == W.get_nb_col() );
        FABULOUS_ASSERT( H.get_nb_row() == base_V.get_nb_vect() );
        //FABULOUS_DEBUG("base_V.nb_vect="<<base_V.get_nb_vect());

        // Loop over different blocks
        int size_block_sum = 0;
        for (int i = 0; i < nb_block; ++i) {
            const Vector &V_i = base_V[i];
            const int size_block = V_i.get_nb_col();
            //FABULOUS_DEBUG("base_V.block["<<i<<"].size="<<size_block);
            Block<S> tmp{size_block, W_size};

            V_i.dot( W, tmp ); // tmp = V_i.dot( W );
            W.axpy(V_i, tmp, S{-1.0}); // W = W - V_i * tmp

            Block<S> H_k = H.sub_block(size_block_sum, 0, size_block, W_size);
            H_k += tmp;

            size_block_sum += size_block;
            _nb_flops += 2*fps::gemm<S>(dim, W_size, size_block);
            _nb_flops += tmp.get_nb_col() * fps::axpy<S>(size_block);
        }
    }

public:    /* was protected but there is a bug in ICC */
    explicit OrthogonalizerBlockCOMMON(const OrthoParam &param):
        OrthoParam{param},
        _nb_flops{0}
    {
    }

    /**
     * \brief Orthogonalization process dispatcher.
     *
     * \param[in] base_V the current base of the Krylov search space
     * \param[in,out] W block to be orthogonalized against current base
     * \param[out] H block to write the orthogonalization coeffs
     */
    template<class Base, class Vector, class S>
    int64_t dispatch(const Base &base_V, Vector &W, Block<S> &H)
    {
        _nb_flops = 0;

        switch (_scheme) {
        case OrthoScheme::CGS: CGS(base_V, W, H);   break;
        case OrthoScheme::MGS: MGS(base_V, W, H);   break;
        case OrthoScheme::ICGS:
            for (int i = 0; i < _nb_iteration; ++i)
                ICGS(base_V, W, H);
            break;
        case OrthoScheme::IMGS:
            for (int i = 0; i < _nb_iteration; ++i)
                IMGS(base_V, W, H);
            break;
        default: FABULOUS_THROW(Parameter, "Invalid orthogonalization scheme."); break;
        }
        return _nb_flops;
    }

}; // end class OrthogonalizerBlockCOMMON

} // end namespace bgmres
} // end namespace fabulous

#endif // ARNOLDI_ORTHO_BLOCK_COMMON_HPP
