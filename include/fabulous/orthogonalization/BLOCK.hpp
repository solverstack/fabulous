#ifndef ARNOLDI_ORTHO_BLOCK_HPP
#define ARNOLDI_ORTHO_BLOCK_HPP

namespace fabulous {
class Orthogonalizer;
namespace bgmres {
class OrthogonalizerBlockSTD;
}
}

#include "fabulous/utils/Traits.hpp"
#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/orthogonalization/BLOCK_common.hpp"
#include "fabulous/kernel/QR.hpp"

namespace fabulous {
namespace bgmres {

/**
 * \brief Orthogonalization methods for BLOCK variants WITHOUT Inexact Breakdown
 */
class OrthogonalizerBlockSTD : public OrthogonalizerBlockCOMMON
{
private:
    friend class ::fabulous::Orthogonalizer;

    explicit OrthogonalizerBlockSTD(const OrthoParam &param):
        OrthogonalizerBlockCOMMON{param}
    {
    }

    /**
     * \brief Arnoldi Block classical version (WITHOUT Inexact breakdown)
     *
     * \param[in,out] hess the hessenberg
     * \param[in] base the current base of the Krylov search space
     * \param[in,out] W block to be orthogonalized against current base
     * \param ortho_scheme the orthogonalization scheme,
     */
    template<class Hessenberg, class Base, class Vector>
    int64_t run(Hessenberg &hess, const Base &base, Vector &W)
    {
        hess.increase(W.get_nb_col());

        using S = typename Vector::value_type;
        Block<S> H = hess.get_H();
        Block<S> R = hess.get_R();

        int64_t nb_flops = dispatch(base, W, H);
        W.qr(W, R);
        return nb_flops;
    }
};

} // end namespace bgmres
} // end namespace fabulous

#endif // ARNOLDI_ORTHO_BLOCK_HPP
