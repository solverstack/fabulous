#ifndef ARNOLDI_ORTHO_BLOCK_GCR_HPP
#define ARNOLDI_ORTHO_BLOCK_GCR_HPP

namespace fabulous {
namespace bgcr {
template<class> class OrthogonalizerBlock;
}
}

#include <cassert>

#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/kernel/flops.hpp"

namespace fabulous {
namespace bgcr {

/**
 * \brief Orthogonalization method for BLOCK variants of GCR algorithm
 */
template<class S>
class OrthogonalizerBlock : public OrthoParam
{
private:
    int64_t _nb_flops;
    const int _nbrhs_alloc;

private:

    /**
     * \brief Orthogonalization process (Classical Gram Schmidt)
     *
     * Same as above, but we store the ortho coefficients inside a temporary
     * block in order to be able to call this function twice in case of Iterated processus.
     *
     * \param[in] A user matrix callback object
     * \param[in] V the primary base of the Krylov search space
     * \param[in] Z the secondary base of the Krylov search space
     * \param[in,out] W block to be orthogonalized against current base
     */
    template<class Base, class Vector>
    void CGS(const Base &V, const Base &Z, Vector &W, Vector &Zj)
    {
        const int nb_vect = V.get_nb_vect();
        if (nb_vect == 0) {
            return;
        }

        const int W_size = W.get_nb_col();
        Block<S> Theta{nb_vect, W_size};

        BASE_DOT( V, W, Theta ); //  Theta = V.dot( W );
        BASE_AXPY( W, V, Theta, S{-1.0} ); //  W  = W - V * Theta;
        BASE_AXPY( Zj, Z, Theta, S{-1.0} ); //  Z_j = Z_j - Z * Theta

        const int dim = V.get_local_dim();
        _nb_flops += 3*lapacke::flops::gemm<S>(dim, W_size, nb_vect);
    }

    /**
     * \brief Orthogonalization process (Modified Gram Schmidt)
     *
     * Same as above, but we store the ortho coefficients inside a temporary
     * block in order to be able to call this function twice in case of Iterated processus.
     *
     * \param[in] A user matrix callback object
     * \param[in] V the primary base of the Krylov search space
     * \param[in] Z the secondary base of the Krylov search space
     * \param[in,out] W block to be orthogonalized against current base
     */
    template<class Base, class Vector>
    void MGS(const Base &V, const Base &Z, Vector &W, Vector &Zj)
    {
        const int nb_block_V = V.get_nb_block();
        const int W_size = W.get_nb_col();
        Block<S> buf{_nbrhs_alloc, _nbrhs_alloc};

        // Loop over different blocks
        for (int i = 0; i < nb_block_V; ++i) {
            const Vector &V_i = V[i];
            const Vector &Z_i = Z[i];
            const int size_block = V_i.get_nb_col();

            Block<S> Theta = buf.sub_block(0, 0, size_block, W_size);
            V_i.dot( W, Theta ); // Theta = Vi . W
            W.axpy(V_i, Theta, S{-1.0});  //  W  -= V_i * Theta;
            Zj.axpy(Z_i, Theta, S{-1.0}); //  Zj -= Z_i * Theta;

            const int dim = V.get_local_dim();
            _nb_flops += 3*lapacke::flops::gemm<S>(dim, W_size, size_block);
        }
    }

public:
    OrthogonalizerBlock(const OrthoParam &param /*!< orthogonalization parameters */,
                        int nbrhs_alloc /*!< nbRHS */):
        OrthoParam{param},
        _nb_flops{0},
        _nbrhs_alloc{nbrhs_alloc}
    {
    }

    /**
     * \brief Orthogonalization process dispatcher.
     *
     * \param[in] V the primary base of the Krylov search space
     * \param[in] Z the secondary base of the Krylov search space
     * \param[in,out] W block of V to be orthogonalized against current base
     * \param[in,out] Zj block of Z to be orthogonalized against current base
     */
    template<class Base, class Vector>
    int64_t dispatch(const Base &V, const Base &Z, Vector &W, Vector &Zj)
    {
        _nb_flops = 0;

        switch (_scheme) {
        case OrthoScheme::CGS:
            CGS(V, Z, W, Zj);
            break;
        case OrthoScheme::ICGS:
            for (int i = 0; i < _nb_iteration; ++i)
                CGS(V, Z, W, Zj);
            break;
        case OrthoScheme::MGS:
            MGS(V, Z, W, Zj);
            break;
        case OrthoScheme::IMGS:
            for (int i = 0; i < _nb_iteration; ++i)
                MGS(V, Z, W, Zj);
            break;
        default:
            FABULOUS_THROW(Parameter, "Invalid orthogonalization scheme."); break;
        }

        const int size_W = W.get_nb_col();
        Block<S> G{size_W, size_W};
        W.qr(W, G);

        lapacke::trsm('R', 'U', 'N', 'N',
                      Zj.get_local_dim(), Zj.get_nb_col(), S{1.0},
                      G.get_ptr(), G.get_leading_dim(),
                      Zj.get_ptr(), Zj.get_leading_dim()            );
        // Zj = triangular_solve("right", "upper", "", G, Zj);
        // Zj /= G;
        return _nb_flops;
    }

}; // end class OrthogonalizerBlock

} // end namespace bgcr
} // end namespace fabulous

#endif // ARNOLDI_ORTHO_BLOCK_GCR_HPP
