#ifndef ARNOLDI_ORTHO_BLOCK_IB_GCRO_HPP
#define ARNOLDI_ORTHO_BLOCK_IB_GCRO_HPP

namespace fabulous {
class Orthogonalizer;
namespace bgcro {
class OrthogonalizerBlockGCRO;
}
}

#include "fabulous/utils/Traits.hpp"
#include "fabulous/data/Base.hpp"
#include "fabulous/data/BlockPW.hpp"
#include "fabulous/data/BlockPWGeneric.hpp"
#include "fabulous/orthogonalization/BLOCK_common.hpp"
#include "fabulous/kernel/QR.hpp"

namespace fabulous {
namespace bgcro {

/**
 * \brief Orthogonalization methods for BLOCK variants WITH Inexact Breakdown
 */
class OrthogonalizerBlockGCRO : public bgmres::OrthogonalizerBlockCOMMON
{
private:
    friend class ::fabulous::Orthogonalizer;

    explicit OrthogonalizerBlockGCRO(const OrthoParam &param):
        OrthogonalizerBlockCOMMON{param}
    {
    }

    /**
     * \brief Arnoldi Block IB version (WITH Inexact breakdown) for BGCRO
     *
     * \param[in,out] L the projected matrix \f$ \mathscr{L}\f$
     * \param[in] base the current base of the Krylov search space
     * \param[in,out] PW candidate block in Inexact breakdown case
     * \param ortho_scheme the orthogonalization scheme,
     */
    template<class Hessenberg, class Base, class VectorPW, class Vector>
    int64_t run(Hessenberg &L, Base &base, VectorPW &PW, const Vector &Ck)
    {
        using S = typename Vector::value_type;
        Vector &W = PW.get_W(); // candidate part
        Vector &P = PW.get_P();
        L.increase( W.get_nb_col() );

        const int k = Ck.get_nb_col();
        if(k > 0){
            Block<S> Bj_j = L.get_Bj_j(); // hessenberg's parts
            Ck.dot(W, Bj_j); // Bj_j = Ck^H * W;
            W.axpy(Ck, Bj_j, S{-1.0}); // W -= Ck * Bj_j;
        }

        Block<S> H = L.get_H(); // hessenberg's parts
        Block<S> D = L.get_D(); // hessenberg's parts
        Block<S> E = L.get_C(); // hessenberg's parts

        int64_t nb_flops = dispatch(base, W, H);
        if (P.get_nb_col() > 0) {
            P.dot( W, E );
            W.axpy(P, E, S{-1.0}); // W -= P * E;
        }
        W.qr(W, D);
        // nb_flops += 2*lapacke::flops::gemm<S>(W.get_nb_row(), W.get_nb_col(), P.get_nb_col());
        return nb_flops;
    }

}; // end class OrthogonalizerBlockGCRO

} // end namespace bgcro
} // end namespace fabulous

#endif // ARNOLDI_ORTHO_BLOCK_IB_GCRO_HPP
