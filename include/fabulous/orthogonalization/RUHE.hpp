#ifndef FABULOUS_ORTHO_RUHE_HPP
#define FABULOUS_ORTHO_RUHE_HPP

namespace fabulous {
class Orthogonalizer;
namespace bgmres {
class OrthogonalizerRuheSTD;
}
}

#include "fabulous/utils/Traits.hpp"
#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/flops.hpp"

namespace fabulous {
namespace bgmres {

/* **************************** RUHE ******************************** */

/**
 * \brief Orthogonalization methods for RUHE variant WITHOUT Inexact Breakdown
 */
class OrthogonalizerRuheSTD : public OrthoParam
{
private:
    int64_t _nb_flops;
    friend class ::fabulous::Orthogonalizer;

    explicit OrthogonalizerRuheSTD(const OrthoParam &param):
        OrthoParam{param},
        _nb_flops{0}
    {
    }

    /**
     * \brief Arnoldi Ruhe version with CGS ortho
     */
    template<class S, class Vector, class Base>
    void CGS(const Base &base_V,
             Block<S> &H, Block<S> &R, // hessenberg
             Vector &W) // candidate
    {
        const int W_size = W.get_nb_col();
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();
        const int block_size = W.get_nb_col();
        FABULOUS_ASSERT(W_size == block_size);

        namespace fps = lapacke::flops;

        // Loop over vector in W block
        for (int k = 0; k < W_size; ++k) {
            Vector W_k   = W.sub_vector(k, 1);
            Block<S> H_k = H.get_bvect(k);
            Block<S> R_k = R.get_bvect(k);

            { // Ortho against Base
                BASE_DOT( base_V, W_k, H_k ); //  H_k = V.dot( W_k );
                BASE_AXPY( W_k, base_V, H_k, S{-1.0}); // W_k -= V * H_k;
                _nb_flops += 2*fps::gemm<S>(nb_vect, 1, dim);
            }

            if (k > 0)
            { // Ortho against already processed W vectors
                const Vector W0k = W.sub_vector(0, k);
                Block<S> R_k_0k = R_k.sub_block(0, 0, k, 1);
                W0k.dot( W_k, R_k_0k ); // R_k_0k = W0k.dot( W_k )
                W_k.axpy(W0k, R_k_0k, S{-1.0}); //W_k -= W0k * R_k_0k;
                _nb_flops += 2*fps::gemm<S>(dim, 1, k);
            }
            auto n = snorm(W_k);
            _nb_flops += fps::dot<S>(dim);
            if (n == 0.0) {
                FABULOUS_THROW(
                    Numeric,
                    "Rank loss in block candidate for extending the Krylov Space"
                );
            }
            R_k(k) = n; // Hess[k+1,k] = norm(wj_k)
            sscale(W_k, S{1.0} / n);
            _nb_flops += fps::scal<S>(dim);
        }
    }

    /**
     * \brief Arnoldi Ruhe version with CGS ortho
     */
    template<class S, class Vector, class Base>
    void ICGS( const Base &base_V,
               Block<S> &H, Block<S> &R, // hessenberg
               Vector &W) // candidate
    {
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();
        const int block_size = W.get_nb_col();

        namespace fps = lapacke::flops;

        // Loop over vector in W block
        for (int k = 0; k < block_size; ++k) {
            Vector W_k = W.sub_vector(k, 1);
            Block<S> H_k = H.get_bvect(k);
            Block<S> R_k = R.get_bvect(k);

            for (int t = 0; t < _nb_iteration; ++t) { // Double ortho against base
                Block<S> tmp{nb_vect, 1};
                BASE_DOT( base_V, W_k, tmp ); //  tmp = V.dot( W_k );
                BASE_AXPY( W_k, base_V, tmp, S{-1.0} );// W_k -= V * tmp;
                H_k += tmp;
                _nb_flops += 2*fps::gemm<S>(nb_vect, 1, dim) + fps::axpy<S>(nb_vect);
            }
            for (int t = 0; t < _nb_iteration; ++t) {
                // Double ortho against W_j{0->k-1} (already orthogonalized part of W)
                if (k != 0) {
                    const Vector W0k = W.sub_vector(0, k);
                    Block<S> tmp{k, 1};
                    Block<S> R_k_0k = R_k.sub_block(0, 0, k, 1);
                    W0k.dot( W_k , tmp); // tmp = W0k.dot( W_k );
                    W_k.axpy( W0k, tmp, S{-1.0} ); // W_k -= W0k * tmp;
                    R_k_0k += tmp;
                    _nb_flops += 2*fps::gemm<S>(k, 1, dim) + fps::axpy<S>(k);
                }
            }
            auto n = snorm(W_k);
            _nb_flops += fps::dot<S>(dim);
            if (n == 0.0) {
                FABULOUS_THROW(
                    Numeric,
                    "Rank loss in block candidate for extending the Krylov Space"
                );
            }
            R_k(k) = n; // Hess[...] = norm(wj_k)
            sscale(W_k, S{1.0} / n);
            _nb_flops += fps::scal<S>(dim);
        }
    }

    /**
     * \brief Arnoldi Ruhe version with MGS ortho
     */
    template<class S, class Vector, class Base>
    void MGS( const Base &base_V,
              Block<S> &H, Block<S> &R, // hessenberg
              Vector &W ) // candidate
    {
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();
        const int block_size = W.get_nb_col();

        namespace fps = lapacke::flops;

        // Loop over vector in W block
        for (int k = 0; k < block_size; ++k) {
            Vector W_k = W.sub_vector(k, 1);
            Block<S> H_k = H.get_bvect(k);
            Block<S> R_k = R.get_bvect(k);

            for (int n = 0; n < nb_vect; ++n) { //Ortho against base
                const Vector V_n = base_V.sub_vector(n, 1);
                V_n.dot( W_k, H_k(n) ); //  H_k(n) = V_n . W_k
                W_k.axpy( V_n, H_k(n), S{-1.0}); // W_k -= V_n * H_k(n);
                _nb_flops += 2*fps::gemm<S>(dim, 1, 1);
            }
            for (int n = 0; n < k; ++n) { // Ortho against already computed vectors of W
                const Vector W_n = W.sub_vector(n, 1);
                // Block<S> R_kn = R_k.sub_block(n, 0, 1, 1);
                W_n.dot( W_k, R_k(n) ); // R_k(n) = W_n . W_k
                W_k.axpy( W_n, R_k(n), S{-1.0}); // W_k <- W_k - W_n * R_k(n);
                _nb_flops += 2*fps::gemm<S>(dim, 1, 1);
            }
            auto n = snorm(W_k);
            _nb_flops += fps::dot<S>(dim);
            if (n == 0.0) {
                FABULOUS_THROW(
                    Numeric,
                    "Rank loss in block candidate for extending the Krylov Space"
                );
            }
            R_k(k) = n; // Hess[k+1,k] = norm(wj_k)
            sscale(W_k, S{1.0} / n);
            _nb_flops += fps::scal<S>(dim);
        }
    }

    /**
     * \brief Arnoldi Ruhe version with IMGS ortho
     */
    template<class S, class Vector, class Base>
    void IMGS( const Base &base_V,
               Block<S> &H, Block<S> &R, // hessenberg
               Vector &W) // candidate
    {
        const int dim = base_V.get_local_dim();
        const int nb_vect = base_V.get_nb_vect();
        const int block_size = W.get_nb_col();

        namespace fps = lapacke::flops;

        // Loop over vector in W block
        for (int k = 0; k < block_size; ++k) {
            Vector W_k = W.sub_vector(k, 1);
            Block<S> H_k = H.get_bvect(k);
            Block<S> R_k = R.get_bvect(k);

            for (int n = 0; n < nb_vect; ++n) { // Ortho against Base
                for (int t = 0; t < _nb_iteration; ++t) {
                    const Vector V_n = base_V.sub_vector(n, 1);
                    S tmp = S{0.0};
                    V_n.dot(W_k, tmp); // tmp = V_n . W_k
                    W_k.axpy(V_n, tmp, S{-1.0} ); // W_k -= V_n * tmp;
                    H_k(n) += tmp;
                    _nb_flops += 2*fps::gemm<S>(dim, 1, 1);
                }
            }
            for (int n = 0; n < k; ++n) {
                for (int t = 0; t < _nb_iteration; ++t) {
                    const Vector W_n = W.sub_vector(n, 1);
                    S tmp = S{0.0};
                    W_n.dot( W_k, tmp ); // tmp = W_n . W_k;
                    W_k.axpy(W_n, tmp, S{-1.0}); //  W_k -= W_n * tmp;
                    R_k(n) += tmp;
                    _nb_flops += 2*fps::gemm<S>(dim,1,1);
                }
            }
            auto n = snorm(W_k);
            _nb_flops += fps::dot<S>(dim);
            if (n == 0.0) {
                FABULOUS_THROW(
                    Numeric,
                    "Rank loss in block candidate for extending the Krylov Space"
                );
            }
            R_k(k) = n; // Hess[k+1,k] = norm(wj_k)
            sscale(W_k, S{1.0} / n);
            _nb_flops += fps::scal<S>(dim);
        }
    }

    /* *************************** WRAPPER ************************* */
    template<class Hessenberg, class Base, class Vector>
    int64_t run(Hessenberg &hess, const Base &base_V, Vector &W)
    {
        _nb_flops = 0;
        hess.increase(W.get_nb_col());

        using S = typename Vector::value_type;
        Block<S> H = hess.get_H();
        Block<S> R = hess.get_R();

        switch (_scheme) {
        case OrthoScheme:: CGS:  CGS( base_V, H, R, W);                   break;
        case OrthoScheme::ICGS: ICGS( base_V, H, R, W);                   break;
        case OrthoScheme:: MGS:  MGS( base_V, H, R, W);                   break;
        case OrthoScheme::IMGS: IMGS( base_V, H, R, W);                   break;
        default: FABULOUS_THROW(Parameter, "Invalid orthogonalization scheme"); break;
        }
        return _nb_flops;
    }

}; // end class OrthogonalizerRuheSTD

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_ORTHO_RUHE_HPP
