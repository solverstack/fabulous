#ifndef FABULOUS_ORTHOGONALIZER_HPP
#define FABULOUS_ORTHOGONALIZER_HPP

namespace fabulous {
class Orthogonalizer;
}

#include "fabulous/utils/Error.hpp"
#include "fabulous/orthogonalization/Enum.hpp"
#include "fabulous/orthogonalization/RUHE.hpp"
#include "fabulous/orthogonalization/RUHE_IB.hpp"
#include "fabulous/orthogonalization/RUHE_GCRO.hpp"
#include "fabulous/orthogonalization/BLOCK.hpp"
#include "fabulous/orthogonalization/BLOCK_IB.hpp"
#include "fabulous/orthogonalization/BLOCK_GCR.hpp"
#include "fabulous/orthogonalization/BLOCK_GCRO.hpp"

namespace fabulous {

/**
 * \brief Orthogonalization method call wrapper
 */
class Orthogonalizer
{
private:
    OrthoParam _param;

    template<class Hessenberg, class Base, class Vector,
             class = enable_if_t<handle_ib_t<Hessenberg>::value > >
    int64_t run_ruhe_ENABLED(Hessenberg &H, const Base &base, Vector &W)
    {
        bgmres::OrthogonalizerRuheIB ruhe{_param};
        return ruhe.run(H, base, W);
    }

    template<class Hessenberg, class Base, class Vector,
             class = enable_if_t<not handle_ib_t<Hessenberg>::value >,
             class=void>
    int64_t run_ruhe_ENABLED(Hessenberg &H, const Base &base, Vector &W)
    {
        bgmres::OrthogonalizerRuheSTD ruhe{_param};
        return ruhe.run(H, base, W);
    }

    template<class Hessenberg, class Base, class Vector,
             class = enable_if_t<is_optimizable_t<Vector>::value> >
    int64_t run_ruhe(Hessenberg &H, const Base &base, Vector &W)
    {
        return run_ruhe_ENABLED(H, base, W);
    }

    template<class Hessenberg, class Base, class Vector,
             class = enable_if_t<not is_optimizable_t<Vector>::value >,
             class = void >
    int64_t run_ruhe(Hessenberg &H, const Base &base, Vector &W)
    {
        (void) H;
        (void) base;
        (void) W;
        FABULOUS_THROW(Unsupported, "RUHE is not available for generic vector.");
        return 0;
    }

    template<class Hessenberg, class Base, class Vector,
             class = enable_if_t<handle_ib_t<Hessenberg>::value > >
    int64_t run_block(Hessenberg &H, Base &base, Vector &W)
    {
        bgmres::OrthogonalizerBlockIB block{_param};
        return block.run(H, base, W);
    }

    template<class Hessenberg, class Base, class Vector,
             class = enable_if_t<not handle_ib_t<Hessenberg>::value >, class=void>
    int64_t run_block(Hessenberg &H, Base &base, Vector &W)
    {
        bgmres::OrthogonalizerBlockSTD block{_param};
        return block.run(H, base, W);
    }

public:
    OrthoType get_type() const { return _param.get_type(); }
    OrthoScheme get_scheme() const { return _param.get_scheme(); }

    explicit Orthogonalizer(const OrthoParam &param):
        _param{param}
    {
    }

    template<class Hessenberg, class Base, class Vector, class Logger>
    void run(Hessenberg &H, Base &base, Vector &W, Logger &logger)
    {
        logger.notify_ortho_begin();
        int64_t nb_flops = 0;
        switch (_param.get_type()) {
        case OrthoType::RUHE: nb_flops = run_ruhe(H, base, W); break;
            //FABULOUS_THROW(Unsupported, "RUHE disabled!");
        case OrthoType::BLOCK: nb_flops = run_block(H, base, W); break;
        default:
            FABULOUS_THROW(
                Parameter,
                "Choose between blockwise or Ruhe orthogonalization."
            ); break;
        }
        logger.notify_ortho_end(nb_flops);
        H.notify_ortho_end();
    }

    template<class Hessenberg, class Base, class VectorPW, class Logger, class Vector>
    void run_gcro(Hessenberg &H, Base &base, VectorPW &W, Logger &logger, const Vector &Ck)
    {
        logger.notify_ortho_begin();
        int64_t nb_flops = 0;

        if(_param.get_type() == OrthoType::BLOCK){
            bgcro::OrthogonalizerBlockGCRO block{_param};
            nb_flops = block.run(H, base, W, Ck);
        } else if(_param.get_type() == OrthoType::RUHE){
            bgcro::OrthogonalizerRuheGCRO ruhe{_param};
            nb_flops = ruhe.run(H, base, W, Ck);
        } else {
            FABULOUS_THROW(Parameter, "Choose between blockwise or Ruhe orthogonalization.");
        }
        logger.notify_ortho_end(nb_flops);
        H.notify_ortho_end();
    }

    template<class Base, class Logger, class Vector>
    void run_gcr(const Base &V, const Base &Z, Vector &W, Vector &Zj, Logger &logger, int nbrhs)
    {
        if (_param.get_type() == OrthoType::RUHE) {
            FABULOUS_WARNING(
                "Only BLOCK-wise orthogonalization is currently implemented"
                " for BCGR. The BLOCK-wise version will be used"
            );
        }
        logger.notify_ortho_begin();
        using S = typename Vector::value_type;
        bgcr::OrthogonalizerBlock<S> ortho{_param, nbrhs};
        int64_t nb_flops = ortho.dispatch(V, Z, W, Zj);
        logger.notify_ortho_end(nb_flops);
    }

}; // end class Orthogonalizer

} // end namespace fabulous

#endif // FABULOUS_ORTHOGONALIZER_HPP

