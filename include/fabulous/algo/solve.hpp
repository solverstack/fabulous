#ifndef FABULOUS_SOLVE_HPP
#define FABULOUS_SOLVE_HPP



#include "fabulous/algo/Equation.hpp"
#include "fabulous/algo/Parameters.hpp"
#include "fabulous/algo/BGMRes.hpp"
#include "fabulous/algo/BGCRO.hpp"
#include "fabulous/algo/BGCR.hpp"
#include "fabulous/algo/BCG.hpp"

#include "fabulous/orthogonalization/OrthoParam.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Timer.hpp"

/*! \brief main fabulous namespace */
namespace fabulous {

/*! \brief bgmres related methods */
namespace bgmres {

/*! \brief main fabulous bgmres solver entry point */
template<class Algo, class Equation, class Restart>
auto solve(Equation &eq, const Algo &algo, Parameters param,
           OrthoParam ortho, Restart restart)
{
    using S = typename Equation::value_type;
    BGMRes<S> solver;
    solver.solve(eq, algo, param, ortho, restart);
    return solver.get_logger();
}

/*! \brief main fabulous bgmres solver entry point */
template<class Algo, class Equation>
auto solve(Equation &eq, const Algo &algo, Parameters param,
           OrthoParam ortho = orthogonalization())
{
    using S = typename Equation::value_type;
    BGMRes<S> solver{};
    solver.solve(eq, algo, param, ortho, classic_restart());
    return solver.get_logger();
}

} // end namespace bgmres

/*! \brief bgcro related methods */
namespace bgcro {

/*! \brief main fabulous bgcro solver entry point */
template<class Algo, class Equation, class Restart, class Vector>
auto solve(Equation &eq, const Algo &algo, Parameters param, Vector &Uk,
           OrthoParam ortho, Restart restart, const bool Uk_const = false)
{
    using S = typename Equation::value_type;
    BGCRO<S> solver{Uk_const};
    solver.solve(eq, algo, param, ortho, restart, Uk);
    return solver.get_logger();
}

/*! \brief main fabulous bgcro solver entry point */
template<class Algo, class Equation, class Vector>
auto solve(Equation &eq, const Algo &algo, Parameters param, Vector &Uk,
           OrthoParam ortho = orthogonalization(), const bool Uk_const = false)
{
    using S = typename Equation::value_type;
    BGCRO<S> solver{Uk_const};
    solver.solve(eq, algo, param, ortho, classic_restart(), Uk);
    return solver.get_logger();
}


} // end namespace bgcro

/*! \brief bgcr related methods */
namespace bgcr {

/*! \brief main fabulous bgcr solver entry point */
template<class Algo, class Equation,
         class S = typename Equation::value_type,
         class P = typename Equation::primary_type >
Logger solve(Equation &eq, const Algo &algo,
                Parameters param, OrthoParam ortho)
{
    BGCR<S> solver;
    solver.solve(eq, algo, param, ortho);
    return solver.get_logger();
}

} // end namespace bgcr


/*! \brief bfbcg related methods */
namespace bcg {

/*! \brief main fabulous bcg solver entry point */
template<class Algo, class Equation,
         class S = typename Equation::value_type,
         class P = typename Equation::primary_type >
Logger solve(Equation &eq, const Algo &algo, Parameters param)
{
    BCG<S> solver;
    solver.solve(eq, algo, param);
    return solver.get_logger();
}

} // end namespace bcg
} // end namespace fabulous

#endif // FABULOUS_SOLVE_HPP
