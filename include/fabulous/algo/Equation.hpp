#ifndef FABULOUS_EQUATION_HPP
#define FABULOUS_EQUATION_HPP

namespace fabulous {
namespace bgmres { template<class S> class BGMRes; }
namespace bgcro { template<class S> class BGCRO; }
namespace bgcr { template<class S> class BGCR; }
namespace bcg { template<class S> class BCG; }
template<class,class,class,class> struct Equation;
} // end namespace fabulous

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/data/Block.hpp"

namespace fabulous {

/**
 * \brief Tuple container for A, X, B and epsilon
 */
template<class Matrix, class Precond, class Vector, class Callback>
struct Equation
{
public:
    using value_type = typename Vector::value_type;
    using primary_type = typename Arithmetik<value_type>::primary_type;

    const Matrix &A;
    const Precond &M;
    const Callback &CB;
    Vector &X;
    Vector &B;
    std::vector<primary_type> epsilon;

public:
    explicit Equation(const Matrix &A_,
                      const Precond &M_,
                      const Callback &CB_,
                      Vector &X_, Vector &B_,
                      const std::vector<primary_type> &epsilon_):
        A(A_), M{M_}, CB{CB_},
        X(X_), B(B_),
        epsilon(epsilon_)
    {
    }

}; // end struct Equation

/**
 * \brief Helper function to create Equation object
 */
template<class Matrix, class Precond,
         class Callback, class Vector, class P>
inline Equation<Matrix, Precond, Vector, Callback>
equation( const Matrix &A,
          const Precond &M,
          const Callback &CB,
          Vector &X,
          Vector &B,
          std::vector<P> epsilon  )
{
    return Equation<Matrix,Precond,Vector,Callback>{A, M, CB, X, B, epsilon};
}

/*! \brief can substitute a preconditioner or a callback and succesfully do nothing */
struct noop_placeholder
{
    operator bool() const { return false; }
    template<class...Args> int64_t operator()(const Args...) const
    {
        return 0;
    }
};

} // end namespace fabulous

#endif // FABULOUS_EQUATION_HPP
