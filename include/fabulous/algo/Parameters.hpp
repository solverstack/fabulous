#ifndef FABULOUS_PARAMETERS_HPP
#define FABULOUS_PARAMETERS_HPP

namespace fabulous {
namespace bgmres { template<class S> class BGMRes; }
namespace bgcro { template<class S> class BGCRO; }
namespace bgcr { template<class S> class BGCR; }
namespace bcg { template<class S> class BCG; }

/**
 * \brief Hold parameters for BGMRES algorithm
 */
struct Parameters
{
public:
    int max_mvp; /*!< maximum number of matrix vector product */
    int max_space; /*!< maximum size of krylov search space */
    int max_kept_direction; /*!< maximum number of kept direction for IB variants */
    bool need_real_residual; /*!< In GMRES algorithms, compute X and R at each iteration
                              *   such that the user can access them in CallBack
                              *   \warning This will consume lot of time */
    int logger_user_data_size; /* set the number of user data
                                 * slots available for user in logger */
    bool quiet; /*!< whether solver output any information on terminal or stay silent */

    explicit Parameters(int max_mvp_=0, int max_space_=-1):
        max_mvp{max_mvp_},
        max_space{max_space_},
        max_kept_direction{-1},
        need_real_residual{false},
        logger_user_data_size{4},
        quiet{false}
    {
    }

    void set_max_kept_direction(int max_kept_direction_)
    {
        max_kept_direction = max_kept_direction_;
    }

    void set_max_mvp(int val)
    {
        max_mvp = val;
    }

    void set_max_space(int val)
    {
        max_space = val;
    }

    void set_real_residual(bool val)
    {
        need_real_residual = val;
    }

    void set_logger_user_data_size(int val)
    {
        logger_user_data_size = val;
    }

    void set_quiet(bool val)
    {
        quiet = val;
    }

}; // end class Parameters

/**
 * \brief Helper function to build Parameters object
 * \param max_mvp maximum number of matrix vector product
 * \param max_space maximum size of krylov search space
 */
inline Parameters parameters(int max_mvp, int max_space=-1)
{
    return Parameters{max_mvp, max_space};
}

} // end namespace fabulous

#endif // FABULOUS_PARAMETERS_HPP
