#ifndef FABULOUS_ALGO_TYPE_HPP
#define FABULOUS_ALGO_TYPE_HPP

#include "fabulous/utils/Meta.hpp"

#include <fabulous/arnoldi/ArnoldiIB.hpp>
#include <fabulous/arnoldi/ArnoldiDR.hpp>
#include <fabulous/arnoldi/ArnoldiIBDR.hpp>
#include <fabulous/arnoldi/ArnoldiGCR.hpp>
#include <fabulous/arnoldi/BCGIterations.hpp>
#include <fabulous/arnoldi/BCGIterations2.hpp>
#include <fabulous/arnoldi/ArnoldiIBGCRO.hpp>


namespace fabulous {

#define FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(Name_, Type_)            \
    inline std::ostream& operator<<(std::ostream &o, const Type_ &) \
    {                                                                   \
        return (o << #Name_);                                           \
    }                                                                   \

FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(BGCRO, bgcro::ARNOLDI)

FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(BGMRES, bgmres::ARNOLDI_STD)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(QR-BGMRES, bgmres::ARNOLDI_QR)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(IB-BGMRES, bgmres::ARNOLDI_IB)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(QR-BGMRES, bgmres::ARNOLDI_QRDR)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(IB-BGMRES, bgmres::ARNOLDI_IBDR)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(QR-IB-BGMRES, bgmres::ARNOLDI_QRIB)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(QR-IB-BGMRES, bgmres::ARNOLDI_QRIBDR)

#ifdef FABULOUS_USE_CHAMELEON
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(CHAM_QR-BGMRES, bgmres::ARNOLDI_CHAM_QR)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(CHAM_TL-BGMRES, bgmres::ARNOLDI_CHAM_TL)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(CHAM_IB-BGMRES, bgmres::ARNOLDI_CHAM_IB)
#endif // FABULOUS_USE_CHAMELEON

FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(BGCR, bgcr::ARNOLDI)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(BFBCG, bcg::BCG_ITER)
FABULOUS_ALGO_OVERLOAD_STREAM_OPERATOR(BFBCG, bcg::BCG_ITER2)

} // end namespace fabulous

#endif // FABULOUS_ALGO_TYPE_HPP
