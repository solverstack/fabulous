#ifndef FABULOUS_BGCR_HPP
#define FABULOUS_BGCR_HPP

#include <algorithm>
#include <iostream>

namespace fabulous {
namespace bgcr {
template<class S> class BGCR;
}
}


#include "fabulous/data/Block.hpp"
#include "fabulous/data/Base.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"
#include "fabulous/restart/Restarter.hpp"
#include "fabulous/algo/Equation.hpp"
#include "fabulous/algo/Parameters.hpp"

namespace fabulous {
namespace bgcr {

/**
 * \brief %Block General Conjugate Residual (%BGCR) algorithm
 */
template<class S>
class BGCR
{
public:
    using value_type = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;

private:
    using P = primary_type;

    Logger _logger;
    int _nb_restart;
    int _nb_iteration;
    int _mvp;

public:
    explicit BGCR():
        _logger{},
        _nb_restart{0},
        _nb_iteration{0},
        _mvp{0}
    {
    }

private:
    template<class Precond>
    void print_precond(const Precond &M, std::ostream &o = std::cerr)
    {
        using fabulous::Color;
        o << "Right Pre Conditionner :               [";
        if ( M )
            o<<Color::green<< "USED";
        else
            o<<Color::red<< "NOT USED";
        o<<Color::reset<<"]\n";
    }

    void print_iteration_start_info(
        bool quiet, int size_to_span, std::ostream &o = std::cerr)
    {
        if (quiet) return;
        o << "##################  Restart number: " <<_nb_restart <<"  #################\n";
        o << "##################  SizeToSpan    : " <<size_to_span <<"  ##############\n";
    }

    void print_iteration_end_info(
        bool quiet, int size_krylov_space, std::ostream &o = std::cerr)
    {
        if (quiet) return;
        o <<"######################################################\n";
        o <<" Restart Done                          : "<< _nb_restart <<"\n";
        o <<" Total iterations                      : "
          << _nb_iteration<<"(+"<<_nb_restart<<")\n";
        o <<" Size of Krylov Space                  : "<< size_krylov_space <<"\n";
        o <<" Total Number of Matrix vector Product : "<< _mvp<<"\n";
        o <<"######################################################\n";
    }

    template<class Precond, class Algo>
    void print_start_info(
        bool quiet, int dim, int nbRHS, int maxMVP, int max_krylov_space_size,
        const OrthoParam &ortho, const std::vector<P> &epsilon,
        const Precond &M, Algo &algo,std::ostream &o = std::cerr)
    {
        if (quiet) return;

        Arithmetik<S> arith;
        o << "#######################################################\n";
        std::stringstream ss;
        ss << algo;
        int l = (55-(ss.str().length()+2))/2;
        o << std::string(l, '#') << " " << ss.str() << " " << std::string(l, '#') << "\n";
        o << "#######################################################\n";
        o << "Dimension of Problem :                 "<<dim<<"\n";
        o << "Number of Right Hand Side :            "<<nbRHS<<"\n";
        o << "Maximum nb of Mat Vect product:        "<<maxMVP<<"\n";
        o << "Max Size Krylov space before restart : "<<max_krylov_space_size<<"\n";
        o << "Orthogonalization Scheme :             "<<ortho<<"\n";
        o << "Arithmetic used :                      "<<arith<<"\n";
        if (epsilon.size() == 1)
            o << "Targeted Tolerance :                   "<<epsilon.front()<<"\n";
        else
            o << "Multi precision is being used\n";
        print_precond(M, o);
        o << "#######################################################\n";
    }

    template<class Vector>
    void check_params(const Vector &X, const Vector &B, int max_space, int max_mvp)
    {
        TIMER_TRACE;
        // FABULOUS_ASSERT( X.get_nb_row() == B.get_nb_row()  );
        FABULOUS_ASSERT( X.get_nb_col() == B.get_nb_col()  );

        // int problem_dimension = B.get_local_dim();
        int nb_right_hand_sides = B.get_nb_col();

        // if (problem_dimension <= 0) {
        //     FABULOUS_THROW(Parameter, "invalid problem dimension: null or negative value");
        // }

        if (nb_right_hand_sides <= 0) {
            FABULOUS_THROW(Parameter, "invalid number of right hand sides: null or negative value");
        }

        if (max_space <= 0) {
            FABULOUS_THROW(Parameter, "invalid maximum krylov space size");
        }

        if ( max_space < nb_right_hand_sides) {
            FABULOUS_THROW(Parameter,
                           "maximum krylov search space is lower "
                           "than number of right hand sides");
        }

        if (max_mvp <= 0) {
            FABULOUS_THROW(Parameter, "invalid maximum matrix vector product");
        }

        if ( max_mvp < nb_right_hand_sides) {
            FABULOUS_THROW(Parameter,
                           "maximum matrix vector product is lower "
                           "than number of right hand sides");
        }

        if (max_mvp <= max_space) {
            warning("maximum matrix vector product is lower than maximum"
                    " krylov space size. No restart is possible");
        }
    }

    /**
     * \brief BGCR with Inexact Breakdowns, restart and preconditionner
     *
     * \param[in] A Matrix, need to provide <br/>
     * MatBlockVect(), size(), useRightPreCond(), PrecondBlockVect(), DotProduct()
     *
     * \param[in,out] X Initial guess solution and solution in ouput
     * \param[in] B Block containing Right Hand Side
     * \param algo instance of Algorithm as returned by ::fabulous::bgcr::std()
     * \param max_mvp Maximum number of Matrix Vector Product.
     * \param max_space maximum size of Krylov space
     * \param[in] epsilon Target accuracy
     * \param ortho OrthoParam
     * (through QR factorization) or vector wise arnoldi. <br/>
     * (In distributed, only the vector wise will works)
     *
     * \return Total Number of Matrix vector product done
     *  (cumulated size of the spanned Krylov Spaces)
     */
    template<class Algo, class Matrix,
             class Precond, class Callback,
             class Vector>
    int solve( const Matrix &A,
               const Precond &M,
               const Callback &CB,
               Vector &X, Vector &B,
               Algo algo, Parameters param,
               const std::vector<P> &epsilon, OrthoParam ortho )
    {
        TIMER_TRACE;
        const int nbRHS = B.get_nb_col();
        // const int dim   = B.get_nb_row();
        const int max_space = param.max_space;
        const int max_mvp = param.max_mvp;
        check_params(X, B, max_space, max_mvp);

        reset();
        print_start_info( param.quiet, -1, nbRHS, max_mvp, max_space,
                          ortho, epsilon, M, algo  );

        std::vector<P> normB = B.cwise_norm();
        std::vector<P> inv_normB = array_inverse(normB);
        X.cwise_scale(inv_normB);
        B.cwise_scale(inv_normB);

        auto V = make_base(X, max_space+nbRHS);
        auto Z = make_base(X, max_space+nbRHS);

        bool convergence = false;
        while (!convergence && _mvp < max_mvp) {
            //Compute nb of mat vect product to give to Arnoldi procedure
            const int size_to_span = std::max(
                std::min(max_space, max_mvp-_mvp),
                nbRHS
            );
            print_iteration_start_info(param.quiet, size_to_span);
            if (nbRHS > size_to_span) {
                break;
            }
            auto arnoldi = make_arnoldi<S>(algo, _logger, nbRHS, size_to_span, param);
            convergence = arnoldi.run( A, M, CB, X, B, V, Z, epsilon, ortho );
            _nb_iteration += arnoldi.get_nb_iteration();
            int mvp = arnoldi.get_nb_mvp();
            _mvp += mvp;
            if (mvp == 0) {
                break;
            }
            ++ _nb_restart;
            print_iteration_end_info(param.quiet, arnoldi.get_krylov_space_size());
        }

        if (_mvp == 0) {
            warning(
                "No matrix multiplication was performed. "
                "You may want to check the algorithm parameters"
            );
        }

        X.cwise_scale(normB);
        B.cwise_scale(normB);
        return _mvp;
    }

public:

    void reset()
    {
        _nb_restart = 0;
        _nb_iteration = 0;
        _mvp = 0;
        _logger.reset();
    }

    template<class Algo, class Equation>
    int solve(Equation &eq, Algo algo, Parameters param, OrthoParam ortho)
    {
        _logger.resize_user_data(param.logger_user_data_size);
        _logger.set_quiet(param.quiet);
        return solve(eq.A, eq.M, eq.CB, eq.X, eq.B, algo, param, eq.epsilon, ortho);
    }

    Logger &get_logger()
    {
        return _logger;
    }

}; // end class BGCR

} // end namespace bgcr
} // end namespace fabulous

#endif // FABULOUS_BGCR_HPP
