#ifndef FABULOUS_BGMRES_HPP
#define FABULOUS_BGMRES_HPP

#include <algorithm>
#include <iostream>

namespace fabulous {
namespace bgmres {
template<class S> class BGMRes;
}
}

#include "fabulous/data/Block.hpp"
#include "fabulous/data/Base.hpp"
#include "fabulous/data/BaseGeneric.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Timer.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"
#include "fabulous/restart/Restarter.hpp"
#include "fabulous/algo/Equation.hpp"
#include "fabulous/algo/Parameters.hpp"
#include "fabulous/algo/AlgoType.hpp"


namespace fabulous {
namespace bgmres {

/**
 * \brief %Block General Minimum Residual (%BGMRes) algorithm
 */
template<class S>
class BGMRes
{
public:
    using value_type = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;

private:
    using P = primary_type;

    Logger _logger;
    int _nb_restart;
    int _nb_iteration;
    int _mvp;

private:
    template<class Precond>
    void print_precond(const Precond &M, std::ostream &o = std::cerr)
    {
        using fabulous::Color;
        o << "Right Pre Conditionner :               [";
        if ( M )
            o<<Color::green<< "USED";
        else
            o<<Color::red<< "NOT USED";
        o<<Color::reset<<"]\n";
    }

    void print_iteration_start_info(
        bool quiet, int size_to_span, std::ostream &o = std::cerr)
    {
        if (quiet) return;

        o << "##################  Restart number: " <<_nb_restart <<"  #################\n";
        o << "##################  SizeToSpan    : " <<size_to_span <<"  ##############\n";
    }

    void print_iteration_end_info(
        bool quiet, int size_krylov_space, std::ostream &o = std::cerr)
    {
        if (quiet) return;

        o <<"######################################################\n";
        o <<" Restart Done                          : "<< _nb_restart <<"\n";
        o <<" Total iterations                      : "
          << _nb_iteration<<"(+"<<_nb_restart<<")\n";
        o <<" Size of Krylov Space                  : "<< size_krylov_space <<"\n";
        o <<" Total Number of Matrix vector Product : "<< _mvp<<"\n";
        o <<"######################################################\n";
    }

    template<class Precond, class RestartParam, class Algo>
    void print_start_info(
        bool quiet, int dim, int nbRHS, int maxMVP, int max_krylov_space_size,
        const OrthoParam &ortho, const RestartParam &restart_param,
        const std::vector<P> &epsilon, Precond &M, Algo &algo,
        std::ostream &o = std::cerr)
    {
        if (quiet) return;

        Arithmetik<S> arith;
        o << "#######################################################\n";
        std::stringstream ss;
        ss << algo << get_type(restart_param);
        int l = (55-(ss.str().length()+2))/2;
        o << std::string(l, '#') << " " << ss.str() << " " << std::string(l, '#') << "\n";
        o << "#######################################################\n";
        o << "Dimension of Problem :                 "<<dim<<"\n";
        o << "Number of Right Hand Side :            "<<nbRHS<<"\n";
        o << "Maximum nb of Mat Vect product:        "<<maxMVP<<"\n";
        o << "Max Size Krylov space before restart : "<<max_krylov_space_size<<"\n";
        o << "Orthogonalization Scheme :             "<<ortho<<"\n";
        o << "Arithmetic used :                      "<<arith<<"\n";
        if (epsilon.size() == 1)
            o << "Targeted Tolerance :                   "<<epsilon.front()<<"\n";
        else
            o << "Multi precision is being used\n";
        print_precond(M, o);
        o << "#######################################################\n";
        o << restart_param;
    }

    template<class Vector>
    void check_params(const Vector &X, const Vector &B, int max_space, int max_mvp)
    {
        TIMER_TRACE;
        // FABULOUS_ASSERT( X.get_nb_row() == B.get_nb_row()  );
        FABULOUS_ASSERT( X.get_nb_col() == B.get_nb_col()  );

        // int problem_dimension = B.get_nb_row();
        int nb_right_hand_sides = B.get_nb_col();

        // if (problem_dimension <= 0) {
        //     FABULOUS_THROW(Parameter, "invalid problem dimension: null or negative value");
        // }

        if (nb_right_hand_sides <= 0) {
            FABULOUS_THROW(Parameter, "invalid number of right hand sides: null or negative value");
        }

        if (max_space <= 0) {
            FABULOUS_THROW(Parameter, "invalid maximum krylov space size");
        }

        if ( max_space < nb_right_hand_sides) {
            FABULOUS_THROW(Parameter,
                           "maximum krylov search space is lower "
                           "than number of right hand sides");
        }

        if (max_mvp <= 0) {
            FABULOUS_THROW(Parameter, "invalid maximum matrix vector product");
        }

        if ( max_mvp < nb_right_hand_sides) {
            FABULOUS_THROW(Parameter,
                           "maximum matrix vector product is lower "
                           "than number of right hand sides");
        }

        if (max_mvp <= max_space) {
            warning("maximum matrix vector product is lower than maximum"
                    " krylov space size. No restart is possible");
        }
    }

    void check_norm_B(std::vector<P> &normB)
    {
        TIMER_TRACE;
        const unsigned N = normB.size();
        for (unsigned i = 0; i < N; ++i) {
            if (normB[i] == 0.0) {
                FABULOUS_WARNING("right hand side have some vector made of zeros");
                FABULOUS_WARNING(" B["<<i<<"] == 0.0 !!");
                normB[i] = 1.0;
            }
        }
    }

    /**
     * \brief BGMRes with Inexact Breakdowns, restart and preconditionner
     *
     * Ref: IB-BGMRES-DR (Giraud Jing Agullo): p.21
     *
     * \param[in] A Matrix, need to provide <br/>
     * MatBlockVect(), size(), useRightPreCond(), PrecondBlockVect(), DotProduct()
     *
     * \param[in,out] X Initial guess solution and solution in ouput
     * \param[in] B Block containing Right Hand Side
     * \param algo instance of Algorithm as returned by one of the following:
     *   -     ::fabulous::bgmres::std()
     *   -     ::fabulous::bgmres::qr()
     *   -     ::fabulous::bgmres::qrdr()
     *   -     ::fabulous::bgmres::ib()
     *   -     ::fabulous::bgmres::ibdr()
     *   -     ::fabulous::bgmres::qribdr()
     *   -     ::fabulous::bgmres::cham_qr()
     *   -     ::fabulous::bgmres::cham_tl()
     * \param max_mvp Maximum number of Matrix Vector Product.
     * \param max_krylov_space_size maximum size of Krylov space
     * \param[in] epsilon Target accuracy
     * \param[in] restart_param DeflatedRestart or ClassicRestart instance
     * \param ortho OrthoParam
     * (through QR factorization) or vector wise arnoldi. <br/>
     * (In distributed, only the vector wise will works)
     *
     * \return Total Number of Matrix vector product done
     *  (cumulated size of the spanned Krylov Spaces)
     */
    template<class Algo, class RestartParam,
             class Matrix, class Precond,
             class Callback, class Vector>
    int solve( const Matrix &A,
               const Precond &M,
               const Callback &CB,
               Vector &X, Vector &B,
               Algo algo, Parameters param,
               const std::vector<P> &epsilon,
               OrthoParam ortho, RestartParam restart_param )
    {
        TIMER_TRACE;
        const int nbRHS = B.get_nb_col();
         // const int dim   = B_.get_nb_row();
        const int max_space = param.max_space;
        const int max_mvp = param.max_mvp;

        check_params(X, B, max_space, max_mvp);

        reset();
        print_start_info( param.quiet, -1, nbRHS, max_mvp, max_space,
                          ortho, restart_param, epsilon, M, algo  );
        std::vector<P> normB = B.cwise_norm();
        check_norm_B(normB);
        std::vector<P> inv_normB = array_inverse(normB);
        X.cwise_scale(inv_normB);
        B.cwise_scale(inv_normB);

        const int nb_eigen_pair = restart_param.get_k();

        auto base = make_base(X, max_space+nbRHS);
        auto restarter = make_restarter(restart_param, base, param.quiet);
        bool convergence = false;
        while (!convergence && _mvp < max_mvp) {
            //Compute nb of mat vect product to give to Arnoldi procedure
            const int size_to_span = std::max(
                std::min(max_space, max_mvp-_mvp),
                nbRHS
            );
            param.max_space = size_to_span;
            print_iteration_start_info(param.quiet, size_to_span);
            if (nbRHS + nb_eigen_pair > size_to_span) {
                break;
            }

            auto arnoldi = make_arnoldi<S>(algo, _logger, nbRHS, param);
            convergence = arnoldi.run(A, M, CB, X, B, epsilon, restarter, ortho);
            _nb_iteration += arnoldi.get_nb_iteration();
            const int mvp = arnoldi.get_nb_mvp();
            _mvp += mvp;
            if (mvp == 0 || _nb_iteration == 0) {
                break;
            }
            ++ _nb_restart;
            print_iteration_end_info(param.quiet, arnoldi.get_krylov_space_size());
        }

        if (_mvp == 0) {
            warning(
                "No matrix multiplication was performed. "
                "You may want to check the algorithm parameters"
            );
        }
        if (_nb_iteration == 0) {
            warning(
                "No iterations was performed. "
                "You may want to check the algorithm parameters"
            );
        }
        X.cwise_scale(normB);
        B.cwise_scale(normB);
        return _mvp;
    }

public:
    explicit BGMRes():
        _logger{},
        _nb_restart{0},
        _nb_iteration{0},
        _mvp{0}
    {
    }

    void reset()
    {
        _nb_restart = 0;
        _nb_iteration = 0;
        _mvp = 0;
        _logger.reset();
    }

    template<class Algo, class Equation, class RestartParam>
    int solve(Equation &eq, Algo algo, Parameters param,
              OrthoParam ortho, RestartParam restart_param)
    {
        _logger.resize_user_data(param.logger_user_data_size);
        _logger.set_quiet(param.quiet);
        return solve(eq.A, eq.M, eq.CB, eq.X, eq.B, algo, param,
                     eq.epsilon, ortho, restart_param);
    }

    Logger &get_logger()
    {
        return _logger;
    }

}; // end class BGMRes

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_BGMRES_HPP
