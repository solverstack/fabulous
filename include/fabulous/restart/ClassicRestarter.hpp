#ifndef FABULOUS_CLASSIC_RESTARTER_HPP
#define FABULOUS_CLASSIC_RESTARTER_HPP

namespace fabulous {
namespace bgmres {
template<class S> class ClassicRestarter;
}
}

#include "fabulous/restart/RestartParam.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/data/Base.hpp"

namespace fabulous {
namespace bgmres {

/*!
 * \brief Stub class that can subtitute Restarter< DeflatedRestart \<S>,S> but does nothing
 */
template<class Base>
class ClassicRestarter
{
public:
    using base_type    = Base;
    using vector_type  = typename Base::vector_type;
    using value_type   = typename vector_type::value_type;
    using primary_type = typename Arithmetik<value_type>::primary_type;

private:
    using S = value_type;
    using P = primary_type;

    Base &_base;

public:
    ClassicRestarter(const ClassicRestart&, Base &base, bool/*quiet*/=true):
        _base{base}
    {
    }

    operator bool() const { return false; }
    Base &get_base() { return _base; }

    void save_LS(const Block<S> &) const {}
    template<class HESS> void save_hess(const HESS&) const {}
    template<class BLOCK> void save_PW(const BLOCK&) const {}

    template<class ... Params> std::tuple<P,P,bool> run(const Params& ... ) const
    {
        FABULOUS_THROW(Unsupported, "Restarting procedure for classic restart must not be called!\n");
        return std::make_tuple(-1.0, -1.0, false);
    }

    template<class ... Params> std::tuple<P,P,bool> run_const(const Params& ... ) const
    {
        FABULOUS_THROW(Unsupported, "Restarting procedure for classic restart must not be called!\n");
        return std::make_tuple(-1.0, -1.0, false);
    }
};

} // end namespace bgmres

} // end namespace fabulous


#endif // FABULOUS_CLASSIC_RESTART_HPP
