#ifndef FABULOUS_DEFLATED_RESTARTER_BGCRO_HPP
#define FABULOUS_DEFLATED_RESTARTER_BGCRO_HPP

namespace fabulous {
namespace bgcro {
template<class> class HessGCRO;
template<class> class DeflatedRestarter;
}
}

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Timer.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/svd.hpp"
#include "fabulous/kernel/ggev.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/restart/EigenUtils.hpp"
#include "fabulous/data/Base.hpp"
#include "fabulous/data/BlockPW.hpp"

#include "fabulous/restart/RestartParam.hpp"
#include "fabulous/hessenberg/HessIB_GCRO.hpp"

namespace fabulous {
namespace bgcro {

/*!
 * \brief Handle the Deflated Restarting with eigen values
 */
template<class Base>
class DeflatedRestarter
{
public:
    using base_type    = Base;
    using vector_type  = typename Base::vector_type;
    using value_type   = typename vector_type::value_type;
    using primary_type = typename Arithmetik<value_type>::primary_type;

private:
    using Vector = vector_type;
    using S = value_type;
    using P = primary_type;

    const bool _quiet;
    const int _k; /*!<Number of eigen pair to keep*/
    const S _target; /*!< targeted eigen value*/
    Base &_base_V; /*!< Last Base generated */
    Block<S> _saved_Hess;  /*!< Last Hessenberg generated*/
    vector_type _saved_PW; /*!< Last candidate generated; used only for IB+DR
                            * STORED in assembled (concatenated) form*/
    Block<S> _LS; /*!< Least square residual*/

public:
    DeflatedRestarter(const DeflatedRestart<S> &param, Base &base, bool quiet):
        _quiet{quiet},
        _k{param.get_k()},
        _target{param.get_target()},
        _base_V{base},
        _saved_Hess{},
        _saved_PW{_base_V.get_ref(), 0},
        _LS{}
    {
    }

private:


    // Compute F_^{H} x V^{H} x [Ck, V]
    void apply_FH_VH_W(const Vector &Ck, const Vector &in, Block<S> &out){

        TIMER_TRACE;
        const int k = Ck.get_nb_col();
        const int nb_vect = _base_V.get_nb_vect();
        const int nbRHS = _saved_PW.get_nb_col();
        const int size = k + nb_vect; // F.get_nb_col()

        FABULOUS_ASSERT(in.get_nb_col() == out.get_nb_col());
        FABULOUS_ASSERT(_saved_Hess.get_nb_row() == size + nbRHS);
        FABULOUS_ASSERT(_saved_Hess.get_nb_col() == size);
        FABULOUS_ASSERT(out.get_nb_row() == size);

        const Vector PW = _saved_PW;
        const Block<S> L = _saved_Hess.sub_block(k, k, nb_vect, nb_vect);
        const Block<S> GED = _saved_Hess.sub_block(k + nb_vect, k, nbRHS, nb_vect);

        Block<S> out2 = out.sub_block(k, 0, nb_vect, out.get_nb_col());
        if(k > 0){
            const Block<S> Bm = _saved_Hess.sub_block(0, k, k, nb_vect);
            Block<S> out1 = out.sub_block(0, 0, k, out.get_nb_col());
            Ck.dot(in, out1);
            Bm.dot(out1, out2);
        } else{
            out2.zero();
        }

        Block<S> tmp{nb_vect, in.get_nb_col()};
        BASE_DOT(_base_V, in, tmp);
        block_dot_add(L, tmp, out2);
        Block<S> tmp2{nbRHS, in.get_nb_col()};
        PW.dot(in, tmp2);
        block_dot_add(GED, tmp2, out2);

    }

    // Compute the k vector with the smallest spectal information
    Block<S> compute_Gk(const Vector &Uk, const Vector &Ck)
    {
        TIMER_TRACE;
        const int k = Uk.get_nb_col();
        const int nb_vect = _base_V.get_nb_vect();
        const int size = k + nb_vect; // Fm.get_nb_col()
        const Block<S> Fm = _saved_Hess;
        Block<S> A{size, size};
        Block<S> B{size, size};

        // A <- Fm_^{h} * Fm_
        Fm.dot(Fm, A);

        // B <- Fm_^{h} * Vm_1^{h} * Wm
        if(k > 0){
            Block<S> Bk = B.sub_vector(0, k);
            apply_FH_VH_W(Ck, Uk, Bk);
        }

        // pesemistic on the orthogonality of the basis
        int ni = 0;
        for(int i = 0; i < _base_V.get_nb_block(); i++){
            const Vector Vi = _base_V[i];
            const int pi = Vi.get_nb_col();
            Block<S> Bpi = B.sub_vector(k + ni, pi);
            apply_FH_VH_W(Ck, Vi, Bpi);
            ni += pi;
        }
        // obtimistic on the orthogonality of the basis
        //B.sub_block(k, k, nb_vect, nb_vect) = trans(Fm.sub_block(k, k, nb_vect, nb_vect));


        Block<S> vleft{};   // hold left eigen vectors that won't be computed
        Block<S> vright{size, size}; // hold right eigen vectors before sorting them
        /* complex alpha in order to store the eigen value that
         * might be complex even in real case */
        Block<std::complex<P>> alpha{size, 1};
        Block<S> beta{size, 1};

        int err = lapacke::ggev(
            size,
            A.get_ptr(), A.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha.get_ptr(), beta.get_ptr(),
            vleft.get_ptr(), vleft.get_leading_dim(),
            vright.get_ptr(), vright.get_leading_dim()
        );

        if (err != 0) {
            FABULOUS_THROW(Kernel, "ggev (generalized eigen value) err="<<err);
        }

        std::vector<int> ind{};
        GetKEigenPairs<S,P>(alpha, beta, _k, _target, ind);

        int new_k = ind.size();
        for (int i=0; !_quiet && i < new_k; ++i) {
            std::cerr << "i=" << i << " : index=" << ind[i] <<" : eigen val : "
                      << alpha.at(ind[i],0) / beta.at(ind[i], 0)<<"\n";
        }

        // Complex spectral values new_k = _k or _k+1
        Block<S> G{size, new_k};
        for (int j = 0; j < new_k; ++j)
            memcpy(G.get_vect(j), vright.get_vect(ind[j]), size * sizeof(S));
        return G;
    }

    void print_dr_start(int p, int nm, int dim)
    {
        if (_quiet) return;
        std::cerr<<"About to DR ################## !!!\n"
                 <<"Cste ::\n"
                 <<"\tk :: "<<_k<<"\n"
                 <<"\ttarget :: "<<_target<<"\n"
                 <<"\tp :: "<<p<<"\n"
                 <<"\tnm :: "<<nm<<"\n"
                 <<"\tnbCol in Hess :: "<<_saved_Hess.get_F().get_nb_col()<<"\n"
                 <<"\t_Base nbCols :: "<<_base_V.get_nb_vect()<<"\n"
                 <<"\tldh :: "<<_saved_Hess.get_F().get_leading_dim()<<"\n"
                 <<"\tdim :: "<<dim<<"\n"
                 <<std::endl;
    }

public:

    operator bool () { return true; }
    Base &get_base() { return _base_V; }
    template<class BLOCK> void save_LS(BLOCK &LS) { _LS = LS; }
    template<class BLOCK> void save_hess(BLOCK &hess) { _saved_Hess = hess; }
    template<class BLOCK_PW> void save_PW(BLOCK_PW &PW) { _saved_PW = PW.concat_PW(); }

    /*!
     * \brief This function handle the deflation of eigenvalues at restart for BGCRO algorithm.
     *
     * \param[out] Lambda1 first right-hand-side of the least square
     * \param[out] PWnew first vector of the search space
     * \param[in,out] Uk deflated search basis
     * \param[in,out] Ck deflated residual basis
     */
    template<class VectorPW>
    void run(Block<S> &Lambda1, VectorPW &PWnew, Vector &Uk, Vector &Ck, const bool Uk_const = false)
    {
        TIMER_RESTART;
        Timer time;
        time.start();

        //  k : the  current  size of Uk / Ck (0 if first solve)
        // _k : the   wanted  size of Uk / Ck
        int k = Uk.get_nb_col();
        const int nb_block = _base_V.get_nb_block();
        const int nb_vect = _base_V.get_nb_vect();
        const int nbRHS = _saved_PW.get_nb_col();
        const int size = k + nb_vect; // F.get_nb_col()

        // If Vm is empty, can not deflate
        FABULOUS_ASSERT( nb_block > 0 );

        // Computing V1 and lambda1
        qr::InPlaceQRFacto(_LS, Lambda1);
        PWnew.set_size_P(0);
        Vector &V1 = PWnew.get_W();
        V1.zero();
        if(k > 0){
            Block<S> LS1 = _LS.sub_block(0, 0, k, nbRHS);
            V1.axpy(Ck, LS1);
        }
        Block<S> LS2 = _LS.sub_block(k, 0, nb_vect, nbRHS);
        Block<S> LS3 = _LS.sub_block(k + nb_vect, 0, nbRHS, nbRHS);
        BASE_AXPY(V1, _base_V, LS2);
        V1.axpy(_saved_PW, LS3);

        if(Uk_const){ // Don't need to compute the new Uk / Ck values
            // Emptying the base
            _base_V.reset();

            time.stop();
            return;
        }

        // Computing Gk
        Block<S> G = compute_Gk(Uk, Ck);
        // Uk might be of dimension _k or _k+1 depending on the eigen values
        const int new_k = G.get_nb_col();

        // Computing F*G = Q * R
        Block<S> FG{size + nbRHS, new_k};
        FG.axpy(_saved_Hess, G);
        Block<S> R{new_k, new_k};

        qr::InPlaceQRFacto(FG, R);

        // Allocate Uk and Ck if first solve
        if(k == 0){
            Uk = Vector{V1, new_k};
            Ck = Vector{V1, new_k};
        }

        // Computing Uk_new = G * R^{-1}
        if(k > 0){
            // Uk_new = Uk * G_1
            Vector Uk_tmp{Uk, new_k};
            Block<S> G1 = G.sub_block(0, 0, k, new_k);
            Uk_tmp.axpy(Uk, G1);
            Uk = Uk_tmp.copy();
        }
        // now Uk has new_k columns

        // Uk_new += Vm * G_2
        Block<S> G2 = G.sub_block(k, 0, nb_vect, new_k);
        BASE_AXPY(Uk, _base_V, G2);

        // Uk_new *= R^{-1}
        Uk.trsm(Uk, R);

        // Computing Ck_new = Ck * Q
        if(k > 0){
            // Ck_new = Ck * Q_1
            Vector Ck_tmp{Ck, new_k};
            Block<S> Q1 = FG.sub_block(0, 0, k, new_k);
            Ck_tmp.axpy(Ck, Q1);
            Ck = Ck_tmp.copy();
        }
        // now Ck has new_k columns

        // Ck_new += Vm * Q_2
        Block<S> Q2 = FG.sub_block(k, 0, nb_vect, new_k);
        BASE_AXPY(Ck, _base_V, Q2);
        // Ck_new += [P, W] * Q_3
        Block<S> Q3 = FG.sub_block(k + nb_vect, 0, nbRHS, new_k);
        Ck.axpy(_saved_PW, Q3);

        // Emptying the base
        _base_V.reset();

        time.stop();
    }

}; // end class Restarter

} // end namespace bgcro
} // end namespace fabulous

#endif // FABULOUS_DEFLATED_RESTARTER_BGCRO_HPP
