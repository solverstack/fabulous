#ifndef FABULOUS_EIGEN_UTILS_HPP
#define FABULOUS_EIGEN_UTILS_HPP

#include <complex>
#include <limits>
#include <type_traits>

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"

namespace fabulous {

/**
 * \brief Find the k nearest eigen values from target
 *
 * \param k number of eigen pair wanted
 * \param target the targeted eigen value
 * \param[in] alpha as returned by the GGEV Lapack Kernel
 * \param[in] beta as returned by the GGEV Lapack Kernel
 * \param[out] ind the indices of the found vectors
 *
 * \return the indexes of the k nearest ones to target
 */
template<class S, class P>
void GetKIndexesComplex(Block<std::complex<P>> &alpha, Block<S> &beta,
                        int k, S target, std::vector<int> &ind)
{
    using Complex = std::complex<P>;

    for (int i = 0; i < k; ++i) {
        P min = std::numeric_limits<P>::max();

        int last_ind_found = -1;
        for (int j = 0; j < alpha.get_nb_row(); ++j) {
            //Check if already found
            if (std::find_if(
                    ind.begin(), ind.end(),
                    [j](const int& a) -> bool { return (a == j);  }) == ind.end())
            {
                Complex diff = (alpha.at(j,0)/beta.at(j,0)) - target;
                if (std::norm(diff) < min) // Check if min
                {
                    min = std::norm(diff);
                    last_ind_found = j;
                }
            }
        }

        FABULOUS_ASSERT( last_ind_found != -1 );
        ind.emplace_back(last_ind_found);
    }
}

/**
 * \brief Find the k (or k+1) nearest eigen values from target

 * This function take into account that  we may need an extra eigen pair
 * in order to be sure we got the conjugate of the last eigen value.
 *
 * \param k number of eigen pair wanted
 * \param target the targeted eigen value
 * \param[in] alpha as returned by the GGEV Lapack Kernel
 * \param[in] beta as returned by the GGEV Lapack Kernel
 * \param[out] ind the indices of the found vectors
 *
 * \return the indexes of the k nearest ones to target
 */
template<class S, class P>
void GetKIndexesReal( Block<std::complex<P>> &alpha, Block<S> &beta,
                      int k, S target, std::vector<int> &ind)
{
    using Complex = std::complex<P>;

    for (int i = 0; i < k; ++i) {
        P min = std::numeric_limits<P>::max();

        int last_ind_found = -1;
        for (int j = 0; j < alpha.get_nb_row(); ++j)
        {
            //Check if already found
            if (std::find_if(
                    ind.begin(), ind.end(),
                    [j](const int& a) -> bool { return (a == j); }) == ind.end())
            {
                //Check if min
                Complex diff = (alpha.at(j,0)/beta.at(j,0)) - target;

                if (std::norm(diff) < min) {
                    min = std::norm(diff);
                    last_ind_found = j;
                }
            }
        }
        FABULOUS_ASSERT( last_ind_found != -1 );
        std::complex<P> curr_eigen_val;
        curr_eigen_val = alpha.at(last_ind_found, 0) / beta.at(last_ind_found, 0);

        if (curr_eigen_val.imag() == P{0.0}) {
            ind.emplace_back(last_ind_found);
        } else { // if complex
            ++i; // we will add two indexes
            if (curr_eigen_val.imag() > P{0.0}) {
                // If it's the first of the conjugate pair
                ind.emplace_back(last_ind_found);
                ind.emplace_back(last_ind_found+1);
            } else {
                // Otherwise it's the second of the conjugate pair
                ind.emplace_back(last_ind_found);
                ind.emplace_back(last_ind_found-1);
            }
        }
    }
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
template<class S, class P>
void GetKEigenPairs(Block<std::complex<P>> &alpha, Block<S> &beta,
                    int k, S target, std::vector<int> &ind)
{
    FABULOUS_DISABLE_INVALID_ARITHMETIC(S);
}

#pragma GCC diagnostic pop

template<> //S = float AND P = float
inline void GetKEigenPairs(Block<std::complex<float>> &alpha, Block<float> &beta,
                           int k, float target, std::vector<int> &ind)
{
    GetKIndexesReal<float,float>(alpha, beta, k, target, ind);
}

template<> //S = double AND P = double
inline void GetKEigenPairs(Block<std::complex<double>> &alpha, Block<double> &beta,
                           int k, double target, std::vector<int> &ind)
{
    GetKIndexesReal<double,double>(alpha, beta, k, target, ind);
}

template<> //S = complex<float> AND P = float
inline void GetKEigenPairs(Block<std::complex<float>> &alpha,
                           Block<std::complex<float>> &beta,
                           int k, std::complex<float> target,
                           std::vector<int> &ind)
{
    GetKIndexesComplex<std::complex<float>,float>(alpha, beta, k, target, ind);
}

template<> //S = complex<double> AND P = double
inline void GetKEigenPairs(Block<std::complex<double>> &alpha,
                           Block<std::complex<double>> &beta,
                           int k, std::complex<double> target,
                           std::vector<int> &ind)
{
    GetKIndexesComplex<std::complex<double>,double>(alpha, beta, k, target, ind);
}

/**
 * \brief Check the results of the generalized eigen values problem
 *
 * \f$ || A*v - \lambda B * v || \f$
 */
template<class S, class P>
void CheckEigenResults(Block<S> &A, Block<S> &B, Block<S> &vr,
                       Block<std::complex<P>> &alpha, Block<S> &beta)
{
    const int M = A.get_nb_row();
    Block<S> res{M,M};

    for (int i=0; i< M; i++) {
        S theta_i = alpha(i) / beta(i);

        // res_i = A*vr_i
        lapacke::gemm(
            M, 1, M,
            A.get_ptr(), A.get_leading_dim(),
            vr.get_vect(i), vr.get_leading_dim(),
            res.get_vect(i), res.get_leading_dim(),
            S{1.0}, S{0.0}
        );

        lapacke::gemm( // res_i = (alpha[i]/beta[i])B * vr_i - res_i
            M, 1, M,
            B.get_vect(), B.get_leading_dim(),
            vr.get_vect(i), vr.get_leading_dim(),
            res.get_vect(i), res.get_leading_dim(),
            theta_i, S{-1.0}
        );
    }
    auto minmax = min_max_norm(res);
    P normA = min_max_norm(A).first; //Norm of A
    P normB = min_max_norm(B).first;

    std::cerr<<"Checking the eigen values/ eigen vectors after Eigen prob. solved\n";
    std::cerr<<"\t\tMin max norm of res :\tMIN\t"<<minmax.first
             <<"\tMAX\t"<<minmax.second<<"\n";
    std::cerr<<"\t\t Norms of matrices A and B : \n";
    std::cerr<<"\t\t A : "<<normA<<"\t B : "<<normB<<"\n";

    std::cerr<<"\n\n";
    std::cerr<<"\t\t\tResults normed : "<< minmax.first / std::max(normA,normB)<<"\t"
             <<minmax.second / std::max(normA,normB)<<"\n";
}

} // end namespace fabulous

#endif // FABULOUS_EIGEN_UTILS_HPP
