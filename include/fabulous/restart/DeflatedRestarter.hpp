#ifndef FABULOUS_DEFLATED_RESTARTER_HPP
#define FABULOUS_DEFLATED_RESTARTER_HPP

namespace fabulous {
namespace bgmres {
template<class> class HessIBDR;
template<class> class DeflatedRestarter;
}
}

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Timer.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/svd.hpp"
#include "fabulous/kernel/ggev.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/restart/EigenUtils.hpp"
#include "fabulous/data/Base.hpp"
#include "fabulous/data/BlockPW.hpp"

#include "fabulous/restart/RestartParam.hpp"

namespace fabulous {
namespace bgmres {

/*!
 * \brief Handle the Deflated Restarting with eigen values
 */
template<class Base>
class DeflatedRestarter
{
public:
    using base_type    = Base;
    using vector_type  = typename Base::vector_type;
    using value_type   = typename vector_type::value_type;
    using primary_type = typename Arithmetik<value_type>::primary_type;

private:
    using S = value_type;
    using P = primary_type;

    const bool _quiet;
    const int _k; /*!<Number of eigen pair to keep*/
    const S _target; /*!< targeted eigen value*/
    Base &_base_V; /*!< Last Base generated */
    Block<S> _saved_Hess;  /*!< Last Hessenberg generated*/
    vector_type _saved_PW; /*!< Last candidate generated; used only for IB+DR
                            * STORED in assembled (concatenated) form*/
    Block<S> _LS; /*!< Least square residual*/

public:
    DeflatedRestarter(const DeflatedRestart<S> &param, Base &base, bool quiet):
        _quiet{quiet},
        _k{param.get_k()},
        _target{param.get_target()},
        _base_V{base},
        _saved_Hess{},
        _saved_PW{_base_V.get_ref(), 0},
        _LS{}
    {
    }

private:
    std::tuple<Block<S>,Block<S>,int>
    compute_underline_G(const int nm, const int p)
    {
        TIMER_TRACE;
        // ALGORITHM 2; STEP 3
        Block<S> A{nm, nm};
        Block<S> B{nm, nm};
        Timer timer_gemm1, timer_conj, timer_ggev;
        Timer timer_get_eigen_pair, timer_end;

        timer_gemm1.start();
        const Block<S> Hm = _saved_Hess.sub_block( 0, 0, nm+p, nm );
        Hm.dot(Hm, A); // A <- Hm_^{h} * Hm_
        timer_gemm1.stop();

        timer_conj.start();
        const Block<S> L = _saved_Hess.sub_block(0, 0, nm, nm);
        B = trans(L); // B <- Hm^{h}
        timer_conj.stop();

        Block<S> vleft{};   // hold left eigen vectors that won't be computed
        Block<S> vright{nm, nm}; // hold right eigen vectors before sorting them
        /* complex alpha in order to store the eigen value that
         * might be complex even in real case */
        Block<std::complex<P>> alpha{nm, 1};
        Block<S> beta{nm, 1};

        timer_ggev.start();
        int err = lapacke::ggev(
            nm,
            A.get_ptr(), A.get_leading_dim(),
            B.get_ptr(), B.get_leading_dim(),
            alpha.get_ptr(), beta.get_ptr(),
            vleft.get_ptr(), vleft.get_leading_dim(),
            vright.get_ptr(), vright.get_leading_dim()
        );
        timer_ggev.stop();

        if (err != 0) {
            FABULOUS_THROW(Kernel, "ggev (generalized eigen value) err="<<err);
        }

        timer_get_eigen_pair.start();
        std::vector<int> ind{};
        GetKEigenPairs<S,P>(alpha, beta, _k, _target, ind);
        timer_get_eigen_pair.stop();

        int new_k = ind.size();
        for (int i=0; !_quiet && i < new_k; ++i) {
            std::cerr << "i=" << i << " : index=" << ind[i] <<" : eigen val : "
                      << alpha.at(ind[i],0)/ beta.at(ind[i], 0)<<"\n";
        }

        // ALGORITHM 4; STEP 2: "Append R_{LS_m} to ~G ..."
        Block<S> G{nm+p, new_k+p}; // block to hold \underline{G}
        Block<S> Gright = G.sub_block(0, new_k, nm+p, p);
        Gright.copy(_LS); // Copy last residual (R_{LS_m}) inside right part of G
        // Copy back the corresponding eigen vectors inside left part G.

        timer_end.start();
        Block<S> Gleft  = G.sub_block(0, 0, nm, new_k);
        for (int j = 0; j < new_k; ++j)
            memcpy(Gleft.get_vect(j), vright.get_vect(ind[j]), nm*sizeof(S));

        // ALGORITHM 4; STEP 3 Reduced QR Factorization of \underline{G}
        Block<S> Qg = G; Block<S> Rg{new_k+p, new_k+p};
        qr::InPlaceQRFacto(Qg, Rg);
        timer_end.stop();

        // FABULOUS_DEBUG("gemm_time="<<timer_gemm1.get_length());
        // FABULOUS_DEBUG("conj_time="<<timer_conj.get_length());
        // FABULOUS_DEBUG("ggev_time="<<timer_ggev.get_length());
        // FABULOUS_DEBUG("get_eigen_pair_time="<<timer_get_eigen_pair.get_length());
        // FABULOUS_DEBUG("end_time="<<timer_end.get_length());

        return std::make_tuple(Qg, Rg, new_k);
    }

    void print_dr_start(int p, int nm, int dim)
    {
        if (_quiet) return;
        std::cerr<<"About to DR ################## !!!\n"
                 <<"Cste ::\n"
                 <<"\tk :: "<<_k<<"\n"
                 <<"\ttarget :: "<<_target<<"\n"
                 <<"\tp :: "<<p<<"\n"
                 <<"\tnm :: "<<nm<<"\n"
                 <<"\tnbCol in Hess :: "<<_saved_Hess.get_nb_col()<<"\n"
                 <<"\t_Base nbCols :: "<<_base_V.get_nb_vect()<<"\n"
                 <<"\tldh :: "<<_saved_Hess.get_leading_dim()<<"\n"
                 <<"\tdim :: "<<dim<<"\n"
                 <<"\tLS : leading dim : "<<_LS.get_leading_dim()<<"\n"
                 <<"\tLS : nbrow : "<<_LS.get_nb_row()<<"\n"
                 <<std::endl;
    }

public:

    operator bool () { return true; }
    Base &get_base() { return _base_V; }
    template<class BLOCK> void save_LS(BLOCK &LS) { _LS = LS; }
    template<class BLOCK> void save_hess(BLOCK &hess) { _saved_Hess = hess; }
    template<class BLOCK_PW> void save_PW(BLOCK_PW &PW) { _saved_PW = PW.concat_PW(); }

    /*!
     * \brief This function handle the deflation of eigenvalues at restart. (STD variant)
     *
     * \param[out] hess the new hessenberg to be filled with H1new
     * \param[in] epsilon backward error threshold
     */
    template<class Hessenberg, class = enable_if_t<not handle_ib_t<Hessenberg>::value >>
    std::tuple<P,P,bool> run(Hessenberg &hess, const std::vector<P> &epsilon)
    {
        TIMER_RESTART;
        Timer time;
        time.start();

        const int p   = _LS.get_nb_col();
        const int nb_block = _base_V.get_nb_block();
        const int nm  = _base_V.get_nb_vect(nb_block-1);
        const int dim = -1/*_base_V.get_dimension()*/;
        FABULOUS_ASSERT( nb_block > 0 );
        print_dr_start(p, nm, dim);

        int k;
        Block<S> Qg, Rg;
        std::tie(Qg, Rg, k) = compute_underline_G(nm, p);

        vector_type V1new{_base_V[0], k};
        const Block<S> Qg_1 = Qg.sub_block( 0, 0, nm, k );
        V1new.zero();
        BASE_AXPY(V1new, _base_V, Qg_1); // V1 new = Vm * Qg{1:nm,1:k}

        vector_type V2new{_base_V[0], p};
        const Block<S> Qg_2 = Qg.sub_block( 0, k, nm+p, p );
        V2new.zero();
        BASE_AXPY(V2new, _base_V, Qg_2); // V2 new = Vm+1 * Qg{1:nm+p,k+1:k+p}

        // Modify Hess ...
        hess.reserve_DR(k);
        Block<S> H1new = hess.get_H1new();
        FABULOUS_ASSERT( H1new.get_ptr() != _saved_Hess.get_ptr() );
        FABULOUS_ASSERT( H1new.get_nb_row() == k+p );

        {// Strat 1  :: H1new = Qg^{H} * Hm_ * Qg(1:nm,1:k)
            Block<S> tmp_HmXQg{nm+p, k};
            const Block<S> Hm = _saved_Hess.sub_block(0, 0, nm+p, nm);
            tmp_HmXQg = Hm * Qg_1; // tmp = Hm_ * Qg(1:nm,1:k)
            Qg.dot(tmp_HmXQg, H1new); // H1new = Qg^{H} * tmp
        }

        _base_V.reset(); // Update base:
        _base_V.add_vector( V1new );
        _base_V.add_vector( V2new );

        // R1 is made from the last columns of R
        Block<S> R1 = Rg.sub_block(0, k, k+p, p);
        hess.set_rhs(R1);// important to do this before orthogonalization end
        hess.notify_ortho_end();

        time.stop();
        double length = time.get_length();
        if (!_quiet) {
            std::cerr<<"\tElapsed Time for Computing the Deflation at restart: "<<length<<" seconds\n";
            std::cerr<<Color::bold<<Color::yellow<<"\t\tDR done"<<Color::reset<<std::endl;
        }
        return min_max_conv(R1, epsilon);
    }

    /*!
     * \brief This function handle the deflation of eigenvalues at restart. (IB variant)
     *
     * \param[out] hess the new hessenberg to be filled with F1new
     * \param[out] PWnew PW block for IB-BGMRES-DR algorithm
     * \param[in] epsilon backward error threshold
     */
    template<class Hessenberg, class VectorPW,
             class = enable_if_t<handle_ib_t<Hessenberg>::value> >
    std::tuple<P,P,bool> run(Hessenberg &hess, VectorPW &PWnew, const std::vector<P> &epsilon)
    {
        TIMER_RESTART;
        Timer time;
        time.start();
        const int p   = _LS.get_nb_col();
        const int nm  = _saved_Hess.get_nb_col();
        const int dim = -1/*_base.get_dimension()*/;

        print_dr_start(p, nm, dim);
        FABULOUS_ASSERT( _base_V.get_nb_vect() >= nm );
        FABULOUS_ASSERT( _saved_Hess.get_nb_row() == nm + p );
        FABULOUS_ASSERT( _LS.get_nb_row() == nm + p );

        int k;
        Block<S> Qg, Rg;
        std::tie(Qg, Rg, k) = compute_underline_G(nm, p);

        /* ALGORITHM 4; STEP 3 (Second Part) "The matrix consisting of
         last p columns of R_G is denoted a \Lambda_1^{}new}" */
        Block<S> Lambda1new = Rg.sub_block(0, k, k+p, p);


        /* ALGORITHM 4; STEP 4:   PWnew := [Vm,[Pm-1,~Wm]]*Qg(:,k+1:k+p)*/
        // PWnew := Vm * Qg(1:nm:,k+1:k+p)*/
        const Block<S> Qg_1 = Qg.sub_block(0, k, nm, p);
        vector_type &W = PWnew.get_W();
        W.zero();
        BASE_AXPY(W, _base_V, Qg_1);

        // PWnew := PWnew + [Pm-1,~Wm]*Qg(nm+1:nm+p, k+1:k+p)
        const Block<S> Qg_2 = Qg.sub_block(nm, k, p, p);
        W.axpy(_saved_PW, Qg_2);

        // V1_new = Vm * Qg{1:nm,1:k}
        vector_type V1new{_base_V[0], k};
        const Block<S> Qg_3 = Qg.sub_block(0, 0, nm, k);
        V1new.zero();
        BASE_AXPY(V1new, _base_V, Qg_3);

        hess.reserve_DR(k);
        Block<S> F1new = hess.get_F1new();
        FABULOUS_ASSERT( F1new.get_ptr() != _saved_Hess.get_ptr() );
        FABULOUS_ASSERT( F1new.get_nb_row() == k+p );
        Block<S> L1new = F1new.sub_block(0, 0, k, k);
        Block<S> H1new = F1new.sub_block(k, 0, p, k);
        Block<S> H1neww{p, k};

        { // compute L1new := Qg(1:nm,1:k)^{H}*Lm*Qg(1:nm,1:k)
            Block<S> tmpLmXQg{nm, k};
            const Block<S> Lm = _saved_Hess.sub_block(0, 0, nm, nm);
            tmpLmXQg = Lm * Qg_3; // tmp := Lm*Qg(1:nm,1:k)
            Qg_3.dot(tmpLmXQg, L1new); // L1new := Qg(1:nm,1:k)^{H}*tmp
        }
        { // compute H1new := Qg(:,k:k+p)^{H}*Fm*Qg(1:nm,1:k)
            Block<S> tmpFmXQg{nm+p, k};
            const Block<S> Fm = _saved_Hess.sub_block(0, 0, nm+p, nm);
            tmpFmXQg = Fm * Qg_3;
            const Block<S> Qg_4 = Qg.sub_block(0, k, nm+p, p);
            Qg_4.dot(tmpFmXQg, H1neww);//  H1neww = Qg_4^{H} * tmpFmXQg;
        }
        /* ALGORITHM 4; STEP 5 "Reorthogonalize PWnew against V1new "*/
        Block<S> RV{k+p, p};
        Block<S> RV_1 = RV.sub_block( 0, 0, k, p );
        Block<S> RV_2 = RV.sub_block( k, 0, p, p );

        V1new.dot( W, RV_1 ); // RV = DotProduct( V1new, PWnew )
        W.axpy(V1new, RV_1, S{-1.0}); // W -= V1new * RV_1;
        W.qr(W, RV_2);

        /* ALGORITHM 4; STEP 6 "Compute L1 H1 and Lambda1 " */
        // L1 = L1new + H1new*RV
        L1new += RV_1 * H1neww;

        // H1 = RV*H1new
        H1new = RV_2 * H1neww;

        Block<S> Lambda1{k+p, p};
        Block<S> Lambda1_1 = Lambda1.sub_block(0, 0, k, p);
        Block<S> Lambda1_2 = Lambda1.sub_block(k, 0, p, p);
        const Block<S> L1new_1 = Lambda1new.sub_block(0, 0, k, p);
        const Block<S> L1new_2 = Lambda1new.sub_block(k, 0, p, p);

        Lambda1_1.copy(L1new_1);
        Lambda1_1 += RV_1 * L1new_2;
        Lambda1_2 = RV_2 * L1new_2;
        // ALGORITHM 4; STEP 7 --> in do_R_criterion()

        // Update
        _base_V.reset();
        _base_V.add_vector( V1new );
        hess.init_lambda(Lambda1);/* important to do this before
                                   * notify_ortho_end() (for qr version mainly) */
        hess.init_phi_deflated_restart(k);
        hess.notify_ortho_end();

        time.stop();
        double length = time.get_length();
        if (!_quiet) {
            std::cerr<<"\tElapsed Time for Computing the Deflation at restart: "<<length<<" seconds\n";
            std::cerr<<Color::bold<<Color::yellow<<"\t\tDR done"<<Color::reset<<std::endl;
        }
        return min_max_conv(Lambda1, epsilon);
    }

}; // end class Restarter

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_DEFLATED_RESTARTER_HPP
