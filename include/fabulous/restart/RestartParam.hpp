#ifndef FABULOUS_RESTART_PARAM_HPP
#define FABULOUS_RESTART_PARAM_HPP

#include <iostream>

namespace fabulous {
// Restart Parameter class:
class ClassicRestart;
template<class> class DeflatedRestart;
}

#include "fabulous/utils/Meta.hpp"

namespace fabulous {

/*!
 * \brief parameter class for classic (no deflation) restarting
 */
class ClassicRestart
{
public:
    ClassicRestart() = default;
    int get_k() { return 0; }
};

/*!
 * \brief helper function to build a ClassicRestart object
 */
inline ClassicRestart classic_restart()
{
    return ClassicRestart{};
}

/*!
 * \brief parameters for restarting with eigen values deflation
 */
template<class S>
class DeflatedRestart
{
private:
    int _k; /*!< nb_eigen_pair */
    S _target;
public:
    DeflatedRestart(int k, S target): _k(k), _target(target) {}
    S get_target() const { return _target; }
    int get_k() const { return _k; }
};

/*!
 * \brief helper function to build a DeflatedRestart object
 */
template<class S>
inline DeflatedRestart<S> deflated_restart(int k, S target)
{
    return DeflatedRestart<S>{k, target};
}

/*!
 * \brief display ClassicRestart type name
 */
inline std::ostream& operator<<(std::ostream &o, const Type<ClassicRestart>&)
{
    o << "";
    return o;
}

/*!
 * \brief display DeflatedRestart type name
 */
template<class S>
inline std::ostream& operator<<(std::ostream &o, const Type<DeflatedRestart<S>>&)
{
    o << "-DR";
    return o;
}

/*!
 * \brief display DeflatedRestart values
 */
template<class S>
inline std::ostream& operator<<(std::ostream &o, const DeflatedRestart<S> &param)
{
    o << "####################    DR PARAM   ####################\n";
    o << "Size of recycled space at restart : "<<param.get_k()<<"\n";
    o << "Targeted eigen value for restart  : "<<param.get_target()<<"\n";
    o << "#######################################################\n";
    return o;
}

/*!
 * \brief display ClassicRestart values
 */
inline std::ostream& operator<<(std::ostream &o, const ClassicRestart&)
{
    /* Display nothing here because there is nothing to display ;) */
    return o;
}

} // end namespace fabulous

#endif // FABULOUS_RESTART_PARAM_HPP
