#ifndef FABULOUS_RESTARTER_HPP
#define FABULOUS_RESTARTER_HPP

#include "fabulous/restart/ClassicRestarter.hpp"
#include "fabulous/restart/DeflatedRestarter.hpp"
#include "fabulous/restart/DeflatedRestarterBGCRO.hpp"
#include "fabulous/data/BaseGeneric.hpp"

namespace fabulous {
namespace bgmres {

template<class Base>
ClassicRestarter<Base>
make_restarter(ClassicRestart restart, Base &base, bool quiet)
{
    return ClassicRestarter<Base>{restart, base, quiet};
}

template<class Base, class S>
DeflatedRestarter<Base>
make_restarter(DeflatedRestart<S> restart, Base &base, bool quiet)
{
    return DeflatedRestarter<Base>{restart, base, quiet};
}

} // end namespace bgmres
namespace bgcro {

template<class Base>
bgmres::ClassicRestarter<Base>
make_restarter(ClassicRestart restart, Base &base, bool quiet)
{
    return bgmres::ClassicRestarter<Base>{restart, base, quiet};
}

template<class Base, class S>
DeflatedRestarter<Base>
make_restarter(DeflatedRestart<S> restart, Base &base, bool quiet)
{
    return DeflatedRestarter<Base>{restart, base, quiet};
}

} // end namespace bgcro
} // end namespace fabulous

#include "fabulous/restart/DeflatedRestarter.hpp"
#include "fabulous/restart/ClassicRestarter.hpp"

#endif // FABULOUS_RESTARTER_HPP
