#ifndef FABULOUS_BLOCK_PW_GENERIC_HPP
#define FABULOUS_BLOCK_PW_GENERIC_HPP

namespace fabulous {
template<class> class GenericPW;
}

#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/flops.hpp"
#include "fabulous/data/Block.hpp"

namespace fabulous {

/**
 * \brief Represent a block of datas containing P and W concatenated
 *
 * This use a cursor to know where P ends and W starts is needed.
 */
template<class Vector>
class GenericPW
{
public:
    using vector_type = Vector;
    using value_type = typename Vector::value_type;
    using primary_type = typename Arithmetik<value_type>::primary_type;

private:
    int _nbRHS;
    vector_type _P;
    vector_type _W;

private:
    using S = value_type;
    using P = primary_type;
public:

    GenericPW(const vector_type &ref, int nbRHS):
        _nbRHS{nbRHS},
        _P{ref, 0},
        _W{ref, nbRHS}
    {
    }

    /**
     * \brief W part (candidate for base expansion)
     */
    vector_type& get_W()
    {
        return _W;
    }

    /**
     * \brief P part (candidate part that take into account discarded directions)
     */
    vector_type& get_P()
    {
        return _P;
    }

    void set_size_P(int size)
    {
        _P = vector_type{_P, size};
        _W = vector_type{_W, _nbRHS - size};
    }

    vector_type concat_PW()
    {
        vector_type PW{_P, _nbRHS};
        PW.zero();

        if ( _P.get_nb_col() > 0 )
        {
            Block<S> P_sub_select{_P.get_nb_col(), _nbRHS};
            for (int i = 0; i < _P.get_nb_col(); ++i) {
                P_sub_select(i, i) = S{1.0};
            }
            PW.axpy(_P, P_sub_select);
        }

        if ( _W.get_nb_col() > 0 )
        {
            Block<S> W_sub_select{_W.get_nb_col(), _nbRHS};
            for (int i = 0; i < _W.get_nb_col(); ++i) {
                W_sub_select(i, _P.get_nb_col()+i) = S{1.0};
            }
            PW.axpy(_W, W_sub_select);
        }
        return PW;
    }

    /**
     * \brief Check othogonality between P and W
     */
    void check_ortho_PW(const std::string &name="PW")
    {
        if ( _P.get_nb_col() == 0 ) {
            return;
        }
        const int M = _P.get_nb_col();
        const int N = _W.get_nb_col();
        Block<S> tmp{M, N};
        _P.dot( _W, tmp );

        auto MM = min_max_norm(tmp);
        FABULOUS_DEBUG("CHECK_ORTHO PW["<<Color::green<<name<<Color::reset<<"]: (B^H*B - I) "
                       <<"Min="<<MM.first<<" Max="<<MM.second);
    }

}; // end class BlockPW

template<class Vector,
         class = enable_if_t<not is_optimizable_t<Vector>::value >,
         class = void >
GenericPW<Vector> make_PW(const Vector& ref, int nbRHS)
{
    return GenericPW<Vector>{ ref, nbRHS };
}

} // end namespace fabulous

#endif // FABULOUS_BLOCK_PW_GENERIC_HPP
