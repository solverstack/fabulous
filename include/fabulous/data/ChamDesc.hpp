#ifndef FABULOUS_CHAMELEON_DESC_HPP
#define FABULOUS_CHAMELEON_DESC_HPP

#include <memory>
#include <string>
#include <cassert>

#include <chameleon.h>

// THIS IS A HACK unless this get added in chameleon interface
extern "C" CHAM_desc_t *morse_desc_submatrix(CHAM_desc_t *descA, int i, int j, int m, int n);

namespace fabulous {
template<class> class ChamDesc;
template<class> class ChamDesc_p;
}

#include "fabulous/utils/Chameleon.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/data/TileMapper.hpp"

namespace fabulous {

/* **************************************************** */

namespace {

template<class U>
void *desc_get_blk_addr(const CHAM_desc_t *A, int m, int n)
{
    size_t mm = m + A->i / A->mb;
    size_t nn = n + A->j / A->nb;

    ChamDesc_p<U> *desc = reinterpret_cast<ChamDesc_p<U>*>(A->mat);
    return desc->get_blk_addr(mm, nn);
}

template<class U>
int desc_get_blk_ld(const CHAM_desc_t *D, int i)
{
    ChamDesc_p<U> *desc = reinterpret_cast<ChamDesc_p<U>*>(D->mat);
    return desc->get_blk_ld(i);
}

template<class U>
int desc_get_rankof(const CHAM_desc_t *D, int i, int j)
{
    ChamDesc_p<U> *desc = reinterpret_cast<ChamDesc_p<U>*>(D->mat);
    return desc->get_blk_rankof(i, j);
}

}

/* **************************************************** */

/**
 * \brief Helper class to implement Pointer semantic of the ChamDesc class
 */
template<class U>
class ChamDesc_p
{
private:
    const DataChameleonI &_tiles;
    DescPtr _descriptor;

    FABULOUS_DELETE_COPY_MOVE(ChamDesc_p);
public:

    // parent descriptor
    ChamDesc_p(const DataChameleonI &p, int mt, int nt):
        _tiles{p}
    {
        const int tile_size = _tiles.get_inner_tile_size();
        const int BS = _tiles.get_block_size();
        const int lm = mt * BS;
        const int ln = nt * BS;

        CHAM_desc_t *desc;
        CHAMELEON_Desc_Create_User(
            &desc, this, Arithmetik<U>::morse_type,
            tile_size, tile_size, tile_size*tile_size,
            lm, ln, // whole matrix size
            0, 0,   // submatrix start
            lm, ln, // submatrix size
            1, 1,
            desc_get_blk_addr<U>,
            desc_get_blk_ld<U>,
            desc_get_rankof<U>
        );
        _descriptor.reset(desc, DescDeleter{});
    }

    // sub descriptor
    ChamDesc_p(const ChamDesc_p<U> &o, int it, int jt, int mt, int nt):
        _tiles(o._tiles)
    {
        // int tile_size = _tiles.get_tile_size();

        const int BS = _tiles.get_block_size();
        const int i  = it * BS;
        const int j  = jt * BS;
        const int lm = mt * BS;
        const int ln = nt * BS;

        CHAM_desc_t *desc;
        desc = morse_desc_submatrix(o._descriptor.get(), i, j, lm, ln);

        // FABULOUS_ASSERT( it <= p._mt );
        // FABULOUS_ASSERT( jt <= p._nt );

        _descriptor.reset(desc, DescDeleter{});
    }

    CHAM_desc_t *get()
    {
        return _descriptor.get();
    }

public:
    void *get_blk_addr(int i, int j) const
    {
        return _tiles.get_blk_addr(i, j);
    }

    int get_blk_rankof(int i, int j) const
    {
        return _tiles.get_rank_of(i, j);
    }

    int get_blk_ld(int i) const
    {
        return _tiles.get_blk_ld(i);
    }

}; // end class ChamDesc_p

/**
 * \brief Wrapper for CHAM_desc_t object
 *
 * This object has the semantic as a smart(reference counting)
 * pointer and can be copied safely
 */
template<class U>
class ChamDesc
{
private:
    std::shared_ptr<ChamDesc_p<U>> _pimpl;
private:
    ChamDesc(const DataChameleonI &tiles, int mt, int nt):
        _pimpl{new ChamDesc_p<U>{tiles, mt, nt}}
    {
    }

    ChamDesc(const ChamDesc& o, int it, int jt, int mt, int nt):
        _pimpl{new ChamDesc_p<U>{*o._pimpl.get(), it, jt, mt, nt}}
    {
    }

public:
    static ChamDesc create_parent_descriptor(const DataChameleonI &tiles, int mt, int nt)
    {
        return ChamDesc{tiles, mt, nt};
    }

    static ChamDesc create_sub_descriptor(const ChamDesc& o, int it, int jt, int mt, int nt)
    {
        return ChamDesc{o, it, jt, mt, nt};
    }

    CHAM_desc_t *get()
    {
        return _pimpl->get();
    }

    CHAM_desc_t *operator*()
    {
        return get();
    }

}; // end class ChamDesc

} // end namespace fabulous

#endif // FABULOUS_CHAMELEON_DESC_HPP
