#ifndef FABULOUS_BLOCK_PW_HPP
#define FABULOUS_BLOCK_PW_HPP

namespace fabulous {
template<class> class BlockPW;
}

#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/flops.hpp"
#include "fabulous/data/Block.hpp"

namespace fabulous {

/**
 * \brief Represent a block of datas containing P and W concatenated
 *
 * This use a cursor to know where P ends and W starts is needed.
 */
template<class Vector>
class BlockPW
{
public:
    using vector_type = Vector;
    using value_type   = typename Vector::value_type;
    using primary_type = typename Arithmetik<value_type>::primary_type;

    Vector _data;
    Vector _P;
    Vector _W;
    int _nbRHS;
    int _cursor; /*!< number of vector in P part */

    using S = value_type;
    using P = primary_type;

    BlockPW(const vector_type &ref, int nbRHS):
        _data{ref, nbRHS},
        _P{_data.sub_vector(0, 0)},
        _W{_data.sub_vector(0, nbRHS)},
        _nbRHS{nbRHS},
        _cursor{0}
    {
    }

    /**
     * \brief W part (candidate for base expansion)
     */
    vector_type& get_W()
    {
        return _W;
    }

    /**
     * \brief P part (candidate part that take into account discarded directions)
     */
    vector_type& get_P()
    {
        return _P;
    }

    void set_size_P(int size)
    {
        _cursor = size;
        _P = _data.sub_vector(0, _cursor);
        _W = _data.sub_vector(_cursor, _nbRHS-_cursor);
    }

    vector_type concat_PW()
    {
        vector_type PW{_data, _nbRHS};
        PW.copy(_data);
        return PW;
    }

    /**
     * \brief Check othogonality between P and W
     */
    void check_ortho_PW(const std::string &name="PW")
    {
        if (!_cursor) {
            return;
        }
        const vector_type P = get_P();
        const vector_type W = get_W();
        const int M = P.get_nb_col();
        const int N = W.get_nb_col();

        Block<S> tmp{M, N};
        tmp = P.dot( W );

        auto MM = tmp.get_min_max_norm();
        FABULOUS_DEBUG("CHECK_ORTHO PW["<<Color::green<<name<<Color::reset<<"]: (B^H*B - I) "
                       <<"Min="<<MM.first<<" Max="<<MM.second);
    }

    int get_local_dim() const
    {
        return _data.get_local_dim();
    }

    vector_type sub_vector(int offset, int width) const
    {
        return _data.sub_vector(offset, width);
    }

    const typename vector_type::value_type* get_ptr() const
    {
        return _data.get_ptr();
    }

}; // end class BlockPW

template<class Vector,
         class = enable_if_t<is_optimizable_t<Vector>::value > >
BlockPW<Vector> make_PW(const Vector& ref, int nbRHS)
{
    return BlockPW<Vector>{ ref, nbRHS };
}



} // end namespace fabulous

#endif // FABULOUS_BLOCK_PW_HPP
