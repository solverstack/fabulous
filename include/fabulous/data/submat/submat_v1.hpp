#ifndef FABULOUS_CHAMELEON_DESC_HPP
#define FABULOUS_CHAMELEON_DESC_HPP

#include <memory>
#include <string>
#include <cassert>

#include <chameleon.h>

namespace fabulous {
namespace experimental {
template<class> class ChamDesc;
};
};

#include "fabulous/data/TileTable.hpp"
#include "fabulous/utils/Chameleon.hpp"

namespace fabulous {
namespace experimental {

template< class U > class desc_impl;

template< class U >
static void *fabulous_desc_get_blk_addr(const CHAM_desc_t *A, int m, int n)
{
    size_t mm = m + A->i / A->mb;
    size_t nn = n + A->j / A->nb;
    FABULOUS_DEBUG("mm="<< mm<<" m=" <<m<<" A->i="<<A->i<< " A->mb="<<A->mb);
    FABULOUS_DEBUG("nn="<< nn<<" n=" <<n<<" A->j="<<A->j<< " A->nb="<<A->nb);
    dump_descriptor(A);

    desc_impl<U> *desc = reinterpret_cast<desc_impl<U>*>(A->mat);
    return desc->get_blk_addr(mm, nn);
}

template< class U >
static int fabulous_desc_get_blk_ld(const CHAM_desc_t *D, int i)
{
    desc_impl<U> *desc = reinterpret_cast<desc_impl<U>*>(D->mat);
    return desc->get_blk_ld(i);
}

template< class U >
static int fabulous_desc_get_rankof(const CHAM_desc_t *D, int i, int j)
{
    desc_impl<U> *desc = reinterpret_cast<desc_impl<U>*>(D->mat);
    return desc->get_blk_rankof(i, j);
}

/* ********** */

// constant block size!
template<class U> class desc_impl {
private:
    friend class ChamDesc<U>;

    // parameter with user block size:
    int _it, _jt;  // block indices of beginning of submatrix
    int _mt, _nt;   // block size of submatrix

    int _block_size; // user block_size;
    int _tile_size;  // morse tile_size;
    int _tile_per_block;

    std::shared_ptr<TileTable<U>> _blocks; // dynarray of morse tile_size
    CHAM_desc_t *_desc;

    desc_impl(const desc_impl&) = delete;
    desc_impl(desc_impl&&) = delete;
    desc_impl& operator=(const desc_impl&) = delete;
    desc_impl& operator=(desc_impl&&) = delete;

    desc_impl(int mt, int nt, int block_size, bool zero_tile, std::string name):
        _it(0), _jt(0),
        _mt(mt), _nt(nt),
        _block_size(block_size),
        _tile_size(0),
        _desc(nullptr)
    {
        CHAMELEON_Get(CHAMELEON_TILE_SIZE, &_tile_size);

        if (_tile_size == 0 || block_size % _tile_size != 0) {
            assert( _block_size < _tile_size );
            _tile_size = _block_size;
        }
        CHAMELEON_Set(CHAMELEON_TILE_SIZE, _tile_size);
        FABULOUS_DEBUG("rootdesc it="<<_it<<" jt="<<_jt<< " mt="<<_mt<< " nt="<<_nt);

        int lm = _mt * block_size;
        int ln = _nt * block_size;
        int ldt = lm / _tile_size;
        assert( lm % _tile_size == 0 );

        _tile_per_block = _block_size / _tile_size;
        _blocks.reset(new TileTable<U>{ldt, _tile_size, zero_tile, name});

        int err = CHAMELEON_Desc_Create_User(
            &_desc, this, Arithmetik<U>::morse_type,
            _tile_size, _tile_size, _tile_size*_tile_size,
            lm, ln, // whole matrix
            0, 0,   // submatrix start
            lm, ln, // submatrix size
            1, 1,
            fabulous_desc_get_blk_addr<U>,
            fabulous_desc_get_blk_ld<U>,
            fabulous_desc_get_rankof<U>
        );
        assert( err == CHAMELEON_SUCCESS );

        std::cout<<"Block_size: "<<_block_size<<"\n";
        std::cout<<"Tile_size: "<<_tile_size<<"\n";
        std::cout<<"Tile_per_block: "<<_tile_per_block<<"\n";
    }

    // create submatrix
    desc_impl(const desc_impl &o, int it, int jt, int mt, int nt):
        _it(o._it+it), _jt(o._jt+jt),
        _mt(mt), _nt(nt),
        _block_size(o._block_size), // nbRHS (multiple of tile_size or == tile_size)
        _tile_size(o._tile_size),   // morse tile size
        _tile_per_block(o._tile_per_block),
        _blocks(o._blocks), // same blocks  (copy the ptr; not the whole table !!)
        _desc(nullptr)
    {
        assert( o._it == 0 );
        assert( o._jt == 0 );
        assert( o._it + it <= o._mt );
        assert( o._jt + jt <= o._nt );
        FABULOUS_DEBUG("subdesc it="<<_it<<" jt="<<_jt<< " mt="<<_mt<< " nt="<<_nt);

        _desc = morse_desc_submatrix(o._desc,
                                     _it*_block_size, _jt*_block_size,
                                     _mt*_block_size, _nt*_block_size);

    }

    CHAM_desc_t *get()
    {
        return _desc;
    }

public:

    ~desc_impl() // public for shared_ptr
    {
        //FABULOUS_DEBUG("call descriptor destroy");
        if (_desc)
            CHAMELEON_Desc_Destroy(&_desc);
    }

    void *get_blk_addr(int m, int n)
    {
        FABULOUS_DEBUG("m="<<m<<" n="<<n);
        return _blocks->get_blk_addr(m, n);
    }

    int get_blk_rankof(int, int) const
    {
        return 0;
    }

    int get_blk_ld(int) const
    {
        return _tile_size;
    }

};

/* ********** */

template< class U > class ChamDesc {
private:
    std::shared_ptr<desc_impl<U>> _pimpl;

public:
    ChamDesc() {}
    ChamDesc(int mt, int nt, int block_size, bool zero_tile, std::string name):
        _pimpl{new desc_impl<U>{mt, nt, block_size, zero_tile, name}}
    {
    }

    ChamDesc(ChamDesc &o, int mt, int nt, int it, int jt):
        _pimpl{new desc_impl<U>{*o._pimpl, mt, nt, it, jt}}
    {
    }

    inline CHAM_desc_t *get() { return _pimpl->get(); }
    inline const CHAM_desc_t *get() const { return _pimpl->get(); }
    inline operator CHAM_desc_t*(){ return get(); }
    inline operator const CHAM_desc_t*(){ return get(); }
};


} // end namespace experimental
} // end namespace fabulous

#endif // FABULOUS_CHAMELEON_DESC_HPP
