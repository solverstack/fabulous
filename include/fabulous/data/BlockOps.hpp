#ifndef FABULOUS_BLOCK_OPS_H
#define FABULOUS_BLOCK_OPS_H

#include <vector>
#include <complex>

#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/kernel/svd.hpp"

namespace fabulous {


/*! \brief Wrapper for the other decomposition_svd() method  */
template<class S, class P>
int decomposition_svd(Block<S> &B, Block<S> &U1,
                      const std::vector<P> &epsilon,
                      const std::vector<P> &inv_epsilon,
                      const int computational_blocking = 0)
{
    if ( epsilon.size() == 1 ) {
        return decomposition_svd(B, U1, epsilon.front(), computational_blocking);
    } else {
        B.cwise_scale(inv_epsilon);
        return decomposition_svd(B, U1, P{1.0}, computational_blocking);
    }
}

/*!
 * \brief Compute SVD of Block B
 *
 *  \f$ M = \mathbb{U}_1 \Sigma_1 \mathbb{V}_1 + \mathbb{U}_2 \Sigma \mathbb{V}_2 \f$
 *
 * \param[in,out] B the block to be decomposed
 * \param[out] U1 vectors associated with singular values
 *     superior to threshold.
 *
 * \param threshold minimum norm of kept singular values
 *
 * \return count of singular values greater(in module)
 *     than the epsilon threshold
 */
template<class S, class P>
int decomposition_svd(Block<S> &B, Block<S> &U1, P threshold, const int computational_blocking = 0)
{
    std::vector<P> sigma, superb;

    const int M = B.get_nb_row();
    const int N = B.get_nb_col();

    sigma.resize(N);
    superb.resize(N-1);

    Block<S> U{M, N};
    Block<S> V{};

    const int err = lapacke::gesvd(
        'S', /* only first _n vectors computes */
        'N', /* V not computed */
        M, N,
        B.get_ptr(), B.get_leading_dim(),
        sigma.data(),
        U.get_ptr(), U.get_leading_dim(),
        V.get_ptr(), V.get_leading_dim(),
        superb.data()
    );
    if (err != 0) {
        if (err > 0) {
            for (int i=0; i < err; ++i)
                std::cout << superb[i] << "\t";
            std::cout<<"\n";
        }
        FABULOUS_THROW(Kernel, "SVD kernel err="<<err);
    }

    // Count the number of Singular Value superior to threshold

    int nb_krylov_direction = 0;
    while (nb_krylov_direction < N && sigma[nb_krylov_direction] > threshold) {
        ++ nb_krylov_direction;
    }

    if(computational_blocking == 0){ // Deflauft behavior, U1 is the whole U and return the number of columns
        U1 = U.copy();
        return nb_krylov_direction;
    }

    if(computational_blocking > 0){
        nb_krylov_direction = std::min(computational_blocking, nb_krylov_direction);
    }

    // Set U1 to the corect size and value
    U1 = U.sub_block(0, 0, U.get_nb_row(), nb_krylov_direction);

    return nb_krylov_direction;
}


/*! \brief short lived temporaries types to enable nice operation syntax */
namespace block_ops {

template<class S>
struct Mul
{
    S _alpha;
    const Block<S> _a;
    const Block<S> _b;

    Mul(const Block<S> &a, const Block<S> &b, S alpha=S{1.0}):
        _alpha{alpha}, _a{a}, _b{b}
    {
    }

    inline void eval(Block<S> &c, S beta=S{0.0}) const
    {
        FABULOUS_ASSERT( _a.get_nb_col() == _b.get_nb_row() );
        FABULOUS_ASSERT( _a.get_nb_row() == c.get_nb_row() );
        FABULOUS_ASSERT( _b.get_nb_col() == c.get_nb_col() );

        FABULOUS_ASSERT( _a.get_ptr() != c.get_ptr() );
        FABULOUS_ASSERT( _b.get_ptr() != c.get_ptr() );

        lapacke::gemm(c.get_nb_row(), c.get_nb_col(), _a.get_nb_col(),
                      _a.get_ptr(), _a.get_leading_dim(),
                      _b.get_ptr(), _b.get_leading_dim(),
                      c.get_ptr(), c.get_leading_dim(),
                      _alpha, beta
        );
    }
};

template<class S>
struct DotProduct
{
    const Block<S> &a;
    const Block<S> &b;
    operator S() const
    {
        S val = S{0.0};
        Block<S> c{1, 1, 1, &val};
        c = a.dot(b);
        return val;
    }
};

template<class S>
struct Scal
{
    const Block<S> &_a;
    S _alpha;

    Scal(const Block<S> &a, S alpha):
        _a{a}, _alpha{alpha}
    {
    }
};

template<class S>
struct Trans
{
    const Block<S> &_a;

    Trans(const Block<S> &a):
        _a{a}
    {
    }

    inline void eval(Block<S> &c) const
    {
        const int M = c.get_nb_row();
        const int N = c.get_nb_col();
        FABULOUS_ASSERT( _a.get_nb_row() == N && _a.get_nb_col() == M );
        FABULOUS_ASSERT( _a.get_ptr() != c.get_ptr() );

        for (int j = 0; j < N; ++j) {
            for (int i = 0; i < M; ++i) {
                c(i, j) = fabulous::conj(_a(j, i));
            }
        }
    }
};

} // end namespace block_ops


template<class S>
inline block_ops::Trans<S> trans(const Block<S> &a)
{
    return block_ops::Trans<S>{a};
}

template<class S>
inline block_ops::Mul<S> operator*(const Block<S> &a, const Block<S> &b)
{
    return block_ops::Mul<S>{a, b};
}

template<class S>
inline block_ops::Scal<S> operator*(const Block<S> &a, S b)
{
    return block_ops::Scal<S>{a, b};
}

template<class S>
inline block_ops::Scal<S>
operator*(S b, const Block<S> &a)
{
    return block_ops::Scal<S>{a, b};
}

template<class S>
inline block_ops::Scal<S>
operator*(S b, block_ops::Scal<S> a)
{
    a._alpha *= b;
    return a;
}

template<class S>
inline block_ops::Mul<S>
operator*(block_ops::Scal<S> a, const Block<S> &b)
{
    return block_ops::Mul<S>{a._a, b, a._alpha};
}

template<class S>
inline block_ops::Scal<S>
operator-(const Block<S> &a)
{
    return block_ops::Scal<S>{a, S{-1.0}};
}

/*

 Overloadable operators:

 =               (binary,  affectation)
 += -= *= /=     (binary,  affectation + operation)
 %= ^=           (binary,  affectation + operation)
 &= |=           (binary,  affectation + operation)
 >>= <<=         (binary,  affectation + operation)
 []              (binary,  array member access)
 ()              (n-ary,   function call)

 <  >            (binary,  comparison)
 <= >=           (binary,  comparison)
 != ==           (binary,  equality)
 +  -  * / %     (binary,  operation)
 << >>           (binary,  shift)
 || &&           (binary,  logical and, logical or)
 |  &            (binary,  bit-wise logical or, and)
 !               (unary,   logical not)
 ^               (binary,  bitwise xor)
 ~               (unary,   binary complement)
 *               (unary,   dereference)
 ->              (unary,   dereference+member access)
 ->*             (unary,   dereference+pointer to member access)
 &               (unary,   take address of)
 +  -            (unary,   sign)
 ++ --           (unary,   increment / decrement)
 ,               (binary, comma operator (sequencing))
 new             (allocation)
 new[]
 delete          (deallocation)
 delete[]
 ""              (user literals)


 Not overloadable operators:
 ? :          (ternary, conditional, (a.k.a el constinator ))
 ::           (scope resolution)
 .*           (member access through pointer to member)
 .            (member access)

 */

} // end namespace fabulous

#endif // FABULOUS_BLOCK_OPS_H
