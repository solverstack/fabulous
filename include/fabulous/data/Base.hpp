#ifndef FABULOUS_BASE_HPP
#define FABULOUS_BASE_HPP

namespace fabulous {
template<class> class Base;
}

#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/data/Block.hpp"

namespace fabulous {

/**
 * \brief This class store the base of the spanned krylov space.
 */
template<class Vector>
class Base
{
public:
    using vector_type  = Vector;
    using value_type   = typename Vector::value_type;
    using primary_type = typename Arithmetik<value_type>::primary_type;

private:
    using S = value_type;
    using P = primary_type;

private:
    const int _max_krylov_space_size; /*!< maximum number of vector in base */
    int _nb_block; /*!< number of block currently in base */
    int _nb_vect; /*!< number of vector currently in base */

    vector_type _data; /*!< buffer for base vector */
    std::vector<int> _block_sizes; // (block size may change)
    std::vector<int> _block_sizes_sum;
    std::vector<vector_type> _blocks; /*!< array of blocks */

public:
    /**
     * \param[in] ref an already existing vector needed to construct a new vector
     * \param[in] max_krylov_space_size maximum size of search space allowed
     *          (number of column allocated)

     */
    Base(const Vector &ref = Vector{}, int max_krylov_space_size = 0):
        _max_krylov_space_size{max_krylov_space_size},
        _nb_block{0},
        _nb_vect{0},
        _data{ref, _max_krylov_space_size, "base"},
        _block_sizes{},
        _block_sizes_sum{},
        _blocks{}
    {
        _block_sizes_sum.emplace_back(0);
    }

    /**
     * \brief reset all base counters
     */
    void reset()
    {
        _nb_block = 0;
        _nb_vect = 0;
        _blocks.clear();
        _block_sizes.clear();
        _block_sizes_sum.clear();
        _block_sizes_sum.emplace_back(0);
    }

    int get_nb_block() const { return _nb_block; }
    int get_nb_vect() const { return _nb_vect; }
    int get_nb_vect(int nb_block) const { return _block_sizes_sum[nb_block]; }
    int get_block_size(int k) const { return _block_sizes[k]; }

    int get_local_dim() const { return _data.get_local_dim(); }
    const vector_type& get_ref() { return _data; }
    vector_type sub_vector(int offset, int width) const
    {
        return _data.sub_vector(offset, width);
    }

    vector_type get_W(int block_size)
    {
        return _data.sub_vector(_nb_vect, block_size);
    }

    const vector_type &get_Vj()
    {
        return _blocks.back();
    }

    void check_ortho(std::string name="Base")
    {
        sub_vector(_data, 0, _nb_vect).check_ortho(name);
    }

private:
    void increase(int block_size)
    {
        _blocks.push_back( get_W(block_size) );

        ++ _nb_block;
        _nb_vect += block_size;
        _block_sizes.push_back(block_size);
        _block_sizes_sum.push_back(_nb_vect);
    }

public:
    void add_vector(vector_type &v)
    {
        vector_type w = get_W( v.get_nb_col() );
        if ( v.get_ptr() != w.get_ptr() ) {
            w.copy(v);
            FABULOUS_DEBUG("DEBUG-WARNING: inserting vector in base by copy");
        }
        increase( v.get_nb_col() );
    }

    const vector_type& operator[](int i) const
    {
        FABULOUS_ASSERT( i < _nb_block );
        return _blocks[i];
    }

}; // end class Base

template<class Vector,
         class = enable_if_t<is_optimizable_t<Vector>::value > >
auto make_base(const Vector &v, int max_krylov_space_size)
{
    FABULOUS_DEBUG("making INCORE optimized base ");
    return Base<Vector>{ v, max_krylov_space_size };
}

template<class Vector, typename S>
void BASE_AXPY(Vector &VY_output, const Base<Vector> &base_V, const Block<S> &Y, S alpha = S{1.0})
{
    Vector baseBlock = base_V.sub_vector(0, Y.get_nb_row());
    VY_output.axpy(baseBlock, Y, alpha);
}

template<class Vector, typename S>
void BASE_DOT(const Base<Vector> &base_V, const Vector &W, Block<S> &H)
{
    Vector baseBlock = base_V.sub_vector(0, H.get_nb_row());
    baseBlock.dot(W, H);
}

} // end namespace fabulous

#endif // FABULOUS_BASE_HPP
