#ifndef FABULOUS_BLOCK_HPP
#define FABULOUS_BLOCK_HPP

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>

namespace fabulous {
template<class> class Block;
}


#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Color.hpp"
#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Timer.hpp"

#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/svd.hpp"
#include "fabulous/kernel/flops.hpp"

#include "fabulous/data/BlockOps.hpp"

namespace fabulous {

template<class S>
void default_block_dot(const Block<S> &v1, const Block<S> &v2, Block<S> &c)
{
    lapacke::Tgemm(c.get_nb_row(), c.get_nb_col(), v1.get_nb_row(),
                   v1.get_ptr(), v1.get_leading_dim(),
                   v2.get_ptr(), v2.get_leading_dim(),
                   c.get_ptr(), c.get_leading_dim(),
                   S{1.0}, S{0.0});
}

template<class S>
void gemm(const Block<S> &v1, const Block<S> &v2, Block<S> &c, const S a = S{1.0}, const S b = S{0.0})
{
    lapacke::gemm(c.get_nb_row(), c.get_nb_col(), v1.get_nb_col(),
                   v1.get_ptr(), v1.get_leading_dim(),
                   v2.get_ptr(), v2.get_leading_dim(),
                   c.get_ptr(), c.get_leading_dim(),
                   a, b);
}

template<class S>
void block_dot_add(const Block<S> &v1, const Block<S> &v2, Block<S> &c)
{
    lapacke::Tgemm(c.get_nb_row(), c.get_nb_col(), v1.get_nb_row(),
                   v1.get_ptr(), v1.get_leading_dim(),
                   v2.get_ptr(), v2.get_leading_dim(),
                   c.get_ptr(), c.get_leading_dim(),
                   S{1.0}, S{1.0});
}

/*!
 * \brief Matrix storage: Lapack/FORTRAN style (Column major layout)
 * 2D array of scalars
 *
 * block objects have pointer semantics:
 * multiple block object can refer to the same block or to part
 * (subblocks) of the sameblocks
 */
template<class S>
class Block
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;

private:
    using P = primary_type;
    friend class MatrixIWLoader;
    friend class MatrixMarketLoader;
    typedef std::shared_ptr<S> SPtr;

protected:
    int _m; /*!< number of lines */
    int _n; /*!< number of columns */
    int _ld; /*!< leading dimension of array */

private:
    SPtr _sptr; /*!< shared ptr, memory deleter for block data */

    FABULOUS_DISABLE_INVALID_ARITHMETIC(S);

    void resize(int m, int n, int /*nnz*/)
    {
        auto dot = _dot;
        *this = Block<S>{m, n};
        _dot = dot;
    }


protected:
    S *_ptr; /*!< pointer to block data */

private:
    std::function<void(const Block&,const Block&,Block&)> _dot;

public:

    explicit Block(const Block &o, int nb_col, const std::string &name="vec"):
        Block{o.get_nb_row(), nb_col, name}
    {
    }

    explicit Block(int m, int n, const std::string &name = "tmp"):
        _m{m}, _n{n},
        _ld{std::max(_m, 1)},
        _ptr{nullptr},
        _dot( default_block_dot<S> )
    {
        size_t bufsize = ((_m * _n) != 0) ? (_n * _ld) : (0UL);

#if ! FABULOUS_MEMORY_LOGGING
        (void) name;
#endif

        if (bufsize != 0) {
            FABULOUS_LOG_ALLOC(name, m, n, sizeof(S));
        }
        _sptr.reset(new S[bufsize], [=](auto v) {
                if (bufsize != 0) {
                    FABULOUS_LOG_FREE(name, m, n, sizeof(S));
                }
                std::default_delete<S[]>{}(v);
            }
        );
        _ptr = _sptr.get();
        std::fill(_ptr, _ptr+bufsize, S{0.0});

        FABULOUS_ASSERT( _ld >= _m );
        FABULOUS_ASSERT( (_m >= 0) && (_n >= 0) );
        FABULOUS_ASSERT( _ld > 0 );
    }

    explicit Block():
        Block(0, 0)
    {
    }

    explicit Block(int m, int n, int ld, S *ptr):
        _m{m}, _n{n}, _ld{ld},
        _ptr{ptr},
        _dot( default_block_dot<S> )
    {
        FABULOUS_ASSERT( _ptr != nullptr );
        FABULOUS_ASSERT( _ld >= _m );
        FABULOUS_ASSERT( (_m >= 0) && (_n >= 0) );
        FABULOUS_ASSERT( _ld > 0 );
    }

    explicit Block(int m, int n, int ld, S* ptr, const SPtr &sptr):
        _m{m}, _n{n}, _ld{ld},
        _sptr{sptr}, _ptr{ptr},
        _dot( default_block_dot<S> )
    {
        FABULOUS_ASSERT( _ptr != nullptr );
        FABULOUS_ASSERT( _ld >= _m );
        FABULOUS_ASSERT( (_m >= 0) && (_n >= 0) );
        FABULOUS_ASSERT( _ld > 0 );
    }

    explicit Block(int m, int n, int ld, const SPtr &sptr):
        Block{m, n, ld, sptr.get(), sptr}
    {
    }


    /*!
     *  \brief create a block describing of sub_block of current block
     *
     * No copy is performed; The new block will reference the same
     * subblock from the current block
     *
     *  \param i starting line of subblock
     *  \param j starting column of subblock
     *  \param m number of line in subblock
     *  \param n number of column in subblock
     */
    Block sub_block(int i, int j, int m, int n)
    {
        FABULOUS_ASSERT( i >= 0 );
        FABULOUS_ASSERT( j >= 0 );
        FABULOUS_ASSERT( m >= 0 );
        FABULOUS_ASSERT( n >= 0 );
        FABULOUS_ASSERT( (i+m) <= _m );
        FABULOUS_ASSERT( (j+n) <= _n );
        return Block{m, n, _ld, get_ptr(i, j), _sptr};
    }

    const Block sub_block(int i, int j, int m, int n) const
    {
        FABULOUS_ASSERT( (i >= 0) && (j >= 0) );
        FABULOUS_ASSERT( (m >= 0) && (n >= 0) );
        FABULOUS_ASSERT( ((i+m) <= _m) && ((j+n) <= _n) );
        return Block{m, n, _ld, (S*)get_ptr(i, j), _sptr};
    }

    inline int get_local_dim() const  { return get_nb_row(); }
    inline int get_nb_row() const { return _m; }
    inline int get_nb_col() const { return _n; }
    inline int get_leading_dim() const { return _ld; }


    inline S *get_ptr() { return _ptr; }
    inline const S *get_ptr() const { return _ptr; }

    inline S *get_ptr(int i, int j) { return _ptr + j*_ld + i; }
    inline const S *get_ptr(int i, int j) const { return _ptr + j*_ld + i; }

    inline S *get_vect(int j = 0) { return get_ptr(0, j); }
    inline const S *get_vect(int j = 0) const { return get_ptr(0, j); }

    inline Block<S> get_bvect(int j) { return sub_block(0, j, _m, 1); }
    inline const Block<S> get_bvect(int j) const { return sub_block(0, j, _m, 1); }

    inline S &at(int i = 0, int j = 0) { return _ptr[j*_ld + i]; }
    inline const S &at(int i = 0, int j = 0) const { return _ptr[j*_ld + i]; }
    inline S &operator()(int i, int j=0) { return at(i, j); }
    inline const S &operator()(int i, int j=0) const { return at(i, j); }

    /*! \brief fill the block with zeros */
    void zero()
    {
        for (int j = 0; j < _n; ++j)
            std::fill(get_ptr(0, j), get_ptr(0, j)+_m, S{0.0});
    }

    /*! \brief copy the Block given as a parameter into 'this' Block */
    void copy(const Block &o)
    {
        int M = std::min(_m, o._m);
        for (int j = 0; j < std::min(_n, o._n); ++j)
            std::copy(o.get_vect(j), o.get_vect(j)+M, get_vect(j));
    }

    /*! \brief return a deep copy of the block (memory is duplicated) */
    Block copy() const
    {
        Block cpy{_m, _n};
        cpy.copy(*this);
        return cpy;
    }

    /*! \brief pretty print the block values in a tabular format */
    void display(const std::string &name ="", std::ostream &o = std::cout) const
    {
        if (name != "")
            o << name <<":\n";
        for (int i = 0; i < _m; ++i) {
            for (int j = 0; j < _n; ++j)
                o << at(i, j) << " ";
            o << "\n";
        }
        o << "\n";
    }

    void display_bitmap(const std::string &name ="", const double esp = 1e-15) const
    {
        std::ostream &o = std::cout;
        if (name != "")
            o << name <<":\n";
        for (int i = 0; i < _m; ++i) {
            for (int j = 0; j < _n; ++j)
                (std::norm(at(i, j)) > esp) ? o << ". " : o << "  ";
            o << "\n";
        }
        o << "\n";
    }

    /*! \brief print information about the Block for debug purposes */
    void debug(const std::string &name="", std::ostream &o = std::cerr) const
    {
        if (name != "")
            o << name <<":\n";
        o << "M="<<_m<<"\n";
        o << "N="<<_n<<"\n";
        o << "LD="<<_ld<<"\n";
        o << "\n";
    }

    /*! \brief check orthogonality of the block columns */
    void check_ortho(const std::string &name = "B") const
    {
        Block A{_n, _n};
        lapacke::Tgemm(_n, _n, _m,
                       get_ptr(), get_leading_dim(),
                       get_ptr(), get_leading_dim(),
                       A.get_ptr(), A.get_leading_dim(),
                       S{1.0}, S{0.0});
        for (int k = 0; k < _n; ++k)
            A.at(k, k) -= S{1.0};
        auto MM = min_max_norm(A);
        FABULOUS_DEBUG(
            "CHECK_ORTHO["<<Color::green<<name<<Color::reset<<"]: (B^H*B - I) "
            <<"Min="<<MM.first<<" Max="<<MM.second
        );
    }

    /*! check that the block is filled with zeros */
    bool check_null() const
    {
        for (int j = 0; j < _n; ++j) {
            for (int i = 0; i < _m; ++i) {
                FABULOUS_ASSERT( at(i, j) == S{0.0} );
                if ( at(i, j) == S{0.0} )
                    return false;
            }
        }
        FABULOUS_DEBUG("check_null OK");
        return true;
    }

    bool check_nan() const
    {
        for (int j = 0; j < _n; ++j) {
            for (int i = 0; i < _m; ++i) {
                FABULOUS_ASSERT( not fabulous::isnan(at(i, j)) );
                if (fabulous::isnan(at(i,j)))
                    return false;
            }
        }
        return true;
    }


    /*! \brief return norm of k-th vector inside the Block */
    P get_norm(int k) const
    {
        const S *ptr = get_vect(k);
        S res = S{0.0};
        lapacke::dot(_m, ptr, 1, ptr, 1, &res);
        return std::sqrt(fabulous::real(res));
    }

    P norm() const
    {
        FABULOUS_ASSERT( get_nb_col() == 1 );
        return get_norm(0);
    }

public:

    void dot(const Block &v, Block &c) const
    {
        FABULOUS_ASSERT( this->get_nb_row() == v.get_nb_row() );
        FABULOUS_ASSERT( this->get_nb_col() == c.get_nb_row() );
        FABULOUS_ASSERT( v.get_nb_col() == c.get_nb_col() );
        FABULOUS_ASSERT( this->get_ptr() != c.get_ptr() );
        FABULOUS_ASSERT( v.get_ptr() != c.get_ptr() );

        FABULOUS_ASSERT( c.get_leading_dim() >= c.get_nb_row() );
        FABULOUS_ASSERT( this->get_leading_dim() >= c.get_nb_row() );
        FABULOUS_ASSERT( this->get_leading_dim() >= this->get_nb_row() );
        FABULOUS_ASSERT( v.get_leading_dim() >= v.get_nb_row() );

        _dot( *this, v, c );
    }

    void dot(const Block &v, S &c) const
    {
        Block tmp{1, 1, 1, &c};
        dot(v, tmp);
    }

    void axpy(const Block &b, const Block &s, S alpha=S{1.0})
    {
        *this += alpha*b*s;
    }

    void axpy(const Block &b, S &s, S alpha=S{1.0})
    {
        *this += alpha*(b*s);
    }

    void cwise_axpy(const Block &b, S alpha=S{1.0})
    {
        *this += b*alpha;
    }

    std::vector<P> cwise_norm() const
    {
        const int N = get_nb_col();
        std::vector<P> norms(N);
        for (int j = 0; j < N; ++j) {
            norms[j] = get_norm(j);
        }
        return norms;
    }

    /*! \brief scale vectors in the Block with given coefficients */
    void cwise_scale(const std::vector<P> &coef)
    {
        const int M = get_nb_row();
        const int N = get_nb_col();
        FABULOUS_ASSERT( coef.size() >= (unsigned) N );

        for (int j = 0; j < N; ++j) {
            if (fabulous::isnan(coef[j])) {
                FABULOUS_THROW(Numeric, "encountered nan");
            }
            lapacke::scal(M, S{coef[j]}, get_vect(j), 1);
        }
    }


    template<class V, class = enable_if_t<Arithmetik<V>::value>>
    void scale(const V alpha)
    {
        const int M = get_nb_row();
        const int N = get_nb_col();

        if (fabulous::isnan(alpha)) {
            FABULOUS_THROW(Numeric, "encountered nan");
        }
        for (int j = 0; j < N; ++j) {
            lapacke::scal(M, S{alpha}, get_vect(j), 1);
        }
    }

    void qr(Block<S> &q, Block<S> &r)
    {
        if ( q.get_ptr() != get_ptr() ) {
            q.copy(*this);
        }
        ::fabulous::qr::InPlaceQRFacto(q, r);
    }

    /*! \brief Compute this * r^{-1} = q */
    void trsm(Block<S> &q, Block<S> &r)
    {
        if ( q.get_ptr() != get_ptr() ) {
            q.copy(*this);
        }
        int err = lapacke::trsm('R', 'U', 'N', 'N', q.get_nb_row(), r.get_nb_col(), S{1.0},
                                r.get_ptr(), r.get_leading_dim(),
                                q.get_ptr(), q.get_leading_dim());
        FABULOUS_ASSERT(err == 0);
    }

    Block sub_vector(int offset, int width) const
    {
        return sub_block(0, offset, get_nb_row(), width);
    }

private:
    void add(const Block<S> &B, S alpha=S{1.0})
    {
        const int M = B.get_nb_row();
        const int N = B.get_nb_col();
        FABULOUS_ASSERT( get_nb_row() == M );
        FABULOUS_ASSERT( get_nb_col() == N );

        if ( fabulous::isnan(alpha) ) {
            FABULOUS_THROW(Numeric, "encountered nan");
        }
        for (int j = 0; j < N; ++j) {
            lapacke::axpy(M, S{alpha}, B.get_vect(j), 1, get_vect(j), 1);
        }
    }

public /* operators */:

    template<class P, class = enable_if_t<Arithmetik<P>::value>>
    Block& operator*=(P s)
    {
        this->scale(s);
        return *this;
    }

    Block& operator+=(Block<S> &o)
    {
        this->add(o);
        return *this;
    }

    Block& operator-=(Block<S> &o)
    {
        this->add(o, S{-1.0});
        return *this;
    }

    Block& operator+=(block_ops::Scal<S> rhs)
    {
        this->add(rhs._a, rhs._alpha);
        return *this;
    }

    Block& operator-=(block_ops::Scal<S> rhs)
    {
        this->add(rhs._a, -rhs._alpha);
        return *this;
    }

    Block& operator=(block_ops::Mul<S> rhs)
    {
        rhs.eval(*this, S{0.0});
        return *this;
    }

    Block& operator+=(block_ops::Mul<S> rhs)
    {
        rhs.eval(*this, S{1.0});
        return *this;
    }

    Block& operator-=(block_ops::Mul<S> rhs)
    {
        rhs._alpha = -rhs._alpha;
        rhs.eval(*this, S{1.0});
        return *this;
    }

    Block& operator=(block_ops::Trans<S> rhs)
    {
        rhs.eval(*this);
        return *this;
    }

    /****************************/
    /** DISTRIBUTED facilities **/
    /** MUST NOT to used by fabulous core **/
    /** it is meant to be helper functions for user implementations **/

    /*! \brief return norm of k-th vector inside the Block */
    template<class Dot>
    P norm(int k, const Dot &dot) const
    {
        const S *ptr = get_vect(k);
        S res = S{0.0};
        dot(1, 1, ptr, _ld, ptr, _ld, &res, 1);
        return std::sqrt(fabulous::real(res));
    }

 }; // end class Block

template<class U>
std::ostream& operator<<(std::ostream &o, const Block<U> &b)
{
    b.display("", o);
    return o;
}

// 2 functions needed for RUHE variant
template<class S>
inline auto snorm(const Block<S> &a) -> typename Block<S>::primary_type
{
    S buf = S{0.0};
    Block<S> block{1, 1, 1, &buf};
    FABULOUS_ASSERT( a.get_nb_col() == 1 );
    a.dot(a, block);
    return std::sqrt(fabulous::real(buf));
}

template<class S, class P>
void sscale(Block<S> &a, P val)
{
    a.scale(val);
}


} // end namespace fabulous

#endif // FABULOUS_BLOCK_HPP
