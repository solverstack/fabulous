#ifndef FABULOUS_TILE_MAPPER_HPP
#define FABULOUS_TILE_MAPPER_HPP

#include <utility>
#include <memory>
#include <unordered_map>
#include <vector>
#include <cassert>

#include <pthread.h>
#include <chameleon.h>

namespace fabulous {
template<class> class HessenbergTileMapper;
template<class> class ClassicTileMapper;
template<class> class LapackTileMapper;
}

#include "fabulous/utils/Error.hpp"
#include "fabulous/data/Block.hpp"

namespace fabulous {

inline int compute_inner_tile_size(const int nb)
{
    int tile_size;
    CHAMELEON_Get(CHAMELEON_TILE_SIZE, &tile_size);
    FABULOUS_ASSERT( tile_size != 0 );
    int new_tile_size = tile_size;

    if (nb % tile_size != 0) {
        if ( nb > tile_size ) {
            FABULOUS_THROW(
                Parameter,
                "nbRHS must be either lower than CHAMELEON tile size,"
                " or a multiple of CHAMELEON tile size!"
            );
        }
        FABULOUS_ASSERT( nb < tile_size );
        new_tile_size = nb;
    }

    if (new_tile_size != tile_size) {
        tile_size = new_tile_size;
        CHAMELEON_Set(CHAMELEON_TILE_SIZE, new_tile_size);
    }
    return tile_size;
}

/*! \brief DataInterface for ChamDesc objects (base class) */
class DataChameleonI
{
public:
    virtual void *get_blk_addr(int i, int j) const = 0;
    virtual int get_rank_of(int i, int j) const = 0;
    virtual int get_blk_ld(int i) const = 0;
    virtual int get_inner_tile_size() const = 0;
    virtual int get_block_size() const = 0;
    virtual ~DataChameleonI(){}
}; // end class DataChameleonI

/*!
 * \brief Tile container for ChamDesc Object ('HESSENBERG')
 *
 * Only the upper hessenbergs blocks are stored.
 *
 * The blocks are stocked continuously in memory and they are numbered this way:
 \verbatim
 | 0 2 5 |
 | 1 3 6 |
 |   4 7 |
 |     8 |
 \endverbatim
 *
 * It is easy to compute the number (z) (hence the address) of a block given
 * its (i,j) coordinate (starting from 0): <br/>
 * \f$ z = \frac{(j+1)(j+2)}{2} + i - 1 \f$
 *
 * Each block can have multiple inner tiles. <br/>
 * Each tile is fully identified by its container block's coordinates
 * and by its "inner coordinates". <br/> The inner coordinate systems is the same
 * as the ClassicTileMapper coordinates system (DPLASMA/Chameleon style).
 */
template<class S>
class HessenbergTileMapper : public DataChameleonI
{
private:
    const int _mt, _nt;
    const int _nb;
    const int _block_bsize;
    const int _nb_block;

    const int _inner_tile_size;
    const int _tile_bsize;
    const int _inner_block_tile_ratio;

    Block<S> _data;
public:

    HessenbergTileMapper(int mt, int nt, int nb):
        _mt{mt}, _nt{nt},
        _nb{nb},
        _block_bsize{nb*nb},
        _nb_block{ ((nt+2)*(nt+1))/2 - 1 },
        _inner_tile_size{compute_inner_tile_size(nb)},
        _tile_bsize{_inner_tile_size * _inner_tile_size},
        _inner_block_tile_ratio{nb / _inner_tile_size},
        _data{_block_bsize, _nb_block}
    {
        FABULOUS_ASSERT( mt == (nt+1) );
    }

    int get_inner_tile_size() const override
    {
        return _inner_tile_size;
    }

    int get_block_size() const override
    {
        return _nb;
    }

private:
    void *get_blk_addr(int i, int j) const override
    {
        int IBLOCK = i / _inner_block_tile_ratio;
        int itile  = i % _inner_block_tile_ratio;
        int JBLOCK = j / _inner_block_tile_ratio;
        int jtile  = j % _inner_block_tile_ratio;

        FABULOUS_ASSERT( JBLOCK < _nt );
        FABULOUS_ASSERT( IBLOCK <= JBLOCK + 1 /* hessenberg structure check */ );

        int block_id = (((JBLOCK+1)*(JBLOCK+2)) / 2 ) + IBLOCK-1;
        int tile_lid = jtile * _inner_block_tile_ratio + itile;
        return (void*) ( _data.get_vect(block_id) + tile_lid*_tile_bsize);
    }

    int get_blk_ld(int) const override
    {
        return _inner_tile_size;
    }

    int get_rank_of(int,int) const override
    {
        return 0;
    }

}; // end class HessenbergTileMapper


/**
 * \brief Tile container for ChamDesc Object ('Classic')
 *
 * All blocks are stored.
 *
 * The blocks are stocked continuously in memory and they are numbered in a
 * DPLASMA/Chameleon style (like LAPACK-style but tiled per block):
 \verbatim
 | 0 4  8 |
 | 1 5  9 |
 | 2 6 10 |
 | 3 7 11 |
 \endverbatim
 *
 * It is easy to compute the number (z) (hence the address) of a block given
 * its (i,j) coordinate: <br/>
 * \f$ z = j * LD + i \f$
 *
 * Each block can have multiple inner tiles. <br/>
 * Each tile is fully identified by its container block's coordinates
 * and by its "inner coordinates". <br/> The inner coordinate systems is the same
 * as the global coordinates system (DPLASMA/Chameleon style).
  */
template<class S>
class ClassicTileMapper : public DataChameleonI
{
private:
    const int _mt, _nt;
    const int _nb;
    const int _block_bsize;
    const int _nb_block;

    const int _inner_tile_size;
    const int _tile_bsize;
    const int _inner_block_tile_ratio;

    Block<S> _data;
public:

    ClassicTileMapper(int mt, int nt, int nb):
        _mt{mt}, _nt{nt},
        _nb{nb},
        _block_bsize{nb*nb},
        _nb_block{mt*nt},
        _inner_tile_size{compute_inner_tile_size(nb)},
        _tile_bsize{_inner_tile_size * _inner_tile_size},
        _inner_block_tile_ratio{nb / _inner_tile_size},
        _data{_block_bsize, _nb_block}
    {
    }

    int get_inner_tile_size() const override
    {
        return _inner_tile_size;
    }

    int get_block_size() const override
    {
        return _nb;
    }

private:
    void *get_blk_addr(int i, int j) const override
    {
        const int IBLOCK = i / _inner_block_tile_ratio;
        const int itile  = i % _inner_block_tile_ratio;
        const int JBLOCK = j / _inner_block_tile_ratio;
        const int jtile  = j % _inner_block_tile_ratio;

        FABULOUS_ASSERT( IBLOCK < _mt );
        FABULOUS_ASSERT( JBLOCK < _nt );

        const int block_id = JBLOCK * _mt + IBLOCK;
        const int tile_lid = jtile * _inner_block_tile_ratio + itile;

        return (void*) (_data.get_vect(block_id) + tile_lid*_tile_bsize);
    }

    int get_blk_ld(int) const override
    {
        return _inner_tile_size;
    }

    int get_rank_of(int,int) const override
    {
        return 0;
    }

}; // end class ClassicTileMapper

/*!
 * \brief Tile container for ChamDesc Object ('Lapack')
 *
 * All blocks are stored. Lapack Style
 *
 *
 * It is easy to compute the number (z) (hence the address) of a block given
 * its (i,j) coordinate: <br/>
 * \f$ z = j * LD + i \f$
 *
 * Each block can have multiple inner tiles. <br/>
 * Each tile is fully identified by its container block's coordinates
 * and by its "inner coordinates". <br/> The inner coordinate systems is the same
 * as the global coordinates system (DPLASMA/Chameleon style).
  */
template<class S>
class LapackTileMapper : public DataChameleonI
{
private:
    const int _mt, _nt;
    const int _nb;
    const int _block_bsize;
    const int _lm, _ln;
    const int _inner_tile_size;
    const int _tile_bsize;
    const int _inner_block_tile_ratio;

    Block<S> _data;
public:

    LapackTileMapper(int mt, int nt, int nb):
        _mt{mt}, _nt{nt},
        _nb{nb},
        _block_bsize{nb*nb},
        _lm{mt*nb}, _ln{nt*nb},
        _inner_tile_size{compute_inner_tile_size(nb)},
        _tile_bsize{_inner_tile_size * _inner_tile_size},
        _inner_block_tile_ratio{nb / _inner_tile_size},
        _data{_lm, _ln}
    {
    }

    Block<S> &get_block()
    {
        return _data;
    }

    int get_inner_tile_size() const override
    {
        return _inner_tile_size;
    }

    int get_block_size() const override
    {
        return _nb;
    }

private:
    void *get_blk_addr(int i, int j) const override
    {
        const int I = i * _inner_tile_size;
        const int J = j * _inner_tile_size;
        return (void*) _data.get_ptr(I, J);
    }

    int get_blk_ld(int) const override
    {
        return _data.get_leading_dim();
    }

    int get_rank_of(int,int) const override
    {
        return 0;
    }

}; // end class LapackTileMapper

} // end namespace fabulous

#endif // FABULOUS_TILE_MAPPER_HPP
