#ifndef FABULOUS_BASE_GENERIC_HPP
#define FABULOUS_BASE_GENERIC_HPP

namespace fabulous {
template<class S> class BaseGeneric;
}

#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/data/Block.hpp"

namespace fabulous {
/**
 * \brief This class store the base of the spanned krylov space.
 */
template<class Vector>
class BaseGeneric
{
public:
    using vector_type  = Vector;
    using value_type   = typename Vector::value_type;
    using primary_type = typename Arithmetik<value_type>::primary_type;

private:
    using S = value_type;
    using P = primary_type;

private:
    const vector_type &_reference_vec;
    const int _max_krylov_space_size; /*!< maximum number of vector in base */
    std::vector<vector_type> _blocks; /*!< array of blocks */
    std::vector<int> _block_sizes_sum;
    int _nb_vect; /*!< number of vector currently in base */

public:
    const vector_type &get_ref() const { return _reference_vec; }

    /**
     * \param[in] ref an already existing vector needed to construct a new vector
     * \param[in] max_krylov_space_size maximum size of search space allowed
     *          (number of column allocated)
     */
    BaseGeneric(const Vector &ref, int max_krylov_space_size):
        _reference_vec{ref},
        _max_krylov_space_size{max_krylov_space_size},
        _blocks{},
        _block_sizes_sum{},
        _nb_vect{0}
    {
        _block_sizes_sum.emplace_back(0);
    }

    int get_local_dim() const { return _reference_vec.get_nb_local_row()/*-1*/; }


    /**
     * \brief reset all base counters
     */
    void reset()
    {
        _blocks.clear();
        _block_sizes_sum.clear();
        _block_sizes_sum.emplace_back(0);
        _nb_vect = 0;
    }

    int get_nb_block() const { return _blocks.size(); }
    int get_nb_vect() const { return _nb_vect; }
    int get_nb_vect(int nb_block) const
    {
        FABULOUS_ASSERT( nb_block >= 0 );
        FABULOUS_ASSERT( (unsigned) nb_block <= _blocks.size() ) ;
        return _block_sizes_sum[nb_block];
    }

    vector_type get_W(int block_size) const
    {
        return vector_type{_reference_vec, block_size};
    }

    const vector_type& get_Vj() const
    {
        return _blocks.back();
    }

    void add_vector(vector_type &v)
    {
        _blocks.emplace_back( v );
        _nb_vect += v.get_nb_col();
        _block_sizes_sum.push_back(_nb_vect);
    }

    const vector_type& operator[](size_t i) const
    {
        FABULOUS_ASSERT( i < _blocks.size() );
        return _blocks[i];
    }

    /**
     * \brief Get a spesific vector from the base.
     * \brief Interface matching the dense base class.
     */
    const vector_type sub_vector(int offset, int width) const
    {
        FABULOUS_ASSERT( width == 1 );
        int i = 0;
        while(offset >= _blocks[i].get_nb_col()){
            offset -= _blocks[i].get_nb_col();
            i++;
        }
        return _blocks[i].sub_vector(offset, 1);
    }


}; // end class BaseGeneric

template<class Vector,
         class=enable_if_t<not is_optimizable_t<Vector>::value >,
         class=void >
auto make_base(const Vector &reference, int max_krylov_space_size)
{
    FABULOUS_DEBUG("making GENERIC base ");
    return BaseGeneric<Vector>{ reference, max_krylov_space_size };
}

template<class Vector, typename S>
void BASE_AXPY(Vector &VY_output, const BaseGeneric<Vector> &base_V, const Block<S> &Y, S alpha = S{1.0})
{
    FABULOUS_ASSERT( VY_output.get_nb_col() == Y.get_nb_col() );

    const int nrhs = Y.get_nb_col();
    const int nb_block = base_V.get_nb_block();

    int block_size_sum = 0;
    for (int i = 0; i < nb_block; ++i) {
        if (block_size_sum >= Y.get_nb_row()) {
            break;
        }
        const auto &V_i = base_V[i];
        const int block_size = V_i.get_nb_col();
        Block<S> y_i = Y.sub_block(block_size_sum, 0, block_size, nrhs);

        VY_output.axpy(V_i, y_i, alpha);
        block_size_sum += block_size;
    }
}

template<class Vector, typename S>
void BASE_DOT(const BaseGeneric<Vector> &base_V, const Vector &W, Block<S> &H)
{
    FABULOUS_ASSERT( W.get_nb_col() == H.get_nb_col() );
    const int nrhs = H.get_nb_col();
    const int nb_block = base_V.get_nb_block();

    int block_size_sum = 0;
    for (int i = 0; i < nb_block; ++i) {
        if (block_size_sum >= H.get_nb_row()) {
            break;
        }
        const auto &V_i = base_V[i];
        const int block_size = V_i.get_nb_col();

        Block<S> h_i = H.sub_block(block_size_sum, 0, block_size, nrhs);
        V_i.dot( W, h_i ); // h_i <- V_i . W
        block_size_sum += block_size;
    }
}

} // end namespace fabulous

#endif // FABULOUS_BASE_GENERIC_HPP
