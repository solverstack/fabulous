#ifndef FABULOUS_KERNEL_CHOLESKY_HPP
#define FABULOUS_KERNEL_CHOLESKY_HPP

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/kernel/spe/cholesky.hpp"

namespace fabulous {
namespace lapacke {

template<class S>
int potrf(char uplo, int M, S *A, int lda)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::potrf(uplo, M, A, lda);
    FABULOUS_END_KERNEL_PERF(potrf, S, M, 0, 0, 0);
    return err;
}

template<class S>
int potrs(char uplo, int M, int NRHS, const S *A, int lda, S *B, int ldb)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::potrs(uplo, M, NRHS, A, lda, B, ldb);
    FABULOUS_END_KERNEL_PERF(potrf, S, M, NRHS, 0, 0);
    return err;
}

} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_KERNEL_CHOLESKY_HPP
