#ifndef FABULOUS_BLAS_HPP
#define FABULOUS_BLAS_HPP

#include <complex>
#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/ext/cblas.h"
#include "fabulous/ext/lapacke.h"

#include "fabulous/kernel/spe/blas.hpp"

namespace fabulous {

/*! \brief LAPACKE kernel generic wrappers */
namespace lapacke {

template<class S>
void Tgemm(int M, int N, int K,
           const S *A, int lda,
           const S *B, int ldb,
           S *C, int ldc,
           S alpha = S{1.0}, S beta = S{0.0})
{
    FABULOUS_BEGIN_KERNEL_PERF;
    spe::Tgemm(M, N, K, A, lda, B, ldb, C, ldc, alpha, beta);
    FABULOUS_END_KERNEL_PERF(gemm, S, M, N, K, 0);
}

template<class S>
void gemm(int M, int N, int K,
          const S *A, int lda,
          const S *B, int ldb,
          S *C, int ldc,
          S alpha = S{1.0}, S beta = S{0.0})
{
    FABULOUS_BEGIN_KERNEL_PERF;
    spe::gemm(M, N, K, A, lda, B, ldb, C, ldc, alpha, beta);
    FABULOUS_END_KERNEL_PERF(gemm, S, M, N, K, 0);
}

template<class S>
void scal(int M, S coef, S *X, int incX)
{
    FABULOUS_BEGIN_KERNEL_PERF;
    spe::scal(M, coef, X, incX);
    FABULOUS_END_KERNEL_PERF(scal, S, M, 0, 0, 0);
}

template<class S>
void axpy(int M, S coef, const S *X, int incX, S *Y, int incY)
{
    FABULOUS_BEGIN_KERNEL_PERF;
    spe::axpy(M, coef, X, incX, Y, incY);
    FABULOUS_END_KERNEL_PERF(axpy, S, M, 0, 0, 0);
}

template<class S>
void dot(int M, const S *X, int incX, const S *Y, int incY, S *ret)
{
    FABULOUS_BEGIN_KERNEL_PERF;
    spe::dot(M, X, incX, Y, incY, ret);
    FABULOUS_END_KERNEL_PERF(dot, S, M, 0, 0, 0);
}

template<class S>
int trsm(char side, char uplo, char trans, char unit,
         int M, int NRHS, S alpha, const S *A, int lda, S *B, int ldb)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    const CBLAS_SIDE cside = (side == 'R' || side == 'r') ? CblasRight : CblasLeft;
    const CBLAS_UPLO cuplo = (uplo == 'U' || uplo == 'u') ? CblasUpper : CblasLower;
    const CBLAS_TRANSPOSE ctrans = ((trans == 'C' || trans == 'c')
                                    ? CblasConjTrans
                                    : ((trans == 'T' || trans == 't')
                                       ? CblasTrans
                                       : CblasNoTrans ));
    const CBLAS_DIAG cunit = (unit == 'U' || unit == 'u')  ? CblasUnit : CblasNonUnit;
    err = spe::trsm(cside, cuplo, ctrans, cunit, M, NRHS, alpha, A, lda, B, ldb);
    FABULOUS_END_KERNEL_PERF(trsm, S, M, NRHS, 0, 0);
    return err;
}

/**
 * \brief Solve the system AX=B with A triangular sup
 *
 * See https://software.intel.com/en-us/node/520783
 *
 * \param M size of squared matrix A and number of rows of B
 * \param NRHS number of right hand sides (number of column of B)
 * \param A array containing triangular A
 * \param lda leading dim of A
 * \param B array containing B
 * \param ldb leading dim of b
 */
template<class S>
int trsm(int M, int NRHS, const S *A, int lda, S *B, int ldb)
{
    return trsm('L', 'U', 'N', 'N', M, NRHS, S{1.0}, A, lda, B, ldb);
}

template<class S>
int lacpy(char uplo, int M, int N, S *A, int lda, S *B, int ldb)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::lacpy(uplo, M, N, A, lda, B, ldb);
    FABULOUS_END_KERNEL_PERF(lacpy, S, M, N, 0, 0);
    return err;
}

} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_BLAS_HPP
