#ifndef FABULOUS_GGEV_HPP
#define FABULOUS_GGEV_HPP

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/kernel/spe/ggev.hpp"

namespace fabulous {
namespace lapacke {

/**
 * \brief Generic Solve for the Generalized Eigen value Problem
 *
 * \param N Order of a,b
 * \param A first matrice
 * \param lda leading dim of a
 * \param B second matrice
 * \param ldb leading dim of b
 * \param alpha eigen values computed
 * \param beta eigen values computed theta_i is alpha[i]/beta[i]
 * \param Vl left eigen vectors (not used)
 * \param ldvl leading dim of eigen vectors
 * \param Vr right eigen vectors
 * \param ldvr leading dim of right eigen vectors
 *
 * Ref: https://software.intel.com/en-us/node/521179
 */
template<class S, class P,
         class = enable_if_t<std::is_same<P, typename Arithmetik<S>::primary_type>::value> >
int ggev(int N, S *A, int lda, S *B, int ldb,
         std::complex<P> *alpha, S *beta,
         S *Vl, int ldvl,
         S *Vr, int ldvr )
{
    int err = 0;

    err = spe::ggev(N, A, lda, B, ldb, alpha, beta, Vl, ldvl, Vr, ldvr);

    return err;
}

} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_GGEV_HPP
