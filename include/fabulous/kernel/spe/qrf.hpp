#ifndef FABULOUS_KERNEL_QRF_SPE_HPP
#define FABULOUS_KERNEL_QRF_SPE_HPP

#include <vector>
#include "fabulous/ext/lapacke.h"
#include "fabulous/utils/Arithmetic.hpp"

namespace fabulous {
namespace lapacke {
namespace spe {

#ifdef FABULOUS_LAPACKE_NANCHECK

/********  QRF **************/

#define FABULOUS_SPECIALIZE_GEQRF(_1, S_, _3, _4, _5, _6, prefix_, ...) \
    inline int geqrf(                                                   \
        int m, int n, S_ *A, int lda, std::vector<S_> &tau)             \
    {                                                                   \
        tau.resize(n);                                                  \
        return LAPACKE_##prefix_##geqrf(LAPACK_COL_MAJOR, m, n,         \
                                        A, lda, tau.data());            \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_GEQRF)

/********  GQR **************/


#define FABULOUS_SPECIALIZE_GQR(_1, S_, _3, _4, _5,             \
                                _6, prefix_, unitary_, ...)     \
    inline int ungqr(                                           \
        int m, int n, int p, S_ *A, int lda, S_ *tau)           \
    {                                                           \
        return LAPACKE_##prefix_##unitary_##gqr(                \
            LAPACK_COL_MAJOR, m, n, p, A, lda, tau              \
        );                                                      \
    }

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_GQR)

/************* MQR ********************/

#define FABULOUS_SPECIALIZE_MQR(_1, S_, _3, _4, _5,             \
                                _6, prefix_, unitary_, ...)     \
    inline int unmqr(char trans,                                \
                     int m, int n, int k,                       \
                     const S_ *A, int lda,                      \
                     const S_ *tau,                             \
                     S_ *C, int ldc)                            \
    {                                                           \
        return LAPACKE_##prefix_##unitary_##mqr(                \
            LAPACK_COL_MAJOR, 'L', trans, m, n, k,              \
            A, lda, tau, C, ldc                                 \
        );                                                      \
    }

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_MQR)

#else // FABULOUS_LAPACKE_NANCHECK

/********  QRF **************/

#define FABULOUS_SPECIALIZE_GEQRF(_1, S_, P_, _4, _5,                   \
                                  _6, prefix_, ...)                     \
    inline int geqrf(                                                   \
        int m, int n, S_ *A, int lda, std::vector<S_> &tau)             \
    {                                                                   \
        tau.resize(n);                                                  \
        lapack_int info = 0;                                            \
        lapack_int lwork = -1;                                          \
        S_ *work = nullptr;                                             \
        S_ work_query;                                                  \
        info = LAPACKE_##prefix_##geqrf_work(                           \
            LAPACK_COL_MAJOR, m, n, A, lda, tau.data(),                 \
            &work_query, lwork                                          \
        );                                                              \
        if ( info != 0 )                                                \
            return info;                                                \
        lwork = (lapack_int) *((P_*)&work_query);                       \
        work = (S_*) LAPACKE_malloc( sizeof(S_) * lwork );              \
        if (work == nullptr)                                            \
            return LAPACK_WORK_MEMORY_ERROR;                            \
        info = LAPACKE_##prefix_##geqrf_work(                           \
            LAPACK_COL_MAJOR, m, n, A, lda, tau.data(), work, lwork);   \
        LAPACKE_free( work );                                           \
        return info;                                                    \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_GEQRF)

/********  GQR **************/

#define FABULOUS_SPECIALIZE_GQR(_1, S_, P_, _4, _5,                     \
                                _6, prefix_, unitary_, ...)             \
    inline int ungqr(                                                   \
        int m, int n, int p, S_ *A, int lda, S_ *tau)                   \
    {                                                                   \
        lapack_int info = 0;                                            \
        lapack_int lwork = -1;                                          \
        S_ *work = nullptr;                                             \
        S_ work_query;                                                  \
        info = LAPACKE_##prefix_##unitary_##gqr_work(                   \
            LAPACK_COL_MAJOR, m, n, p, A, lda, tau,                     \
            &work_query, lwork                                          \
        );                                                              \
        if ( info != 0 )                                                \
            return info;                                                \
        lwork = (lapack_int) *((P_*)&work_query);                       \
        work = (S_*) LAPACKE_malloc( sizeof(S_) * lwork );              \
        if (work == nullptr)                                            \
            return LAPACK_WORK_MEMORY_ERROR;                            \
        info = LAPACKE_##prefix_##unitary_##gqr_work(                   \
            LAPACK_COL_MAJOR, m, n, p, A, lda, tau, work, lwork);       \
        LAPACKE_free( work );                                           \
        return info;                                                    \
    }

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_GQR)

/************* MQR ********************/

#define FABULOUS_SPECIALIZE_MQR(_1, S_, P_, _4, _5,             \
                                _6, prefix_, unitary_, ...)     \
    inline int unmqr(char trans,                                \
                     int m, int n, int k,                       \
                     const S_ *A, int lda,                      \
                     const S_ *tau,                             \
                     S_ *C, int ldc)                            \
    {                                                           \
        lapack_int info = 0;                                    \
        lapack_int lwork = -1;                                  \
        S_ *work = nullptr;                                     \
        S_ work_query;                                          \
        info = LAPACKE_##prefix_##unitary_##mqr_work(           \
            LAPACK_COL_MAJOR, 'L', trans, m, n, k,              \
            A, lda, tau, C, ldc, &work_query, lwork             \
        );                                                      \
        if ( info != 0 )                                        \
            return info;                                        \
        lwork = (lapack_int) *((P_*) &work_query);              \
        work = (S_*) LAPACKE_malloc( sizeof(S_) * lwork );      \
        if ( work == nullptr )                                  \
            return LAPACK_WORK_MEMORY_ERROR;                    \
        info = LAPACKE_##prefix_##unitary_##mqr_work(           \
            LAPACK_COL_MAJOR, 'L', trans, m, n, k,              \
            A, lda, tau, C, ldc, work, lwork                    \
        );                                                      \
        LAPACKE_free( work );                                   \
        return info;                                            \
    }

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_MQR)

#endif // FABULOUS_LAPACKE_NANCHECK

} // end namespace spe
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_KERNEL_QRF_SPE_HPP
