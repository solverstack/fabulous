#ifndef FABULOUS_KERNEL_SVD_SPE_HPP
#define FABULOUS_KERNEL_SVD_SPE_HPP

#include <complex>
#include "fabulous/ext/lapacke.h"
#include "fabulous/ext/cblas.h"

namespace fabulous {
namespace lapacke {
namespace spe {

#ifdef FABULOUS_LAPACKE_NANCHECK

inline int gesvd(char jobu, char jobvt,
                 int m, int n,
                 float *A, int ldA,
                 float *sigma,
                 float *U, int ldU,
                 float *V, int ldV,
                 float *sup)
{
    return LAPACKE_sgesvd(LAPACK_COL_MAJOR, jobu, jobvt, m, n,
                          A, ldA, sigma, U, ldU, V, ldV, sup);
}

inline int gesvd(char jobu, char jobvt,
                 int m, int n,
                 double *A, int ldA,
                 double *sigma,
                 double *U, int ldU,
                 double *V, int ldV,
                 double *sup)
{
    return LAPACKE_dgesvd(LAPACK_COL_MAJOR, jobu, jobvt, m, n,
                          A, ldA, sigma, U, ldU, V, ldV, sup);
}

inline int gesvd(char jobu,char jobvt,
                 int m ,int n,
                 std::complex<float> *A, int ldA,
                 float* sigma,
                 std::complex<float> *U, int ldU,
                 std::complex<float> *V, int ldV,
                 float * sup)
{
    return LAPACKE_cgesvd(LAPACK_COL_MAJOR, jobu, jobvt, m, n,
                          A, ldA, sigma, U, ldU, V, ldV, sup);
}

inline int gesvd(char jobu, char jobvt,
                 int m, int n,
                 std::complex<double> *A, int ldA,
                 double *sigma,
                 std::complex<double> *U, int ldU,
                 std::complex<double> *V, int ldV,
                 double *sup)
{
    return LAPACKE_zgesvd(LAPACK_COL_MAJOR, jobu, jobvt, m, n,
                          A, ldA, sigma, U, ldU, V, ldV, sup);
}

#else // FABULOUS_LAPACKE_NANCHECK

inline int gesvd( char jobu, char jobvt, int m, int n,
                  float *A, int lda, float *sigma,
                  float *U, int ldu, float *V, int ldv, float *superb)
{
    lapack_int info = 0;
    lapack_int lwork = -1;
    float *work = NULL;
    float work_query;

    info = LAPACKE_sgesvd_work(
        LAPACK_COL_MAJOR, jobu, jobvt,
        m, n, A, lda, sigma, U, ldu, V, ldv, &work_query, lwork );
    if ( info != 0 )
        return info;
    lwork = (lapack_int) work_query;
    work = (float*) LAPACKE_malloc( sizeof(float) * lwork );
    if ( work == nullptr )
        return LAPACK_WORK_MEMORY_ERROR;
    info = LAPACKE_sgesvd_work(
        LAPACK_COL_MAJOR, jobu, jobvt,
        m, n, A, lda, sigma, U, ldu, V, ldv, work, lwork );
    for (lapack_int i = 0; i < std::min(m,n)-1; ++i)
        superb[i] = work[i+1];
    LAPACKE_free( work );
    return info;
}

inline int gesvd( char jobu, char jobvt, int m, int n,
                  double *A, int lda, double *sigma,
                  double *U, int ldu, double *V, int ldv, double *superb)
{
    lapack_int info = 0;
    lapack_int lwork = -1;
    double *work = NULL;
    double work_query;

    info = LAPACKE_dgesvd_work(
        LAPACK_COL_MAJOR, jobu, jobvt,
        m, n, A, lda, sigma, U, ldu, V, ldv, &work_query, lwork );
    if ( info != 0 )
        return info;
    lwork = (lapack_int) work_query;
    work = (double*) LAPACKE_malloc( sizeof(double) * lwork );
    if ( work == nullptr )
        return LAPACK_WORK_MEMORY_ERROR;
    info = LAPACKE_dgesvd_work(
        LAPACK_COL_MAJOR, jobu, jobvt,
        m, n, A, lda, sigma, U, ldu, V, ldv, work, lwork );
    for (lapack_int i = 0; i < std::min(m,n)-1; ++i)
        superb[i] = work[i+1];
    LAPACKE_free( work );
    return info;
}

inline int gesvd( char jobu, char jobvt, int m, int n,
                  std::complex<float> *A, int lda,
                  float *sigma,
                  std::complex<float> *U, int ldu,
                  std::complex<float> *V, int ldv,
                  float *superb )
{
    lapack_int info = 0;
    lapack_int lwork = -1;
    float* rwork = NULL;
    std::complex<float> *work = NULL;
    std::complex<float> work_query;

    rwork = (float*) LAPACKE_malloc( sizeof(float) * std::max(1, 5*std::min(m,n)) );
    if ( rwork == nullptr )
        return LAPACK_WORK_MEMORY_ERROR;
    info = LAPACKE_cgesvd_work(
        LAPACK_COL_MAJOR, jobu, jobvt,
        m, n, A, lda, sigma, U, ldu, V, ldv, &work_query, lwork, rwork );
    if ( info != 0 ) {
        LAPACKE_free( rwork );
        return info;
    }
    lwork = (lapack_int) *((float*)&work_query);
    work = (std::complex<float>*) LAPACKE_malloc( sizeof(std::complex<float>) * lwork );
    if ( work == NULL ) {
        LAPACKE_free( rwork );
        return LAPACK_WORK_MEMORY_ERROR;
    }
    info = LAPACKE_cgesvd_work(
        LAPACK_COL_MAJOR, jobu, jobvt,
        m, n, A, lda, sigma, U, ldu, V, ldv, work, lwork, rwork );
    for (lapack_int i = 0; i < std::min(m,n)-1; ++i)
        superb[i] = rwork[i+1];
    LAPACKE_free( rwork );
    LAPACKE_free( work );
    return info;
}

inline int gesvd(char jobu, char jobvt, int m, int n,
                 std::complex<double> *A, int lda,
                 double *sigma,
                 std::complex<double> *U, int ldu,
                 std::complex<double> *V, int ldv,
                 double *superb)
{
    lapack_int info = 0;
    lapack_int lwork = -1;
    double* rwork = NULL;
    std::complex<double> *work = NULL;
    std::complex<double> work_query;

    rwork = (double*) LAPACKE_malloc( sizeof(double) * std::max(1, 5*std::min(m,n)) );
    if ( rwork == nullptr )
        return LAPACK_WORK_MEMORY_ERROR;
    info = LAPACKE_zgesvd_work(
        LAPACK_COL_MAJOR, jobu, jobvt,
        m, n, A, lda, sigma, U, ldu, V, ldv, &work_query, lwork, rwork );
    if ( info != 0 ) {
        LAPACKE_free( rwork );
        return info;
    }
    lwork = (lapack_int) *((double*)&work_query);
    work = (std::complex<double>*) LAPACKE_malloc( sizeof(std::complex<double>) * lwork );
    if ( work == NULL ) {
        LAPACKE_free( rwork );
        return LAPACK_WORK_MEMORY_ERROR;
    }
    info = LAPACKE_zgesvd_work(
        LAPACK_COL_MAJOR, jobu, jobvt,
        m, n, A, lda, sigma, U, ldu, V, ldv, work, lwork, rwork );
    for (lapack_int i = 0; i < std::min(m,n)-1; ++i)
        superb[i] = rwork[i+1];
    LAPACKE_free( rwork );
    LAPACKE_free( work );
    return info;
}

#endif // FABULOUS_LAPACKE_NANCHECK

} // end namespace spe
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_KERNEL_SVD_SPE_HPP
