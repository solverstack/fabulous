#ifndef FABULOUS_CHAMELEON_TOPLEVEL_SPE_HPP
#define FABULOUS_CHAMELEON_TOPLEVEL_SPE_HPP

#ifndef FABULOUS_USE_CHAMELEON
# error "This file must be included nowhere if FABULOUS_USE_CHAMELEON is not set"
#endif

#include <complex>
#include <chameleon.h>

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/ext/cblas.h"
#include "fabulous/ext/lapacke.h"

namespace fabulous {
namespace chameleon {
namespace toplevel {

/*! \brief wrapper specializations for the 4 arithmetics */
namespace spe {

#define FABULOUS_SPECIALIZE_TL_CHAM_GELS(_1, S_, P_, _4, _5, _6, prefix_, ...) \
    inline int gels(int m, int n, int nrhs, S_ *A, int lda, S_ *B, int ldb) \
    {                                                                   \
        CHAM_desc_t *T = nullptr;                                      \
        CHAMELEON_Alloc_Workspace_##prefix_##gels(m, n, &T, 1, 1);          \
        int err = CHAMELEON_##prefix_##gels(ChamNoTrans, m, n, nrhs,       \
                                            A, lda, T, B, ldb); \
        CHAMELEON_Desc_Destroy(&T);                                         \
        return err;                                                     \
    }

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_TL_CHAM_GELS)

} // end namespace spe
} // end namespace toplevel
} // end namespace chameleon
} // end namespace fabulous

#endif // FABULOUS_CHAMELEON_TOPLEVEL_SPE_HPP
