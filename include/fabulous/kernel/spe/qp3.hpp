#ifndef FABULOUS_QP3_SPE_HPP
#define FABULOUS_QP3_SPE_HPP

#include <complex>
#include <vector>

#include "fabulous/ext/cblas.h"
#include "fabulous/ext/lapacke.h"

namespace fabulous {
namespace lapacke {
namespace spe {

#ifdef FABULOUS_LAPACKE_NANCHECK

/********  QRF **************/

#define FABULOUS_SPECIALIZE_GEQP3(_1, S_, P_, _4, _5, _6, prefix_, ...) \
    inline int geqp3(int m, int n, S_ *A, int lda,                      \
                     std::vector<int> &jpvt, std::vector<S_> &tau)      \
    {                                                                   \
        tau.resize(n);                                                  \
        jpvt.resize(n);                                                 \
        std::fill(jpvt.begin(), jpvt.end(), 0);                         \
        return LAPACKE_##prefix_##geqp3(                                \
            LAPACK_COL_MAJOR, m, n,                                     \
            A, lda, jpvt.data(), tau.data()                             \
        );                                                              \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_GEQP3)

#else // FABULOUS_LAPACKE_NANCHECK

/********  QRF **************/

inline int geqp3( int m, int n, float *A, int lda,
                  std::vector<int> &jpvt, std::vector<float> &tau)
{
    tau.resize(n);
    jpvt.resize(n);
    std::fill(jpvt.begin(), jpvt.end(), 0);
    lapack_int info = 0;
    lapack_int lwork = -1;
    float *work = nullptr;
    float work_query;
    info = LAPACKE_sgeqp3_work(
        LAPACK_COL_MAJOR, m, n, A, lda, jpvt.data(),
        tau.data(), &work_query, lwork
    );
    if ( info != 0 ) {
        return info;
    }
    lwork = (lapack_int) *((float*)&work_query);
    work = (float*) LAPACKE_malloc( sizeof(float) * lwork );
    if (work == nullptr) {
        return LAPACK_WORK_MEMORY_ERROR;
    }
    info = LAPACKE_sgeqp3_work(
        LAPACK_COL_MAJOR, m, n, A, lda, jpvt.data(),
        tau.data(), work, lwork
    );
    LAPACKE_free( work );
    return info;
}

inline int geqp3( int m, int n, double *A, int lda,
                  std::vector<int> &jpvt, std::vector<double> &tau)
{
    tau.resize(n);
    jpvt.resize(n);
    std::fill(jpvt.begin(), jpvt.end(), 0);
    lapack_int info = 0;
    lapack_int lwork = -1;
    double *work = nullptr;
    double work_query;
    info = LAPACKE_dgeqp3_work(
        LAPACK_COL_MAJOR, m, n, A, lda, jpvt.data(),
        tau.data(), &work_query, lwork
    );
    if ( info != 0 ) {
        return info;
    }
    lwork = (lapack_int) *((double*)&work_query);
    work = (double*) LAPACKE_malloc( sizeof(double) * lwork );
    if (work == nullptr) {
        return LAPACK_WORK_MEMORY_ERROR;
    }
    info = LAPACKE_dgeqp3_work(
        LAPACK_COL_MAJOR, m, n, A, lda, jpvt.data(),
        tau.data(), work, lwork
    );
    LAPACKE_free( work );
    return info;
}



inline int geqp3( int m, int n, std::complex<float> *A, int lda,
                  std::vector<int> &jpvt, std::vector<std::complex<float>> &tau)
{
    tau.resize(n);
    jpvt.resize(n);
    std::fill(jpvt.begin(), jpvt.end(), 0);
    lapack_int info = 0;
    lapack_int lwork = -1;
    std::complex<float> *work = nullptr;
    std::complex<float> work_query;
    float *rwork = NULL;

    rwork = (float*) LAPACKE_malloc( sizeof(float) * std::max(1, 2*n) );
    if ( rwork == nullptr ) {
        return LAPACK_WORK_MEMORY_ERROR;
    }

    info = LAPACKE_cgeqp3_work(
        LAPACK_COL_MAJOR, m, n, A, lda, jpvt.data(),
        tau.data(), &work_query, lwork, rwork
    );
    if ( info != 0 ) {
        LAPACKE_free( rwork );
        return info;
    }
    lwork = (lapack_int) *((float*)&work_query);
    work = (std::complex<float>*) LAPACKE_malloc( sizeof(std::complex<float>) * lwork );

    if (work == nullptr) {
        LAPACKE_free( rwork );
        return LAPACK_WORK_MEMORY_ERROR;
    }
    info = LAPACKE_cgeqp3_work(
        LAPACK_COL_MAJOR, m, n, A, lda, jpvt.data(),
        tau.data(), work, lwork, rwork
    );
    LAPACKE_free( work );
    return info;
}

inline int geqp3( int m, int n, std::complex<double> *A, int lda,
                  std::vector<int> &jpvt, std::vector<std::complex<double>> &tau)
{
    tau.resize(n);
    jpvt.resize(n);
    std::fill(jpvt.begin(), jpvt.end(), 0);
    lapack_int info = 0;
    lapack_int lwork = -1;
    std::complex<double> *work = nullptr;
    std::complex<double> work_query;
    double *rwork = NULL;

    rwork = (double*) LAPACKE_malloc( sizeof(double) * std::max(1, 2*n) );
    if ( rwork == nullptr ) {
        return LAPACK_WORK_MEMORY_ERROR;
    }

    info = LAPACKE_zgeqp3_work(
        LAPACK_COL_MAJOR, m, n, A, lda, jpvt.data(),
        tau.data(), &work_query, lwork, rwork
    );
    if ( info != 0 ) {
        LAPACKE_free( rwork );
        return info;
    }
    lwork = (lapack_int) *((double*)&work_query);
    work = (std::complex<double>*) LAPACKE_malloc( sizeof(std::complex<double>) * lwork );

    if (work == nullptr) {
        LAPACKE_free( rwork );
        return LAPACK_WORK_MEMORY_ERROR;
    }
    info = LAPACKE_zgeqp3_work(
        LAPACK_COL_MAJOR, m, n, A, lda, jpvt.data(),
        tau.data(), work, lwork, rwork
    );
    LAPACKE_free( work );
    return info;
}

#endif // FABULOUS_LAPACKE_NANCHECK

} // end namespace spe
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_QP3_SPE_HPP
