#ifndef FABULOUS_KERNEL_TPQRT_SPE_HPP
#define FABULOUS_KERNEL_TPQRT_SPE_HPP

#include <vector>
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/ext/lapacke.h"

namespace fabulous {
namespace lapacke {
namespace spe {

#ifdef FABULOUS_LAPACKE_NANCHECK

/********  TPQRT **************/

#define FABULOUS_SPECIALIZE_TPQRT(_1, S_, P_, _4, _5, _6, prefix_, ...) \
    inline int tpqrt(int m, int n, int l, int ib,                       \
                     S_ *A, int lda, S_ *B, int ldb,                    \
                     std::vector<S_> &T)                                \
    {                                                                   \
        T.resize(ib*n);                                                 \
        return LAPACKE_##prefix_##tpqrt(                                \
            LAPACK_COL_MAJOR, m, n, l, ib, A, lda, B, ldb,              \
            T.data(), ib                                                \
        );                                                              \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_TPQRT)

/************* TPMQRT ********************/

#define FABULOUS_SPECIALIZE_TPMQRT(_1, S_, P_, _4, _5, _6, prefix_, ...) \
    inline int tpmqrt(char trans, int m, int n, int k, int l, int ib,   \
                      const S_ *V, int ldv,                             \
                      const S_ *T, int ldt,                             \
                      S_ *A, int lda, S_ *B, int ldb)                   \
    {                                                                   \
        /* This function had buggy workspace allocation */              \
        /* in netlib lapack (fixed May 4 2017)  */                      \
        /* you may want do use the -DFABULOUS_LAPACKE_NANCHECK=OFF */   \
        /* cmake switch to use the other function if problems */        \
        /* do appear with your lapack distribution */                   \
        return LAPACKE_##prefix_##tpmqrt(                               \
            LAPACK_COL_MAJOR, 'L', trans, m, n, k, l, ib,               \
            V, ldv, T, ldt,                                             \
            A, lda, B, ldb                                              \
        );                                                              \
    }

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_TPMQRT)

#else // FABULOUS_LAPACKE_NANCHECK

/********  TPQRT **************/

#define FABULOUS_SPECIALIZE_TPQRT(_1, S_, P_, _4, _5, _6, prefix_, ...) \
    inline int tpqrt(int m, int n, int l, int ib, S_ *A, int lda,       \
                     S_ *B, int ldb, std::vector<S_> &T)                \
    {                                                                   \
        T.resize(ib*n);                                                 \
        lapack_int info = 0;                                            \
        const size_t worksiz = std::max(1,ib) * std::max(1,n);          \
        S_ *work = (S_*) LAPACKE_malloc( sizeof(S_) * worksiz );        \
        if (work == nullptr)                                            \
            return LAPACK_WORK_MEMORY_ERROR;                            \
        info = LAPACKE_##prefix_##tpqrt_work(                           \
            LAPACK_COL_MAJOR, m, n, l, ib, A, lda, B, ldb,              \
            T.data(), ib, work                                          \
        );                                                              \
        LAPACKE_free( work );                                           \
        return info;                                                    \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_TPQRT)

/********  TPMQRT **************/

#define FABULOUS_SPECIALIZE_TPMQRT(_1, S_, P_, _4, _5, _6, prefix_, ...) \
    inline int tpmqrt(char trans, int m, int n, int k, int l, int ib,   \
                      const S_ *V, int ldv,                             \
                      const S_ *T, int ldt,                             \
                      S_ *A, int lda,                                   \
                      S_ *B, int ldb)                                   \
    {                                                                   \
        lapack_int info = 0;                                            \
        const size_t worksiz = std::max(1,ib) * std::max(1,n);          \
        S_ *work = (S_*) LAPACKE_malloc( sizeof(S_) * worksiz );        \
        if (work == nullptr)                                            \
            return LAPACK_WORK_MEMORY_ERROR;                            \
        info = LAPACKE_##prefix_##tpmqrt_work(                          \
            LAPACK_COL_MAJOR, 'L', trans, m, n, k, l, ib,               \
            V, ldv, T, ldt,                                             \
            A, lda, B, ldb, work                                        \
        );                                                              \
        LAPACKE_free( work );                                           \
        return info;                                                    \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_TPMQRT)

#endif // FABULOUS_LAPACKE_NANCHECK


} // end namespace spe
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_KERNEL_TPQRT_SPE_HPP
