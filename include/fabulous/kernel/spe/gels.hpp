#ifndef FABULOUS_KERNEL_GELS_SPE_HPP
#define FABULOUS_KERNEL_GELS_SPE_HPP

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/ext/lapacke.h"

namespace fabulous {
namespace lapacke {
namespace spe {

#ifdef FABULOUS_LAPACKE_NANCHECK

#define FABULOUS_SPECIALIZE_GELS(_1, type_, _3, _4, _5, _6, prefix_, ...) \
    inline int gels(int m, int n, int nbRHS, type_ *A, int lda, type_ *B, int ldb) \
    {                                                                   \
        return LAPACKE_##prefix_##gels(LAPACK_COL_MAJOR, 'N',           \
                                       m, n, nbRHS, A, lda, B, ldb);    \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_GELS)

#else // FABULOUS_LAPACKE_NANCHECK

#define FABULOUS_SPECIALIZE_GELS(_1, S_, P_, _4, _5, _6, prefix_, ...)  \
    inline int gels(int m, int n, int nrhs, S_ *A, int lda, S_ *B, int ldb) \
    {                                                                   \
        lapack_int info = 0;                                            \
        lapack_int lwork = -1;                                          \
        S_ *work = nullptr;                                             \
        S_ work_query;                                                  \
        info = LAPACKE_##prefix_##gels_work(                            \
            LAPACK_COL_MAJOR, 'N', m, n, nrhs, A, lda, B, ldb,          \
            &work_query, lwork                                          \
        );                                                              \
        if ( info != 0 )                                                \
            return info;                                                \
        lwork = (lapack_int) *((P_*)&work_query);                       \
        work = (S_*) LAPACKE_malloc( sizeof(S_) * lwork );              \
        if (work == nullptr)                                            \
            return LAPACK_WORK_MEMORY_ERROR;                            \
        info = LAPACKE_##prefix_##gels_work(                            \
            LAPACK_COL_MAJOR, 'N',                                      \
            m, n, nrhs, A, lda, B, ldb,                                 \
            work, lwork                                                 \
        );                                                              \
        LAPACKE_free( work );                                           \
        return info;                                                    \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_GELS)

#endif // FABULOUS_LAPACKE_NANCHECK


} // end namespace spe
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_KERNEL_GELS_SPE_HPP
