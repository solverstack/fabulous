#ifndef FABULOUS_BLAS_SPE_HPP
#define FABULOUS_BLAS_SPE_HPP

#include <complex>
#include "fabulous/ext/cblas.h"
#include "fabulous/ext/lapacke.h"

namespace fabulous {
namespace lapacke {

/*! \brief wrapper specializations for the 4 arithmetics */
namespace spe {

/************* T-GEMM ********************/

inline void Tgemm(int m, int n, int k,
                  const float *A, int lda,
                  const float *B, int ldb,
                  float *C, int ldc,
                  float alpha, float beta)
{
    cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans,
                m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}

inline void Tgemm(int m, int n, int k,
                  const double *A, int lda,
                  const double *B, int ldb,
                  double *C, int ldc,
                  double alpha, double beta)
{
    cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans,
                m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}

inline void Tgemm(int m, int n, int k,
                  const std::complex<float> *A, int lda,
                  const std::complex<float> *B, int ldb,
                  std::complex<float> *C, int ldc,
                  std::complex<float> alpha,
                  std::complex<float> beta)
{
    cblas_cgemm(CblasColMajor, CblasConjTrans, CblasNoTrans,
                m, n, k, &alpha, A, lda, B, ldb, &beta, C, ldc);
}

inline void Tgemm(int m, int n, int k,
                  const std::complex<double> *A, int lda,
                  const std::complex<double> *B, int ldb,
                  std::complex<double> *C, int ldc,
                  std::complex<double> alpha,
                  std::complex<double> beta)
{
    cblas_zgemm(CblasColMajor, CblasConjTrans, CblasNoTrans,
                m, n, k, &alpha, A, lda, B, ldb, &beta, C, ldc);
}

/************* GEMM ********************/

inline void gemm(int m, int n, int k,
                 const float *A, int lda,
                 const float *B, int ldb,
                 float *C, int ldc,
                 float alpha, float beta)
{
    cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
                m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}

inline void gemm(int m, int n, int k,
                 const double *A, int lda,
                 const double *B, int ldb,
                 double *C, int ldc,
                 double alpha, double beta)
{
    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
                m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}

inline void gemm(int m, int n, int k,
                 const std::complex<float> *A, int lda,
                 const std::complex<float> *B, int ldb,
                 std::complex<float> *C, int ldc,
                 std::complex<float> alpha, std::complex<float> beta)
{
    cblas_cgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
                m, n, k, &alpha, A, lda, B, ldb, &beta, C, ldc);
}

inline void gemm(int m, int n, int k,
                 const std::complex<double> *A, int lda,
                 const std::complex<double> *B, int ldb,
                 std::complex<double> *C, int ldc,
                 std::complex<double> alpha,
                 std::complex<double> beta)
{
    cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
                m, n, k, &alpha, A, lda, B, ldb, &beta, C, ldc);
}

/* ******* scal **********/

inline void scal(int N, float coef, float *X, int incX)
{
    cblas_sscal(N, coef, X, incX);
}

inline void scal(int N, double coef, double *X, int incX)
{
    cblas_dscal(N, coef, X, incX);
}

inline void scal(int N, std::complex<float> coef, std::complex<float> *X, int incX)
{
    cblas_cscal(N, &coef, X, incX);
}

inline void scal(int N, std::complex<double> coef, std::complex<double> *X, int incX)
{
    cblas_zscal(N, &coef, X, incX);
}

/* ******* axpy **********/

inline void axpy(int N, float coef, const float *X, int incX, float *Y, int incY)
{
    cblas_saxpy(N, coef, X, incX, Y, incY);
}

inline void axpy(int N, double coef, const double *X, int incX, double *Y, int incY)
{
    cblas_daxpy(N, coef, X, incX, Y, incY);
}

inline void axpy(int N,
                 std::complex<float> coef,
                 const std::complex<float> *X, int incX,
                 std::complex<float> *Y, int incY)
{
    cblas_caxpy(N, &coef, X, incX, Y, incY);
}

inline void axpy(int N, std::complex<double> coef,
                 const std::complex<double> *X, int incX,
                 std::complex<double> *Y, int incY)
{
    cblas_zaxpy(N, &coef, X, incX, Y, incY);
}


/* ******* dot **********/

inline void dot(int N,
                const float *X, int incX,
                const float *Y, int incY, float *ret)
{
    *ret = cblas_sdot(N, X, incX, Y, incY);
}

inline void dot(int N,
                const double *X, int incX,
                const double *Y, int incY, double *ret)
{
    *ret = cblas_ddot(N, X, incX, Y, incY);
}

inline void dot(int N,
                const std::complex<float> *X, int incX,
                const std::complex<float> *Y, int incY,
                std::complex<float> *ret)
{
    cblas_cdotc_sub(N, X, incX, Y, incY, ret);
}

inline void dot(int N,
                const std::complex<double> *X, int incX,
                const std::complex<double> *Y, int incY,
                std::complex<double> *ret)
{
    cblas_zdotc_sub(N, X, incX, Y, incY, ret);
}

/********* TRSM **********/

inline int trsm(CBLAS_SIDE side, CBLAS_UPLO uplo,
                CBLAS_TRANSPOSE trans, CBLAS_DIAG diag,
                int m, int n, float alpha,
                const float *A, int lda, float *B, int ldb)
{
    cblas_strsm( CblasColMajor, side, uplo, trans, diag,
                 m, n, alpha, A, lda, B, ldb);
    return 0;
}

inline int trsm(CBLAS_SIDE side, CBLAS_UPLO uplo,
                CBLAS_TRANSPOSE trans, CBLAS_DIAG diag,
                int M, int N, double alpha,
                const double *A, int lda,
                double *B, int ldb)
{
    cblas_dtrsm(CblasColMajor, side, uplo, trans, diag,
                M, N, alpha, A, lda, B, ldb);
    return 0;
}

inline int trsm(CBLAS_SIDE side, CBLAS_UPLO uplo,
                CBLAS_TRANSPOSE trans, CBLAS_DIAG diag,
                int M, int N, std::complex<float> alpha,
                const std::complex<float> *A, int lda,
                std::complex<float> *B, int ldb)
{
    cblas_ctrsm(CblasColMajor, side, uplo, trans, diag,
                M, N, &alpha, A, lda, B, ldb);
    return 0;
}

inline int trsm(CBLAS_SIDE side, CBLAS_UPLO uplo,
                CBLAS_TRANSPOSE trans, CBLAS_DIAG diag,
                int M, int N, std::complex<double> alpha,
                const std::complex<double> *A, int lda,
                std::complex<double> *B, int ldb)
{
    cblas_ztrsm(CblasColMajor, side, uplo, trans, diag,
                M, N, &alpha, A, lda, B, ldb);
    return 0;
}


#ifdef FABULOUS_LAPACKE_NANCHECK

/********* LACPY **********/

inline int lacpy(char uplo, int M, int N, float *A, int lda, float *B, int ldb)
{
    return LAPACKE_slacpy(LAPACK_COL_MAJOR, uplo, M, N, A, lda, B, ldb);
}

inline int lacpy(char uplo, int M, int N, double *A, int lda, double *B, int ldb)
{
    return LAPACKE_dlacpy(LAPACK_COL_MAJOR, uplo, M, N, A, lda, B, ldb);
}

inline int lacpy(char uplo, int M, int N,
                 std::complex<float> *A, int lda,
                 std::complex<float> *B, int ldb)
{
    return LAPACKE_clacpy(LAPACK_COL_MAJOR, uplo, M, N, A, lda, B, ldb);
}

inline int lacpy(char uplo, int M, int N,
                 std::complex<double> *A, int lda,
                 std::complex<double> *B, int ldb)
{
    return LAPACKE_zlacpy(LAPACK_COL_MAJOR, uplo, M, N, A, lda, B, ldb);
}

#else // FABULOUS_LAPACKE_NANCHECK

/********* LACPY **********/

inline int lacpy(char uplo, int M, int N, float *A, int lda, float *B, int ldb)
{
    return LAPACKE_slacpy_work(LAPACK_COL_MAJOR, uplo, M, N, A, lda, B, ldb);
}

inline int lacpy(char uplo, int M, int N, double *A, int lda, double *B, int ldb)
{
    return LAPACKE_dlacpy_work(LAPACK_COL_MAJOR, uplo, M, N, A, lda, B, ldb);
}

inline int lacpy(char uplo, int M, int N,
                 std::complex<float> *A, int lda,
                 std::complex<float> *B, int ldb)
{
    return LAPACKE_clacpy_work(LAPACK_COL_MAJOR, uplo, M, N, A, lda, B, ldb);
}

inline int lacpy(char uplo, int M, int N,
                 std::complex<double> *A, int lda,
                 std::complex<double> *B, int ldb)
{
    return LAPACKE_zlacpy_work(LAPACK_COL_MAJOR, uplo, M, N, A, lda, B, ldb);
}

#endif // FABULOUS_LAPACKE_NANCHECK

} // end namespace spe
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_BLAS_SPE_HPP
