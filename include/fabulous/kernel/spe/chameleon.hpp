#ifndef FABULOUS_CHAMELEON_KERNEL_SPE_HPP
#define FABULOUS_CHAMELEON_KERNEL_SPE_HPP

#include <complex>
#include <chameleon.h>

#include "fabulous/utils/Chameleon.hpp"
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Utils.hpp"

namespace fabulous {
namespace chameleon {

/*! \brief wrapper specializations for the 4 arithmetics */
namespace spe {

template<class S>
int gels(CHAM_desc_t*,CHAM_desc_t*,CHAMELEON_sequence_t*)
{
    FABULOUS_DISABLE_TEMPLATE_INSTANTIATION(S);
    return -1;
}

template<class S>
int geqrf(CHAM_desc_t*,DescPtr&,CHAMELEON_sequence_t*)
{
    FABULOUS_DISABLE_TEMPLATE_INSTANTIATION(S);
    return -1;
}

template<class S>
int unmqr(CHAMELEON_enum,CHAM_desc_t*,CHAM_desc_t*,CHAM_desc_t*,CHAMELEON_sequence_t*)
{
    FABULOUS_DISABLE_TEMPLATE_INSTANTIATION(S);
    return -1;
}

template<class S>
int trsm(CHAM_desc_t*,CHAM_desc_t*,CHAMELEON_sequence_t*)
{
    FABULOUS_DISABLE_TEMPLATE_INSTANTIATION(S);
    return -1;
}

template<class S>
int lacpy(CHAMELEON_enum,CHAM_desc_t*,CHAM_desc_t*,CHAMELEON_sequence_t*)
{
    FABULOUS_DISABLE_TEMPLATE_INSTANTIATION(S);
    return -1;
}

/*************************************/
/*************** GELS ****************/

#define FABULOUS_SPECIALIZE_CHAM_GELS(_1, type_, _3, _4, _5, _6, prefix_, ...) \
    template<>                                                          \
    inline int gels<type_>(                                             \
        CHAM_desc_t *A, CHAM_desc_t *B, CHAMELEON_sequence_t *seq)        \
    {                                                                   \
        CHAMELEON_request_t r = CHAMELEON_REQUEST_INITIALIZER;                  \
        CHAM_desc_t *T = nullptr;                                      \
        CHAMELEON_Alloc_Workspace_##prefix_##gels_Tile(A->lm, A->ln, &T, 1, 1); \
        int err = CHAMELEON_##prefix_##gels_Tile_Async(ChamNoTrans, A, T, B, seq, &r); \
        /* probably UB to del this here in aync mode */                 \
        CHAMELEON_Desc_Destroy(&T);                                         \
        return err;                                                     \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_CHAM_GELS)

/*************************************/
/**************** QRF ****************/

#define FABULOUS_SPECIALIZE_CHAM_GEQRF(_1, type_, _3, _4, _5, _6, prefix_, ...) \
    template<>                                                          \
    inline int geqrf<type_>(                                            \
        CHAM_desc_t *A, DescPtr &tau, CHAMELEON_sequence_t *seq)           \
    {                                                                   \
        CHAMELEON_request_t r = CHAMELEON_REQUEST_INITIALIZER;                  \
        CHAM_desc_t *T = nullptr;                                      \
        CHAMELEON_Alloc_Workspace_##prefix_##geqrf_Tile(A->lm, A->ln, &T, 1, 1); \
        tau.reset(T, DescDeleter{});                                    \
        return CHAMELEON_##prefix_##geqrf_Tile_Async(A, T, seq, &r);        \
    }

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_CHAM_GEQRF)

/*************************************/
/*************** MQR *****************/

#define FABULOUS_SPECIALIZE_CHAM_MQR(_1, type_, _3, _4, _5, _6, prefix_, unitary_, ...) \
    template<>                                                          \
    inline int unmqr<type_>(                                            \
        CHAMELEON_enum trans,                                               \
        CHAM_desc_t *A, CHAM_desc_t *T, CHAM_desc_t *C,              \
        CHAMELEON_sequence_t *seq)                                          \
    {                                                                   \
        CHAMELEON_request_t r = CHAMELEON_REQUEST_INITIALIZER;                  \
        return CHAMELEON_##prefix_##unitary_##mqr_Tile_Async(               \
            ChamLeft, trans, A, T, C, seq, &r                          \
        );                                                              \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_CHAM_MQR)

/*************************************/
/************** TRSM *****************/

#define FABULOUS_SPECIALIZE_CHAM_TRSM(_1, type_, _3, _4, _5, _6, prefix_, ...) \
    template<>                                                          \
    inline int trsm<type_>(                                             \
        CHAM_desc_t *A, CHAM_desc_t *B, CHAMELEON_sequence_t *seq)        \
    {                                                                   \
        CHAMELEON_request_t r = CHAMELEON_REQUEST_INITIALIZER;                  \
        return CHAMELEON_##prefix_##trsm_Tile_Async(                        \
            ChamLeft, ChamUpper,                                      \
            ChamNoTrans, ChamNonUnit,                                 \
            1.0f, A, B, seq, &r                                         \
        );                                                              \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_CHAM_TRSM)

/*************************************/
/************** LACPY ****************/

#define FABULOUS_SPECIALIZE_CHAM_LACPY(_1, type_, _3, _4, _5, _6, prefix_, ...) \
    template<>                                                          \
    inline int lacpy<type_>(                                            \
        CHAMELEON_enum uplo, CHAM_desc_t *A, CHAM_desc_t *B, CHAMELEON_sequence_t *seq) \
    {                                                                   \
        CHAMELEON_request_t r = CHAMELEON_REQUEST_INITIALIZER;                  \
        return CHAMELEON_##prefix_##lacpy_Tile_Async(uplo, A, B, seq, &r);  \
    }                                                                   \

FABULOUS_ARITHMETIC_LIST(FABULOUS_SPECIALIZE_CHAM_LACPY)

} // end namespace spe
} // end namespace chameleon
} // end namespace fabulous

#endif // FABULOUS_CHAMELEON_KERNEL_SPE_HPP
