#ifndef FABULOUS_GGEV_SPE_HPP
#define FABULOUS_GGEV_SPE_HPP

#include <complex>
#include <vector>

#include "fabulous/ext/cblas.h"
#include "fabulous/ext/lapacke.h"

namespace fabulous {
namespace lapacke {
namespace spe {

#ifdef FABULOUS_LAPACKE_NANCHECK

// Real versions, with a tricks to split the complex vector into 2 real's one
inline int ggev(int n,
                float *A, int lda,
                float *B, int ldb,
                std::complex<float> *alpha,
                float *beta,
                float *vl, int ldvl,
                float *vr, int ldvr)
{
    // temporary real vectors to hold real part and imag part of alpha
    std::vector<float> alphar, alphai;
    alphar.resize(n, 0.0);
    alphai.resize(n, 0.0);

    int out = LAPACKE_sggev(LAPACK_COL_MAJOR,'N','V',
                            n, A, lda,
                            B, ldb,
                            alphar.data(), alphai.data(),
                            beta,
                            vl,ldvl,
                            vr,ldvr);
    std::complex<float> J{0,1};
    for (int i=0; i<n; ++i) // Copy back inside alpha
        alpha[i] = alphar[i] + J * alphai[i];
    return out;
}

// Real versions, with a tricks to split the complex vector into 2 real's one
inline int ggev(int n,
                double *A, int lda,
                double *B, int ldb,
                std::complex<double> *alpha,
                double *beta,
                double *vl, int ldvl ,
                double *vr, int ldvr)
{
    // temporary real vectors to hold real part and imag part of alpha
    std::vector<double> alphar, alphai;
    alphar.resize(n, 0.0);
    alphai.resize(n, 0.0);

    int out = LAPACKE_dggev(
        LAPACK_COL_MAJOR, 'N', 'V',
        n, A, lda,
        B, ldb,
        alphar.data(), alphai.data(),
        beta,
        vl, ldvl,
        vr, ldvr
    );
    std::complex<double> J{0, 1};
    for (int i=0; i<n; ++i) // Copy back inside alpha
        alpha[i] = alphar[i] + J * alphai[i];

    return out;
}

inline int ggev(int n,
                std::complex<float> *A, int lda,
                std::complex<float> *B, int ldb,
                std::complex<float> *alpha,
                std::complex<float> *beta,
                std::complex<float> *vl, int ldvl,
                std::complex<float> *vr, int ldvr)
{
    return LAPACKE_cggev(LAPACK_COL_MAJOR,'N','V', n,
                         A, lda,
                         B, ldb,
                         alpha,beta,
                         vl,ldvl,
                         vr,ldvr);
}

inline int ggev(int n,
                std::complex<double> *A, int lda,
                std::complex<double> *B, int ldb,
                std::complex<double> *alpha,
                std::complex<double> *beta,
                std::complex<double> *vl, int ldvl,
                std::complex<double> *vr, int ldvr)
{
    return LAPACKE_zggev(LAPACK_COL_MAJOR,'N','V', n,
                         A,lda,
                         B,ldb,
                         alpha,beta,
                         vl,ldvl,
                         vr,ldvr);

}

#else // FABULOUS_LAPACKE_NANCHECK

// Real versions, with a tricks to split the complex vector into 2 real's one
inline int ggev(int n, float *A, int lda,
                float *B, int ldb,
                std::complex<float> *alpha,
                float *beta,
                float *vl, int ldvl,
                float *vr, int ldvr)
{
    std::vector<float> ar, ai;
    ar.resize(n); ai.resize(n);

    lapack_int info = 0;
    lapack_int lwork = -1;
    float *work = nullptr;
    float work_query;
    info = LAPACKE_sggev_work(LAPACK_COL_MAJOR,'N','V',
                              n, A, lda, B, ldb, ar.data(), ai.data(),
                              beta, vl, ldvl, vr, ldvr, &work_query, lwork);
    if ( info != 0 )
        return info;
    lwork = (lapack_int) work_query;
    work = (float*) LAPACKE_malloc(sizeof(float) * lwork );
    if ( work == NULL )
        return LAPACK_WORK_MEMORY_ERROR;
    info = LAPACKE_sggev_work(LAPACK_COL_MAJOR,'N','V',
                              n, A, lda, B, ldb, ar.data(), ai.data(),
                              beta, vl, ldvl, vr, ldvr, work, lwork);
    std::complex<float> J{0,1};
    for (int i=0; i<n; ++i) // Copy back inside alpha
        alpha[i] = ar[i] + J * ai[i];
    LAPACKE_free( work );
    return info;
}


// Real versions, with a tricks to split the complex vector into 2 real's one
inline int ggev(int n, double *A, int lda,
                double *B, int ldb,
                std::complex<double> *alpha,
                double *beta,
                double *vl, int ldvl,
                double *vr, int ldvr)
{
    std::vector<double> ar, ai;
    ar.resize(n); ai.resize(n);

    lapack_int info = 0;
    lapack_int lwork = -1;
    double *work = nullptr;
    double work_query;
    info = LAPACKE_dggev_work(LAPACK_COL_MAJOR,'N','V',
                              n, A, lda, B, ldb, ar.data(), ai.data(),
                              beta, vl, ldvl, vr, ldvr, &work_query, lwork);
    if ( info != 0 )
        return info;
    lwork = (lapack_int) work_query;
    work = (double*) LAPACKE_malloc(sizeof(double) * lwork );
    if ( work == NULL )
        return LAPACK_WORK_MEMORY_ERROR;
    info = LAPACKE_dggev_work(LAPACK_COL_MAJOR,'N','V',
                              n, A, lda, B, ldb, ar.data(), ai.data(),
                              beta, vl, ldvl, vr, ldvr, work, lwork);
    std::complex<double> J{0,1};
    for (int i=0; i<n; ++i) // Copy back inside alpha
        alpha[i] = ar[i] + J * ai[i];
    LAPACKE_free( work );
    return info;
}

// complex versions

inline int ggev(int n,
                std::complex<float> *A, int lda,
                std::complex<float> *B, int ldb,
                std::complex<float> *alpha,
                std::complex<float> *beta,
                std::complex<float> *vl, int ldvl,
                std::complex<float> *vr, int ldvr)
{
    lapack_int info = 0;
    lapack_int lwork = -1;
    float *rwork = nullptr;
    std::complex<float> *work = nullptr;
    std::complex<float> work_query;

    rwork = (float*) LAPACKE_malloc( sizeof(float) * std::max(1, 8*n) );
    if ( rwork == nullptr )
        return LAPACK_WORK_MEMORY_ERROR;

    info = LAPACKE_cggev_work(LAPACK_COL_MAJOR,'N','V',
                              n, A, lda, B, ldb, alpha, beta,
                              vl, ldvl, vr, ldvr, &work_query, lwork, rwork);
    if ( info != 0 ) {
        LAPACKE_free( rwork );
        return info;
    }
    lwork = (lapack_int) *((float*)&work_query);
    work = (std::complex<float>*) LAPACKE_malloc( sizeof(std::complex<float>) * lwork );
    if ( work == NULL ) {
        LAPACKE_free( rwork );
        return LAPACK_WORK_MEMORY_ERROR;
    }
    info = LAPACKE_cggev_work(LAPACK_COL_MAJOR,'N','V',
                              n, A, lda, B, ldb, alpha, beta,
                              vl, ldvl, vr, ldvr, work, lwork, rwork );
    LAPACKE_free( work );
    return info;
}

inline int ggev(int n,
                std::complex<double> *A, int lda,
                std::complex<double> *B, int ldb,
                std::complex<double> *alpha,
                std::complex<double> *beta,
                std::complex<double> *vl, int ldvl,
                std::complex<double> *vr, int ldvr)
{
    lapack_int info = 0;
    lapack_int lwork = -1;
    double *rwork = nullptr;
    std::complex<double> *work = nullptr;
    std::complex<double> work_query;

    rwork = (double*) LAPACKE_malloc( sizeof(double) * std::max(1, 8*n) );
    if ( rwork == nullptr )
        return LAPACK_WORK_MEMORY_ERROR;

    info = LAPACKE_zggev_work(LAPACK_COL_MAJOR,'N','V',
                              n, A, lda, B, ldb, alpha, beta,
                              vl, ldvl, vr, ldvr, &work_query, lwork, rwork);
    if ( info != 0 ) {
        LAPACKE_free( rwork );
        return info;
    }
    lwork = (lapack_int) *((double*)&work_query);
    work = (std::complex<double>*) LAPACKE_malloc( sizeof(std::complex<double>) * lwork );
    if ( work == NULL ) {
        LAPACKE_free( rwork );
        return LAPACK_WORK_MEMORY_ERROR;
    }
    info = LAPACKE_zggev_work(LAPACK_COL_MAJOR,'N','V',
                              n, A, lda, B, ldb, alpha, beta,
                              vl, ldvl, vr, ldvr, work, lwork, rwork );
    LAPACKE_free( work );
    return info;
}

#endif // FABULOUS_LAPACKE_NANCHECK

} // end namespace spe
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_GGEV_SPE_HPP
