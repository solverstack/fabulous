#ifndef FABULOUS_KERNEL_CHOLESKY_SPE_HPP
#define FABULOUS_KERNEL_CHOLESKY_SPE_HPP

#include "fabulous/ext/lapacke.h"

namespace fabulous {
namespace lapacke {
namespace spe {

#ifdef FABULOUS_LAPACKE_NANCHECK

// POTRF

inline int potrf(char uplo, int n, float *A, int lda)
{
    return LAPACKE_spotrf(LAPACK_COL_MAJOR, uplo, n, A, lda);
}

inline int potrf(char uplo, int n, double *A, int lda)
{
    return LAPACKE_dpotrf(LAPACK_COL_MAJOR, uplo, n, A, lda);
}

inline int potrf(char uplo, int n, std::complex<float> *A, int lda)
{
    return LAPACKE_cpotrf(LAPACK_COL_MAJOR, uplo, n, A, lda);
}

inline int potrf(char uplo, int n, std::complex<double> *A, int lda)
{
    return LAPACKE_zpotrf(LAPACK_COL_MAJOR, uplo, n, A, lda);
}

// POTRS


inline int potrs(char uplo, int n, int nrhs,
                 const float *A, int lda,
                 /**/  float *B, int ldb)
{
    return LAPACKE_spotrs(LAPACK_COL_MAJOR, uplo, n, nrhs, A, lda, B, ldb);
}

inline int potrs(char uplo, int n, int nrhs,
                 const double *A, int lda,
                 /**/  double *B, int ldb)
{
    return LAPACKE_dpotrs(LAPACK_COL_MAJOR, uplo, n, nrhs, A, lda, B, ldb);
}

inline int potrs(char uplo, int n, int nrhs,
                 const std::complex<float> *A, int lda,
                 /**/  std::complex<float> *B, int ldb)
{
    return LAPACKE_cpotrs(LAPACK_COL_MAJOR, uplo, n, nrhs, A, lda, B, ldb);
}

inline int potrs(char uplo, int n, int nrhs,
                 const std::complex<double> *A, int lda,
                 /**/  std::complex<double> *B, int ldb)
{
    return LAPACKE_zpotrs(LAPACK_COL_MAJOR, uplo, n, nrhs, A, lda, B, ldb);
}

#else // FABULOUS_LAPACKE_NANCHECK

// POTRF

inline int potrf(char uplo, int n, float *A, int lda)
{
    return LAPACKE_spotrf_work(LAPACK_COL_MAJOR, uplo, n, A, lda);
}

inline int potrf(char uplo, int n, double *A, int lda)
{
    return LAPACKE_dpotrf_work(LAPACK_COL_MAJOR, uplo, n, A, lda);
}

inline int potrf(char uplo, int n, std::complex<float> *A, int lda)
{
    return LAPACKE_cpotrf_work(LAPACK_COL_MAJOR, uplo, n, A, lda);
}

inline int potrf(char uplo, int n, std::complex<double> *A, int lda)
{
    return LAPACKE_zpotrf_work(LAPACK_COL_MAJOR, uplo, n, A, lda);
}

// POTRS

inline int potrs(char uplo, int n, int nrhs,
                 const float *A, int lda,
                 /**/  float *B, int ldb)
{
    return LAPACKE_spotrs_work(LAPACK_COL_MAJOR, uplo, n, nrhs, A, lda, B, ldb);
}

inline int potrs(char uplo, int n, int nrhs,
                 const double *A, int lda,
                 /**/  double *B, int ldb)
{
    return LAPACKE_dpotrs_work(LAPACK_COL_MAJOR, uplo, n, nrhs, A, lda, B, ldb);
}

inline int potrs(char uplo, int n, int nrhs,
                 const std::complex<float> *A, int lda,
                 /**/  std::complex<float> *B, int ldb)
{
    return LAPACKE_cpotrs_work(LAPACK_COL_MAJOR, uplo, n, nrhs, A, lda, B, ldb);
}

inline int potrs(char uplo, int n, int nrhs,
                 const std::complex<double> *A, int lda,
                 /**/  std::complex<double> *B, int ldb)
{
    return LAPACKE_zpotrs_work(LAPACK_COL_MAJOR, uplo, n, nrhs, A, lda, B, ldb);
}

#endif // FABULOUS_LAPACKE_NANCHECK

} // end namespace spe
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_KERNEL_CHOLESKY_SPE_HPP
