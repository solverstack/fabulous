#ifndef FABULOUS_SVD_HPP
#define FABULOUS_SVD_HPP

#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/kernel/spe/svd.hpp"

namespace fabulous {
namespace lapacke {

/**
 * Computation of SVD
 *
 * Proto lapack_int LAPACKE_cgesvd( int matrix_layout, char jobu,
 * char jobvt, lapack_int m, lapack_int n, lapack_complex_float* a,
 * lapack_int lda, float* s, lapack_complex_float* u, lapack_int ldu,
 * lapack_complex_float* vt, lapack_int ldvt, float* superb );
 *
 * Ref: https://software.intel.com/en-us/node/521150
 */
template<class S, class P,
         class = enable_if_t<std::is_same<P, typename Arithmetik<S>::primary_type>::value> >
int gesvd(char jobu, char jobvt,
          int M, int N,
          S *A, int lda,
          P *Sigma,
          S *U, int ldu,
          S *V, int ldv,
          P *superb)
{
    int err = 0;
    err = spe::gesvd(jobu, jobvt, M, N, A, lda, Sigma, U, ldu, V, ldv, superb);
    return err;
}

/*********** SVD *************/

} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_SVD_HPP
