#ifndef FABULOUS_FLOPS_HPP
#define FABULOUS_FLOPS_HPP

#include <cstdint>
#include "fabulous/utils/Arithmetic.hpp"
#include "fabulous/utils/Traits.hpp"

namespace fabulous {
namespace lapacke {

/*! \brief function to compute flops for lapacke kernels */
namespace flops {

template<class S, class = enable_if_t<is_real_t<S>::value>>
inline constexpr double flop_per_mul()
{
    return 1.0;
}

template<class S, class = enable_if_t<is_complex_t<S>::value>, class=void>
inline constexpr double flop_per_mul()
{
    return 6.0;
}

template<class S, class = enable_if_t<is_real_t<S>::value>>
inline constexpr double flop_per_add()
{
    return 1.0;
}

template<class S, class = enable_if_t<is_complex_t<S>::value>, class=void>
inline constexpr double flop_per_add()
{
    return 2.0;
}

#define FABULOUS_KERNEL_FLOPS_DEFINE_MNK(kernel_, multiplications_, additions_) \
    inline constexpr double kernel_##_add(double M, double N, double K) \
    { return ((void)M, (void) N, (void) K, (additions_)); }             \
    inline constexpr double kernel_##_mul(double M, double N, double K) \
    { return ((void)M, (void) N, (void) K, (multiplications_)); }       \
    template<class S>                                                   \
    inline constexpr double kernel_(double M, double N, double K)       \
    {                                                                   \
        return flop_per_mul<S>() * kernel_##_mul(M, N, K)               \
            + flop_per_add<S>() * kernel_##_add(M, N, K);               \
    }                                                                   \

#define FABULOUS_KERNEL_FLOPS_DEFINE_MN(kernel_, multiplications_, additions_) \
    inline constexpr double kernel_##_add(double M, double N)           \
    { return ((void) M, (void) N, (additions_)); }               \
    inline constexpr double kernel_##_mul(double M, double N)           \
    { return ((void) M, (void) N, (multiplications_)); }         \
    template<class S>                                                   \
    inline constexpr double kernel_(double M, double N, double=-1.0)    \
    {                                                                   \
        return flop_per_mul<S>() * kernel_##_mul(M, N)                  \
            + flop_per_add<S>() * kernel_##_add(M, N);                  \
    }                                                                   \

#define FABULOUS_KERNEL_FLOPS_DEFINE_M(kernel_, multiplications_, additions_) \
    inline constexpr double kernel_##_add(double M)                     \
    { return ((void) M, (additions_)); }                                \
    inline constexpr double kernel_##_mul(double M)                     \
    { return ((void) M, (multiplications_)); }                          \
    template<class S>                                                   \
    inline constexpr double kernel_(double M, double=-1.0,double=-1.0)  \
    {                                                                   \
        return flop_per_mul<S>() * kernel_##_mul(M)                     \
            + flop_per_add<S>() * kernel_##_add(M);                     \
    }                                                                   \

FABULOUS_KERNEL_FLOPS_DEFINE_MNK(gemm, M*N*K, M*N*K)
FABULOUS_KERNEL_FLOPS_DEFINE_MN(trmm, M*M*N/2., M*M*N/2.)

FABULOUS_KERNEL_FLOPS_DEFINE_M(axpy, M, M)
FABULOUS_KERNEL_FLOPS_DEFINE_M(dot, M, M)
FABULOUS_KERNEL_FLOPS_DEFINE_M(
    scal,
    M,
    ((void) M, 0.0)
)
FABULOUS_KERNEL_FLOPS_DEFINE_MN(
    geqrf,
    ((M>N)
     ? (N * (N * (  0.5-(1./3.) * N + M) + M + 23. / 6.))
     : (M * (M * ( -0.5-(1./3.) * M + N) + 2.*N + 23. / 6.))),
    ((M>N)
     ? (N * (N * ( 0.5-(1./3.)*N + M) + 5./6.))
     : (M * (M * (-0.5-(1./3.)*M + N) + N +  5. / 6.)))
)

FABULOUS_KERNEL_FLOPS_DEFINE_MNK(
    ungqr,
    (K * (2.* M*N + 2. * N - 5./3. + K * ( 2./3. * K - (M + N) - 1.))),
    (K * (2.* M*N + N - M + 1./3. + K * ( 2./3. * K - (M + N)     )))
)

FABULOUS_KERNEL_FLOPS_DEFINE_MNK(
    unmqr,
    (K * N * (2.*M - K + 2.)),
    (K * N * (2.*M - K + 1.))
)

FABULOUS_KERNEL_FLOPS_DEFINE_MNK(
    right_unmqr,
    (K * (2.*M*N - M*K + M + N - 0.5*K + 0.5)),
    (K * (2.*M*N - M*K + M))
)

FABULOUS_KERNEL_FLOPS_DEFINE_MN(
    trsm,
    (N*M*(M + 1.0) * 0.5 ),
    (N*M*(M - 1.0) * 0.5 )
)

FABULOUS_KERNEL_FLOPS_DEFINE_MN(
    right_trsm,
    (M*N*(N + 1.0) * 0.5 ),
    (M*N*(N - 1.0) * 0.5 )
)

FABULOUS_KERNEL_FLOPS_DEFINE_MN(
    lacpy,
    (0.0),
    (0.0)
)

FABULOUS_KERNEL_FLOPS_DEFINE_M(
    potrf,
    (M * (M * ((1./2.) + (1./6)*M) + (1./3.))),
    (M * ((1./6.)*M*M  - (1./6.)))
)

FABULOUS_KERNEL_FLOPS_DEFINE_MN(
    potrs,
    (N * (M*M + M)),
    (N * (M*M - M))
)

FABULOUS_KERNEL_FLOPS_DEFINE_M(
    ttqrt,
    (0.0),
    (M*M*M*(2./3.))
)

FABULOUS_KERNEL_FLOPS_DEFINE_M(
    ttmqrt,
    (0.0),
    (M*M*M*(6./3.))
)

FABULOUS_KERNEL_FLOPS_DEFINE_MN(
    tsqrt,
    (0.0),
    (M*N*N*(6./3.))
)

FABULOUS_KERNEL_FLOPS_DEFINE_MNK(
    tsmqrt,
    (0.0),
    (M*N*K*(12./3.))
)

template<class S>
inline constexpr double gels(double M, double N, double NRHS)
{
    return geqrf<S>(M, N) + unmqr<S>(M, NRHS, N) + trsm<S>(N, NRHS);
}

} // end namespace flops
} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_FLOPS_HPP
