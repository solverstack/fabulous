#ifndef FABULOUS_CHAMELEON_KERNEL_HPP
#define FABULOUS_CHAMELEON_KERNEL_HPP

#include "fabulous/utils/Logger.hpp"
#include "fabulous/kernel/spe/chameleon.hpp"

namespace fabulous {

/**
 * \brief Generic (templatized) wrappers for Chameleon (MORSE) kernels
 *
 * This is the interface for the '*_Tile_Async' functions of chameleon.
 */
namespace chameleon {

/**
 * \brief Solve Least square problem \f$ A*Y = B \f$
 * \param[in,out] A the A matrix, overwritten with factoization data
 * \param[in,out] B the right hand sides, overwritten with the solution
 * and the residual
 * \param seq MORSE sequence object
 */
template<class S>
int gels(CHAM_desc_t *A, CHAM_desc_t *B, CHAMELEON_sequence_t *seq)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::gels<S>(A, B, seq);
    FABULOUS_END_KERNEL_PERF(gels, S, A->m, A->n, B->n, 0);
    return err;
}

/**
 * \brief QR-Factorisation
 * \param[in,out] A matrix to factorize
 * \param[out] tau output extra factorization data
 * \param seq MORSE sequence object
 */
template<class S>
int geqrf(CHAM_desc_t *A, DescPtr &tau, CHAMELEON_sequence_t *seq)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::geqrf<S>(A, tau, seq);
    FABULOUS_END_KERNEL_PERF(geqrf, S, A->m, A->n, 0, 0);
    return err;
}

/**
 * \brief Apply Q to a matrix C
 * where Q is the orthogonal part obtained of QR factorization
 *
 * \f$ C := Q * C \f$ <br/>
 * \f$ C := Q^{T} * C \f$ <br/>
 * \f$ C := Q^{H} * C \f$
 *
 * \param trans Can be ChamNoTrans or ChamTrans or ChamConjTrans
 * to know if we need  Q*C Q*{T} * C or Q^{H}*C
 * \param[in] A matrix as returned by geqrf
 * (Q is computed with Householders reflectors from A)
 * \param[in] T extra factorization data returned by GEQRF
 * \param[in,out] C the C matrix to be multiplied
 * \param seq MORSE sequence object
 */
template<class S>
int unmqr(CHAMELEON_enum trans,
          CHAM_desc_t *A, CHAM_desc_t *T, CHAM_desc_t *C,
          CHAMELEON_sequence_t *seq)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::unmqr<S>(trans, A, T, C, seq);
    FABULOUS_END_KERNEL_PERF(unmqr, S, C->m, C->n, A->n, 0);
    return err;
}

/**
 * \brief Solve the system \f$AX=B\f$ with A upper triangular
 *
 * \param A descriptor to A
 * \param B descriptor to B
 * \param seq MORSE sequence object
 *
 * Ref : https://software.intel.com/en-us/node/520907
 */
template<class S>
int trsm(CHAM_desc_t *A, CHAM_desc_t *B, CHAMELEON_sequence_t *seq)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::trsm<S>(A, B, seq);
    FABULOUS_END_KERNEL_PERF(trsm, S, A->m, B->n, 0, 0);
    return err;
}

/**
 * \brief copy matrix A to matrix B
 * \param uplo either ChamLower(lower triangular matrix is copied),
 * ChamUpper (upper triangular matrix is copied) or ChamUpperLower*
 * (whole matrix is copied)
 * \param A the A matrix
 * \param B the B matrix
 * \param seq a MORSE sequence object
 */
template<class S>
int lacpy(CHAMELEON_enum uplo,
          CHAM_desc_t *A, CHAM_desc_t *B, CHAMELEON_sequence_t *seq)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::lacpy<S>(uplo, A, B, seq);
    FABULOUS_END_KERNEL_PERF(lacpy, S, A->m, B->n, 0, 0);
    return err;
}

} // end namespace chameleon
} // end namespace fabulous

#endif // FABULOUS_CHAMELEON_KERNEL_HPP
