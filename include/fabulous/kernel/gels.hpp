#ifndef FABULOUS_GELS_HPP
#define FABULOUS_GELS_HPP

#include "fabulous/utils/Logger.hpp"
#include "fabulous/kernel/spe/gels.hpp"

namespace fabulous {
namespace lapacke {

/**
 * \brief Solve Least square problem
 *
 *   solve the least square problem \f$ X = argmin(|| A*X - B ||) \f$
 *
 * \param M number of lines in A
 * \param N number of column in A
 * \param NRHS number of column in B
 * \param[in,out] A the matrix. Overwritten with factorization data.
 * \param lda leading dimension of A
 * \param[in,out] B input: the right hand size <br/>
 *                  output: the solution and the residual
 * \param ldb leading dimension of B
 */
template<class S>
int gels(int M, int N, int NRHS, S *A, int lda, S *B, int ldb)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::gels(M, N, NRHS, A, lda, B, ldb);
    FABULOUS_END_KERNEL_PERF(gels, S, M, N, NRHS, 0);
    return err;
}

} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_GELS_HPP
