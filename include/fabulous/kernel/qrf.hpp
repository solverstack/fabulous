#ifndef FABULOUS_QRF_HPP
#define FABULOUS_QRF_HPP

#include <vector>
#include "fabulous/utils/Logger.hpp"
#include "fabulous/kernel/spe/qrf.hpp"

namespace fabulous {
namespace lapacke {

/******************* BASIC QR FACTORIZATION ************************/

/**
 * QR-Factorisation (full)
 * \param M number of lines
 * \param N number of col
 * \param[in,out] A data to factorize
 * \param lda leading dim of a
 * \param[out] tau on output: extra factorization data (needed for unmqr and ungqr)
 */
template<class S>
int geqrf(int M, int N, S *A, int lda, std::vector<S> &tau)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::geqrf(M, N, A, lda, tau);
    FABULOUS_END_KERNEL_PERF(geqrf, S, M, N, 0, 0);
    return err;
}

/**
 * \brief Generation of Orthogonal part after QR
 *
 * ref: https://software.intel.com/en-us/node/521010
 *
 * \param M number of lines in A
 * \param N number of columns in Q
 * \param K number of householder reflectors
 * \param[in,out] A the matrix with factorization data from GEQRF
 * \param lda leading dim of matrix A
 * \param[in] tau extra factorization data as returned by geqrf
 */
template<class S>
int ungqr(int M, int N, int K, S *A, int lda, S *tau)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::ungqr(M, N, K, A, lda, tau);
    FABULOUS_END_KERNEL_PERF(ungqr, S, M, N, K, 0);
    return err;
}

/**
 * \brief Apply Q to a matrix C where Q is the orthogonal part
 *  obtained from Lapack GEQRF call
 *
 * \param trans char Can be 'N' or 'T', to know if we need Q^{H}*C or Q*C.
 * \param M Integer number of rows in C
 * \param N Integer number of columns in C
 * \param K Integer Number of elementary reflectors in Q
 * \param A array returned by geqrf
 * \param lda leading dim of a
 * \param tau array returned by geqrf
 * \param C the MxN matrix to be multiplied
 * \param ldc leading dimension of C
 *
 * Ref: https://software.intel.com/en-us/node/521011
 *
 * lapack_int LAPACKE_sunmqr (int matrix_layout, char side, char
 * trans, lapack_int m, lapack_int n, lapack_int k, const float* a,
 * lapack_int lda, const float* tau, float* c, lapack_int ldc);
 */
template<class S>
int unmqr(char trans, int M, int N, int K,
          const S *A, int lda, const S *tau, S *C, int ldc)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = spe::unmqr(trans, M, N, K, A, lda, tau, C, ldc);
    FABULOUS_END_KERNEL_PERF(unmqr, S, M, N, K, 0);
    return err;
}

} // end namespace lapacke

} // end namespace fabulous


#endif // FABULOUS_QRF_HPP
