#ifndef FABULOUS_QR_HPP
#define FABULOUS_QR_HPP

#include <cassert>

#include "fabulous/utils/Utils.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/qrf.hpp"
#include "fabulous/kernel/flops.hpp"

namespace fabulous {

/**
 * \brief methods that apply on Blocks (QR factorization)
 */
namespace qr {

/**
 * \brief compute QR factorization of user-distribution Block
 *
 *  This factorization use the Gram-Schmidt method
 *
 * \param[in,out] Q the orthogonal part of the QR factorization
 * \param[out] R the R part of the QR factorization
 * \param[in] dot Callback for DotProduct method
 *
 * \note R MUST BE already allocated ( Q.get_nb_col() x Q.get_nb_col() )
 */
template<class Dot, class S>
int64_t InPlaceQRFactoMGS_User(Block<S> &Q, Block<S> &R, const Dot &dot)
{
    namespace fps = lapacke::flops;

    int M = Q.get_nb_row();
    int N = Q.get_nb_col();
    int LD = Q.get_leading_dim();

    if (R.get_nb_col() != 0) {
        FABULOUS_ASSERT( R.get_nb_col() == Q.get_nb_col() );
        FABULOUS_ASSERT( R.get_nb_col() == R.get_nb_row() );
    }

    int64_t nb_flops = 0;

    for (int j = 0; j < N; ++j) {
        for (int i = 0; i < j; ++i) {
            S val; // dot = DOT(Q_i, Q_j)
            dot(1, 1,
                Q.get_vect(i), LD,
                Q.get_vect(j), LD,
                &val, 1);

            // Qj = Qj - dot(Qj,Qi)*Qi
            lapacke::axpy(M, -val, Q.get_vect(i), 1, Q.get_vect(j), 1);
            nb_flops += fps::axpy<S>(M);
            if (R.get_nb_col() != 0)
                R(i, j) = val;
        }
        auto n = Q.norm(j, dot);
        nb_flops += fps::dot<S>(M);
        if (R.get_nb_col() != 0) {
            R(j, j) = n;
        }
        if (n == 0.0) {
            #ifndef FABULOUS_IB_EXACT_BREAKDOWN_CONTINUE
            // FABULOUS_DEBUG("column [j="<<j<<"]");
            // FABULOUS_THROW(
            //     Numeric,
            //     "Rank loss in block candidate for extending the Krylov Space"
            // );
            #else
            FABULOUS_WARNING("Rank loss in block candidate for extending the Krylov Space");
            FABULOUS_WARNING("Continuing since IB can address rank loss problems");
            #endif
        } else {
            lapacke::scal(M, S{1.0} / n, Q.get_vect(j), 1);
            nb_flops += fps::scal<S>(M);
        }
        // Qj = (1/n)Qj
    }
    return nb_flops;
}

/**
 * \brief Compute QR factorization
 *
 * Compute QR factorization using Lapack kernels.
 * The first parameter Q is the input matrix and will be overwritten with the
 * Q factor of the factorization.
 *
 * \param[in,out] Q when the method returns: the Q part of the factorization
 * \param[out] R the R part of the factorization
 *       it is assumed either the user allocated a 'R' Block such as:
 *                 R.get_nb_col() == this->get_nb_col() and
 *                 R.get_nb_row() == this->get_nb_col()
 *       or the user passed a block of size (0x0) and R will no be computed
 */
template<class S>
int64_t InPlaceQRFacto(Block<S> &Q, Block<S> &R)
{
    int M = Q.get_nb_row();
    int N = Q.get_nb_col();
    int LD = Q.get_leading_dim();

    if ( R.get_nb_col() != 0 ) {
        FABULOUS_ASSERT( R.get_nb_col() == Q.get_nb_col() );
        FABULOUS_ASSERT( R.get_nb_row() == Q.get_nb_col() );
    }

    namespace fps = lapacke::flops;
    std::vector<S> tau;
    lapacke::geqrf(M, N, Q.get_ptr(), LD, tau);
    int64_t nb_flops = fps::geqrf<S>(M, N);
    if (R.get_nb_col() != 0) {
        lapacke::lacpy( 'U', M, N, Q.get_ptr(), LD,
                        R.get_ptr(), R.get_leading_dim());
    }
    lapacke::ungqr(M, N, N, Q.get_ptr(), LD, tau.data());
    nb_flops += fps::ungqr<S>(M, N, N);
    return nb_flops;
}

template<class S>
int64_t OutOfPlaceQRFacto(const Block<S> &B, Block<S> &Q, Block<S> &R)
{
    FABULOUS_ASSERT( &Q != &B );
    FABULOUS_ASSERT( Q.get_ptr() != B.get_ptr() );
    Q.copy(B);
    return InPlaceQRFacto(Q, R);
}

} // end namespace qr
} // end namespace fabulous

#endif // FABULOUS_QR_HPP
