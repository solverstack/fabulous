#ifndef FABULOUS_TPQRT_HPP
#define FABULOUS_TPQRT_HPP

#include "fabulous/utils/Logger.hpp"
#include "fabulous/kernel/spe/tpqrt.hpp"

namespace fabulous {
namespace lapacke {

/****** TILED TRIANGULAR over PENTAGONAL QR FACTORIZATION (for QR+IB(+DR)) *********/

/**
 * \brief TRIANGULAR-over-PENTAGONAL QR-Factorisation (full)
 * \param M number of lines
 * \param N number of col
 * \param L the size of the trapezoidal part
 * \param IB tile size for the tiled factorization algorithm
 * \param[in,out] A upper triangular matrix N-by-N A
 * \param lda leading dim of A
 * \param[in,out] B pentagonal M-by-N matrix, overwritten with V
 * \param ldb leading dim of B
 * \param[out] T on output: extra factorization data (needed for ormqr and orgqr)
 *
 * Ref: https://software.intel.com/en-us/node/521034
 */
template<class S>
int tpqrt(int M, int N, int L, int IB, S *A, int lda, S *B, int ldb, std::vector<S> &T)
{
    return spe::tpqrt(M, N, L, IB, A, lda, B, ldb, T);
}

/**
 * \brief TRIANGULAR-over-PENTAGONAL QR UPDATE kernel
 *
 * Apply Q to a matrix B where Q is the
 * orthogonal part obtained from Lapack GEQRF call
 *
 * \param Trans char Can be 'N' or 'T', to know if we need Q^{H}*C or Q*C.
 * \param M Integer number of rows in C
 * \param N Integer number of columns in C
 * \param K Integer Number of elementary reflectors in Q
 * \param L the size of the trapezoidal part
 * \param IB same block size that was passed to the tpqrt kernel
 * \param V array returned by tpqrt
 * \param ldv leading dim of V
 * \param A array returned by tpqrt
 * \param lda leading dim of A
 * \param T tau factor as returned by tpqrt
 * \param ldt leading dimension of T
 * \param B the MxN matrix to be multiplied
 * \param ldb leading dimension of B
 *
 * Ref: https://software.intel.com/en-us/node/521035
 */
template<class S>
int tpmqrt(char Trans, int M, int N, int K, int L, int IB,
           const S *V, int ldv,
           const S *T, int ldt,
           S *A, int lda,
           S *B, int ldb)
{
    return spe::tpmqrt(Trans, M, N, K, L, IB, V, ldv, T, ldt, A, lda, B, ldb);
}

/** TS and TT wrapper */

template<class S>
int tsqrt(int M, int N, int IB,
          S *A, int lda,
          S *B, int ldb, std::vector<S> &T)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = tpqrt(M, N, 0, IB, A, lda, B, ldb, T);
    FABULOUS_END_KERNEL_PERF(tsqrt, S, M, N, 0, IB);
    return err;
}

template<class S>
int tsmqrt(char trans, int M, int N, int K, int IB,
           const S *V, int ldv,
           const S *T, int ldt,
           S *A, int lda,
           S *B, int ldb)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = tpmqrt(trans, M, N, K, 0, IB, V, ldv, T, ldt, A, lda, B, ldb);
    FABULOUS_END_KERNEL_PERF(tsmqrt, S, M, N, K, IB);
    return err;
}

template<class S>
int ttqrt(int M, int IB,
          S *A, int lda,
          S *B, int ldb, std::vector<S> &T)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = tpqrt(M, M, M, IB, A, lda, B, ldb, T);
    FABULOUS_END_KERNEL_PERF(ttqrt, S, M, 0, 0, IB);
    return err;
}

template<class S>
int ttmqrt(char trans, int M, int N, int K, int IB,
           const S *V, int ldv,
           const S *T, int ldt,
           S *A, int lda,
           S *B, int ldb)
{
    int err = 0;
    FABULOUS_BEGIN_KERNEL_PERF;
    err = tpmqrt(trans, M, N, K, K, IB, V, ldv, T, ldt, A, lda, B, ldb);
    FABULOUS_END_KERNEL_PERF(ttmqrt, S, M, N, K, IB);
    return err;
}

} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_TPQRT_HPP
