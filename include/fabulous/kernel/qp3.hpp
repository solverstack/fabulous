#ifndef FABULOUS_KERNEL_QP3_HPP
#define FABULOUS_KERNEL_QP3_HPP

#include "fabulous/utils/Logger.hpp"
#include "fabulous/kernel/spe/qp3.hpp"

namespace fabulous {
namespace lapacke {

/******************* BASIC QR FACTORIZATION ************************/

/**
 * QR-Factorisation column pivoting
 * \param M number of lines
 * \param N number of col
 * \param[in,out] A data to factorize
 * \param lda leading dim of a
 * \param[out] jpvt on exit: the column permutation
 * \param[out] tau on output: extra factorization data (needed for ormqr and orgqr)
 */
template<class S>
int geqp3(int M, int N, S *A, int lda, std::vector<int> &jpvt, std::vector<S> &tau)
{
    int err = 0;

    err = spe::geqp3(M, N, A, lda, jpvt, tau);

    return err;
}

} // end namespace lapacke
} // end namespace fabulous

#endif // FABULOUS_KERNEL_QP3_HPP
