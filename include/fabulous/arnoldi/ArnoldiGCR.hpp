#ifndef FABULOUS_ARNOLDI_GCR_HPP
#define FABULOUS_ARNOLDI_GCR_HPP

#include <limits>
#include <numeric>

namespace fabulous {
namespace bgcr {
template<class S> class Arnoldi;
}
}

#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/algo/Parameters.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/utils/ConvergenceCheck.hpp"
#include "fabulous/orthogonalization/Orthogonalizer.hpp"

namespace fabulous {
namespace bgcr {

/**
 * \brief %Arnoldi iterations
 *
 * GELS kernel is used to solve the LeastSquare problem on the projected matrix
 *
 * This class support Restarting
 */
template<class S>
class Arnoldi
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
    using P = primary_type;

private:
    void print_header(int nb_mvp, std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "#######################################################\n";
        o << "#################### Arnoldi(BGCR) ###################\n";
        o << "######## Mat Vect product scheduled "<< nb_mvp <<" ###########\n";
    }

    void print_footer(std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "################# Iterations done: "<<_iteration_count<<"(+1)\n";
        o << "#######################################################\n";
    }

private:
    Logger &_logger; /*!< logger for code profiling/performance measurement */
    const bool _quiet;

    const int _nbRHS; /*!< number of right hand sides */
    const int _max_krylov_space_size; /*!< maximum allowed size for V and Z (number of vectors) */
    int _size_krylov_space; /*!< size of base Z and V (number of vector) */
    int _nb_mvp; /*!< number of matrix vector product */
    int _iteration_count; /*!< inner loop cycle count */
    int _nb_kept_direction; /*!< number of vector in Vj and Zj */

    // computational blocking Specific
    const int _max_kept_direction;

public:
    Arnoldi(Logger &logger, int nbRHS, int max_krylov_space_size, const Parameters &param ):
        _logger{logger},
        _quiet{param.quiet},
        _nbRHS{nbRHS},
        _max_krylov_space_size{max_krylov_space_size},
        _size_krylov_space{0},
        _nb_mvp{0},
        _iteration_count{0},
        _nb_kept_direction{_nbRHS},
        _max_kept_direction{param.max_kept_direction}
    {
    }

public:
    int get_krylov_space_size() const { return _size_krylov_space; }
    int get_nb_mvp() const { return _nb_mvp; }
    int get_nb_iteration() const { return _iteration_count; }

private:

    /**
     *  \brief Save direction of X(J) who have converged into X and
     *         Compact XJ with not yet converged direction
     *         Compact RJ with not yet converged residual
     *
     *  \param A user matrix callback object
     *  \param RJ compacted R(J)
     *  \param XJ compacted X(J)
     *  \param X solution vector
     *  \param J array describing RJ and XJ layout; see comments near to J declaration in run() method
     *  \param J_work temporary array; same size of J
     */
    template<class Vector>
    void save_converged_X( const Vector &XJ,
                           /**/  Vector &X,
                           const std::vector<int> &J,
                           const std::vector<int> &J_work)
    {
        TIMER_TRACE;
        int nb_lost_direction = 0;
        // Save converged XJ into X
        if (!_quiet)
            std::cerr << "Lost direction [";
        for (int i = 0; i < _nbRHS; ++i) {
            if (J_work[i] == -1 && J[i] != -1) {
                const int k = J[i];
                if (!_quiet)
                    std::cerr << " " << i;
                ++ nb_lost_direction;
                if (!_quiet && nb_lost_direction >= 20) {
                    nb_lost_direction = 0;
                }
                std::copy(XJ.get_vect(k),
                          XJ.get_vect(k)+XJ.get_local_dim(),
                          X.get_vect(i));
            }
        }
        if (!_quiet)
            std::cerr << " ]\n";
    }

    template<class Vector>
    void compact_X_and_R( Vector &XJ, Vector &RJ,
                          const std::vector<int> &J,
                          const std::vector<int> &J_work)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( XJ.get_nb_col() >= _nb_kept_direction );
        FABULOUS_ASSERT( RJ.get_nb_col() >= _nb_kept_direction );

        int sizeJ = 0;
        // Compact XJ and RJ
        for (int i = 0; i < _nbRHS; ++i) {
            if (J_work[i] != -1) {
                ++ sizeJ;
                if (J_work[i] != J[i]) {
                    const int from = J[i];
                    const int to = J_work[i];
                    FABULOUS_ASSERT(from > to);
                    std::copy(XJ.get_vect(from),
                              XJ.get_vect(from)+XJ.get_local_dim(),
                              XJ.get_vect(to));
                    std::copy(RJ.get_vect(from),
                              RJ.get_vect(from)+XJ.get_local_dim(),
                              RJ.get_vect(to));
                }
            }
        }
        RJ = RJ.sub_vector(0, sizeJ);
        XJ = XJ.sub_vector(0, sizeJ);
    }

    int compact_little_r( Block<S> &r,
                          const std::vector<int> &J,
                          const std::vector<int> &J_work)
    {
        TIMER_TRACE;
        int sizeJ = 0;
        // Compact little r
        for (int i = 0; i < _nbRHS; ++i) {
            if (J_work[i] != -1) {
                ++ sizeJ;
                if (J_work[i] != J[i]) {
                    const int from = J[i];
                    const int to = J_work[i];
                    FABULOUS_ASSERT(from > to);
                    std::copy(r.get_vect(from),
                              r.get_vect(from)+r.get_local_dim(),
                              r.get_vect(to));
                }
            }
        }
        r = r.sub_block(0, 0, r.get_local_dim(), sizeJ);
        return sizeJ;
    }

    /**
     *  \brief DEFLATE
     *
     *  This function will compute each not yet converged residual norm from RJ
     *  If a vector have converged (its residual part norm is below threshold),
     *  its XJ part is saved back into X and removed from XJ and RJ.
     *
     *  \param A user matrix callback object
     *  \param[out] Y output value ( full rank block spanning R(J) )
     *  \param[in,out] RJ compacted R(J). On output: potentially even more compacted R(J)
     *  \param[in] J array describing RJ layout; see comments near to J declaration in run() method
     *  \param[out] J_work temporary array; same size of J
     *  \param[in] epsilon array of threshold
     *        - size==1 => same threshold for all vectors
     *        - otherwise size=nbRHS
     *  \param[in] inv_epsilon array of inverse of threshold
     *        - size==1 => same threshold for all vectors
     *        - otherwise size=nbRHS
     */
    template<class Vector>
    std::tuple<P,P,bool> deflate( Vector &Y, Vector &RJ,
                                  const std::vector<int> &J,
                                  std::vector<int> &J_work,
                                  const std::vector<P> &epsilon,
                                  const std::vector<P> &inv_epsilon )
    {
        TIMER_TRACE;
        P min = std::numeric_limits<P>::max();
        P max = 0.0;
        int nb_removed_before = 0;
        int sizeJ = RJ.get_nb_col();

        Vector Q{Y, sizeJ};
        Block<S> r{sizeJ, sizeJ};
        RJ.qr(Q, r);

        for (int j = 0; j < _nbRHS; ++j) {
            const int k = J[j];
            if (k != -1) { // compute norm of not yet converged Residual vectors
                const auto N = r.get_norm(k);
                const auto epsilon_j = epsilon.size() > 1 ? epsilon[j] : epsilon.front();
                min = std::min(min, N);
                max = std::max(max, N);

                if (N > epsilon_j) {
                    // compute new abscissa of column in RJ,XJ:
                    J_work[j] = J[j] - nb_removed_before;
                } else {
                    ++ nb_removed_before;
                    J_work[j] = -1;
                }
            } else {
                J_work[j] = -1;
            }
        }
        const int newSizeJ = compact_little_r(r, J, J_work);
        if (!_quiet && newSizeJ != sizeJ) {
            std::cerr<<Color::warning<<"### Inexact Breakdown "<<Color::reset
                     <<Color::ib_mark2<<"Mark II"<<Color::reset
                     <<Color::warning<<" Occured!! ###\n"<<Color::reset;
            std::cerr<<"Nb direction kept: "
                     << newSizeJ<<"/"<<_nbRHS
                     << " ("<<Color::error<<"-"<<(sizeJ-newSizeJ)<<Color::reset<<")\n";
        }

        int nb_kept_direction;
        if (newSizeJ != 0) {
            Block<S> U{};
            nb_kept_direction = decomposition_svd(r, U, epsilon, inv_epsilon, _max_kept_direction);
            FABULOUS_ASSERT( U.get_local_dim() == sizeJ );
            FABULOUS_ASSERT( U.get_nb_col() == nb_kept_direction );
            Y = Y.sub_vector(0, nb_kept_direction);
            Y.zero();
            Y.axpy(Q, U);
        } else {
            nb_kept_direction = 0;
        }

        const int old_nb_kept_direction = _nb_kept_direction;
        _nb_kept_direction = nb_kept_direction;
        if (!_quiet && old_nb_kept_direction != _nb_kept_direction) {
            std::cerr<<Color::warning<<"### Inexact Breakdown "<<Color::reset
                     <<Color::ib_mark3<<"Mark III"<<Color::reset
                     <<Color::warning<<" Occured!! ###\n"<<Color::reset;
            std::cerr<<"Nb direction kept: "
                     <<_nb_kept_direction<<"/"<<_nbRHS
                     << " ("<<Color::error
                     <<"-"<<(old_nb_kept_direction-_nb_kept_direction)
                     << Color::reset<<")\n";
        }
        return std::make_tuple(min, max, _nb_kept_direction == 0);
    }

    /**
     *  \brief save all vector from XJ into X
     *
     *  used when algorithm need to restart and all vector have not converged yet
     *
     *  \param XJ compacted X(J)
     *  \param X solution vector
     *  \param J array describing XJ layout; see comments near to J declaration in run() method
     */
    template<class Vector>
    void save_everyone_X(const Vector &XJ, Vector &X, std::vector<int> &J)
    {
        TIMER_TRACE;
        for (int j = 0; j < _nbRHS; ++j) {
            const int k = J[j];
            if (k != -1) {
                std::copy(XJ.get_vect(k),
                          XJ.get_vect(k)+XJ.get_local_dim(),
                          X.get_vect(j));
            }
        }
    }

    template<class Matrix, class Vector>
    void compute_residual_directly(Vector &R,
                                   const Vector &B,
                                   const Matrix &A,
                                   const Vector &X)
    {
        TIMER_TRACE;
        R.copy(B);
        A(X, R, S{-1.0}, S{1.0}); // R <- B - A*X
        _nb_mvp += X.get_nb_col();
    }

    /**
     *  \brief run initial iteration (initialization) of GCR algorithm
     *
     *  \param A user callback matrix
     *  \param X solution vector
     *  \param B right hand sides vector
     *  \param epsilon array of threshold
     *        - size==1 => same threshold for all vectors
     *        - otherwise size=nbRHS
     *  \param RJ compacted R(J)
     *  \param XJ compacted X(J)
     *  \param J array describing RJ and XJ layout; see comments near to J declaration in run() method
     *  \param J_work temporary array; same size of J
     */
    template<class Matrix, class Vector, class Epsilon>
    void initial_iteration(const Matrix &A,
                           Vector &X, const Vector &B,
                           const Epsilon &epsilon,
                           const Epsilon &inv_epsilon,
                           Vector &Y, Vector &RJ, Vector XJ,
                           std::vector<int> &J,
                           std::vector<int> &J_work)
    {
        TIMER_TRACE;
        _logger.notify_iteration_begin(_iteration_count, _size_krylov_space);
        XJ.copy(X);
        compute_residual_directly(RJ, B, A, X); // RJ <- B - A*X

        // Y <- notConverged( R(J) )
        auto MinMaxConv = deflate(Y, RJ, J, J_work, epsilon, inv_epsilon);
        save_converged_X(XJ, X, J, J_work);
        compact_X_and_R(RJ, XJ, J, J_work);
        std::swap(J, J_work); // J <- J_work

        //        _size_krylov_space = V.get_nb_vect();
        _logger.notify_iteration_end(
            _iteration_count, _size_krylov_space, _nb_mvp, MinMaxConv);
        // _logger.log_real_residual(A, B, X);
    }

    template<class Base, class Vector>
    void do_ortho(OrthoParam &orthoparm, Base &V, const Base &Z, Vector &W, Vector &Zj)
    {
        TIMER_ORTHO;
        Orthogonalizer ortho{orthoparm};
        // Orthogonalization (arnoldi process)
        ortho.run_gcr(V, Z, W, Zj, _logger, _nbRHS);
    }

    template<class Vector, class Precond>
    void apply_right_precond(Vector &Zj, const Precond &M, const Vector &W)
    {
        TIMER_TRACE;
        _logger.notify_precond_begin();
        int64_t flops = 0;
        if ( M ) {
            flops = M(W, Zj);
        } else {
            Zj.copy(W);
        }
        _logger.notify_precond_end(flops);
    }

    template<class Vector, class Matrix>
    void matrix_vector_product(Vector &W, const Matrix &A, const Vector &Zj)
    {
        TIMER_MVP;
        _logger.notify_mvp_begin();
        int64_t flops = A(Zj, W);
        _logger.notify_mvp_end(flops);
        _nb_mvp += W.get_nb_col();
    }

    /**
     *  \brief Phi <- V_j^{T} * R(J)
     */
    template<class Vector>
    void compute_phi(Block<S> &Phi, const Vector &Vj, const Vector &RJ)
    {
        TIMER_TRACE;
        const int sizeJ = RJ.get_nb_col();
        FABULOUS_ASSERT( Vj.get_nb_col() == _nb_kept_direction );
        FABULOUS_ASSERT( sizeJ >= _nb_kept_direction );
        Phi = Phi.sub_block( 0, 0, _nb_kept_direction, sizeJ );
        Vj.dot( RJ, Phi ); // Phi = Vj . RJ
    }

public:

    /**
     * \brief Arnoldi iterations, BGCR solver, right preconditionning and partial convergence management
     *
     * Solution will be stored inside X at the end of computation.
     * Ref IB-BGMRES-DR (Giraud Jing Agullo): ANNEX Section 5 p.9 Algorithm 4
     *
     * \param[in] A user matrix
     * \param[in] M user preconditioner
     * \param[in] callback user generic callback
     * \param[in,out] X initial guess (input), and best approximated solution (output)
     * \param[in] B the right hand sides
     * \param[in] V the primary base
     * \param[in] Z the secondary base
     * \param[in] epsilon tolerance (backward error) for residual
     * \param orthoparm Orthogonalizer
     * \return whether the convergence was reached
     */
    template<class Matrix, class Precond,
             class Vector, class Callback,class Base>
    bool run( const Matrix &A, const Precond &M,
              const Callback &callback,
              Vector &X, const Vector &B,
              Base &V, Base &Z,
              const std::vector<P> &epsilon,
              OrthoParam &orthoparm )
    {
        TIMER_TRACE;
        print_header(_max_krylov_space_size);
        V.reset();
        Z.reset();

        Vector RJ{X, _nbRHS}; // R(J) stored as a dense lapack col major block (see J variable)
        Vector XJ{X, _nbRHS}; // X(J) stored as a dense lapack col major block (see J variable)
        Vector Y{X, _nbRHS};
        Block<S> Phi{_nbRHS, _nbRHS};
        /* J array of size nbRHS:
         *  - [ J[k] == -1 ] <=> [ k \notin J ]
         *  - [ m >= 0 and J[k] == m ] <=> [ k \in J and R(:,k) == RJ(:,m) ]  */
        std::vector<int> J(_nbRHS);
        std::iota(J.begin(), J.end(), 0);
        std::vector<int> Jwork = J;

        auto inv_epsilon = array_inverse(epsilon);
        initial_iteration(A, X, B, epsilon, inv_epsilon, Y, RJ, XJ, J, Jwork);

        while (_nb_kept_direction > 0 && // Main Loop
               _size_krylov_space + _nb_kept_direction <= _max_krylov_space_size)
        {
            ++ _iteration_count;
            _logger.notify_iteration_begin(_iteration_count, _size_krylov_space);
            Vector Zj = Z.get_W( _nb_kept_direction );
            Vector W  = V.get_W( _nb_kept_direction );

            apply_right_precond(Zj, M, Y); // Z_j <- M * Y
            matrix_vector_product(W, A, Zj); // W <- A * Zj
            do_ortho(orthoparm, V, Z, W, Zj); // arnoldi process (CGS/MGS/CGS2/IMGS2)

            Z.add_vector( Zj );
            V.add_vector( W  );

            Vector Vj = V.get_Vj();
            compute_phi(Phi, Vj, RJ); // Phi <- V_j^{T} * R(J)
            XJ.axpy(Zj, Phi);  // X <- X + Zj * Phi;
            RJ.axpy(Vj, Phi, S{-1.0}); //  RJ <- RJ - Vj * Phi;
            callback(_iteration_count, _logger, XJ, RJ);

            // Y <- notConverged( R(J) )
            auto MinMaxConv = deflate(Y, RJ, J, Jwork, epsilon, inv_epsilon);
            save_converged_X(XJ, X, J, Jwork);
            compact_X_and_R(XJ, RJ, J, Jwork);
            std::swap(J, Jwork); // J <- J_work

            _size_krylov_space = V.get_nb_vect();
            _logger.notify_iteration_end(
                _iteration_count, _size_krylov_space, _nb_mvp, MinMaxConv);
            // _logger.log_real_residual(A, B, X, V, _hess);
        }

        #ifdef FABULOUS_DEBUG_MODE
        if (_nb_kept_direction == 0) {
            auto mm = eval_current_solution(A, B, X);
            fprintf(stderr, "real residual=(%e;%e)\n", mm.first, mm.second);
        }
        #endif
        if (_nb_kept_direction > 0) {
            save_everyone_X(XJ, X, J);
        }
        print_footer();

        _logger.set_convergence(_nb_kept_direction == 0);
        return (_nb_kept_direction == 0);
    }
}; // end class ArnoldiGCR


/*! \brief NOT TEMPLATE designator for ::fabulous::bgcr::Arnoldi */
class ARNOLDI {};
inline auto std() { return ARNOLDI{}; }

template<class S>
auto make_arnoldi(ARNOLDI,Logger &logger, int nbRHS, int size_to_span, const Parameters &param)
{
    return Arnoldi<S>{logger, nbRHS, size_to_span, param};
}

} // end namespace bgcr
} // end namespace fabulous

#endif // FABULOUS_ARNOLDI_GCR_HPP
