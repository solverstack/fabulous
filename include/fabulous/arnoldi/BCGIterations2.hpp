#ifndef FABULOUS_BCG_ITERATIONS2_HPP
#define FABULOUS_BCG_ITERATIONS2_HPP

#include <limits>
#include <numeric>

namespace fabulous {
namespace bcg {
template<class S> class BcgIterations2;
}
}


#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/kernel/cholesky.hpp"
#include "fabulous/kernel/qp3.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/algo/Parameters.hpp"

namespace fabulous {
namespace bcg {

/**
 * \brief %BCG iterations
 *
 * Breadown-Free Block Conjugate Gradient (BF-BCG)
 *
 * This class support Restarting
 */
template<class S>
class BcgIterations2
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
    using P = primary_type;

private:
    void print_header(std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "#######################################################\n";
        o << "#################### BcgIterations(BFBCG) ###################\n";
        o << "######## Mat Vect product scheduled "<< _max_mvp <<" ###########\n";
    }

    void print_footer(std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "################# Iterations done: "<<_iteration_count<<"(+1)\n";
        o << "#######################################################\n";
    }

private:
    Logger &_logger; /*!< logger for code profiling/performance measurement */
    const bool _user_wants_real_X_R;
    const bool _quiet; /*!< no output if true */
    const int _nbRHS; /*!< number of right hand sides */
    const int _max_mvp; /*!< maximum number of matrix vector product */

    int _nb_mvp; /*!< number of matrix vector product */
    int _iteration_count; /*!< inner loop cycle count */
    int _nb_kept_direction; /*!< number of vector in XJ and RJ; number of direction not converged yet */

public:
    BcgIterations2(Logger &logger, int nbRHS, Parameters param):
        _logger{logger},
        _user_wants_real_X_R{param.need_real_residual},
        _quiet{param.quiet},
        _nbRHS{nbRHS},
        _max_mvp{param.max_mvp},
        _nb_mvp{0},
        _iteration_count{0},
        _nb_kept_direction{_nbRHS}
    {
    }

public:
    int get_krylov_space_size() const { return 0; }
    int get_nb_mvp() const { return _nb_mvp; }
    int get_nb_iteration() const { return _iteration_count; }

private:

    /**
     *  \brief Save direction of X(J) who have converged into X and
     *         Compact XJ with not yet converged direction
     *         Compact RJ with not yet converged residual
     *
     *  \param A user matrix callback object
     *  \param RJ compacted R(J)
     *  \param XJ compacted X(J)
     *  \param X solution vector
     *  \param J array describing RJ and XJ layout; see comments near to J declaration in run() method
     *  \param J_work temporary array; same size of J
     */
    template<class Vector>
    void save_converged_X( Vector &XJ, Vector &X,
                           const std::vector<int> &J,
                           const std::vector<int> &J_work)
    {
        TIMER_TRACE;
        int nb_lost_direction = 0;
        // Save converged XJ into X
        if (!_quiet)
            std::cerr << "Lost direction [";
        for (int i = 0; i < _nbRHS; ++i) {
            if (J_work[i] == -1 && J[i] != -1) {
                int k = J[i];
                if (!_quiet)
                    std::cerr << " " << i;
                ++ nb_lost_direction;
                if (!_quiet && nb_lost_direction >= 20) {
                    nb_lost_direction = 0;
                    std::cerr << "\n";
                }
                std::copy(XJ.get_vect(k),
                          XJ.get_vect(k)+XJ.get_nb_row(),
                          X.get_vect(i));
            }
        }
        if (!_quiet)
            std::cerr << " ]\n";
    }

    template<class Vector>
    void compact_X_and_R( Vector &XJ, Vector &RJ, Vector &Z,
                          const std::vector<int> &J,
                          const std::vector<int> &J_work)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( XJ.get_nb_col() >= _nb_kept_direction );
        FABULOUS_ASSERT( RJ.get_nb_col() >= _nb_kept_direction );

        int sizeJ = 0;
        // Compact XJ and RJ
        for (int i = 0; i < _nbRHS; ++i) {
            if (J_work[i] != -1) {
                ++ sizeJ;
                if (J_work[i] != J[i]) {
                    int from = J[i];
                    int to = J_work[i];
                    FABULOUS_ASSERT(from > to);
                    std::copy(XJ.get_vect(from),
                              XJ.get_vect(from)+XJ.get_nb_row(),
                              XJ.get_vect(to));
                    std::copy(RJ.get_vect(from),
                              RJ.get_vect(from)+XJ.get_nb_row(),
                              RJ.get_vect(to));
                }
            }
        }
        RJ = RJ.sub_vector(0, sizeJ);
        XJ = XJ.sub_vector(0, sizeJ);
        Z  =  Z.sub_vector(0, sizeJ);
    }

    template<class Vector>
    auto deflate_1(Vector &RJ,
                   const std::vector<int> &J,
                   std::vector<int> &J_work,
                   const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        P min = std::numeric_limits<P>::max();
        P max = 0.0;
        int nb_removed_before = 0;
        const int sizeJ = RJ.get_nb_col();
        Block<S> Q{RJ, sizeJ};
        Block<S> r{sizeJ, sizeJ};
        RJ.qr(Q, r);

        int newSizeJ = 0;
        for (int j = 0; j < _nbRHS; ++j) {
            const int k = J[j];
            if (k != -1) {
                const auto N = r.get_norm(k);
                const auto epsilon_j = epsilon.size() > 1 ? epsilon[j] : epsilon.front();
                min = std::min(min, N);
                max = std::max(max, N);
                if (N > epsilon_j) {
                    // compute new abscissa of column in RJ,XJ
                    J_work[j] = J[j] - nb_removed_before;
                    ++ newSizeJ;
                } else {
                    ++ nb_removed_before;
                    J_work[j] = -1;
                }
            } else {
                J_work[j] = -1;
            }
        }

        if (!_quiet && newSizeJ != sizeJ) {
            std::cerr<<Color::warning<<"### Inexact Breakdown "<<Color::reset
                     <<Color::ib_mark2<<"Mark II"<<Color::reset
                     <<Color::warning<<" Occured!! ###\n"<<Color::reset;
            std::cerr<<"Nb direction kept: "
                     << newSizeJ<<"/"<<_nbRHS
                     << " ("<<Color::error<<"-"<<(sizeJ-newSizeJ)<<Color::reset<<")\n";
        }
        return std::make_tuple(min, max, newSizeJ == 0);
    }

    template<class Vector, class Epsilon>
    void deflate_2(Vector &P, Vector &Z,
                   const Epsilon &epsilon,
                   const Epsilon &inv_epsilon)
    {
        TIMER_TRACE;
        const int sizeJ = Z.get_nb_col();
        Block<S> r{sizeJ, sizeJ};
        Z.qr(Z, r);

        int nb_kept_direction;
        if (sizeJ != 0) {
            Block<S> U{};
            nb_kept_direction = decomposition_svd(r, U, epsilon, inv_epsilon, -1);
            FABULOUS_ASSERT( U.get_nb_col() == nb_kept_direction );
            Block<S> Pi = P.sub_vector(0, nb_kept_direction);
            const int dim = Pi.get_local_dim();
            lapacke::gemm(dim, Pi.get_nb_col(), U.get_nb_row(),
                          Z.get_ptr(), Z.get_leading_dim(),
                          U.get_ptr(), U.get_leading_dim(),
                          Pi.get_ptr(), Pi.get_leading_dim(),
                          S{1.0}, S{0.0}      );
        } else {
            nb_kept_direction = 0;
        }

        int old_nb_kept_direction = _nb_kept_direction;
        _nb_kept_direction = nb_kept_direction;
        if (!_quiet && old_nb_kept_direction != _nb_kept_direction) {
            std::cerr<<Color::warning<<"### Inexact Breakdown "<<Color::reset
                     <<Color::ib_mark3<<"Mark III"<<Color::reset
                     <<Color::warning<<" Occured!! ###\n"<<Color::reset;
            std::cerr<<"Nb direction kept: "
                     <<_nb_kept_direction<<"/"<<_nbRHS
                     << " ("<<Color::error
                     <<"-"<<(old_nb_kept_direction-_nb_kept_direction)
                     << Color::reset<<")\n";
        }
        P = P.sub_vector( 0, _nb_kept_direction );
    }

    /**
     *  \brief save all vector from XJ into X
     *
     *  used when algorithm need to restart and all vector have not converged yet
     *
     *  \param XJ compacted X(J)
     *  \param X solution vector
     *  \param J array describing XJ layout; see comments near to J declaration in run() method
     */
    template<class Vector>
    void save_everyone_X(const Vector &XJ, Vector &X, const std::vector<int> &J)
    {
        TIMER_TRACE;
        for (int j = 0; j < _nbRHS; ++j) {
            int k = J[j];
            if (k != -1) {
                std::copy(XJ.get_vect(k),
                          XJ.get_vect(k)+XJ.get_local_dim(),
                          X.get_vect(j));
            }
        }
    }

    template<class Matrix, class Vector>
    void compute_residual_directly(/**/  Vector &R,
                                   const Vector &B,
                                   const Matrix &A,
                                   const Vector &X)
    {

        TIMER_TRACE;
        R.copy(B);
        _logger.notify_mvp_begin();
        A(X, R, S{-1.0}, S{1.0}); // R <- B - A*X
        _logger.notify_mvp_end(0);
        _nb_mvp += X.get_nb_col();
    }

    /**
     *  \brief run initialization iteration of BCG algorithm
     *
     *  \param A user callback matrix
     *  \param X solution vector
     *  \param B right hand sides vector
     *  \param epsilon array of threshold
     *        - size==1 => same threshold for all vectors
     *        - otherwise size=nbRHS
     *  \param R block residual
     *  \param P block conjugate directions
     *  \param Z block secondary directions
     */
    template<class Matrix, class Precond, class Vector,
             class Callback, class Epsilon>
    void initial_iteration(const Matrix &A,
                           const Precond &M,
                           const Callback &CB,
                           Vector &X, const Vector &B,
                           const Epsilon &epsilon,
                           const Epsilon &inv_epsilon,
                           Vector &RJ, Vector XJ,
                           Vector &P, Vector &Z,
                           std::vector<int> &J,
                           std::vector<int> &Jwork    )
    {
        TIMER_TRACE;
        _logger.notify_iteration_begin(_iteration_count, _nb_mvp);
        XJ.copy(X);
        compute_residual_directly(RJ, B, A, X); // R <- B - A*X
        auto MinMaxConv = deflate_1(RJ, J, Jwork, epsilon); // deflate_1( R )
        save_converged_X(XJ, X, J, Jwork);
        compact_X_and_R(XJ, RJ, Z, J, Jwork);
        std::swap(J, Jwork);
        call_user_callback(A, CB, X, B, XJ, J);
        apply_right_precond(Z, M, RJ); // Z <- M * R
        deflate_2(P, Z, epsilon, inv_epsilon); // P <- deflate(Z)
        _logger.notify_iteration_end(_iteration_count, _nb_mvp, _nb_mvp, MinMaxConv);
    }

    template<class Precond, class Vector>
    void apply_right_precond(Vector &Zj, const Precond &M, const Vector &R)
    {
        TIMER_TRACE;
        _logger.notify_precond_begin();
        int64_t flops = 0;
        if ( M ) {
            flops = M(R, Zj);
        } else {
            Zj.copy(R);
        }
        _logger.notify_precond_end(flops);
    }

    template<class Matrix, class Vector>
    void matrix_vector_product(Vector &Q, const Matrix &A, const Vector &P)
    {
        TIMER_MVP;
        FABULOUS_ASSERT( P.get_nb_col() == _nb_kept_direction );
        if ( Q.get_nb_col() != _nb_kept_direction ) {
            Q = Q.sub_vector( 0, _nb_kept_direction );
        }
        _logger.notify_mvp_begin();
        int64_t flops = A(P, Q); // Qi <- A * Pi
        _logger.notify_mvp_end(flops);
        _nb_mvp += P.get_nb_col();
    }

    /**
     *  \brief Phi <- V_j^{}T} * R(J)
     */
    template<class Vector>
    void compute_alpha(Block<S> &alpha,
                       const Vector &P, const Vector &Q,
                       const Vector &R, Block<S> &PtQ)
    {
        TIMER_TRACE;
        const int sizeJ = R.get_nb_col();
        FABULOUS_ASSERT( P.get_nb_col() == _nb_kept_direction );
        FABULOUS_ASSERT( Q.get_nb_col() == _nb_kept_direction );

        PtQ = PtQ.sub_block( 0, 0, _nb_kept_direction, _nb_kept_direction );
        P.dot( Q, PtQ ); // PtQ = P . Q

        alpha = alpha.sub_block( 0, 0, _nb_kept_direction, sizeJ );
        P.dot( R, alpha ); // alpha = P . R

        int err;
        err = lapacke::potrf('L', _nb_kept_direction,
                             PtQ.get_ptr(), PtQ.get_leading_dim());
        if ( err != 0 ) {
            FABULOUS_THROW(
                Kernel,
                "potrf (cholesky) failed with error code='"<<err<<"'"
                << ( (err > 0)
                     ? " (Positive value means input Matrix is not SPD)"
                     : "" )
            );
        }

        err = lapacke::potrs('L', _nb_kept_direction, sizeJ,
                             PtQ.get_ptr(), PtQ.get_leading_dim(),
                             alpha.get_ptr(), alpha.get_leading_dim());
        if ( err != 0 ) {
            FABULOUS_THROW(Kernel, "potrs failed with error code='"<<err<<"'");
        }
    }

    /**
     *  \brief beta <- (P_i^{T} Q_i)^{-1} * (Q_i^{T} Z_i)
     */
    template<class Vector>
    void compute_beta(Block<S> &beta,
                      const Block<S> &PtQ,
                      const Vector &Q, const Vector &Z)
    {
        TIMER_TRACE;
        const int sizeJ = Z.get_nb_col();
        beta = beta.sub_block( 0, 0, _nb_kept_direction, sizeJ );
        Q.dot( Z, beta ); // beta = Q . Z
        int err = lapacke::potrs('L', _nb_kept_direction, sizeJ,
                                 PtQ.get_ptr(), PtQ.get_leading_dim(),
                                 beta.get_ptr(), beta.get_leading_dim());
        if ( err != 0 ) {
            FABULOUS_THROW(Kernel, "potrs failed with error code='"<<err<<"'");
        }
    }

    template<class Matrix, class Callback, class Vector>
    void call_user_callback(const Matrix &A,
                            const Callback &callback,
                            Vector &X, const Vector &B,
                            const Vector &XJ,
                            const std::vector<int> &J)
    {
        TIMER_TRACE;
        if (_user_wants_real_X_R) {
            Vector R{X, _nbRHS};
            save_everyone_X(XJ, X, J);
            compute_residual_directly(R, B, A, X);
            callback(_iteration_count, _logger, X, R);
        } else {
            Vector dummy{X, 0};
            callback(_iteration_count, _logger, dummy, dummy);
        }
    }

public:

    /**
     * \brief BcgIterations, BF-BCG solver, right preconditionning and partial convergence management
     *
     * Solution will be stored inside X at the end of computation.
     * Hao Ji Yaohang Li, A breakdown-free block conjugate gradient method,
     * Springer Science+Business Media Dordrecht 2016
     *
     * \param[in] A user Matrix
     * \param[in] M user preconditioner
     * \param[in] CB user generic callback
     * \param[in,out] X initial guess (input), and best approximated solution (output)
     * \param[in] B the right hand sides
     * \param[in] epsilon tolerance (backward error) for residual
     *
     * \return whether the convergence was reached
     */
    template<class Matrix, class Precond, class Callback, class Vector>
    bool run( const Matrix &A, const Precond &M, const Callback &CB,
              Vector &X, const Vector &B,
              const std::vector<P> &epsilon )
    {
        TIMER_TRACE;
        print_header();

        Vector RJ{X, _nbRHS}; // residual R(J)
        Vector XJ{X, _nbRHS}; // X(J)
        Vector Z{ X, _nbRHS}; // z
        Vector P{ X, _nbRHS}; // search space direction
        Vector Q{ X, _nbRHS}; // A*P
        Block<S> alpha{_nbRHS, _nbRHS};
        Block<S> beta{_nbRHS, _nbRHS};
        Block<S> PtQ{_nbRHS, _nbRHS};
        std::vector<int> J(_nbRHS);
        std::iota(J.begin(), J.end(), 0);
        std::vector<int> Jwork = J;

        auto inv_epsilon = array_inverse(epsilon);
        initial_iteration(A, M, CB, X, B, epsilon, inv_epsilon,
                          RJ, XJ, P, Z, J, Jwork);

        bool convergence = false;
        while (!convergence
               && _nb_kept_direction > 0 && _nb_mvp < _max_mvp)
        {
            ++ _iteration_count;
            _logger.notify_iteration_begin(_iteration_count, _nb_mvp);
            matrix_vector_product(Q, A, P); // Q <- A * P
            compute_alpha(alpha, P, Q, RJ, PtQ);// alpha <- (P^{T} Q)^{-1} * (P^{T} R)
            XJ.axpy(P, alpha);  // X <- X + P*alpha
            RJ.axpy(Q, alpha, S{-1.0}); // R <- R - Q*alpha
            //compute_residual_directly(R, A, XJ, B);
            call_user_callback(A, CB, X, B, XJ, J);

            auto MinMaxConv = deflate_1(RJ, J, Jwork, epsilon); // deflate_1( R )
            convergence = std::get<2>(MinMaxConv);
            if (!convergence) {
                save_converged_X(XJ, X, J, Jwork);
                compact_X_and_R(XJ, RJ, Z, J, Jwork);
                std::swap(J, Jwork);
                apply_right_precond(Z, M, RJ); // Z <- M*R
                compute_beta(beta, PtQ, Q, Z);// beta <- (P^{T} Q)^{-1} * (Q^{T} Z)
                Z.axpy(P, beta, S{-1.0}); // Z <- Z - P * beta
                deflate_2(P, Z, epsilon, inv_epsilon); // P_i <- orth( Z )
            }
            _logger.notify_iteration_end(_iteration_count, _nb_mvp, _nb_mvp, MinMaxConv);
            // _logger.log_real_residual(A, B, X, _V, _hess);
        }
        FABULOUS_DEBUG("nb_kept_direction="<<_nb_kept_direction);

        save_everyone_X(XJ, X, J);

        #ifdef FABULOUS_DEBUG_MODE
        if (_nb_kept_direction == 0) {
            auto mm = eval_current_solution(A, B, X);
            fprintf(stderr, "real residual=(%e;%e)\n", mm.first, mm.second);
        }
        #endif
        print_footer();
        _logger.set_convergence(_nb_kept_direction == 0);
        return (_nb_kept_direction == 0);
    }

}; // end class BcgIterations2

/*! \brief NOT TEMPLATE designator for BcgIterations2 */
class BCG_ITER2 {};
inline auto v2() { return BCG_ITER2{}; }

template<class S>
auto make_bcgiter(BCG_ITER2, Logger &logger, int nbRHS, const Parameters &param)
{
    return BcgIterations2<S>{logger, nbRHS, param};
}

} // end namespace bcg
} // end namespace fabulous

#endif // FABULOUS_BCG_ITERATIONS2_HPP
