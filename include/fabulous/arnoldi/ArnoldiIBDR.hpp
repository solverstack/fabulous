#ifndef FABULOUS_ARNOLDI_IB_DR_HPP
#define FABULOUS_ARNOLDI_IB_DR_HPP

namespace fabulous {
namespace bgmres {
template<class Hessenberg, class S> class ArnoldiIBDR;
}
}

#include "fabulous/data/Base.hpp"
#include "fabulous/data/BlockPW.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/algo/Parameters.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"
#include "fabulous/orthogonalization/Orthogonalizer.hpp"
#include "fabulous/restart/Restarter.hpp"
#include "fabulous/kernel/QR.hpp"

#include <fabulous/hessenberg/HessQRIB.hpp>
#include <fabulous/hessenberg/HessIBDR.hpp>
#include <fabulous/hessenberg/HessQRIBDR.hpp>

namespace fabulous {
namespace bgmres {

/**
 * \brief %Arnoldi iterations with Inexact Breakdowns and Deflated Restarting
 *
 * the GELS kernel is used to solve the LeastSquare problem on the projected matrix
 */
template<class Hessenberg, class S>
class ArnoldiIBDR
{
    static_assert(
        arnoldiXhessenberg<ArnoldiIBDR, Hessenberg>::value,
        "ArnoldiIBDR cannot be combined with this Hessenberg"
    );
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
    using P = primary_type;

private:

    void print_header(int nb_mvp, std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "#######################################################\n";
        o << "#################### Arnoldi ##########################\n";
        o << "######### Mat Vect product scheduled: "<< nb_mvp <<" ###########\n";
    }

    void print_footer(std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "################# Iterations done: "<<_iteration_count<<"(+1)\n";
        o << "#######################################################\n";
    }

private:
    Logger &_logger;
    const bool _user_wants_real_X_R;
    const bool _quiet;

    Hessenberg _F;
    bool _solution_computed;

    // GENERIC
    const int _nbRHS;
    const int _max_space;
    int _nb_mvp;
    int _size_krylov_space;
    int _iteration_count;

    // IB SPECIFIC:
    const int _max_kept_direction;
    int _nb_direction_kept;
    int _old_nb_direction_kept;
    int _nb_direction_discarded;
    bool _ib_update_scheduled;
    Block<S> _U; /*!< right vector of SVD decomposition of R_local */

    // DR specific
    bool _cold_restart;

public:
    explicit ArnoldiIBDR(Logger &logger, int nbRHS, const Parameters &param):
        _logger{logger},
        _user_wants_real_X_R{param.need_real_residual},
        _quiet{param.quiet},
        _F{param.max_space, nbRHS, _logger},
        _solution_computed{false},
        _nbRHS{nbRHS},
        _max_space{param.max_space},
        _nb_mvp{0},
        _size_krylov_space{0},
        _iteration_count{0},
        _max_kept_direction{((param.max_kept_direction <= 0)
                             ? (_nbRHS)
                             : (param.max_kept_direction))},
        _nb_direction_kept{nbRHS},
        _old_nb_direction_kept{nbRHS},
        _nb_direction_discarded{0},
        _ib_update_scheduled{false},
        _cold_restart{false}
    {
    }

public:
    int get_krylov_space_size() const { return _size_krylov_space; }
    int get_nb_mvp() const { return _nb_mvp; }
    int get_nb_iteration() const { return _iteration_count; }

private:

    /************************* COMPUTE HELPERS *******************************/

    template<class Matrix, class Vector>
    void compute_real_residual(/**/  Vector &R, const Vector &B,
                               const Matrix &A, const Vector &X)
    {
        TIMER_TRACE;
        _logger.notify_mvp_begin();
        R.copy(B);
        int64_t flops = A(X, R, S{-1.0}, S{1.0}); // R <- B - A*X
        _logger.notify_mvp_end(flops);
        _nb_mvp += X.get_nb_col();
    }

    template<class Matrix, class Precond, class Vector, class VectorPW>
    void W_AMV(const Matrix &A, const Precond &M,
               const Vector &V, VectorPW &PW) // W = A*M*V
    {
        TIMER_MVP;
        Vector &W = PW.get_W();

        if ( M ) {
            int64_t flops;

            _logger.notify_precond_begin();
            const int nrhs = V.get_nb_col();
            Vector MV{V, nrhs};
            flops = M(V, MV); // MV = M * V
            _logger.notify_precond_end(flops);

            _logger.notify_mvp_begin();
            flops = A(MV, W); // B = A*MV
            _logger.notify_mvp_end(flops);
        } else {
            int64_t flops;
            _logger.notify_mvp_begin();
            flops = A(V, W); //  W = A * V
            _logger.notify_mvp_end(flops);
        }
        _nb_mvp += V.get_nb_col();
    }

    template<class Precond, class Base, class Vector>
    void compute_solution(const Precond &M, const Base &base, Vector &X)
    {
        TIMER_COMPUTE_SOLUTION;
        Block<S> Y = _F.alloc_least_square_sol();
        _F.solve_least_square(Y);
        X_plus_MVY(X, M, base, Y); // X_n = X_0 + M*V*Y
    }

    /************************* ALGORITHM HELPERS *******************************/

    template<class Matrix, class Precond, class Base, class Vector>
    bool check_real_residual( const Matrix &A, const Precond &M,
                              const Vector &X, const Vector &B,
                              const Base &base, Vector &X_tmp,
                              const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        X_tmp.copy(X);
        compute_solution(M, base, X_tmp); // X_tmp += M * base * Y

        Vector R{X, _nbRHS};
        compute_real_residual(R, B, A, X_tmp); // R <- B - A*sol
        auto MinMaxConv = min_max_conv(R, epsilon);
        #ifdef FABULOUS_DEBUG_MODE
        auto min = std::get<0>(MinMaxConv);
        auto max = std::get<1>(MinMaxConv);
        fprintf(stderr, "real residual=(%e;%e)\n", min, max);
        #endif

        if (std::get<2>(MinMaxConv)) {
            _solution_computed = true;
            _cold_restart = false;
            return true;
        }
        _cold_restart = true;
        return false;
    }

    template<class Base, class VectorPW>
    void do_ortho(const OrthoParam &orthoparm, const Base &base, VectorPW &PW)
    {
        TIMER_ORTHO;
        Orthogonalizer ortho{orthoparm};
        // Ortho and filling of L (the (IB) Hessenberg)
        ortho.run(_F, base, PW, _logger);
    }

    template<class Matrix, class Base,
             class Vector, class VectorPW,
             class Epsilon, class Restarter>
    bool initial_iteration(const Matrix &A, Base &base,
                           const Vector &X, const Vector &B,
                           VectorPW &PW, Restarter &restarter,
                           const Epsilon &epsilon, const Epsilon &inv_epsilon)
    {
        TIMER_TRACE;
        _logger.notify_iteration_begin(_iteration_count, _size_krylov_space);

        std::tuple<P,P,bool> MinMaxConv;
        if (restarter && base.get_nb_block() > 0) {
            MinMaxConv = restarter.run(_F, PW, epsilon);
            bool conv = std::get<2>(MinMaxConv);
            do_R_criterion(conv, epsilon, inv_epsilon);
        } else {
            base.reset();
            MinMaxConv = inexact_breakdown_on_R0(A, base, X, B, PW, epsilon, inv_epsilon);
        }

        _size_krylov_space = base.get_nb_vect();
        _logger.notify_iteration_end(
            _iteration_count, _size_krylov_space, _nb_mvp, MinMaxConv);
        //_logger.log_real_residual(A, B, X);
        return std::get<2>(MinMaxConv);
    }

    /************************* IB on R0 ********************************/

    template<class Base, class Vector, class VectorPW>
    void handle_IB_on_R0(Block<S> &U, Base &base, VectorPW &PW,
                         const Vector &Q0, const Block<S> &Lambda0)
    {
        TIMER_TRACE;
        if (!_quiet) {
            std::cerr<<Color::yellow<<Color::bold;
            std::cerr<<"\t\tINEXACT BREAKDOWN on R0\n";
            std::cerr<<"\t\tNb direction kept: "<<_nb_direction_kept<<"/"<<_nbRHS;
            std::cerr<<Color::reset<<std::endl;
        }

        int M = U.get_nb_row();
        Block<S> U1 = U.sub_block(0, 0, M, _nb_direction_kept);
        Block<S> U2 = U.sub_block(0, _nb_direction_kept, M, _nb_direction_discarded);
        Vector V1 = base.get_W(_nb_direction_kept);
        V1.zero();
        V1.axpy(Q0, U1); // V1 = Q0 * U1
        base.add_vector( V1 );

        PW.set_size_P(_nb_direction_discarded);
        Vector &P = PW.get_P();
        P.zero();
        P.axpy(Q0, U2); // P = Q0 * U2

        Block<S> Lambda1{_nbRHS, _nbRHS};
        U.dot(Lambda0, Lambda1); // Lambda1 = trans(U) * Lambda0;

        _F.init_lambda(Lambda1);
        _F.init_phi_ibr0(_nb_direction_kept); // Create and initialize Phi (\Phi_1)
    }

    template<class Matrix, class Base,
             class Vector, class VectorPW,
             class Epsilon>
    std::tuple<P,P,bool>
    inexact_breakdown_on_R0( const Matrix &A, Base &base,
                             const Vector &X, const Vector &B,
                             VectorPW &PW,
                             const Epsilon &epsilon,
                             const Epsilon &inv_epsilon)
    {
        TIMER_TRACE;
        Vector Q0{X, _nbRHS};
        Block<S> Lambda0{_nbRHS, _nbRHS};
        Block<S> L0{_nbRHS, _nbRHS};
        Block<S> U{_nbRHS, _nbRHS};

        compute_real_residual(Q0, B, A, X); // QO <- B - A*X
        Q0.qr(Q0, Lambda0);
        L0.copy(Lambda0);

        _nb_direction_kept = decomposition_svd(L0, U, epsilon, inv_epsilon);
        _nb_direction_kept = std::min(_nb_direction_kept, _max_kept_direction);
        _old_nb_direction_kept = _nb_direction_kept;
        _nb_direction_discarded = _nbRHS - _nb_direction_kept;

        // Test if algorithm need to be launched again
        if (!_quiet && _nb_direction_kept == 0) {
            std::cerr << "\t\tTOTAL INEXACT BREAKDOWN on R0\n";
            std::cerr << "\t\tNb direction kept: 0 / " << _nbRHS << std::endl;
            return min_max_conv(Lambda0, epsilon);
        }

        if (_nb_direction_discarded > 0) {
            handle_IB_on_R0(U, base, PW, Q0, Lambda0);
        } else {
            if (!_quiet) {
                std::cerr<<"\t\tNo INEXACT BREAKDOWN on R0\n";
            }
            Vector W = base.get_W(_nbRHS);
            W.copy(Q0);
            base.add_vector( W );
            _F.init_lambda(Lambda0);
        }
        return min_max_conv(Lambda0, epsilon);
    }

    /************************* IB ********************************/

    void print_ib_occured()
    {
        if (_quiet) return;
        std::cerr<<Color::warning<<"### Inexact Breakdown Occured!! ###\n"<<Color::reset;
        std::cerr<<"Nb direction kept: "
                 <<_nb_direction_kept<<"/"<<_nbRHS
                 << " ("<<Color::error
                 <<"-"<<(_old_nb_direction_kept-_nb_direction_kept)
                 << Color::reset<<")\n";
    }

    /**
     * \brief perform update on PW and Hessenberg due to Inexact Breakdown
     */
    template<class Base, class VectorPW>
    void do_IB_update(Base &base, VectorPW &PW)
    {
        TIMER_TRACE;
        using Vector = typename VectorPW::vector_type;

        if (!_ib_update_scheduled) {
            return;
        }
        _ib_update_scheduled = false;
        _logger.notify_ib_begin();
        int64_t flops = 0;
        if (_nb_direction_discarded == 0) {
            Vector W = base.get_W(_nbRHS);
            W.copy(PW.get_W());
            base.add_vector( W );
            Block<S> identity{_nbRHS, _nbRHS};
            for (int i = 0; i < _nbRHS; ++i) {
                identity(i, i) = S{1.0};
            }
            flops += _F.update_phi( identity, _nbRHS ); // needed for restart case
            _logger.notify_ib_end( flops );
            _F.after_bottom_line_update( _nbRHS );
            return;
        }

        Block<S> &U = _U;
        FABULOUS_ASSERT( U.get_nb_col() == _nb_direction_kept );

        if ( _nb_direction_kept != _old_nb_direction_kept ) {
            print_ib_occured();
            _old_nb_direction_kept = _nb_direction_kept;
        }

        const int nb_vect = base.get_nb_vect();
        const int p  = _nbRHS;
        const int pj = _nb_direction_kept;
        Block<S> U1_2 = U.sub_block(nb_vect, 0, p, pj);
        Block<S> W1_W2{p, p};
        Block<S> R{};
        Block<S> W1 = W1_W2.sub_block( 0,  0, p,   pj );
        Block<S> W2 = W1_W2.sub_block( 0, pj, p, p-pj );
        flops = qr::OutOfPlaceQRFacto(U1_2, W1_W2, R); // Qr facto of bottom block of U

        // PART 1
        Vector V = base.get_W( _nb_direction_kept );
        Vector PW_vect = PW.concat_PW();
        V.zero();
        V.axpy(PW_vect, W1);  // V_{j+1} = [P_{j-1},~Wj] * \W_1
        // FIXME flops
        base.add_vector( V );

        // PART 2
        PW.set_size_P(_nb_direction_discarded);
        Vector &P = PW.get_P();
        P.zero();
        P.axpy(PW_vect, W2);  // Pj = [P_{j-1},~Wj] * \W_2
        // FIXME flops

        // Update Phi with \W_1 and \W_2, doc Annexe Eq 2.7
        // \Phi(nj+1:nj+p,:) := W1_W2^{H} * \Phi(nj+1:nj+p,:)
        flops += _F.update_phi( W1_W2, pj );

        // L_{j+1,} = | \W_1^{H} | * H_j
        // Gj       = | \W_2^{H} | * H_j
        flops += _F.update_bottom_line( W1_W2 );
        _logger.notify_ib_end( flops );

        // hook that may perform incremental factorization
        _F.after_bottom_line_update( pj );
    }

    template<class P>
    void do_R_criterion(int proj_convergence,
                        const std::vector<P> &epsilon,
                        const std::vector<P> &inv_epsilon)
    {
        TIMER_TRACE;
        Block<S> PR{_F.get_nb_vect()+_nbRHS, _nbRHS};
        Block<S> Y = _F.alloc_least_square_sol();
        _F.solve_least_square(Y);

        _logger.notify_ib_begin();
        int64_t flops;
        flops = _F.compute_proj_residual(PR, Y);

        _nb_direction_kept = decomposition_svd(PR, _U, epsilon, inv_epsilon, _max_kept_direction);
        _nb_direction_discarded = _nbRHS - _nb_direction_kept;

        if (_nb_direction_kept == 0) {
            /* This is suspicious because we already checked convergence on
             real residual if LS residual have converged and this function
             is not called if convergence was detected */
            if (proj_convergence) {
                // Cold Restart: WARNING: Do not DR there because Arnoldi
                // relation would be false from the beginning
                _cold_restart = true;
            } else {
                // Even more suspicious
                FABULOUS_THROW(
                    Numeric,
                    "Number of directions kepts during SVD of projected "
                    "residual is equal to 0, but residual itself has not "
                    "converged. That should not be possible."
                );
            }
        }
        _ib_update_scheduled = true;
        _logger.notify_ib_end(  flops/*FIXME add svd*/ );
    }

    template<class Matrix, class Precond, class Base, class Callback, class Vector>
    void call_user_callback(const Matrix &A,  const Precond &M,
                            const Base &base, const Callback &callback,
                            const Vector &X0, const Vector &B)
    {
        TIMER_TRACE;
        if (_user_wants_real_X_R) {
            Vector R{X0, _nbRHS};
            Vector X{X0, _nbRHS};

            X.copy(X0);
            compute_solution(M, base, X);
            compute_real_residual(R, B, A, X); // R <- B - A*X
            callback(_iteration_count, _logger, X, R);
        } else {
            Vector dummy{X0, 0};
            callback(_iteration_count, _logger, dummy, dummy);
        }
    }

public:

    /**
     * \brief Arnoldi iterations with inexact breakdowns
     *
     * Solution will be stored inside X at the end of computation.
     * Ref: IB-BGMRES-DR (Giraud Jing Agullo): p.21
     *
     * \param[in] A user matrix
     * \param[in] M user preconditioner
     * \param[in] CB user generic callback
     * \param[in,out] X initial guess (input). Best approx. solution (output)
     * \param[in] B the right hand sides
     * \param[in] epsilon tolerance for Residual
     * \param[in,out] restarter hold data to deflate at restart
     * \param orthoparm Orthogonalizer
     *
     * \return whether the convergence was reached
     */
    template<class Matrix, class Precond,
             class Vector,
             class Callback, class Restarter>
    bool run( const Matrix &A,
              const Precond &M,
              const Callback &CB,
              Vector &X, const Vector &B,
              const std::vector<P>  &epsilon,
              Restarter &restarter,
              const OrthoParam &orthoparm)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( _nbRHS == B.get_nb_col() ); // Initial Size of block

        print_header(_max_space);
        auto &base = restarter.get_base();
        auto PW = make_PW(X, _nbRHS);
        Vector X_tmp{X, _nbRHS};

        const auto inv_epsilon = array_inverse(epsilon);
        bool convergence = initial_iteration(A, base, X, B, PW, restarter,
                                             epsilon, inv_epsilon);

        // IS_INVARIANT( _nb_direction_discarded + _nb_direction_kept == nbRHS );
        while (  !convergence // Main Loop
                 && _size_krylov_space + _nb_direction_kept <= _max_space
                 && _nb_direction_kept > 0)
        {
            ++ _iteration_count;
            _logger.notify_iteration_begin(_iteration_count, _size_krylov_space);

            do_IB_update(base, PW);
            Vector Vj = base.get_Vj();
            W_AMV(A, M, Vj, PW); // W <- A*M*Vj: perform matrix multiplication
            do_ortho(orthoparm, base, PW);

            auto MinMaxConv = _F.check_least_square_residual(epsilon);
            bool proj_convergence = std::get<2>(MinMaxConv);
            if (proj_convergence) {
                convergence = check_real_residual(A, M, X, B, base, X_tmp, epsilon);
            } else {
                convergence = false;
            }
            if (!convergence) {
                do_R_criterion(proj_convergence, epsilon, inv_epsilon);
            }

            _size_krylov_space = base.get_nb_vect();
            call_user_callback(A, M, base, CB, X, B);
            _logger.notify_iteration_end(
                _iteration_count, _size_krylov_space, _nb_mvp, MinMaxConv);
            //_logger.log_real_residual(A, B, X, base, _F);
        }

        if (_solution_computed) {
            X.copy(X_tmp);
        } else if (_iteration_count) {
            compute_solution(M, base, X);
        }

        if (_cold_restart) {
            FABULOUS_WARNING("cold restart");
            base.reset();
        } else if (!convergence && restarter) {
            Block<S> PR{_F.get_nb_vect() + _nbRHS, _nbRHS};
            Block<S> Y = _F.alloc_least_square_sol();
            _F.solve_least_square(Y);
            _F.compute_proj_residual(PR, Y);
            restarter.save_LS(PR);
            Block<S> H = _F.get_hess_block();
            restarter.save_hess(H);
            restarter.save_PW(PW);
        }

        #ifdef FABULOUS_DEBUG_MODE
        //if (convergence) {
        auto mm = eval_current_solution(A, B, X);
        printf("real residual=(%e;%e)\n", mm.first, mm.second);
        //}
        #endif

        print_footer();
        _logger.set_convergence(convergence);
        return convergence;
    }

}; // end class ArnoldiIBDR

/*! \brief NOT TEMPLATE designator for ArnoldiIBDR<HessQRIB<S>, S>  */
class ARNOLDI_QRIB {};
inline auto qrib()   { return ARNOLDI_QRIB{}; }

/*! \brief NOT TEMPLATE designator for ArnoldiIBDR<HessIBDR<S>, S>  */
class ARNOLDI_IBDR {};
inline auto ibdr()   { return ARNOLDI_IBDR{}; }

/*! \brief NOT TEMPLATE designator for ArnoldiIBDR<HessQRIBDR<S>, S>  */
class ARNOLDI_QRIBDR {};
inline auto qribdr() { return ARNOLDI_QRIBDR{}; }

template<class S>
auto make_arnoldi(ARNOLDI_QRIB, Logger &logger, int nbRHS, const Parameters &param)
{
    return ArnoldiIBDR<HessQRIB<S>, S>{logger, nbRHS, param};
}

template<class S>
auto make_arnoldi(ARNOLDI_IBDR, Logger &logger, int nbRHS, const Parameters &param)
{
    return ArnoldiIBDR<HessIBDR<S>, S>{logger, nbRHS, param};
}

template<class S>
auto make_arnoldi(ARNOLDI_QRIBDR, Logger &logger, int nbRHS, const Parameters &param)
{
    return ArnoldiIBDR<HessQRIBDR<S>, S>{logger, nbRHS, param};
}

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_ARNOLDI_IB_DR_HPP
