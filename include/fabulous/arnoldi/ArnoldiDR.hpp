#ifndef FABULOUS_ARNOLDI_DR_HPP
#define FABULOUS_ARNOLDI_DR_HPP

namespace fabulous {
namespace bgmres {
template<class Hessenberg, class S> class ArnoldiDR;
}
}


#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/utils/ConvergenceCheck.hpp"
#include "fabulous/algo/Parameters.hpp"
#include "fabulous/restart/Restarter.hpp"
#include "fabulous/orthogonalization/Orthogonalizer.hpp"

#include <fabulous/hessenberg/HessDR.hpp>
#include <fabulous/hessenberg/HessDR_CHAM.hpp>
#include <fabulous/hessenberg/HessQR.hpp>
#include <fabulous/hessenberg/HessQRDR.hpp>
#include <fabulous/hessenberg/HessQR_CHAMSUB.hpp>


namespace fabulous {
namespace bgmres {

/**
 * \brief %Arnoldi iterations
 *
 * GELS kernel is used to solve the LeastSquare problem on the projected matrix
 *
 * This class support DeflatedRestarting
 */
template<class Hessenberg, class S>
class ArnoldiDR
{
    static_assert(
        arnoldiXhessenberg<ArnoldiDR, Hessenberg>::value,
        "ArnoldiDR cannot be combined with this Hessenberg"
    );
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
private:
    using P = primary_type;

private:
    Logger &_logger;
    const bool _user_wants_real_X_R;
    const bool _quiet;

    Hessenberg _hess;
    bool _solution_computed;

    const int _nbRHS;
    const int _max_space;
    int _size_krylov_space;
    int _nb_mvp;
    int _iteration_count;

    // DR SPECIFIC
    bool _cold_restart;

public:
    explicit ArnoldiDR(Logger &logger, int nbRHS, const Parameters &param):
        _logger{logger},
        _user_wants_real_X_R{param.need_real_residual},
        _quiet{param.quiet},
        _hess{param.max_space, nbRHS, _logger},
        _solution_computed(false),
        _nbRHS{nbRHS},
        _max_space{param.max_space},
        _size_krylov_space{0},
        _nb_mvp{0},
        _iteration_count{0},
        _cold_restart{false}
    {
    }

private:
    void print_header(int nb_mvp, std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "#######################################################\n";
        o << "#################### Arnoldi ##########################\n";
        o << "######## Mat Vect product scheduled "<< nb_mvp <<" ###########\n";
    }

    void print_footer(std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "################# Iterations done: "<<_iteration_count<<"(+1)\n";
        o << "#######################################################\n";
    }

public:
    int get_krylov_space_size() const { return _size_krylov_space; }
    int get_nb_mvp() const { return _nb_mvp; }
    int get_nb_iteration() const { return _iteration_count; }

private:

    template<class Matrix, class Vector>
    void compute_real_residual(/**/  Vector &R, const Vector &B,
                               const Matrix &A, const Vector &X)
    {
        TIMER_TRACE;
        _logger.notify_mvp_begin();
        int64_t flops;
        R.copy(B);
        flops = A(X, R, S{-1.0}, S{1.0}); // R <- B - A*X
        _logger.notify_mvp_end(flops);
        _nb_mvp += X.get_nb_col();
    }

    template<class Matrix, class Precond, class Vector>
    void W_AMV(const Matrix &A, const Precond &M,
               const Vector &V, Vector &W         ) // W = A*M*V
    {
        TIMER_MVP;
        if ( M ) {
            int64_t flops;
            _logger.notify_precond_begin();
            const int nrhs = V.get_nb_col();
            Vector MV{V, nrhs};
            flops = M(V, MV); // MV = M * V
            _logger.notify_precond_end(flops);

            _logger.notify_mvp_begin();
            flops = A(MV, W); // W = A * MV
            _logger.notify_mvp_end(flops);
        } else {
            int64_t flops;
            _logger.notify_mvp_begin();
            flops = A(V, W); //  W = A * V
            _logger.notify_mvp_end(flops);
        }
        _nb_mvp += V.get_nb_col();
    }

    template<class Precond, class Base, class Vector>
    void compute_solution(const Precond &M, const Base &base, Vector &X)
    {
        TIMER_COMPUTE_SOLUTION;
        Block<S> Y = _hess.alloc_least_square_sol();
        _hess.solve_least_square(Y);
        X_plus_MVY(X, M, base, Y); // X_n = X_0 + M*V*Y
    }

    template<class Base, class Vector>
    void do_ortho(const OrthoParam &orthoparm, const Base &base, Vector &W)
    {
        TIMER_ORTHO;
        Orthogonalizer ortho{orthoparm};
        // Ortho and filling of L (the (IB) Hessenberg)
        ortho.run(_hess, base, W, _logger);
    }

    template<class Matrix, class Precond, class Base, class Vector>
    bool check_real_residual( const Matrix &A, const Precond &M,
                              const Vector &X, const Vector &B,
                              const Base &base, Vector &X_tmp,
                              const std::vector<P> &epsilon)
    {
        TIMER_TRACE;
        X_tmp.copy(X);
        compute_solution(M, base, X_tmp);
        Vector R{X, _nbRHS};
        compute_real_residual(R, B, A, X_tmp); // R <- B - A*sol

        auto MinMaxConv = min_max_conv(R, epsilon);
        #ifdef FABULOUS_DEBUG_MODE
        auto min = std::get<0>(MinMaxConv);
        auto max = std::get<1>(MinMaxConv);
        fprintf(stderr, "real residual=(%g;%g)\n", min, max);
        #endif

        if (std::get<2>(MinMaxConv)) {
            _solution_computed = true;
            _cold_restart = false;
            return true;
        }
        _cold_restart = true;
        return false;
    }

    template<class Matrix, class Base, class Vector, class Epsilon, class Restarter>
    bool initial_iteration(Matrix &A, Base &base, Vector &X, const Vector &B,
                           const Epsilon &epsilon, Restarter &restarter)
    {
        TIMER_TRACE;
        std::tuple<P,P,bool> MinMaxConv;
        _logger.notify_iteration_begin(_iteration_count, _size_krylov_space);
        if (restarter && base.get_nb_block() > 0) {
            MinMaxConv = restarter.run(_hess, epsilon);
            // check_arnoldi_formula(A, _base, _hess, _nbRHS);
        } else { //No DR, or first run of arnoldi
            base.reset();
            Vector R0 = base.get_W(_nbRHS);
            compute_real_residual(R0, B, A, X); // R0 <- B - A*X
            base.add_vector( R0 );
            Block<S> R1{_nbRHS, _nbRHS}; // R1 = RHS of projected problem
            R0.qr(R0, R1);
            // auto minmaxR0 =  R0.get_min_max_norm(A); */
            _hess.set_rhs(R1);
            MinMaxConv = min_max_conv(R1, epsilon);
        }
        _size_krylov_space = base.get_nb_vect();
        _logger.notify_iteration_end(
            _iteration_count, _size_krylov_space, _nb_mvp, MinMaxConv);
        //_logger.log_real_residual(A, B, X);
        return std::get<2>(MinMaxConv);
    }

    template<class Matrix, class Precond, class Base, class Vector, class Callback>
    void call_user_callback(const Matrix &A,  const Precond &M,
                            const Base &base, const Callback &callback,
                            const Vector &X0, const Vector &B)
    {
        TIMER_TRACE;
        if (_user_wants_real_X_R) {
            Vector R{X0, _nbRHS};
            Vector X{X0, _nbRHS};

            X.copy(X0);
            compute_solution(M, base, X);
            compute_real_residual(R, B, A, X); // R <- B - A*X
            callback(_iteration_count, _logger, X, R);
        } else {
            Vector dummy{X0, 0};
            callback(_iteration_count, _logger, dummy, dummy);
        }
    }

public:

    /**
     * \brief Arnoldi iterations
     *
     * Solution will be stored inside X at the end of computation.
     * Ref IB-BGMRES-DR (Giraud Jing Agullo): p.21
     *
     * \param[in] A user matrix
     * \param[in] M user preconditioner
     * \param[in] CB user generic callback
     * \param[in,out] X initial guess (input), and best approximated solution (output)
     * \param[in] B the right hand sides
     * \param[in] epsilon tolerance (backward error) for residual
     * \param[in,out] restarter hold data to deflate at restart
     * \param orthoparm Orthogonalizer
     *
     * \return whether the convergence was reached
     */
    template<class Matrix, class Precond,
             class Vector, class Callback, class Restarter>
    bool run( const Matrix &A,
              const Precond &M,
              const Callback &CB,
              Vector &X, const Vector &B,
              const std::vector<P> &epsilon,
              Restarter &restarter,
              const OrthoParam &orthoparm )
    {
        TIMER_TRACE;
        print_header(_max_space);

        auto &base = restarter.get_base();
        Vector X_tmp{X, _nbRHS};
        bool convergence = initial_iteration(A, base, X, B, epsilon, restarter);

        while (!convergence && // Main Loop
               _size_krylov_space + _nbRHS <= _max_space)
        {
            ++ _iteration_count;
            _logger.notify_iteration_begin(_iteration_count, _size_krylov_space);
            const Vector &Vj = base.get_Vj(); // V_j
            FABULOUS_ASSERT( Vj.get_nb_col() == _nbRHS );
            Vector W = base.get_W(_nbRHS); // W (candidate)

            W_AMV(A, M, Vj, W); // W := A*M*V_j
            do_ortho(orthoparm, base, W); // W := ~(W ⊥ V)
            base.add_vector(W); // add W to base

            //_hess.display_hess_bitmap("Hess Before Least Square");
            auto MinMaxConv = _hess.check_least_square_residual(epsilon);
            bool proj_convergence = std::get<2>(MinMaxConv);
            if (proj_convergence) {
                convergence = check_real_residual(A, M, X, B, base, X_tmp, epsilon);
            } else {
                convergence = false;
            }
            //check_arnoldi_formula(A, base, _hess, _nbRHS);

            _size_krylov_space = base.get_nb_vect();
            call_user_callback(A, M, base, CB, X, B);
            _logger.notify_iteration_end(
                _iteration_count, _size_krylov_space, _nb_mvp, MinMaxConv);
            //_logger.log_real_residual(A, B, X, base, _hess);
        }

        if (_solution_computed) {
            X.copy(X_tmp);
        } else if (_iteration_count) {
            compute_solution(M, base, X);
        }

        if (_cold_restart) {
            FABULOUS_WARNING("cold restart");
            base.reset();
        } else if (restarter && !convergence) {
            Block<S> PR{_hess.get_nb_vect() + _nbRHS, _nbRHS};
            Block<S> Y = _hess.alloc_least_square_sol();
            _hess.solve_least_square(Y);
            _hess.compute_proj_residual(PR, Y);
            restarter.save_LS(PR);
            Block<S> H = _hess.get_hess_block();
            restarter.save_hess(H);
        }

        print_footer();
        _logger.set_convergence(convergence);
        return convergence;
    }

}; // end class ArnoldiDR

/*! \brief NOT TEMPLATE designator for ArnoldiDR<HessDR<S>, S>  */
class ARNOLDI_STD {};
inline auto std() { return ARNOLDI_STD{}; }


/*! \brief NOT TEMPLATE designator for ArnoldiDR<HessQR<S>, S>  */
class ARNOLDI_QR {};
inline auto qr()     { return ARNOLDI_QR{}; }

/*! \brief NOT TEMPLATE designator for ArnoldiDR<HessQRDR<S>, S>  */
class ARNOLDI_QRDR {};
inline auto qrdr()   { return ARNOLDI_QRDR{}; }

/*! \brief NOT TEMPLATE designator for ArnoldiDR<HessQR_CHAMSUB<S>, S>  */
class ARNOLDI_CHAM_QR {};
inline auto cham_qr() { return ARNOLDI_CHAM_QR{}; }

/*! \brief NOT TEMPLATE designator for ArnoldiDR<HessDR_CHAM<S>, S>  */
class ARNOLDI_CHAM_TL {};
inline auto cham_tl() { return ARNOLDI_CHAM_TL{}; }

template<class S>
auto make_arnoldi(ARNOLDI_STD, Logger &logger, int nbRHS, const Parameters &param)
{
    return ArnoldiDR<HessDR<S>, S>{logger, nbRHS, param};
}

template<class S>
auto make_arnoldi(ARNOLDI_QR, Logger &logger, int nbRHS, const Parameters &param)
{
    return ArnoldiDR<HessQR<S>, S>{logger, nbRHS, param};
}

template<class S>
auto make_arnoldi(ARNOLDI_QRDR, Logger &logger, int nbRHS, const Parameters &param)
{
    return ArnoldiDR<HessQRDR<S>, S>{logger, nbRHS, param};
}

#ifdef FABULOUS_USE_CHAMELEON

template<class S>
auto make_arnoldi(ARNOLDI_CHAM_QR, Logger &logger, int nbRHS, const Parameters &param)
{
    return ArnoldiDR<HessQR_CHAMSUB<S>, S>{logger, nbRHS, param};
}

template<class S>
auto make_arnoldi(ARNOLDI_CHAM_TL, Logger &logger, int nbRHS, const Parameters &param)
{
    return ArnoldiDR<HessDR_CHAM<S>, S>{logger, nbRHS, param};
}

#endif // FABULOUS_USE_CHAMELEON

} // end namespace bgmres
} // end namespace fabulous

#endif // FABULOUS_ARNOLDI_DR_HPP
