#ifndef FABULOUS_BCG_ITERATIONS_HPP
#define FABULOUS_BCG_ITERATIONS_HPP

#include <numeric>

namespace fabulous {
namespace bcg {
template<class S> class BcgIterations;
}
}


#include "fabulous/data/Base.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/kernel/cholesky.hpp"
#include "fabulous/kernel/qp3.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/algo/Parameters.hpp"

namespace fabulous {
namespace bcg {

/**
 * \brief %BCG iterations
 *
 * Breadown-Free Block Conjugate Gradient (BF-BCG)
 *
 * This class support Restarting
 */
template<class S>
class BcgIterations
{
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
    using P = primary_type;

private:
    void print_header(std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "#######################################################\n";
        o << "#################### BcgIterations(BFBCG) ###################\n";
        o << "######## Mat Vect product scheduled "<< _max_mvp <<" ###########\n";
    }

    void print_footer(std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "################# Iterations done: "<<_iteration_count<<"(+1)\n";
        o << "#######################################################\n";
    }

private:
    Logger &_logger; /*!< logger for code profiling/performance measurement */
    const bool _quiet; /*!< no output if true */

    const int _nbRHS; /*!< number of right hand sides */
    const int _max_mvp; /*!< maximum number of matrix vector product */

    int _nb_mvp; /*!< number of matrix vector product */
    int _iteration_count; /*!< inner loop cycle count */
    int _nb_kept_direction; /*!< number of vector in XJ and RJ; number of direction not converged yet */

public:
    BcgIterations(Logger &logger, int nbRHS, Parameters param):
        _logger{logger},
        _quiet{param.quiet},
        _nbRHS{nbRHS},
        _max_mvp{param.max_mvp},
        _nb_mvp{0},
        _iteration_count{0},
        _nb_kept_direction{_nbRHS}
    {
    }

public:
    int get_krylov_space_size() const { return 0; }
    int get_nb_mvp() const { return _nb_mvp; }
    int get_nb_iteration() const { return _iteration_count; }

private:
    template<class Vector, class Epsilon>
    void orth_nodist(Vector &P, Vector &Z,
                     const Epsilon &epsilon,
                     const Epsilon &inv_epsilon)
    {
        TIMER_ORTHO;
        (void) inv_epsilon;
        std::vector<int> jpvt;
        std::vector<S> tau;
        int err;
        const int dim = Z.get_local_dim();
        err = lapacke::geqp3(dim, _nbRHS,
                             Z.get_ptr(), Z.get_leading_dim(),
                             jpvt, tau);
        if ( err != 0 ) {
            FABULOUS_THROW(Kernel, "geqp3 failed with error code='"<<err<<"'");
        }

        int nb_kept_direction = 0;
        for (int j = 0; j < _nbRHS; ++j) {
            const auto epsilon_j = epsilon.size() > 1 ? epsilon[j] : epsilon.front();
            const auto n = std::abs(Z.at(j, j));
            if (n < epsilon_j) {
                break;
            }
            ++ nb_kept_direction;
        }

        err = lapacke::ungqr(dim, _nbRHS, nb_kept_direction,
                             Z.get_ptr(), Z.get_leading_dim(), tau.data());
        if ( err != 0 ) {
            FABULOUS_THROW(Kernel, "ungqr failed with error code='"<<err<<"'");
        }
        Vector Pi = P.sub_vector(0, nb_kept_direction);
        Pi.copy(Z);

        int old_nb_kept_direction = _nb_kept_direction;
        _nb_kept_direction = nb_kept_direction;
        if (!_quiet && old_nb_kept_direction != _nb_kept_direction) {
            std::cerr<<Color::warning<<"### Inexact Breakdown Occured!! ###\n"<<Color::reset;
            std::cerr<<"Nb direction kept: "
                     <<_nb_kept_direction<<"/"<<_nbRHS
                     << " ("<<Color::error
                     <<"-"<<(old_nb_kept_direction-_nb_kept_direction)
                     << Color::reset<<")\n";
        }
    }

    template<class Vector, class Epsilon>
    void orth_dist(Vector &P, Vector &Z,
                   const Epsilon &epsilon,
                   const Epsilon &inv_epsilon)
    {
        TIMER_ORTHO;
        const int sizeJ = Z.get_nb_col();
        Block<S> r{sizeJ, sizeJ};
        Z.qr(Z, r);

        int nb_kept_direction;
        if (sizeJ != 0) {
            Block<S> U{};
            nb_kept_direction = decomposition_svd(r, U, epsilon, inv_epsilon, -1);
            //std::cout << U.get_nb_col() << ',' << nb_kept_direction << '\n';
            FABULOUS_ASSERT( U.get_nb_col() == nb_kept_direction );

            Vector Pi = P.sub_vector(0, nb_kept_direction);
            Pi.zero();
            Pi.axpy(Z, U);
        } else {
            nb_kept_direction = 0;
        }

        int old_nb_kept_direction = _nb_kept_direction;
        _nb_kept_direction = nb_kept_direction;
        if (!_quiet && old_nb_kept_direction != _nb_kept_direction) {
            std::cerr<<Color::warning<<"### Inexact Breakdown "<<Color::reset
                     <<Color::ib_mark3<<"Mark III"<<Color::reset
                     <<Color::warning<<" Occured!! ###\n"<<Color::reset;
            std::cerr<<"Nb direction kept: "
                     <<_nb_kept_direction<<"/"<<_nbRHS
                     << " ("<<Color::error
                     <<"-"<<(old_nb_kept_direction-_nb_kept_direction)
                     << Color::reset<<")\n";
        }
        P = P.sub_vector( 0, _nb_kept_direction );
    }

    template<class Matrix, class Vector>
    void compute_residual_directly(/**/  Vector &R, const Matrix &A,
                                   const Vector &X, const Vector &B)
    {
        TIMER_TRACE;
        R.copy(B);
        _logger.notify_mvp_begin();
        A(X, R, S{-1.0}, S{1.0}); // R <- B - A*X
        _logger.notify_mvp_end(0);
        _nb_mvp += X.get_nb_col();
    }

    /**
     *  \brief run initial iteration (initialization) of BCG algorithm
     *
     *  \param A user callback matrix
     *  \param X solution vector
     *  \param B right hand sides vector
     *  \param epsilon array of threshold
     *        - size==1 => same threshold for all vectors
     *        - otherwise size=nbRHS
     *  \param R block residual
     *  \param P block conjugate directions
     *  \param Z block secondary directions
     */
    template<class Matrix, class Precond,
             class Vector,
             class Callback, class Epsilon>
    void initial_iteration( const Matrix &A,
                            const Precond &M,
                            const Callback &callback,
                            Vector &X, const Vector &B,
                            const Epsilon &epsilon, const Epsilon &inv_epsilon,
                            Vector &R, Vector &P, Vector &Z)
    {
        TIMER_TRACE;
        _logger.notify_iteration_begin(_iteration_count, _nb_mvp);
        compute_residual_directly(R, A, X, B);
        callback(_iteration_count, _logger, X, R);

        auto MinMaxConv = min_max_conv(R, epsilon);
        apply_right_precond(Z, M, R); // Z <- M * R
        orth_dist(P, Z, epsilon, inv_epsilon); // P <- orth( Z )
        (void) inv_epsilon;
        _logger.notify_iteration_end(_iteration_count, _nb_mvp, _nb_mvp, MinMaxConv);
        // _logger.log_real_residual(A, B, X);
    }

    template<class Vector, class Precond>
    void apply_right_precond(Vector &Zj, const Precond &M, const Vector &R)
    {
        TIMER_TRACE;
        _logger.notify_precond_begin();
        int64_t flops = 0;
        if ( M ) {
            flops = M(R, Zj);
        } else {
            Zj.copy(R);
        }
        _logger.notify_precond_end(flops);
    }

    template<class Vector, class Matrix>
    void matrix_vector_product(Vector &Q, const Matrix &A, const Vector &P)
    {
        TIMER_MVP;
        FABULOUS_ASSERT( P.get_nb_col() == _nb_kept_direction );
        if ( Q.get_nb_col() != _nb_kept_direction ) {
            Q = Q.sub_block( 0, 0, Q.get_nb_row(), _nb_kept_direction );
        }

        _logger.notify_mvp_begin();
        int64_t flops = A(P, Q); // Q <- A * P
        _logger.notify_mvp_end(flops);
        _nb_mvp += P.get_nb_col();
    }

    /**
     *  \brief alpha <- (P^{T} Q)^{-1} * (P^{T} R)
     */
    template<class Vector>
    void compute_alpha( Block<S> &alpha, Block<S> &PtQ,
                        const Vector &P, const Vector &Q, const Vector &R )
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( P.get_nb_col() == _nb_kept_direction );
        FABULOUS_ASSERT( Q.get_nb_col() == _nb_kept_direction );

        PtQ = PtQ.sub_block( 0, 0, _nb_kept_direction, _nb_kept_direction );
        P.dot( Q, PtQ ); // PtQ = P . Q

        alpha = alpha.sub_block( 0, 0, _nb_kept_direction, _nbRHS );
        P.dot( R, alpha ); // alpha = P . R

        int err = lapacke::potrf('L', _nb_kept_direction,
                                 PtQ.get_ptr(), PtQ.get_leading_dim());
        if ( err != 0 ) {
            FABULOUS_THROW(
                Kernel,
                "potrf (cholesky) failed with error code='"<<err<<"'"
                << ( (err > 0)
                     ? " (Positive value means input Matrix is not SPD)"
                     : "" )
            );
        }

        err = lapacke::potrs('L', _nb_kept_direction, _nbRHS,
                             PtQ.get_ptr(), PtQ.get_leading_dim(),
                             alpha.get_ptr(), alpha.get_leading_dim());
        if ( err != 0 ) {
            FABULOUS_THROW(Kernel, "potrs failed with error code='"<<err<<"'");
        }
    }

    /**
     *  \brief beta <- (P_i^{T} Q_i)^{-1} * (Q_i^{T} Z_i)
     */
    template<class Vector>
    void compute_beta( Block<S> &beta,  const Block<S> &PtQ,
                       const Vector &Q, const Vector &Z)
    {
        TIMER_TRACE;
        beta = beta.sub_block( 0, 0, _nb_kept_direction, _nbRHS );
        Q.dot( Z, beta ); // beta = Q . Z
        int err;
        err = lapacke::potrs('L', _nb_kept_direction, _nbRHS,
                             PtQ.get_ptr(), PtQ.get_leading_dim(),
                             beta.get_ptr(), beta.get_leading_dim());
        if ( err != 0 ) {
            FABULOUS_THROW(Kernel, "potrs failed with error code='"<<err<<"'");
        }
    }

public:

    /**
     * \brief BcgIterations, BCG solver, right preconditionning and partial convergence management
     *
     * Solution will be stored inside X at the end of computation.
     * Hao Ji Yaohang Li, A breakdown-free block conjugate gradient method,
     * Springer Science+Business Media Dordrecht 2016
     *
     *
     * \param[in] A user matrix
     * \param[in] M user preconditioner
     * \param[in] callback user generic callback
     * \param[in,out] X initial guess (input), and best approximated solution (output)
     * \param[in] B the right hand sides
     * \param[in] epsilon tolerance (backward error) for residual
     *
     * \return whether the convergence was reached
     */
    template<class Matrix, class Precond, class Vector, class Callback>
    bool run( const Matrix &A, const Precond &M,
              const Callback &callback,
              Vector &X, const Vector &B,
              const std::vector<P> &epsilon )
    {
        print_header();

        TIMER_TRACE;
        Vector R{X, _nbRHS}; // residual
        Vector Z{X, _nbRHS}; // z
        Vector P{X, _nbRHS}; // search space direction
        Vector Q{X, _nbRHS}; // A*P
        Block<S> alpha{_nbRHS, _nbRHS};
        Block<S> beta{_nbRHS, _nbRHS};
        Block<S> PtQ{_nbRHS, _nbRHS};

        auto inv_epsilon = array_inverse(epsilon);
        initial_iteration(A, M, callback, X, B, epsilon, inv_epsilon, R, P, Z);

        bool convergence = false;
        while (!convergence
               && _nb_kept_direction > 0 && _nb_mvp < _max_mvp)
        {
            ++ _iteration_count;
            _logger.notify_iteration_begin(_iteration_count, _nb_mvp);
            matrix_vector_product(Q, A, P); // Q <- A * P
            compute_alpha(alpha, PtQ, P, Q, R);// alpha <- (P^{T} Q)^{-1} * (P^{T} R)
            X.axpy(P, alpha); // X <- X + P * alpha;
            R.axpy(Q, alpha, S{-1.0}); // R <- R - Q * alpha;
            //compute_residual_directly(R, A, X, B);
            callback(_iteration_count, _logger, X, R);

            auto MinMaxConv = min_max_conv(R, epsilon);
            convergence = std::get<2>(MinMaxConv);
            if (!convergence) {
                apply_right_precond(Z, M, R); // Z <- M*R
                compute_beta(beta, PtQ, Q, Z);// beta <- (P^{T} Q)^{-1} * (Q^{T} Z)
                Z.axpy(P, beta, S{-1.0}); // Z <- Z - P * beta;
                orth_dist(P, Z, epsilon, inv_epsilon); // P_i <- orth( Z )
            }
            _logger.notify_iteration_end(_iteration_count, _nb_mvp, _nb_mvp, MinMaxConv);
            // _logger.log_real_residual(A, B, X, _V, _hess);
        }
        FABULOUS_DEBUG("nb_kept_direction="<<_nb_kept_direction);

        #ifdef FABULOUS_DEBUG_MODE
        if (_nb_kept_direction == 0) {
            auto mm = eval_current_solution(A, B, X);
            fprintf(stderr, "real residual=(%e;%e)\n", mm.first, mm.second);
        }
        #endif
        print_footer();
        _logger.set_convergence(_nb_kept_direction == 0);
        return (_nb_kept_direction == 0);
    }

}; // end class BcgIterations


/*! \brief NOT TEMPLATE designator for BcgIterations */
class BCG_ITER {};
inline auto std() { return BCG_ITER{}; }

template<class S>
auto make_bcgiter(BCG_ITER, Logger &logger, int nbRHS, const Parameters &param)
{
    return BcgIterations<S>{logger, nbRHS, param};
}

} // end namespace bcg
} // end namespace fabulous

#endif // FABULOUS_BCG_ITERATIONS_HPP
