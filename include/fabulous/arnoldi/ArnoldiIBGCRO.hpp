#ifndef FABULOUS_ARNOLDI_IB_GCRO_HPP
#define FABULOUS_ARNOLDI_IB_GCRO_HPP

namespace fabulous {
namespace bgcro {
template<class Hessenberg, class S> class ArnoldiIBGCRO;
}
}

#include "fabulous/data/Base.hpp"
#include "fabulous/data/BlockPW.hpp"
#include "fabulous/data/BlockPWGeneric.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Logger.hpp"
#include "fabulous/utils/Traits.hpp"
#include "fabulous/utils/ConvergenceCheck.hpp"
#include "fabulous/algo/Parameters.hpp"
#include "fabulous/restart/Restarter.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/orthogonalization/Orthogonalizer.hpp"
#include "fabulous/orthogonalization/OrthoParam.hpp"

#include <fabulous/hessenberg/HessIB_GCRO.hpp>

namespace fabulous {
namespace bgcro {

/**
 * \brief %Arnoldi iterations with inexact breakdowns
 *
 * GELS kernel is used to solve the LeastSquare problem on the projected matrix
 *
 * \warning This class does support DeflatedRestarting
 */
template<class Hessenberg, class S>
class ArnoldiIBGCRO
{
    static_assert(
        arnoldiXhessenberg<ArnoldiIBGCRO, Hessenberg>::value,
        "ArnoldiIB cannot be combined with this Hessenberg"
    );
public:
    using value_type   = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;
    using P = primary_type;

private:
    Logger &_logger;
    const bool _user_wants_real_X_R;
    const bool _quiet;

    Hessenberg _F;
    bool _solution_computed;

    // GENERIC
    const int _nbRHS;
    const int _max_space;
    int _nb_mvp;
    int _size_krylov_space;
    int _iteration_count;

    // IB SPECIFIC:
    const int _max_kept_direction;
    int _nb_direction_kept;
    int _old_nb_direction_kept;
    int _nb_direction_discarded;
    bool _ib_update_scheduled;
    Block<S> _U; /*!< right vector of SVD decomposition of R_local */

    // DR specific
    bool _cold_restart;
    const bool _Uk_const;

public:
    explicit ArnoldiIBGCRO(Logger &logger, int nbRHS, const Parameters &param, const int k, const bool Uk_const = false):
        _logger{logger},
        _user_wants_real_X_R{param.need_real_residual},
        _quiet{param.quiet},
        _F{param.max_space, nbRHS, _logger, k},
        _solution_computed{false},
        _nbRHS(nbRHS),
        _max_space{param.max_space},
        _nb_mvp{0},
        _size_krylov_space{0},
        _iteration_count{0},
        _max_kept_direction{((param.max_kept_direction <= 0)
                             ? (_nbRHS)
                             : (param.max_kept_direction))},
        _nb_direction_kept{nbRHS},
        _old_nb_direction_kept{nbRHS},
        _nb_direction_discarded{0},
        _ib_update_scheduled{false},
        _U{},
        _cold_restart{false},
        _Uk_const{Uk_const}
    {
    }

private:
    void print_header(int nb_mvp, std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "#######################################################\n";
        o << "#################### Arnoldi ##########################\n";
        o << "######### Mat Vect product scheduled: "<< nb_mvp <<" ###########\n";
    }

    void print_footer(std::ostream &o = std::cerr)
    {
        if (_quiet) return;
        o << "################# Iterations done: "<<_iteration_count<<"(+1)\n";
        o << "#######################################################\n";
    }

    template<class Matrix, class Vector>
    void compute_real_residual(/**/  Vector &R, const Vector &B,
                               const Matrix &A, const Vector &X)
    {
        TIMER_TRACE;
        _logger.notify_mvp_begin();
        R.copy(B);
        int64_t flops = A(X, R, S{-1.0}, S{1.0}); // R <- A*X
        _logger.notify_mvp_end(flops);
        _nb_mvp += X.get_nb_col();
    }

    template<class Matrix, class Callback, class Vector, class VectorPW>
    void W_AMV(const Matrix &A, const Callback &M,
               const Vector &V, VectorPW &PW          ) // W = A*M*V
    {
        TIMER_MVP;
        Vector &W = PW.get_W();
        if ( M ) {
            int64_t flops;
            _logger.notify_precond_begin();
            const int nrhs = V.get_nb_col();
            Vector MV{V, nrhs};
            flops = M(V, MV); // MV = M * V
            _logger.notify_precond_end(flops);

            _logger.notify_mvp_begin();
            flops = A(MV, W); // B = A*MV
            _logger.notify_mvp_end(flops);
        } else {
            int64_t flops;
            _logger.notify_mvp_begin();
            flops = A(V, W); //  W = A * V
            _logger.notify_mvp_end(flops);
        }
        _nb_mvp += V.get_nb_col();
    }

    template<class Precond, class Base, class Vector>
    void compute_solution(const Precond &M, const Base &base, Vector &X, const Vector &Uk)
    {
        TIMER_COMPUTE_SOLUTION;
        Block<S> Y = _F.alloc_least_square_sol();
        _F.solve_least_square(Y);

        const int k = Uk.get_nb_col();
        const int nbRHS = Y.get_nb_col();

        Block<S> Y2 = Y.sub_block(k, 0, Y.get_nb_row() - k, nbRHS);
        if(k > 0){
            Block<S> Y1 = Y.sub_block(0, 0, k, nbRHS);
            Vector MUk{};
            if ( M ) {
                MUk = Vector{Uk, k};
                M(Uk, MUk);
            } else {
                MUk = Uk;
            }
            X.axpy(MUk, Y1);
        }
        X_plus_MVY(X, M, base, Y2); // X_n = X_0 + M*V*Y
    }

    template<class Matrix, class Precond, class Base, class Vector>
    bool check_real_residual( const Matrix &A, const Precond &M,
                              const Vector &X, const Vector &B,
                              const Base &base, Vector &X_tmp,
                              const std::vector<P> &epsilon,
                              const Vector &Uk)
    {
        TIMER_TRACE;
        X_tmp.copy(X);
        compute_solution(M, base, X_tmp, Uk);
        Vector R{X_tmp, _nbRHS};
        compute_real_residual(R, B, A, X_tmp); // R <- B - A*sol
        auto MinMaxConv = min_max_conv(R, epsilon);
        #ifdef FABULOUS_DEBUG_MODE
        auto min = std::get<0>(MinMaxConv);
        auto max = std::get<1>(MinMaxConv);
        fprintf(stderr, "real residual=(%g;%g)\n", min, max);
        #endif

        if (std::get<2>(MinMaxConv)) {
            _solution_computed = true;
            _cold_restart = false;
            return true;
        }
        _cold_restart = true;
        return false;
    }

    template<class Base, class VectorPW, class Vector>
    void do_ortho(const OrthoParam &orthoparm, const Base &base, VectorPW &PW, const Vector &Ck)
    {
        TIMER_ORTHO;
        Orthogonalizer ortho{orthoparm};
        // Ortho and filling of F (Hessenberg)
        ortho.run_gcro(_F, base, PW, _logger, Ck);
    }

    template<class Matrix, class Base, class Vector, class VectorPW, class Epsilon, class Restarter>
    bool initial_iteration(const Matrix &A, Base &base,
                           Vector &X, const Vector &B,
                           VectorPW &PW,
                           const Epsilon &epsilon, const Epsilon &inv_epsilon, Restarter &restarter,
                           Vector &Uk, Vector &Ck)
    {
        TIMER_TRACE;
        _logger.notify_iteration_begin(_iteration_count, _size_krylov_space);

        std::tuple<P,P,bool> MinMaxConv;
        if (restarter && base.get_nb_block() > 0) {
            Block<S> Lambda1{_nbRHS, _nbRHS};
            restarter.run(Lambda1, PW, Uk, Ck, _Uk_const);
            _F.update_deflation_space_size(Uk.get_nb_col()); // Change deflation space size if necessary
            MinMaxConv = inexact_breakdown_on_R1(base, PW, Lambda1, epsilon, inv_epsilon);
        } else {
            base.reset();
            _F.update_deflation_space_size(Uk.get_nb_col()); // Change deflation space size if necessary
            MinMaxConv = inexact_breakdown_on_R0(A, base, X, B, PW, epsilon, inv_epsilon);
        }

        _size_krylov_space = base.get_nb_vect();
        _logger.notify_iteration_end(
            _iteration_count, _size_krylov_space, _nb_mvp, MinMaxConv);
        //_logger.log_real_residual(A, B, X);
        return std::get<2>(MinMaxConv);
    }

    /************************* IB on R0 ********************************/

    template<class Vector, class VectorPW, class Base>
    void handle_IB_on_R0(Block<S> &U, Base &base, VectorPW &PW,
                         const Vector &Q0, const Block<S> &Lambda0)
    {
        TIMER_TRACE;
        if (!_quiet) {
            std::cerr<<Color::yellow<<Color::bold;
            std::cerr<<"\t\tINEXACT BREAKDOWN on R0\n";
            std::cerr<<"\t\tNb direction kept: "<<_nb_direction_kept<<"/"<<_nbRHS;
            std::cerr<<Color::reset<<std::endl;
        }

        int M = U.get_nb_row();
        Block<S> U1 = U.sub_block(0, 0, M, _nb_direction_kept);
        Block<S> U2 = U.sub_block(0, _nb_direction_kept, M, _nb_direction_discarded);
        Vector V1 = base.get_W(_nb_direction_kept);
        V1.zero();
        V1.axpy(Q0, U1); // V_1 := Q_0 * \U_1
        base.add_vector(V1);

        PW.set_size_P(_nb_direction_discarded);
        Vector &P = PW.get_P();
        P.zero();
        P.axpy(Q0, U2); // P = Q0 * U2

        Block<S> Lambda1{_nbRHS, _nbRHS};
        U.dot(Lambda0, Lambda1); // Lambda1 = trans(U) * Lambda0;

        _F.init_lambda(Lambda1);
        _F.init_phi(_nb_direction_kept); // Create and initialize Phi (\Phi_1)
    }

    /**
     * Inexact breakdowns on R0, before the first restart
     *
     */
    template<class Matrix, class Base,
             class VectorPW, class Vector,
             class Epsilon>
    std::tuple<P,P,bool>
    inexact_breakdown_on_R0( const Matrix &A, Base &base,
                             Vector &X, const Vector &B,
                             VectorPW &PW,
                             const Epsilon &epsilon, const Epsilon &inv_epsilon)
    {
        TIMER_TRACE;
        Vector Q0{X, _nbRHS};
        Block<S> Lambda0{_nbRHS, _nbRHS};
        Block<S> L0{_nbRHS, _nbRHS};
        Block<S> U{_nbRHS, _nbRHS};

        compute_real_residual(Q0, B, A, X); // Q0 <- B - A*X

        Q0.qr(Q0, Lambda0);
        L0.copy(Lambda0);

        _nb_direction_kept = decomposition_svd(L0, U, epsilon, inv_epsilon);
        _nb_direction_kept = std::min(_nb_direction_kept, _max_kept_direction);
        _old_nb_direction_kept = _nb_direction_kept;
        _nb_direction_discarded = _nbRHS - _nb_direction_kept;

        // Test if algorithm need to be launched again
        if (!_quiet && _nb_direction_kept == 0) {
            std::cerr << "\t\tTOTAL INEXACT BREAKDOWN on R0\n";
            std::cerr << "\t\tNb direction kept: 0 / " << _nbRHS << std::endl;
            return min_max_conv(Lambda0, epsilon);
        }

        if (_nb_direction_discarded > 0) {
            if (!_quiet) {
                std::cerr<<Color::yellow<<Color::bold;
                std::cerr<<"\t\tINEXACT BREAKDOWN on R0\n";
                std::cerr<<"\t\tNb direction kept: "<<_nb_direction_kept<<"/"<<_nbRHS;
                std::cerr<<Color::reset<<std::endl;
            }

            handle_IB_on_R0(U, base, PW, Q0, Lambda0);
        } else {
            if (!_quiet) {
                std::cerr<<"\t\tNo INEXACT BREAKDOWN on R0\n";
            }
            Vector W = base.get_W(_nbRHS);
            W.copy(Q0);
            base.add_vector( W );
            _F.init_lambda(Lambda0);
        }
        return min_max_conv(Lambda0, epsilon);
    }


    /**
     * Inexact breakdowns on the R1 after a restart (R1 = R0 - Uk*Ck^{H}*R0)
     *
     */
    template<class Base, class VectorPW, class Epsilon>
    std::tuple<P,P,bool> inexact_breakdown_on_R1( Base &base, VectorPW &PW, const Block<S> Lambda1,
                                                  const Epsilon &epsilon, const Epsilon &inv_epsilon)
    {
        TIMER_TRACE;
        using Vector = typename VectorPW::vector_type;
        Block<S> L0{_nbRHS, _nbRHS};
        Block<S> U{_nbRHS, _nbRHS};

        L0.copy(Lambda1);

        _nb_direction_kept = decomposition_svd(L0, U, epsilon, inv_epsilon);
        _nb_direction_kept = std::min(_nb_direction_kept, _max_kept_direction);
        _old_nb_direction_kept = _nb_direction_kept;
        _nb_direction_discarded = _nbRHS - _nb_direction_kept;

        // Test if algorithm need to be launched again
        if (!_quiet && _nb_direction_kept == 0) {
            std::cerr << "\t\tTOTAL INEXACT BREAKDOWN on R1\n";
            std::cerr << "\t\tNb direction kept: 0 / " << _nbRHS << std::endl;
            return min_max_conv(Lambda1, epsilon);
        }

        if (_nb_direction_discarded > 0) {
            if (!_quiet) {
                std::cerr<<Color::yellow<<Color::bold;
                std::cerr<<"\t\tINEXACT BREAKDOWN on R1\n";
                std::cerr<<"\t\tNb direction kept: "<<_nb_direction_kept<<"/"<<_nbRHS;
                std::cerr<<Color::reset<<std::endl;
            }

            _ib_update_scheduled = false;
            handle_IB_on_R0(U, base, PW, PW.concat_PW(), Lambda1);
        } else {
            if (!_quiet) {
                std::cerr<<"\t\tNo INEXACT BREAKDOWN on R1\n";
            }
            Vector W = base.get_W(_nbRHS);
            W.copy(PW.get_W());
            base.add_vector( W );
            _F.init_lambda(Lambda1);
        }
        return min_max_conv(Lambda1, epsilon);
    }

    /************************* IB ********************************/

    void print_ib_occured()
    {
        if (_quiet) return;
        std::cerr<<Color::warning<<"### Inexact Breakdown Occured!! ###\n"<<Color::reset;
        std::cerr<<"Nb direction kept: "
                 <<_nb_direction_kept<<"/"<<_nbRHS
                 << " ("<<Color::error
                 <<"-"<<(_old_nb_direction_kept-_nb_direction_kept)
                 << Color::reset<<")\n";
    }

    /**
     * \brief perform update on PW and Hessenberg due to Inexact Breakdown
     */
    template<class Base, class VectorPW>
    void do_IB_update(Base &base, VectorPW &PW)
    {
        TIMER_TRACE;
        using Vector = typename VectorPW::vector_type;
        if (!_ib_update_scheduled) {
            return;
        }
        _ib_update_scheduled = false;
        _logger.notify_ib_begin();
        int64_t flops = 0;
        if (_nb_direction_discarded == 0) {
            Vector W = base.get_W(_nbRHS);
            W.copy(PW.get_W());
            base.add_vector( W );
            Block<S> identity{_nbRHS, _nbRHS};
            for (int i = 0; i < _nbRHS; ++i) {
                identity(i, i) = S{1.0};
            }
            flops += _F.update_phi( identity, _nbRHS ); // needed for restart case
            _logger.notify_ib_end( flops );
            //_F.after_bottom_line_update( _nbRHS );
            return;
        }

        Block<S> &U = _U;
        FABULOUS_ASSERT( U.get_nb_col() == _nb_direction_kept );

        if (_nb_direction_kept != _old_nb_direction_kept) {
            print_ib_occured();
            _old_nb_direction_kept = _nb_direction_kept;
        }
        Block<S> U1_2 = U.sub_block(U.get_nb_row() - _nbRHS, 0, _nbRHS, _nb_direction_kept);
        Block<S> W1_W2{_nbRHS, _nbRHS};
        Block<S> R{};
        flops += qr::OutOfPlaceQRFacto(U1_2, W1_W2, R); // Qr facto of bottom block of U
        Block<S> W1, W2;
        W1 = W1_W2.sub_block(0, 0, _nbRHS, _nb_direction_kept);
        W2 = W1_W2.sub_block(0, _nb_direction_kept, _nbRHS, _nb_direction_discarded);

        // PART 1
        Vector V = base.get_W( _nb_direction_kept );
        Vector PW_vect = PW.concat_PW();
        V.zero();
        V.axpy(PW_vect, W1); // V_{j+1} = [P_{j-1},~Wj] * \W_1

        // FIXME flops
        base.add_vector( V );

        // PART 2
        PW.set_size_P(_nb_direction_discarded);
        Vector &P = PW.get_P();
        P.zero();
        P.axpy(PW_vect, W2); // Pj = [P_{j-1},~Wj] * \W_2
        // FIXME flops

        // Update Phi with \W_1 and \W_2, doc Annexe Eq 2.7
        // \Phi(nj+1:nj+p,:) := W1_W2^{H} * \Phi(nj+1:nj+p,:)
        flops += _F.update_phi(W1_W2, _nb_direction_kept);

        // L_{j+1,} = | \W_1^{H} | * H_j
        // Gj       = | \W_2^{H} | * H_j
        flops += _F.update_bottom_line(W1_W2);
        _logger.notify_ib_end( flops );
    }

    void do_R_criterion(int proj_convergence,
                        const std::vector<P> &epsilon,
                        const std::vector<P> &inv_epsilon)
    {
        TIMER_TRACE;
        Block<S> Y = _F.alloc_least_square_sol();
        _F.solve_least_square(Y);

        _logger.notify_ib_begin();
        int64_t flops;
        Block<S> PR{_F.get_nb_hess_line(), _nbRHS};
        flops = _F.compute_proj_residual(PR, Y);

        _nb_direction_kept = decomposition_svd(PR, _U, epsilon, inv_epsilon, _max_kept_direction);
        _nb_direction_discarded = _nbRHS - _nb_direction_kept;

        if (_nb_direction_kept == 0) { // Suspicious,
            // True residual has not converged but
            // all the right-hand-side have
            _cold_restart = true;
            // Cold Restart: WARNING: Do not DR there because Arnoldi
            // relation would be false from the begining
            if (proj_convergence) {
                // Even more suspicious,
                // converged for least square residual
                FABULOUS_WARNING(
                    "Directions kepts during SVD of projected residual is "
                    " equal to 0, but residual itself has not converged. "
                    "That should not be possible."
                    "Values of Uk may be harmful for convergence"
                    " behaviour of a new right-hand-side."
                );
            }
        }
        _ib_update_scheduled = true;
        _logger.notify_ib_end( flops /* FIXME add svd */ );
    }

    template<class Matrix, class Precond, class Base, class Callback, class Vector>
    void call_user_callback(const Matrix &A,  const Precond &M,
                            const Base &base, const Callback &callback,
                            const Vector &X0, const Vector &B,
                            const Vector &Uk)
    {
        TIMER_TRACE;
        if (_user_wants_real_X_R) {
            Vector R{X0, _nbRHS};
            Vector X{X0, _nbRHS};

            X.copy(X0);
            compute_solution(M, base, X, Uk);
            compute_real_residual(R, B, A, X); // R <- B - A*X
            callback(_iteration_count, _logger, X, R);
        } else {
            /* X0 is passed as a dummy replacement but it is invalid for the user
             * to read from the variable in this case anyway */
            callback(_iteration_count, _logger, X0, X0);
        }
    }

public:

    int get_krylov_space_size() const { return _size_krylov_space; }
    int get_nb_mvp() const { return _nb_mvp; }
    int get_nb_iteration() const { return _iteration_count; }

    /**
     * \brief Arnoldi iterations with inexact breakdowns for BGCRO
     *
     * Solution will be stored inside X at the end of computation.
     * Ref: IB-BGCRO-DR (Giraud Xiang)
     *
     * \param[in] A user matrix
     * \param[in] M user preconditioner
     * \param[in] CB user generic callback
     * \param[in,out] X initial guess (input). Best approx. solution (output)
     * \param[in] B the right hand sides
     * \param[in] epsilon tolerance for Residual
     * \param[in,out] restarter hold the base
     * \param orthoparm Orthogonalizer
     * \param[in,out] Uk deflation space of the search space
     * \param[in,out] Ck deflation space of the residual space
     *
     * \return whether the convergence was reached
     */
    template< class Matrix, class Precond,
              class Callback,
              class Vector, class Restarter>
    bool run( const Matrix &A, const Precond &M,
              const Callback &CB,
              Vector &X, const Vector &B,
              const std::vector<P> &epsilon,
              Restarter &restarter,
              const OrthoParam &orthoparm,
              Vector &Uk,
              Vector &Ck)
    {
        TIMER_TRACE;
        FABULOUS_ASSERT( _nbRHS == B.get_nb_col() ); // Initial Size of block

        print_header(_max_space);
        auto &base_V = restarter.get_base();
        auto PW = make_PW(X, _nbRHS);
        Vector X_tmp{X, _nbRHS};


        const auto inv_epsilon = array_inverse(epsilon);
        bool convergence = initial_iteration(A, base_V, X, B, PW, epsilon, inv_epsilon, restarter, Uk, Ck);
        // IS_INVARIANT( _nb_direction_discarded + _nb_direction_kept == nbRHS );
        while (  !convergence // Main Loop
                 &&  _size_krylov_space + _nb_direction_kept <= _max_space
                 && _nb_direction_kept > 0)
        {
            ++ _iteration_count;
            _logger.notify_iteration_begin(_iteration_count, _size_krylov_space);


            do_IB_update(base_V, PW);
            Vector Vj = base_V.get_Vj();
            W_AMV(A, M, Vj, PW); // W <- A*M*Vj: perform matrix multiplication
            do_ortho(orthoparm, base_V, PW, Ck);

            //_F.display_hess_extended_bitmap();

            auto MinMaxConv = _F.check_least_square_residual(epsilon);
            bool proj_convergence = std::get<2>(MinMaxConv);
            if (proj_convergence) {
                convergence = check_real_residual(A, M, X, B, base_V, X_tmp, epsilon, Uk);
            } else {
                convergence = false;
            }
            if (!convergence) {
                do_R_criterion(proj_convergence, epsilon, inv_epsilon);
            }

            _size_krylov_space = base_V.get_nb_vect();
            call_user_callback(A, M, base_V, CB, X, B, Uk);
            _logger.notify_iteration_end(
                _iteration_count, _size_krylov_space, _nb_mvp, MinMaxConv);
            //_logger.log_real_residual(A, B, X, base, _F);
        }

        if (_solution_computed) {
            X.copy(X_tmp);
        } else if (_iteration_count) {
            compute_solution(M, base_V, X, Uk);
        }

        if (_cold_restart) {
            FABULOUS_WARNING("cold restart");
            base_V.reset();
            const int k = Uk.get_nb_col();
            // Reorthogonalize Ck and update Uk
            if(k > 0){
                if(_Uk_const) {
                    FABULOUS_WARNING("Uk is set to be constant but Ck seems to not be orthogonal.\n"
                                     "Uk may need to be updated.\n");
                }
                // Ck = Q * R
                Block<S> R{k, k};
                Ck.qr(Ck, R);
                // Uk = Uk * R^{-1}
                Uk.trsm(Uk, R);
            }
        } else if (!convergence && restarter) {
            Block<S> PR{_F.get_nb_vect() + _nbRHS, _nbRHS};
            Block<S> Y = _F.alloc_least_square_sol();
            Block<S> H = _F.get_hess_block();
            _F.solve_least_square(Y);
            _F.compute_proj_residual(PR, Y);
            restarter.save_LS(PR);
            restarter.save_hess(H);
            restarter.save_PW(PW);
        }

        #ifdef FABULOUS_DEBUG_MODE
        if (convergence) {
            auto mm = eval_current_solution(A, B, X);
            printf("real residual=(%e;%e)\n", mm.first, mm.second);
        }
        #endif

        print_footer();
        _logger.set_convergence(convergence);
        return convergence;
    }

}; // end class ArnoldiIB

/*! \brief NOT TEMPLATE designator for ArnoldiIB<HessIB<S>, S>  */
class ARNOLDI {};
inline auto ibdr() { return ARNOLDI{}; }


template<class S>
auto make_arnoldi(ARNOLDI, Logger &logger, int nbRHS, const Parameters &param, const int k, const bool Uk_const = false)
{
    return ArnoldiIBGCRO<HessGCRO<S>, S>{logger, nbRHS, param, k, Uk_const};
}

} // end namespace bgcro
} // end namespace fabulous

#endif // FABULOUS_ARNOLDI_IB_GCRO_HPP
