#ifndef FABULOUS_HPP
#define FABULOUS_HPP

#include "fabulous/utils/Timer.hpp"
#include "fabulous/algo/solve.hpp"
#include "fabulous/algo/AlgoType.hpp"
#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/utils/Color.hpp"

#endif // FABULOUS_HPP
