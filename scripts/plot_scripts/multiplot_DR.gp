#This script display convergence (i.e only residuals) of 3 files given
#in parameters


#To move the legend
set key center top

set term png size 1600,900

set output "Output_IB.png"

set title "IB-BGMRes, Residual" font ",14"

set tmargin 30
set lmargin 15
set rmargin 15

set xlabel "Number of Mat Vect Product" font ",8"
set xtics nomirror
set y2tics

set ylabel "Residual norm"
set y2label "Size of Block"
set y2range [0:7]

set tmargin 2
set title "Min Max Residuals" font ",11"
set logscale y

plot filename2 using 2:4  with l title "Min Res IB + CGS RUHE",\
     filename2 using 2:5  with l title "Max Res IB + CGS RUHE",\
     filename2 using 2:6  with l title "Min Real Res IB + CGS RUHE",\
     filename2 using 2:7  with l title "Max Real Res IB + CGS RUHE",\
     filename3 using 2:4  with l title "Min Res IB + CGS BLOCK",\
     filename3 using 2:5  with l title "Max Res IB + CGS BLOCK",\
     filename3 using 2:6  with l title "Min Real Res IB + CGS BLOCK",\
     filename3 using 2:7  with l title "Max Real Res IB + CGS BLOCK",\
     filename2 using 2:3  axes x1y2 with l title "RUHE : Size of Block",\
     filename3 using 2:3  axes x1y2 with l title "BLOCK : Size of Block"
