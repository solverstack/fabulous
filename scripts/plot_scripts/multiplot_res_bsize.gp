#!/usr/bin/env gnuplot

#fake plot in order to sort after and get max size.
plot filename using 1:3 with impulses linewidth 2
# filename="young1c_randomRHS_QR.res"
# filename2="young1c_RANDOM_RHS__CHAM_DESC_BLOCK.res"

#to get the max
VAR = GPVAL_DATA_Y_MAX
# to summ the time over iterations
a=0
cumul_sum(x)=(a=a+x,a)

#To move the legend
set key center top
set term png size 1600,1100
set output "Output_multi.png"

#set multiplot layout 2, 1 title "IB-BGMRes, Residual and Block Size" font ",20"

set tmargin 60
set lmargin 15
set rmargin 15

set xtics nomirror
set ylabel "Residual norm" font ",14"
set tmargin 2
set title "Min Max Residuals" font ",16"
set logscale y

plot filename using 2:4  with l title "Min Res QR",\
     filename using 2:5  with l title "Max Res QR"
#     filename2 using 3:6  with l title "Min Res QR Cham",\
#     filename2 using 3:7  with l title "Max Res QR Cham"

#unset ylabel
#set ylabel "Block Size" font ",14"
#set xlabel "Number of Mat Vect Product" font ",14"
#set y2label "Time" font ",14"
#set y2tics

#set title "Block Size and Elapsed Time" font ",16"


#unset logscale y
#plot filename using 2:(cumul_sum($8)) with lp axes x1y2 title "Cumulative time spent" ,\
#     filename using 2:3  with impulses title "SizeBlock" axes x1y1 lw 2
#
# set style histogram columns
# set style fill solid
# set key autotitle column
# set boxwidth 0.8
# set format y "    "
# set tics scale 0
# set title "Plot 3"
# plot 'immigration.dat' using 2 with histograms, \
#      '' using 7  with histograms , \
#      '' using 8  with histograms , \
#      '' using 11 with histograms
#
#unset multiplot
