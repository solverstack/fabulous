#!/usr/bin/env Rscript

args <- commandArgs(TRUE)
if (length(args) < 1) {
    stop("need filename as parameter")
}

library(ggplot2)
library(reshape2)
library(tools)

df <- read.table(args[1], header=T)
name <- basename(file_path_sans_ext(args[1]))
df$ID <- name

if (length(args) >= 2) {
    for (i in 2:length(args)) {
        d <- read.table(args[i], header=T)
        d$ID <- basename(file_path_sans_ext(args[i]))
        df <- rbind(df, d)
    }
}

plot_fabulous <- function(df) {
    id = df$ID;
    df$ID1 <- paste(id, "minRes")
    df$ID2 <- paste(id, "maxRes")
    df$ID3 <- paste(id, "minRealRes")
    df$ID4 <- paste(id, "maxRealRes")

    p <- ggplot(df, aes(nb_mvp)) +
        geom_line(aes(y=minRes, color=ID1)) +
        geom_line(aes(y=maxRes, color=ID2)) +
        geom_hline(aes(yintercept=1e-7, color="threshold"))
    ##geom_line(aes(y=minRealRes, color=ID3)) +
    ##geom_line(aes(y=maxRealRes, color=ID4))
    pl = p + scale_y_log10()
    return (pl);
}

plot_time <- function(df) {
    p <- ggplot(df, aes(global_iteration)) +
        geom_line(aes(y=time, color="time")) +
        geom_line(aes(y=least_square_time, color="least_square_time")) +
        geom_line(aes(y=mvp_spent, color="mvp_time")) +
        geom_line(aes(y=ortho_spent, color="ortho_time"))
}
## pl <- plot_fabulous(df) + ggtitle("Matcone (dim=1320, nbRHS=26)")
## pl

pl <- plot_time(df)
pl + ggtitle("Arnoldi std")

