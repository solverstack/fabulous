#!/bin/bash

types=("RUHE" "BLOCK")
schemes=("CGS" "ICGS" "MGS" "IMGS")
matrices=("young1c")
# matrices+=( "young4c" "bidiagonalmatrix4" "bcsstk14" "sherman4" )

sizes=(200 400 600)
# algos=("IB" "STD")

plot_space_variable()
{
    for matrix in ${matrices[@]}; do
        for scheme in ${schemes[@]}; do
            for typ in ${types[@]}; do
                ./plot.r ${matrix}_r=200_${scheme}_${typ}_IB_Classic_Restarting.res \
                         ${matrix}_r=400_${scheme}_${typ}_IB_Classic_Restarting.res \
                         ${matrix}_r=600_${scheme}_${typ}_IB_Classic_Restarting.res
                mv Rplots.pdf ${matrix}_r=200-400-600_${scheme}_${typ}_IB_Classic_Restarting.pdf
            done
        done
    done
}

plot_type_variable()
{
    for matrix in ${matrices[@]}; do
        for scheme in ${schemes[@]}; do
            for size in ${sizes[@]}; do
                ./plot.r ${matrix}_r=${size}_${scheme}_RUHE_IB_Classic_Restarting.res \
                         ${matrix}_r=${size}_${scheme}_BLOCK_IB_Classic_Restarting.res
                mv Rplots.pdf ${matrix}_r=${size}_${scheme}_RUHE-BOCK_IB_Classic_Restarting.pdf
            done
        done
    done
}

plot_scheme_variable()
{
    for matrix in ${matrices[@]}; do
        for size in ${sizes[@]}; do
            for typ in ${types[@]}; do
                ./plot.r ${matrix}_r=${size}_CGS_${typ}_IB_Classic_Restarting.res \
                         ${matrix}_r=${size}_ICGS_${typ}_IB_Classic_Restarting.res \
                         ${matrix}_r=${size}_MGS_${typ}_IB_Classic_Restarting.res \
                         ${matrix}_r=${size}_IMGS_${typ}_IB_Classic_Restarting.res
                mv Rplots.pdf ${matrix}_r=${size}_CGS-ICGS-MGS-IMGS_${typ}_IB_Classic_Restarting.pdf
            done
        done
    done
}


plot_space_variable
