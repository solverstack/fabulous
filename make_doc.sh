#!/bin/bash
FABULOUS_ROOT_DIR=$PWD # Fabulous path
# Create documentation in ${FABULOUS_ROOT_DIR}/build/doc
mkdir -p ${FABULOUS_ROOT_DIR}/build_doc
cd ${FABULOUS_ROOT_DIR}/build_doc
cmake .. -DFABULOUS_BUILD_DOC=ON -DBUILD_SHARED_LIBS=OFF
cd doc
make
cd ${FABULOUS_ROOT_DIR}
