\documentclass[c,12pt]{beamer}
%
% Packages pour le français
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[]{algorithm2e}
\usepackage{tikz}
\usepackage{amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage{color}
\errorcontextlines 10000

% pour un pdf lisible à l'écran
% il y a d'autres choix possibles
\usepackage{pslatex}
%
% pour le style et couleurs
\usetheme{CambridgeUS}
%

% contenu de la page de titre
\title[IB BGMRES DR]{
  Block GMRES method with Inexact Breakdown and
  deflated restarting for multiple right-hand sides problems
}

\author{Thomas Mijieux,Cyrille Piacibello,Luc Giraud}
\institute[HiePACS]{Inria, HiePACS}

%
% Fin du préambule
%
\begin{document}

\frame{\titlepage}
\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% RHS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Right hand side management}
\begin{frame}
  \frametitle{Inexact Breakdown on $R0$}
  \begin{block}{Parameters:}
    (number of RHS) $p = 5$
  \end{block}

  \begin{block}{Inexact breakdown on R0:}
    $(Q_0, \Lambda_0) \leftarrow  QR(R_0)$ \\
    $\mathbb{U}_1\Sigma_1\mathbb{V}_1 + \mathbb{U}_2\Sigma_2\mathbb{V}_2 \leftarrow SVD(\Lambda_0)$ \\
    $\Sigma_1 = \{ singular values > \epsilon\}$ ($p_1 = 3$ values) \\
    $\Sigma_2 = \{ singular values \leq \epsilon\} \neq \emptyset$  ($p - p_1$ = 2 values)\\

    $V_1 \leftarrow Q_0\mathbb{U}_1$ (3 vectors)\\
    $P_0 \leftarrow Q_0\mathbb{U}_2$ (2 vectors)\\
    $\Lambda_1 \leftarrow \left[ \begin{smallmatrix} \mathbb{U}_1^H\Lambda_0\\\mathbb{U}_2^H\Lambda_0 \end{smallmatrix} \right]$
    (such  as  $R_0 = [V_1,P_0]\Lambda_1$)
  \end{block}
\end{frame}

\begin{frame}
  \only<1> { \frametitle{Initialization} }
  \only<8> { \frametitle{Expand Matrices} }
  \only<2-4,13-16> { \frametitle{Expand $\mathscr{F}$ with orthogonalization coefficients} }
  \only<5> { \frametitle{R-criterion} }
  \only<6-7> { \frametitle{Update $\mathscr{F}$ structure} }
  \only<9-12> { \frametitle{Update $\Phi$ and $\Lambda$ (RHS)} }

  \begin{minipage}[t]{.48\textwidth}
    \begin{block}{$\mathscr{F}$}
      \begin{tikzpicture}
        %% Grid column 1
        \draw (-0.1,5);
        \draw (-0.1,3-1/5-0.1);

        \only<-7> {
          \draw[step=1/5,dotted,thin,gray] (0,5) grid (3/5,4-3/5);
          \draw (0,5) rectangle (3/5,4-3/5);
        }

        %% Column 1
        \only<2->{ \draw[fill=red,opacity=0.4](0,5) rectangle (3/5,4+2/5); }
        \only<3-7>{ \draw[fill=yellow,opacity=0.4](0,4+2/5) rectangle (3/5,4); }
        \only<4-7>{ \draw[fill=green,opacity=0.4](0,4)--(3/5,4)--(3/5,3+2/5)--cycle; }

        % Grid column2
        \only<8-> {
          \draw[step=1/5,dotted,thin,gray] (0,5) grid (6/5,3-1/5);
          \draw (0,5) rectangle (6/5,3-1/5);
        }

        %% update column 1:
        \only<6-7> {
          \draw[blue,very thick] (0,4+2/5) rectangle (3/5,3+2/5);
        }

        \only<6-> { \draw[fill=purple,opacity=0.4] (0,4+2/5) rectangle (3/5,4-1/5); }
        \only<6> { \draw[red,very thick] (0+1/30,4+2/5-1/30) rectangle (3/5-1/30,4-1/5+1/30); }
        \only<7-> { \draw[fill=orange,opacity=0.6] (0,4-1/5) rectangle (3/5,3+2/5); }
        \only<7> { \draw[red,very thick] (0+1/30,4-1/5-1/30) rectangle (3/5-1/30,3+2/5+1/30); }

        %% Column 2
        \only<13->{ \draw[fill=red,opacity=0.4](3/5,5) rectangle (6/5,4+2/5); }
        \only<14->{ \draw[fill=red,opacity=0.4](3/5,4+2/5) rectangle (6/5,4-1/5); }
        \only<15->{ \draw[fill=yellow,opacity=0.4](3/5,4-1/5) rectangle (6/5,3+2/5); }
        \only<16->{\draw[fill=green,opacity=0.4] (3/5,3+2/5)--(6/5,3+2/5)--(6/5,3-1/5)--cycle; }

      \end{tikzpicture}
    \end{block}
    \only<2-> {
      \begin{block}{Operations}
        \only<2,13-14>{ $\textcolor{red}{H_{i,j}} := V_i^HW$  }
        \only<3,15>{ $\textcolor{red}{C_j} := P_{j-1}^HW$ }
        \only<4,16>{ $\widetilde{W_j}\textcolor{red}{D_j} := W - P_{j-1}^HC_j$ }
        \only<5>{ R-criterion\\ ($\rightarrow$ No Inexact Breakdown) }
        \only<6>{ Update $\mathscr{F}$: $\textcolor{red}{\mathscr{L}_{j+1,:}}
          = \mathbb{W}_1^H\textcolor{blue}{\mathbb{H}_j}$  }
        \only<7>{ Update $\mathscr{F}$: $\textcolor{red}{G_j}
          = \mathbb{W}_2^H\textcolor{blue}{\mathbb{H}_j}$ }
        \only<8>{ Next step: bigger matrices}
        \only<9-10>{ Update $\Phi_j$: \\ $\textcolor{red}{\square} := \textcolor{blue}{[\mathbb{W}_1,\mathbb{W}_2]^H}*\textcolor{red}{\square}$ }
        \only<11-12>{ Update RHS $\textcolor{red}{\Lambda_j} := \textcolor{blue}{\Phi_j\Lambda_1}$ }
      \end{block}
    }
  \end{minipage}
  \hfill
  \begin{minipage}[t]{.48\textwidth}
    \begin{block}{$\Phi_j$  and  (RHS) $\Lambda_j$ }
      \begin{tikzpicture}[shift={(0,1)}]
        \draw (-1.3,7);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% \Lambda_1

        \draw (2,6.5)--(3,6.5)--(3,5.5)--(2,5.5)--cycle;
        \draw[fill=red,opacity=0.4] (2,6.5)--(3,6.5)--(3,5.5)--(2,5.5)--cycle;
        \node[anchor=west] at (3.3,6) { $\Lambda_1$ };
        \only<11-12> { \draw[blue,very thick] (2,6.5)--(3,6.5)--(3,5.5)--(2,5.5)--cycle; }
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% \Phi

        \only<-7> {
          \draw[step=1/5,dotted,very thin,gray] (0,3+2/5) grid (1,5); % grid \Phi
          \draw (0,3+2/5) rectangle (1,5); % border \Phi

          \draw[<->] (-0.3,5)--(-0.3,4);
          \draw[<->] (-0.3,4)--(-0.3,3+2/5);

          \node[anchor=east,align=left] at (-0.4,3.9+3/5) { \tiny $p$ };
          \node[anchor=east,align=left] at (-0.4,3.1+3/5) { \tiny $p_j$ };
        }
        \only<8-> {
          \draw[step=1/5,dotted,very thin,gray] (0,3-1/5) grid (1,5); % grid \Phi
          \draw (0,3-1/5) rectangle (1,5); % border \Phi

          \draw[<->] (-0.3,5)--(-0.3,4+2/5);
          \draw[<->] (-0.3,5-3/5)--(-0.3,4-3/5);
          \draw[<->] (-0.3,3+2/5)--(-0.3,3-1/5);

          \node[anchor=east,align=left] at (-0.4,5-3/10) { \tiny $n_j - 1$ };
          \node[anchor=east,align=left] at (-0.4,3.9) { \tiny $p$ };
          \node[anchor=east,align=left] at (-0.4,3.1) { \tiny $p_j$ };
        }

        \foreach \i in {0, ..., 4} {
          \draw[fill=green,opacity=0.4]
          (0+1/5*\i,5-1/5*\i)--(0+1/5*\i,5-1/5*\i-1/5)
          --(0+1/5*\i+1/5,5-1/5*\i-1/5)--(0+1/5*\i+1/5,5-1/5*\i)--cycle;
        }
        \only<10-> { \draw[fill=brown] (0,4+2/5) rectangle (1, 3+2/5); }
        \only<9-10> { \draw[red,very thick] (0,4+2/5) rectangle (1, 3+2/5); }
        \only<11-12> { \draw[blue,very thick] (0,3-1/5) rectangle (1,5); } % blue border

        \node[anchor=east] at (-0.5,2.5) { $\Phi_j$ };

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% \Lambda

        \only<-7> {
          \draw[step=1/5,dotted,very thin,gray] (2,3+2/5) grid (3,5); % grid \Lambda
          \draw (2,3+2/5) rectangle (3,5); % border \Lambda
        }
        \only<8-> {
          \draw[step=1/5,dotted,very thin,gray] (2,3-1/5) grid (3,5); % grid \Lambda
          \draw (2,3-1/5) rectangle (3,5); % border \Lambda
        }

        \only<11-12> {
          \draw[red,very thick] (2,3-1/5) rectangle (3,5);  % red border \Lambda
          \draw[->] (1.25,3.9) -- (1.75,3.9);
          \draw[->] (2.5,5.4) -- (2.5,5.1);
        }
        \only<-11> { \draw[fill=red,opacity=0.4] (2,5)--(3,5)--(3,4)--(2,4)--cycle; } % red fill
        \only<12-> { \draw[fill=red,opacity=0.4] (2,3+2/5) rectangle (3,5); } % red fillin


        \node[anchor=west] at (3.3,2.5) { $\Lambda_j$ };

      \end{tikzpicture}
    \end{block}
  \end{minipage}
  \vfill

  Note: $n_j = \sum_k{p_k}$ = \only<-7>{3} \only<8->{6}
\end{frame}
\end{document}
