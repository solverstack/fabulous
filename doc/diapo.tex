\documentclass[c,12pt]{beamer}
%
% Packages pour le français
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[]{algorithm2e}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{color}
\errorcontextlines 10000

% pour un pdf lisible à l'écran
% il y a d'autres choix possibles
\usepackage{pslatex}
%
% pour le style et couleurs
\usetheme{CambridgeUS}
%

% contenu de la page de titre
\title[IB BGMRES DR]{Block GMRES method with Inexact Breakdown and
  deflated restarting for multiple right-hand sides problems}

%%\subtitle{Présentation des travaux préliminaires.}

\author{Cyrille Piacibello, Luc Giraud}
\institute[HiePACS]{Inria, HiePACS}

%
% Fin du préambule
%
\begin{document}

\frame{\titlepage}
\setcounter{tocdepth}{2}

\section{Introduction}
\begin{frame}
  \tableofcontents
\end{frame}


\label{sec-4-1}
\subsection{Goals and motivations}
\label{sec-4-1-1}

\begin{frame}[label=sec-4-1-1-2]{Iterative Solver for Multiple RHS}

  \only<1>{
    \begin{block}{GMRES: Problem}
      Solve
      $$ A x = b $$

      with A $\in \mathbb{C}^{n \times n}$,
      x and b $\in \mathbb{C}^{n \times 1}$.
    \end{block}
  }

  \begin{block}{Block GMRES: Problem}
    Solve
    $$ A X = B $$

    with A $\in \mathbb{C}^{n \times n}$,
    X and B $\in \mathbb{C}^{n \times p}$.
  \end{block}

  \only<2,3>{

    \begin{block}{References}
      \begin{itemize}
      \item \only<2>{Restarted Block GMRES with Deflation of Eigenvalues, Ronald B Morgan, \textit{Applied Numerical Mathematics, 2005}}
        \only<3>{Deflated Restarting:: DR}
      \item \only<2>{Exact and Inexact Breakdowns in the Block GMRES method, Mickaël Robbé and Miloud Sadkane, \textit{Linear Algebra and its Applications, 2006}}
        \only<3>{Inexact Breakdown:: IB}
      \item \only<2>{Block GMRES method with Inexact Breakdowns and Deflated Restarting, E. Agullo, L. Giraud and Y-F Jing, \textit{Society for Industrial and Applied Mathematics, 2014}}
        \only<3>{IB and DR combination inside BGMRES:: IB-BGMRES-DR}
      \end{itemize}
    \end{block}
  }
\end{frame}

\section{BGMRES}
\subsection{BGMRES with incremental QR}

\begin{frame}
  \frametitle{Incremental QR for Least Square}

  \begin{block}{Main numerical kernel}
    Least square:
    $$
    min \left\Vert H_m \times Y_m - \left[ \begin{array}{c} R_1 \\ 0_m  \end{array} \right] \right\Vert_2
    $$
    \only<1>{  with $H_m$ the Hessenberg, $R_1$ the R part of initial residual after a QR factorization.}
  \end{block}

  \begin{block}{Incremental QR - Idea}
    \only<2>{
      The Hessenberg matrix is augmented by a block column and a row at each iteration:

      $$
      H_m =
      \begin{pmatrix} H_{m-1} & C_{n_m\times p} \\
        0_{p \times n_m} & R
      \end{pmatrix}
      $$

      Idea: instead of keeping the Hessenberg, we keep a factorized form,
      and update this factorized form at each step.
    }
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{BGMRES with incremental QR - Results}
  \includegraphics[width=.9\paperwidth]{image_dox/Output_Time_BGMRes_QR.png}
\end{frame}

\begin{frame}
  \frametitle{Same results with a restart}
  \includegraphics[width=.9\paperwidth]{image_dox/Output_Time_BGMRes_QR_restart.png}
\end{frame}

\section{IB - Inexact Breakdown}
\label{sec-4-1-2}
\begin{frame}[label=sec-4-1-2-1]{IB - Inexact Breakdown}
  \begin{block}{Arnoldi's relation for GMRES}
    Arnoldi relation:
    $$
    A \times V_m = V_m \times H_m + h_{m+1,m} \times v_{m+1} \times e^{H}_{m}
    $$
    Breakdown occurs when $h_{m+1,m} = 0$.
  \end{block}

  If $h_{m+1,m} = 0$, then the solution is inside the search space $\rightarrow$ Happy Breakdown

  \begin{block}{Arnoldi's relation for Block GMRES}
    Arnoldi relation:
    $$
    A \times \mathcal{V}_m = \mathcal{V}_m \times \mathcal{H}_m + [0_{n \times n_m}, W_m ]
    $$
    Inexact Breakdown occurs when $W_m$ is no longer of full rank.
  \end{block}

  If $W_m$ is low rank, then the search space contains some of the solutions. We can exploit that !
\end{frame}
\begin{frame}[label=sec-4-1-2-2]{IB - Test case}
  This feature is available since June 2016.
  Following test case extracted from Matrix Market.
  \begin{itemize}
  \item Matrix Young1c
  \item Arithmetic: Complex (double)
  \item Size: 841
  \item Number of RHS: 20
  \item Maximum Krylov space before restart: 300.
  \end{itemize}

  Metrics:
  \begin{itemize}
  \item y1-axis: Backward error (Min/Max over rhs displayed)
  \item y2-axis: Size of current block
  \item Right y-axis: Cumulative Time spent during iterations
  \item x-axis: Number of Matrix Vector products computed.
  \end{itemize}
\end{frame}

\begin{frame}[label=sec-4-1-2-3]{IB - Results}
  \includegraphics[width=.9\paperwidth]{image_dox/bgmres_IB_time-histo-bsize.png}
\end{frame}

\section{DR - Deflation at restart}

\begin{frame}[label=sec-4-1-2-4]{DR - Deflation at restart}
  \only<1>{
    \begin{block}{Main Idea}
      The idea is to re-use some of the space
      spanned during previous cycle at restart, to speed up the
      convergence. To select the space, a generalized eigenvalue
      problem need to be solved.
    \end{block}

    \begin{alertblock}{Disclaimer}
      I'm about to be dishonest !

      The following results correspond to what we expect, but the DR version used to generate the graph is not fully stabilized, so results may vary.
    \end{alertblock}
  }%

  \only<2>{
    \centering \includegraphics[width=0.9\paperwidth]{image_dox/bgmres_IB_DR-histo.png}
  }%
\end{frame}

\section{Library}
\label{sec-4-1-3}
\begin{frame}[label=sec-4-1-3-1]{Overview}
  \begin{block}{Features}
    \begin{itemize}
    \item Arithmetics: Single and double, real and complex.
    \item Multiple precision ($\epsilon_i$ for $i \in [1..p]$, corresponding to each RHS)
    \item Right preconditionning (User defined)
    \item Multiple Orthogonalization Schemes (Classical and Modified, iterated or not)
    \end{itemize}
  \end{block}

  \begin{block}{Implementation details}
    \begin{itemize}
    \item Language: C++ (2011).
    \item Computational part: Lapack and Blas through C API.
    \item API: a C API is available.
    \item Build tool: CMake
    \item Documentation: Doxygen
    \end{itemize}
  \end{block}
\end{frame}
\begin{frame}[label=sec-4-1-3-2]{Current state of the library}
  \begin{block}{Three algorithms currently available:}
    \begin{itemize}
    \item BGMRES standard.
    \item BGMRES standard with an incremental QR factorization for computing efficiently the least square problem.
    \item BGMRES with detection of Inexact Breakdown
    \end{itemize}
  \end{block}
  \begin{block}{Current work:}
    \begin{itemize}
    \item BGMRES with Deflation at restart.
    \end{itemize}
  \end{block}
  \begin{block}{Future works:}
    \begin{itemize}
    \item Rely on Chameleon for the computational kernels.
    \item Incremental QR for Inexact Breakdown version. (When variable tile size will be available in Chameleon).
    \item Combining DR and IB.
    \end{itemize}
  \end{block}
\end{frame}

\begin{section}{In depth analysis of algorithms}
  \begin{subsection}{Adressing the Least Square solving}
    \begin{frame}
      \begin{block}{BGMRes}
        \begin{algorithm}[H]
          \only<1>{
            \KwData{Matrix A, Set of $b_i$ RHS denotated B, Set of $x0_i$ starting vectors denotated X0, Tolerance $\epsilon$, Max Number of step m}
            \KwResult{Set of vectors $x_{m_i}$ with $\frac{\|A \times x_{m_i} - b_i\|}{\|b_i\|} < \epsilon$ if converged}
          }
          \only<2>{
            $ R0 \leftarrow B - A \times X0  $\;
            $ \{V0,R1\} \leftarrow QR(R0) $\;
            \For{j from 0 to m}{
              $W = A \times V_{j}$\;
              \For{i from 0 to (j-1)}{
                $H_{i,j} \leftarrow V_i^{H} \times W$\;
                $W \leftarrow W - H_{i,j} \times V_i$\;
              }
              $\{V_j,H_{i+1,j}\} \leftarrow QR(W)$\;
              Solve $\| H \times Y - \begin{bmatrix}
                R1 \\ 0
              \end{bmatrix}\|$\;
              $X_m \leftarrow X_0 + \left[ V_0, ... , V_{j-1} \right] \times Y$\;
            }
          }
          \caption{Simple version}
        \end{algorithm}
      \end{block}
    \end{frame}

    \begin{frame}
      \begin{block}{BGMRes, \textcolor{red}{with right pre-conditionner}}

        \begin{algorithm}[H]
          \only<1>{
            \KwData{Matrix A, Set of $b_i$ RHS denotated B, Set of $x0_i$ starting vectors denotated X0, Tolerance $\epsilon$, Max Number of step m, \textcolor{red}{Right pre-cond M}}
            \KwResult{Set of vectors $x_{m_i}$ with $\frac{\|A \times x_{m_i} - b_i\|}{\|b_i\|} < \epsilon$ if converged}
          }
          \only<2>{
            $ R0 \leftarrow B - A \times X0  $\;
            $ \{V0,R1\} \leftarrow QR(R0) $\;
            \For{j from 0 to m}{
              $W \leftarrow A \times \textcolor{red}{M} \times V_{j}$\;
              \For{i from 0 to (j-1)}{
                $H_{i,j} \leftarrow V_i^{H} \times W$\;
                $W \leftarrow W - H_{i,j} \times V_i$\;
              }
              $\{V_j,H_{i+1,j}\} \leftarrow QR(W)$\;
              Solve $\| H \times Y - \begin{bmatrix}
                R1 \\ 0
              \end{bmatrix}\|$\;
              $X_m \leftarrow X_0 + \textcolor{red}{M} \times \left[ V_0, ... , V_{j-1} \right] \times Y$\;
            }
          }
          \caption{Precond version}
        \end{algorithm}
      \end{block}
    \end{frame}

    \begin{frame}
      \frametitle{Filling of the hessenberg}
      \begin{minipage}[t]{0.48\textwidth}
        \begin{block}{Arnoldi's part}
          \begin{algorithm}[H]
            \For{j from 0 to m}{
              $W \leftarrow A \times V_{j}$\;
              \For{i from 0 to (j-1)}{
                \only<1,4>{
                  $H_{i,j} \leftarrow V_i^{H} \times W$\;
                }
                \only<2,3>{
                  \textcolor{red}{
                    $H_{i,j} \leftarrow V_i^{H} \times W$\;
                  }
                }
                $W \leftarrow W - H_{i,j} \times V_i$\;
              }
              \only<1,2,3>{
                $\{V_j,H_{i+1,j}\} \leftarrow QR(W)$\;
              }
              \only<4>{
                $\{V_j,\textcolor{red}{H_{i+1,j}} \} \leftarrow QR(W)$\;
              }
            }
          \end{algorithm}
        \end{block}
      \end{minipage}\hfill
      \begin{minipage}[t]{0.48\textwidth}
        \begin{block}{Hessenberg}
          \begin{center}
            \begin{tikzpicture}
              %first column fill with gray
              \draw[fill=gray,opacity=0.5] (0,3)--(0,4)--(1,4)--(1,2)--cycle;
              \only<2,3,4>{
                %fill first block of second col
                \draw[fill=red,opacity=0.4] (1,4)--(2,4)--(2,3)--(1,3)--cycle;
              }
              \only<3,4>{
                \draw[fill=red,opacity=0.4] (1,3)--(2,3)--(2,2)--(1,2)--cycle;
              }
              \only<4>{
                \draw[fill=red,opacity=0.4] (1,2)--(2,2)--(2,1)--cycle;
              }
              %Last is grid in order to see it not matter what
              \draw[step=1cm,gray,very thin] (0,0) grid (3,4);
              \draw[black,very thin] (0,3)--(0,4)--(3,4)--(3,0)--cycle;
            \end{tikzpicture}
          \end{center}
          \only<2>{
            \textcolor{red}{
              $j=1$ and $i=0$
            }
          }
          \only<3>{
            \textcolor{red}{
              $j=1$ and $i=1$
            }
          }
        \end{block}
      \end{minipage}
    \end{frame}

    \begin{frame}
      \frametitle{Focus on solving $\| H \times Y - \begin{bmatrix}
          R1 \\ 0
        \end{bmatrix}\|$}
      \only<1>{
        \begin{block}{QR factorization of a block:}
          \begin{tikzpicture}
            \draw[step=1cm,gray,very thin] (0,0) grid (1,2);
            \draw[black,very thin] (0,1)--(0,2)--(1,2)--(1,0)--cycle;
          \end{tikzpicture}
          \hfill
          $\rightarrow$
          \hfill
          \begin{tikzpicture}
            % R factor
            \draw[fill=green,opacity=0.4] (0,2)--(1,1)--(1,2)--cycle;
            % hh reflectors
            \draw[fill=blue,opacity=0.4] (0,2)--(0,1)--(1,0) --(1,1)--cycle;
            % add diag
            \draw[step=1cm,gray,very thin] (0,0) grid (1,2);
            % draw 2 lines 1 col block
            \draw[black,very thin] (0,1)--(0,2)--(1,2)--(1,0)--cycle;
          \end{tikzpicture}
          \hfill
          with
          \begin{tikzpicture}
            \draw[fill=green,opacity=0.4] (0,2)--(1,1)--(1,2)--cycle;
          \end{tikzpicture}
          as the R part and
          \begin{tikzpicture}
            \draw[fill=blue,opacity=0.4] (0,2)--(0,1)--(1,0) --(1,1)--cycle;
          \end{tikzpicture}
          as the Householder reflectors (that can be combined in order to get the
          Q factor, or can be applied to any matrix as it is).
        \end{block}
      }
      \only<2,3,4,5>{
        \begin{block}{Incremental QR of Hessenberg}
          \begin{minipage}[t]{0.48\textwidth}
            \begin{tikzpicture}
              %first col
              \draw[fill=red,opacity=0.4] (0,3)--(0,4)--(1,4)--(1,2)--cycle;
              % R factor
              \draw[fill=green,opacity=0.4] (0,4)--(1,3)--(1,4)--cycle;
              % hh reflectors
              \draw[fill=blue,opacity=0.4] (0,4)--(0,3)--(1,2) --(1,3)--cycle;

              \only<3,4,5>{
                %new arnoldi step
                \draw[fill=red,opacity=0.4] (1,4)--(2,4)--(2,1) --(1,2)--cycle;
              }
              \only<4,5>{
                \draw[fill=yellow,opacity=0.4] (1,4)--(2,4)--(2,2) --(1,2)--cycle;
                %arrow
                \draw[->,thick] (0.5,3)--(1.5,3);
              }
              \only<5>{
                % R factor
                \draw[fill=green,opacity=0.4] (1,3)--(2,2)--(2,3)--cycle;
                % hh reflectors
                \draw[fill=blue,opacity=0.4] (1,3)--(1,2)--(2,1) --(2,2)--cycle;

                % draw 2 lines 1 col block
                \draw[black,very thin] (1,2)--(1,3)--(2,3)--(2,1)--cycle;
              }
              %Grid
              \draw[step=1cm,gray,very thin] (0,0) grid (3,4);
              \draw[black,very thin] (0,3)--(0,4)--(3,4)--(3,0)--cycle;
            \end{tikzpicture}
          \end{minipage}
          %
          \begin{minipage}[b]{0.48\textwidth}
            \begin{itemize}
            \item Step 1: Compute QR facto of first block
            \item Step 2: Fill the col with one arnoldi iteration
            \item Step 3: Apply reflectors to first block
            \item Step 4: Compute QR facto of the two bottom block
            \end{itemize}
          \end{minipage}
        \end{block}
      }
      \only<6,7,8,9>{
        \begin{block}{Incremental QR of Hessenberg}
          \begin{minipage}[t]{0.48\textwidth}
            \begin{tikzpicture}
              %fill everything in red
              \draw[fill=red,opacity=0.4] (0,3)--(0,4)--(3,4)--(3,0)--cycle;

              % R factor
              \draw[fill=green,opacity=0.4] (0,4)--(1,3)--(1,4)--cycle;
              % hh reflectors
              \draw[fill=blue,opacity=0.4] (0,4)--(0,3)--(1,2) --(1,3)--cycle;

              \draw[fill=yellow,opacity=0.4] (1,4)--(2,4)--(2,3) --(1,3)--cycle;

              % R factor
              \draw[fill=green,opacity=0.4] (1,3)--(2,2)--(2,3)--cycle;
              % hh reflectors
              \draw[fill=blue,opacity=0.4] (1,3)--(1,2)--(2,1) --(2,2)--cycle;

              %arrow
              \only<7,8,9>{
                \draw[fill=yellow,opacity=0.4] (2,2)--(2,4)--(3,4)--(3,2)--cycle;
                \draw[->,thick] (0.5,3)--(2.5,3);
              }
              \only<8,9>{
                \draw[fill=yellow,opacity=0.4] (2,1)--(2,3)--(3,3)--(3,1)--cycle;
                \draw[->,thick] (1.5,2)--(2.5,2);
              }
              \only<9>{
                % R factor
                \draw[fill=green,opacity=0.4] (2,2)--(3,2)--(3,1)--cycle;
                % hh reflectors
                \draw[fill=blue,opacity=0.4] (2,2)--(3,1)--(3,0) --(2,1)-- (2,2)-- cycle;
              }
              %Grid
              \draw[step=1cm,gray,very thin] (0,0) grid (3,4);
              \draw[black,very thin] (0,3)--(0,4)--(3,4)--(3,0)--cycle;
            \end{tikzpicture}
          \end{minipage}
          %
          \begin{minipage}[b]{0.48\textwidth}
          \end{minipage}
        \end{block}
      }

    \end{frame}

    \begin{frame}
      \frametitle{Focus on solving $\| H \times Y - \begin{bmatrix}
          R1 \\ 0
        \end{bmatrix}\|$}
      \begin{block}{Back-Solve}
        Least Square problem is shifted from:
        $\| H \times Y - \begin{bmatrix}
          R1 \\ 0
        \end{bmatrix}\|$ to

        $\| R \times Y - \begin{bmatrix}
          RHS
        \end{bmatrix}\|$, with recurrence relation on RHS as follow:
      \end{block}
      \only<2>{
        \begin{block}{Initialization}
          \centering
          RHS =
          \begin{tikzpicture}
            % R factor
            \draw[fill=green,opacity=0.4] (0,2)--(1,1)--(1,2)--cycle;
            % hh reflectors
            \draw[fill=blue,opacity=0.4] (0,2)--(0,1)--(1,0) --(1,1)--cycle;
            % add diag
            \draw[step=1cm,gray,very thin] (0,0) grid (1,2);
            % draw 2 lines 1 col block
            \draw[black,very thin] (0,1)--(0,2)--(1,2)--(1,0)--cycle;
          \end{tikzpicture}
          $\times$
          \begin{tikzpicture}
            \draw[black,very thin] (0,0)--(0,1)--(1,1)--(1,0)--cycle;
            \draw (0.5,0.5) node{R1};
          \end{tikzpicture}
        \end{block}
      }
      \only<3>{
        \begin{block}{Recurrence}
          At step M, apply last Householder generated to last block of RHS:
          \centering
          \begin{tikzpicture}
            % R factor
            \draw[fill=green,opacity=0.4] (0,2)--(1,1)--(1,2)--cycle;
            % hh reflectors
            \draw[fill=blue,opacity=0.4] (0,2)--(0,1)--(1,0) --(1,1)--cycle;
            % add diag
            \draw[step=1cm,gray,very thin] (0,0) grid (1,2);
            % draw 2 lines 1 col block
            \draw[black,very thin] (0,1)--(0,2)--(1,2)--(1,0)--cycle;

            \draw (4.5,1.5) node{RHS};
            \draw[black,very thin] (4,0)--(4,2)--(5,2)--(5,0)--cycle;
            \draw[->] (0.5,1)--(4.5,1.5);
          \end{tikzpicture}
        \end{block}
      }
      \only<4>{
        \begin{block}{Solving}
          Solve the triangular system $R \times Y - RHS$.
        \end{block}
      }
    \end{frame}
    \subsection{BGMRes: Adressing the rank loss during solving}
    \begin{frame}
      \begin{block}{Arnoldi's part}
        \begin{algorithm}[H]
          \For{j from 0 to m}{
            $W \leftarrow A \times V_{j}$\;
            \For{i from 0 to (j-1)}{
              $H_{i,j} \leftarrow V_i^{H} \times W$\;
              $W \leftarrow W - H_{i,j} \times V_i$\;
            }
            $\{V_j,H_{i+1,j}\} \leftarrow QR(W)$\;
          }
        \end{algorithm}
      \end{block}
    \end{frame}
  \end{subsection}
\end{section}
\end{document}
