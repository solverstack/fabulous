\documentclass[c,12pt]{beamer}
%
% Packages pour le français
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[]{algorithm2e}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{color}
\errorcontextlines 10000

% pour un pdf lisible à l'écran
% il y a d'autres choix possibles
\usepackage{pslatex}
%
% pour le style et couleurs
\usetheme{CambridgeUS}
%

% contenu de la page de titre
\title[IB BGMRES DR] {
  Block GMRES method with Inexact Breakdown and
  deflated restarting for multiple right-hand sides problems
}

%% \subtitle{Présentation des travaux préliminaires.}

\author{Thomas Mijieux,Cyrille Piacibello,Luc Giraud}
\institute[HiePACS]{Inria, HiePACS}

%
% Fin du préambule
%
\begin{document}

\frame{\titlepage}
\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Fill ``Hessenberg'' With Inexact Breakdown}
\begin{frame}
  \frametitle{Notations}
  \begin{minipage}[t]{.48\textwidth}
    \begin{tikzpicture}
      \draw[fill=purple,opacity=0.6] (1,14)--(1,13)--(2,13)--(2,14)--cycle;
      \node[anchor=west] at (2.25, 13.5) { $\mathscr{L}_{j+1,:}$ };

      \draw[fill=orange,opacity=0.8] (1,12)--(1,11)--(2,11)--(2,12)--cycle;
      \node[anchor=west] at (2.25, 11.5) { $G_{j-1 }$ };

      \draw[fill=yellow,opacity=0.4] (1,10)--(1,9)--(2,9)--(2,10)--cycle;
      \node[anchor=west] at (2.25, 9.5) { $C_j$ };

      \draw[fill=green,opacity=0.4] (1,8)--(1,7)--(2,7)--(2,8)--cycle;
      \node[anchor=west] at (2.25, 7.5) { $D_j$ };

      %% right part:

      \draw[red,very thick] (5,13)--(5,12)--(6,12)--(6,13)--cycle;
      \node[anchor=west] at (6.25, 12.5) { What is being computed. };

      \draw[blue,very thick] (5,11)--(5,10)--(6,10)--(6,11)--cycle;
      \node[anchor=west] at (6.25, 10.5) { Taking part in the computation. };

      \draw[fill=red,opacity=0.4] (5,9)--(5,8)--(6,8)--(6,9)--cycle;
      \node[anchor=west] at (6.25, 8.5) { Orthogonalization coefficients. };
    \end{tikzpicture}
  \end{minipage}
\end{frame}

\begin{frame}
  \only<1,4,15,24> { \frametitle{Expand the base} }
  \only<2-3,5-7,16-20,25-30> { \frametitle{Expand $\mathscr{F}$ with orthogonalization coefficients} }
  \only<11-14,22-23> { \frametitle{Update $\mathscr{F}$ structure} }
  \only<8-10,21,31> { \frametitle{R-criterion} }
  \only<32> { \frametitle{General structure of $\mathscr{F}$ } }
  \begin{minipage}[t]{.48\textwidth}
    \begin{block}{$\mathscr{F}$}
      \begin{tikzpicture}
        \draw (-0.2,5.2);

        % Grid
        \draw[step=0.3333333333cm,gray,very thin,dashed] (0,0) grid (4,5);
        \draw[step=1cm,gray,very thin] (0,2) grid (2,5);
        \draw[step=1cm,gray,very thin] (0,0)--(0,5)--(4,5)--(4, 0)--cycle;
        \only<-9> {
          \draw[step=1cm,gray,very thin] (0,0) grid (2,2);
          \draw[step=1cm,gray,very thin] (2,0) grid (4,5);
        }
        \only<10-> {
          \foreach \i in {0, ..., 2} {
            \draw[gray,very thin] ({2+(2/3)*\i},0)--({2+(2/3)*\i},5);
          }
          \foreach \i in {0, ..., 3} {
            \draw[gray,very thin] (2,{2-(2/3)*\i})--(4,{2-(2/3)*\i});
          }
          \foreach \i in {2, ..., 4} {
            \draw[gray,very thin] (2,\i)--(4,\i);
          }
        }

        %% L :: 1st column
        \only<2-> { \draw[fill=red,opacity=0.4] (0,5)--(1,5)--(1,4) --(0,4)--cycle;  }
        \only<2>{ \draw[red,very thick] (0,5)--(1,5)--(1,4) --(0,4)--cycle; }
        \only<3-> { \draw[fill=red,opacity=0.4] (0,4)--(1,4)--(1,3)--cycle; }
        \only<3> { \draw[red,very thick] (0,4)--(1,4)--(1,3)--cycle; }

        %% L :: 2nd column
        \only<5-> { \draw[fill=red,opacity=0.4] (1,5)--(2,5)--(2,4) --(1,4)--cycle; }
        \only<5>  { \draw[red,very thick] (1,5)--(2,5)--(2,4) --(1,4)--cycle;  }
        \only<6-> { \draw[fill=red,opacity=0.4] (1,4)--(2,4)--(2,3) --(1,3)--cycle;  }
        \only<6>  { \draw[red,very thick]  (1,4)--(2,4)--(2,3) --(1,3)--cycle;  }
        \only<7-14> { \draw[fill=red,opacity=0.4] (1,3)--(2,3)--(2,2)--cycle; }
        \only<7> { \draw[red,very thick] (1,3)--(2,3)--(2,2)--cycle; }

        % GELS (F-Y - LAMBDA )
        \only<8> { \draw[blue,very thick](0,5)--(2,5)--(2,2)--(0,2)--cycle;  }

        % 1st Breakdown!
        % L_{j+1,:} (fill)
        \only<13-> { \draw[fill=purple,opacity=0.6]  (1,3)--(2,3)--(2,7/3)--(1,7/3)--cycle; }
        \only<13> {
          \draw[red,very thick]
          (1+1/20,3-1/20)--(2-1/20,3-1/20) --   %L_{j+1,:} (border)
          (2-1/20,7/3+1/20)--(1+1/20,7/3+1/20)--cycle;
        }
        % G_j (fill)
        \only<14-23> { \draw[fill=orange,opacity=0.8] (1,2+1/3)--(2,2+1/3)--(2,2)--(1,2)--cycle; }
        \only<14> {
          \draw[red,very thick] % G_j (border)
          (1+1/20,2+1/3-1/20)--(2-1/20,2+1/3-1/20) --
          (2-1/20,2+1/20)--(1+1/20,2+1/20)--cycle;
        }
        \only<13-14> { % H_j ( border )
          \draw[blue,very thick]
          (1-1/20,3+1/20)--(2+1/20,3+1/20)--
          (2+1/20,2-1/20)--(1-1/20,2-1/20)--cycle;
        }

        %% L :: 3th column
        \only<16-> {\draw[fill=red,opacity=0.4] (2,5)--(8/3,5)--(8/3,4) --(2,4)--cycle; }
        \only<16>  {\draw[red,very thick] (2,5)--(8/3,5)--(8/3,4) --(2,4)--cycle; }
        \only<17-> {\draw[fill=red,opacity=0.4] (2,4)--(8/3,4)--(8/3,3) --(2,3)--cycle; }
        \only<17>  {\draw[red,very thick] (2,4)--(8/3,4)--(8/3,3) --(2,3)--cycle; }
        \only<18-> {\draw[fill=red,opacity=0.4] (2,3)--(8/3,3)--(8/3,7/3)--(2,7/3)-- cycle;}
        \only<18>  {\draw[red,very thick] (2,3)--(8/3,3)--(8/3,7/3)--(2,7/3)-- cycle;}

        \only<19-23> {\draw[fill=yellow,opacity=0.4] (2,7/3)--(8/3,7/3)--(8/3,2)--(2,2)-- cycle; }
        \only<19> {\draw[red,very thick] (2,7/3)--(8/3,7/3)--(8/3,2)--(2,2)-- cycle; }
        \only<20-23> {\draw[fill=green,opacity=0.4] (2,2)--(8/3,2)--(8/3,4/3)--cycle; }
        \only<20> {\draw[red,very thick] (2,2)--(8/3,2)--(8/3,4/3)--cycle; }

        % Update 2 (No breakdown; 1st breakdown already occured)
        % F
        \only<21> {\draw[blue,very thick] (3-1/3,4/3)--(3-1/3,5)--(0,5)--(0,4/3)--cycle; }
        \only<22-23> { % H_j ( border )
          \draw[blue,very thick]
          (1-1/20,2+1/3+1/20)--(2+2/3+1/20,2+1/3+1/20)--
          (2+2/3+1/20,1+1/3-1/20)--(1-1/20,1+1/3-1/20)--cycle;
        }
        % L_{j+1,:} (fill)
        \only<22-> { \draw[fill=purple,opacity=0.6]
          (1,2+1/3)--(2+2/3,2+1/3)--(2+2/3,1+2/3)--(1,1+2/3)--cycle; }
        \only<22> {
          \draw[red,very thick]
          (1+1/20,2+1/3-1/20)--(2+2/3-1/20,2+1/3-1/20)-- %L_{j+1,:} (border)
          (2+2/3-1/20,1+2/3+1/20)--(1+1/20,1+2/3+1/20)--cycle;
        }
        \only<23-> { \draw[fill=orange,opacity=0.8]   % G_j (fill)
          (1,2-1/3)--(2+2/3,2-1/3)--(2+2/3,1+1/3)--(1,1+1/3)--cycle; }
        \only<23> {
          \draw[red,very thick]   % G_j (border)
          (1+1/20,2-1/3-1/20)--(2+2/3-1/20,2-1/3-1/20)--
          (2+2/3-1/20,1+1/3+1/20)--(1+1/20,1+1/3+1/20)--cycle;
        }

        %% L :: 4th column
        \only<25->{\draw[fill=red,opacity=0.4] (8/3,5)--(10/3,5)--(10/3,4)--(8/3,4)--cycle;}
        \only<25> {\draw[red,very thick] (8/3,5)--(10/3,5)--(10/3,4)--(8/3,4)--cycle;}
        \only<26->{\draw[fill=red,opacity=0.4] (8/3,4)--(10/3,4)--(10/3,3)--(8/3,3)--cycle;}
        \only<26> {\draw[red,very thick] (8/3,4)--(10/3,4)--(10/3,3)--(8/3,3)--cycle;}
        \only<27->{\draw[fill=red,opacity=0.4] (8/3,3)--(10/3,3)--(10/3,7/3)--(8/3,7/3)-- cycle; }
        \only<27> {\draw[red,very thick] (8/3,3)--(10/3,3)--(10/3,7/3)--(8/3,7/3)-- cycle; }
        \only<28->{\draw[fill=red,opacity=0.4] (8/3,7/3)--(10/3,7/3)--(10/3,5/3)--(8/3,5/3)--cycle;}
        \only<28> {\draw[red,very thick] (8/3,7/3)--(10/3,7/3)--(10/3,5/3)--(8/3,5/3)-- cycle;}
        \only<29->{\draw[fill=yellow,opacity=0.4](8/3,5/3)--(10/3,5/3)--(10/3,4/3)--(8/3,4/3)--cycle; }
        \only<29>{\draw[red,very thick](8/3,5/3)--(10/3,5/3)--(10/3,4/3)--(8/3,4/3)--cycle; }
        \only<30->{ \draw[fill=green,opacity=0.4] (8/3,4/3)--(10/3,4/3)--(10/3,2/3)--cycle; }
        \only<30>{ \draw[red,very thick] (8/3,4/3)--(10/3,4/3)--(10/3,2/3)--cycle; }

        % F
        \only<31>{\draw[blue,very thick] (3+1/3,2/3)--(3+1/3,5)--(0,5)--(0,2/3)--cycle; }


        \only<32>{
          % Hj:
          \draw[blue,very thick] (3+1/3,2/3)--(3+1/3,5/3-1/40)--(0,5/3-1/40)--(0,2/3)--cycle;
          % Lj:
          \draw[green,very thick] (3+1/3,5)--(3+1/3,5/3+1/40)--(0,5/3+1/40)--(0,5)--cycle;
        }

      \end{tikzpicture}
    \end{block}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{.48\textwidth}
    \begin{block}{Base}
      \begin{tikzpicture}[shift={(0,1)}]
        \draw[very thin] (-0.1,3.1);
        \draw[very thin] (-0.1,0.9);

        %% Base grid
        \foreach \i in {0, ..., 2} {
          \draw[gray,very thin] (\i,1)--(\i,3);
        }
        \only<-9> {
          \foreach \i in {3, ..., 4} {
            \draw[gray,very thin] (\i,1)--(\i,3);
          }
        }
        \only<10-> {
          \foreach \i in {0, ..., 3} {
            \draw[gray,very thin] ({2+(2/3)*\i},1)--({2+(2/3)*\i},3);
          }
        }

        \draw[gray,very thin] (0,3)--(4,3);
        \draw[gray,very thin] (0,1)--(4,1);

        %% V_1
        \draw[fill=red,opacity=0.4] (0,1)--(1,1)--(1,3)--(0,3)--cycle;
        \only<1>{ \draw[red,very thick] (0,1)--(1,1)--(1,3)--(0,3)--cycle; }
        \only<2,3,16,25>{ \draw[blue,very thick] (0,1)--(1,1)--(1,3)--(0,3)--cycle; }

        %% V_2
        \only<4-> { \draw[fill=red,opacity=0.4] (1,1)--(2,1)--(2,3)--(1,3)--cycle;  }
        \only<4>{ \draw[red,very thick] (1,1)--(2,1)--(2,3)--(1,3)--cycle; }
        \only<6,17,26>{ \draw[blue,very thick] (1,1)--(2,1)--(2,3)--(1,3)--cycle; }

        %% V_3
        \only<15->  {\draw[fill=red,opacity=0.4] (2,1)--(8/3,1)--(8/3,3)--(2,3)--cycle; }
        \only<15>{\draw[red,very thick] (2,1)--(8/3,1)--(8/3,3)--(2,3)--cycle;}
        \only<18,27>{\draw[blue,very thick] (2,1)--(8/3,1)--(8/3,3)--(2,3)--cycle;}

        %% V_4
        \only<24->{ \draw[fill=red,opacity=0.4] (8/3,1)--(10/3,1)--(10/3,3)--(8/3,3)--cycle;}
        \only<24>{\draw[red,very thick] (8/3,1)--(10/3,1)--(10/3,3)--(8/3,3)--cycle; }
        \only<28>{\draw[blue,very thick] (8/3,1)--(10/3,1)--(10/3,3)--(8/3,3)--cycle; }
      \end{tikzpicture}
    \end{block}
    \begin{block}{Operations}
      \only<1> { $ P_0 = [], \textcolor{red}{V_1}\Lambda_1 = R0$ }
      \only<3> { $V_{j+1}\textcolor{red}{H_{i+1,j}} = W$\\ }
      \only<4> { $\textcolor{red}{V_{j+1}}H_{i+1,j} = W$\\ }
      \only<3-4> {
        (\( \rightarrow \) No inexact breakdown. \\
        Details hidden.)
      }
      \only<7> { $\widetilde{W_j}\textcolor{red}{H_{i+1,j}} = W$ }

      \only<8> { Compute Projected Residual
        $\textcolor{red}{PR} = LeastSquare_Y( \textcolor{blue}{\mathscr{F}} * Y - \Lambda ) $}
      \only<9> {
        Do SVD: $PR = \textcolor{red}{\mathbb{U}_1\Sigma_1 \mathbb{V}_1 + \mathbb{U}_2\Sigma_2 \mathbb{V}_2}$ }
      \only<10>{$\Sigma_2 \neq []$:\\
        \( \rightarrow \) INEXACT BREAKDOWN !
      }
      \only<11> {
        Compute $\textcolor{red}{[\mathbb{W}_1,\mathbb{W}_2]}$ orthogonal,
        $Range(\mathbb{W}_1) = Range(\mathbb{U}_1^{(2)}) $
      }
      \only<12> { $\textcolor{red}{P_j} := [P_{j-1}, \widetilde{W_j}] \mathbb{W}_1$ }
      \only<15,24> { $\textcolor{red}{V_{j+1}} := [P_{j-1}, \widetilde{W_j}] \mathbb{W}_2$  }
      \only<13,22> { $\textcolor{red}{\mathscr{L}_{j+1,:}} := \mathbb{W}_1^H\textcolor{blue}{\mathbb{H}_j}$  }
      \only<14,23> { $\textcolor{red}{G_j} := \mathbb{W}_2^H\textcolor{blue}{\mathbb{H}_j}$ }
      \only<21>{ $LeastSquare_Y(\textcolor{blue}{\mathscr{F}}*Y-\Lambda)$.\\
        Do R-criterion.\\ \( \rightarrow \) No Inexact Breakdown.
      }
      \only<19,29>{ $\textcolor{red}{C_j} := P_{j-1}^HW$ }
      \only<20,30>{ $\widetilde{W_j}\textcolor{red}{D_j} := W - P_{j-1}^HC_j$ }
      \only<31>{ $LeastSquare_Y(\textcolor{blue}{\mathscr{F}}*Y-\Lambda)$.\\ Do R-Criterion. }
      \only<32>{ $\textcolor{blue}{\mathbb{H}_j}$
        $\textcolor{green}{\mathscr{L}_j}$. \\
        Incremental QR of  $\textcolor{green}{\mathscr{L}_j}$ part is possible. (faster least square.)}
      \only<2,5,16,25,17,6,26,18,27,28> { $\textcolor{red}{H_{i,j}} := \textcolor{blue}{V_i^H}W$  }
    \end{block}
  \end{minipage}
\end{frame}
\end{document}
