###
#
# @copyright (c) 2009-2014 The University of Tennessee and The University
#                          of Tennessee Research Foundation.
#                          All rights reserved.
# @copyright (c) 2012-2017 Inria. All rights reserved.
# @copyright (c) 2012-2014 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
#
###
#
#  @file GenPkgConfig.cmake
#
#  @project MORSE
#  MORSE is a software package provided by:
#     Inria Bordeaux - Sud-Ouest,
#     Univ. of Tennessee,
#     King Abdullah Univesity of Science and Technology
#     Univ. of California Berkeley,
#     Univ. of Colorado Denver.
#
#  @author Cedric Castagnede
#  @author Emmanuel Agullo
#  @author Mathieu Faverge
#  @author Florent Pruvost
#
#  @author Thomas Mijieux
#  @date 20-07-2017
#
###

###
#
# GENERATE_PKGCONFIG_FILE: generate file fabulous_cpp.pc and fabulous_c_api.pc
# and fabulous_fortran_api.pc
#
###
macro(GENERATE_PKGCONFIG_FILE)
    set(_output_fabulous_cpp_file "${CMAKE_CURRENT_BINARY_DIR}/fabulous_cpp.pc")
    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/lib/pkgconfig/fabulous_cpp.pc.in"
        "${_output_fabulous_cpp_file}" @ONLY
        )
    set(_output_fabulous_c_api_file "${CMAKE_CURRENT_BINARY_DIR}/fabulous_c_api.pc")
    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/lib/pkgconfig/fabulous_c_api.pc.in"
        "${_output_fabulous_c_api_file}" @ONLY
        )
    set(_output_fabulous_fortran_api_file "${CMAKE_CURRENT_BINARY_DIR}/fabulous_fortran_api.pc")
    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/lib/pkgconfig/fabulous_fortran_api.pc.in"
        "${_output_fabulous_fortran_api_file}" @ONLY
        )
    install(
        FILES  ${_output_fabulous_cpp_file} ${_output_fabulous_c_api_file} ${_output_fabulous_fortran_api_file}
        DESTINATION lib/pkgconfig
        )
endmacro(GENERATE_PKGCONFIG_FILE)

##
## @end file GenPkgConfig.cmake
##
