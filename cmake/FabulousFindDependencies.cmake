if (FABULOUS_BLASMT)
    set(CBLAS_MT ON)
    set(LAPACKE_MT ON)
#else()
#    set(CBLAS_MT OFF)
#    set(LAPACKE_MT OFF)
endif()

find_package(CBLAS REQUIRED)
find_package(LAPACKE REQUIRED)
target_link_libraries(fabulous_cpp INTERFACE MORSE::LAPACKE MORSE::CBLAS)
morse_export_imported_target(MORSE CBLAS cblas fabulous)
morse_export_imported_target(MORSE LAPACKE lapacke fabulous)

if(FABULOUS_USE_CHAMELEON)
    find_package(CHAMELEON REQUIRED)
    target_link_libraries(fabulous_cpp INTERFACE CHAMELEON::chameleon)
endif()
