# -*- mode: org -*-
# -*- coding: utf-8 -*-

#+TITLE: Fabulous installation procedure
#+PROPERTY: header-args:sh :exports none :eval never-export
#+PROPERTY: header-args:emacs-lisp :exports code :eval never-export
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes/readtheorginria/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes/readtheorginria/css/readtheorginria.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes/lib/js/jquery.stickytableheaders.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes/readtheorginria/js/readtheorginria.js"></script>

Go back to file:README.org.

* Dependencies
  =Fabulous= depends on a =Blas= implementation, make sure to have them installed. If you are on a computation center make sure the modules are loaded !

* Linking Fabulous with =cmake=

=Fabulous= =cmake= has been updated to be more modern, to find =Fabulous= in your =cmake= the following may be used
#+begin_src cmake
 include(CMakeFindDependencyMacro)
 find_dependency(FABULOUS REQUIRED)
#+end_src


Then, in order to link your code to =Fabulous= use the corresponding =cmake= target
#+begin_src cmake
  FABULOUS::fabulous_cpp     # C++ library
  FABULOUS::fabulous_c_api   # C interface
  FABULOUS::fabulous_fortran # Fortran interface
#+end_src

* Downloading the source code

  The latest release on [[https://gitlab.inria.fr/solverstack/fabulous/-/releases]] , for example

  #+begin_src shell
    wget https://gitlab.inria.fr/api/v4/projects/2083/packages/generic/source/v1.1.4/fabulous-1.1.4.tar.gz
    tar xvf fabulous-1.1.4.tar.gz
  #+end_src

  or by cloning the depot

  #+begin_src shell
    git clone --recurse-submodules https://gitlab.inria.fr/solverstack/fabulous.git
  #+end_src

* Installing from source

To install =Fabulous= on your machine, first download the sources, then run the following =cmake= / =make= commands
#+begin_src shell
  # In Fabulous directory
  mkdir build
  cd build

  cmake .. -DFABULOUS_BUILD_FORTRAN_API=ON \  # Fortran api
	-DFABULOUS_BUILD_C_API=ON \  # C api
       -DFABULOUS_BUILD_EXAMPLES=ON  # Building examples

  make install
#+end_src

* =docker= image
  Run the =docker= file file:docker-fabulous with the following instruction.
  #+begin_src shell
    docker build -f docker-fabulous -t fabulous .
  #+end_src

  Then you can run the docker image directly.

* Installing with Spack
** Install =spack=
   Install it from your distribution package manager or from its [[https://spack.io/][official project page]].

** Install =Fabulous=

   Note: sometimes, depending on your configuration you will need to add the =+blasmt= option (explicitly allowing multi-threaded blas) to have the best performances.


*** On a local machine

    Once =spack= is installed, not much is necessary
    #+begin_src shell
spack install -v fabulous
    #+end_src

    To see extra option for =Fabulous= installation please use

    #+begin_src shell
spack info fabulous
    #+end_src


*** On a computation center
    You need to load the required modules and check if =spack= finds the modules, for example
    #+begin_src shell
      # Loading modules
      module purge
      module load compiler/gcc/10.1.0     # needs C++17 compiler
      module load build/cmake/3.15.3      # cmake 3.15 minimal
      module load linalg/mkl/2019_update4 # blas library

      # Sometimes spack does not find some dependencies, may need to set environment variable
      #export CMAKE_DIR=/cm/shared/modules/generic/build/cmake/3.15.3

      # Check founded compiler
      spack compiler find

      # Check the specification
      SPEC="fabulous%gcc@10.1.0 ^mkl@exist ^cmake@exist"
      spack spec ${SPEC}
    #+end_src

    On a computation center spack may have troubles fetching the required dependencies
    on the internet. If you encounter this issue, you can create a spack
    mirror on your local machine

**** Local machine
    #+begin_src bash
spack mirror create -d fabulous_mirror -D -o fabulous@develop
tar cf fabulous_mirror.tar fabulous_mirror/
sftp fabulous_mirror.tar computation_center:~
    #+end_src
    Spack mirrors store dependencies as compressed archives already,
    so it is not very useful to add anymore compression at this stage.

**** On the computation center (for instance, in your =$HOME= directory)
    #+begin_src bash
tar xf fabulous_mirror.tar
spack mirror add local_mirror file://${HOME}/fabulous_mirror
    #+end_src

    If everything looks good, you can proceed the installation with:
    #+begin_src shell
spack install -v ${SPEC}
    #+end_src

* With =guix=
In order to have all =Fabulous= possible dependencies, add the following to your =${HOME}/.config/guix/channels.scm= file.
#+begin_src lisp
(append %default-channels
  (list (channel
          (name 'guix-hpc)
          (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
          (branch "master"))))
#+end_src

Then do a =guix pull= to update your channels and having the =fabulous= package available.

Then the installation is simple
#+begin_src shell
guix install fabulous
#+end_src

* Installing on ubuntu 20.04
  Download the package and run it.

#+begin_src shell
curl https://gitlab.inria.fr/solverstack/fabulous/-/package_files/26292/download -o fabulous_1.1.1_amd64.deb
sudo apt-get install -y fabulous_1.1.1_amd64.deb
#+end_src

* Installing on MacOSX with =brew=
  Note : the installation by source also works on MacOSX.

  First install =brew=

  #+begin_src shell
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
#+end_src

Then get the =Fabulous= =brew= package from [[https://gitlab.inria.fr/solverstack/brew-repo]]

#+begin_src shell
git clone https://gitlab.inria.fr/solverstack/brew-repo.git
#+end_src

And run the script

#+begin_src shell
brew install --build-from-source ./brew-repo/fabulous.rb
#+end_src

* Documentation for developers
** With =guix=
   Execute the following command from the root folder of Fabulous.
#+begin_src shell
 guix environment --pure gfortran-toolchain cmake --ad-hoc doxygen -- bash make_doc.sh
#+end_src

** Without =guix=
Make sure you have a fortran compiler and doxygen install, then execute the following command from the root folder of Fabulous.
#+begin_src shell
 bash make_doc.sh
#+end_src

* Installing Fabulous 1.0.1 (for =chameleon= usage)
** WARNING : Deprecated code
   - =Chameleon= usage is deprecated in Fabulous-1.1.0, please refer to Fabulous-1.0.1 to continue using =Chamelon=.
   - The exemples may use deprecated =mkl= blas, please turn off exemple build if it is your case.
   - The following is an install guide for Fabulous-1.1.0, without chameleon and without the exemples.
** From source
Follow the instructions below
#+begin_src shell
  export FABULOUS_INSTALL_DIR=$PWD/install # your install directory
  git clone --recurse-submodules --branch 1.0.1 git@gitlab.inria.fr:solverstack/fabulous.git
  mkdir fabulous/build
  cd fabulous/build
  cmake .. -DFABULOUS_BUILD_FORTRAN_API=ON \  # Fortran api
	-DFABULOUS_BUILD_C_API=ON \  # C api
       -DFABULOUS_BUILD_EXAMPLES=ON \  # Building examples
       -FABULOUS_USE_CHAMELEON=ON \  # chameleon
       -DCMAKE_INSTALL_PREFIX=${FABULOUS_INSTALL_DIR}
  make install
#+end_src

** With =guix=
First configure your channels (see section "INSTALLING on PLAFRIM with =guix=").

Then the installation is simple
#+begin_src shell
guix install fabulous-1.0.1
#+end_src
